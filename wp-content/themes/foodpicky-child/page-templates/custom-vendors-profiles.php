<?php

/*
  Template Name: Custom Vendors profiles
 */
?>

<?php

global $wp_query, $azexo_queried_object, $azexo_vendors_author__in, $azexo_vendors_taxonomies, $azexo_vendors_meta_query;
$azexo_queried_object = get_post(get_option('azexo_vendors_profiles'));
if (empty($azexo_queried_object)) {
    $azexo_queried_object = $wp_query->get_queried_object();
}

$azexo_vendors_taxonomies = isset($azexo_vendors_taxonomies) ? $azexo_vendors_taxonomies : array();
$azexo_vendors_meta_query = isset($azexo_vendors_meta_query) ? $azexo_vendors_meta_query : array();
if (!isset($azexo_vendors_author__in)) {
    add_filter('posts_where', 'azexo_woo_vendors_where_filter');
}
$shop_ids = array();

$profileFormfields = array(
    'type' => 'geolocation',
    'lat_meta_key' => 'latitude',
    'lng_meta_key' => 'longitude',
    'default_radius' => 30,
    'units' => 'mi',
    'max_distance_meta_key' => 'max_distance',
    'placeholder' => 'What is your Postcode?',
    
);

if(!empty($profileFormfields)){
   $shop_ids = devo_custom_geolocation_process($profileFormfields);
}


query_posts(array_merge(array(
    'post_type' => 'azl_profile',
    'post_status' => 'publish',
    'post__in' => isset($shop_ids) ? $shop_ids : array(),
    'posts_per_page' => -1,
    'meta_query' => $azexo_vendors_meta_query,
), $azexo_vendors_taxonomies));
if (!isset($azexo_vendors_author__in)) {
    remove_filter('posts_where', 'azexo_woo_vendors_where_filter');
}
include(azexo_locate_template('list.php'));
?>
