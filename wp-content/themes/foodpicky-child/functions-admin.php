<?php
/**
 *  @file functions-admin.php
 *  @brief Admin End Related Functions and Queries
 */
 
 
 
/* this function need to be improve for product only */
/*****add hook for save post data or visibility visible*****/
/*Adding custom setion for services in woocoorse setting tab product -> services fee*/
 add_filter( 'woocommerce_get_sections_products','add_subtab' );
function add_subtab( $settings_tabs ) {
    $settings_tabs['service_fee_settings'] = __( 'Service Fee', 'woocommerce-custom-settings-tab' );
    return $settings_tabs;
}
 
# add settings ( HTML Form )
add_filter( 'woocommerce_get_settings_products', 'add_subtab_settings', 10, 2 );
function add_subtab_settings( $settings ) {
    $current_section = (isset($_GET['section']) && !empty($_GET['section']))? $_GET['section']:'';
    if ( $current_section == 'service_fee_settings' ) {
        $service_fee_setting = array();
        $service_fee_setting[] = array( 'name' => __( 'Service Fee', 'text-domain' ), 
                                        'type' => 'title', 
                                        'desc' => __( 'Service fee must less than 10', 'text-domain' ), 
                                        'id' => 'service_fee_settings'
                                  );
                                   
        $service_fee_setting[] = array(
                                    'name'     => __( 'Service Fee', 'text-domain' ),
                                    'id'       => 'service_fee_text',
                                    'type'     => 'text',
                                    'default'  => get_option('service_fee_text'),
                                     
                                );
         
        $service_fee_setting[] = array( 'type' => 'sectionend', 'id' => 'test-options' );             
        return $service_fee_setting;
    } else {
        // If not, return the standard settings
        return $settings;
    }
}
/* end */
function save_post_meta( $post_id ) {
	update_post_meta( $post_id, '_visibility', "visible" );
	if ( isset( $_POST['$post_id'] ) ) {
       // update_post_meta( $post_id, 'visibility', visible );
    }

}
add_action( 'save_post', 'save_post_meta', 10, 3 );
 
/* Following code is to add metaboxes for delivery fees for each profiles */
add_action( 'add_meta_boxes', 'profiles_add_delivery_fees_metabox' );
function profiles_add_delivery_fees_metabox()
{
    add_meta_box( 'delivery_fees_meta_box', 'Delivery Fees', 'delivery_fees_meta_box_callback', 'azl_profile', 'normal', 'high' );
}
function delivery_fees_meta_box_callback($post)
{
	// We'll use this nonce field later on when saving.
	wp_nonce_field( 'delivery_fees_meta_box_nonce', 'delivery_fees_nonce' );
	$saved_data = get_post_meta($post->ID,'profile_delivery_data',true);
    ?>

    <table id="myTable" style="border: 1px solid black">
		<thead>
			<th>Outcode</th>
			<th>Delivery Fees</th>
			<th>Delivery Time</th>
			<th></th>
		</thead>
		<tbody>
			<?php if(!empty($saved_data)) : ?>
				<?php foreach($saved_data as $item): ?>
					<tr>
						<td>
							<input type="text" name="delivery[outcode][]" value="<?php echo $item[0] ?>"/>
						</td>
						<td>
							<input type="text" name="delivery[fee][]" value="<?php echo $item[1] ?>"/>
						</td>
						<td>
							<input type="text" name="delivery[time][]" value="<?php echo $item[2] ?>"/>
						</td>
						<td>
							<input type="button" value="Delete" />
						</td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
			<tr>
				<td>
					<input type="text" name="delivery[outcode][]" class="fname" />
				</td>
				<td>
					<input type="text" name="delivery[fee][]" class="fname" />
				</td>
				<td>
					<input type="text" name="delivery[time][]" class="fname" />
				</td>
				<td>
					<input type="button" value="Delete" />
				</td>
			</tr>
			<?php endif; ?>
		</tbody>
	</table>
	<p>
		<input type="button" value="Insert row">
	</p>

	<script>
		jQuery('#myTable').on('click', 'input[type="button"]', function () {
			jQuery(this).closest('tr').remove();
		})
		jQuery('p input[type="button"]').click(function () {
			jQuery('#myTable').append('<tr><td><input type="text" name="delivery[outcode][]" class="fname" /></td><td><input type="text" name="delivery[fee][]" class="fname" /></td><td><input type="text" name="delivery[time][]" class="fname" /></td><td><input type="button" value="Delete" /></td></tr>')
		});
	</script>
    <?php
}

add_action( 'save_post', 'delivery_fees_meta_box_save' );
function delivery_fees_meta_box_save( $post_id )
{
	// Bail if we're doing an auto save
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

	// if our nonce isn't there, or we can't verify it, bail
	if( !isset( $_POST['delivery_fees_nonce'] ) || !wp_verify_nonce( $_POST['delivery_fees_nonce'], 'delivery_fees_meta_box_nonce' ) ) return;

	// if our current user can't edit this post, bail
	if( !current_user_can( 'edit_post' ) ) return;

	$deliveryData = $_POST['delivery'];
	$deliveryData = array_map('array_filter',$deliveryData);
	$outcode = $deliveryData['outcode'];
	$fee = $deliveryData['fee'];
	$time = $deliveryData['time'];

	$del_data = array();
	for($i = 0;$i<=count($outcode);$i++){
		$code = strtolower($outcode[$i]);
		$del_data[$code] = array($code,$fee[$i],$time[$i]);
	}
	$del_data = array_filter(array_map('array_filter',$del_data));
	if(!empty($del_data)){
		update_post_meta($post_id,'profile_delivery_data',$del_data);
	}
}

add_action( 'woocommerce_email_order_meta', 'devo_add_email_order_meta', 10, 3 );
/*
 * @param $order_obj Order Object
 * @param $sent_to_admin If this email is for administrator or for a customer
 * @param $plain_text HTML or Plain text (can be configured in WooCommerce > Settings > Emails)
 */
function devo_add_email_order_meta( $order_obj, $sent_to_admin, $plain_text ){
    if($sent_to_admin){
        $orderNotes = $order_obj->get_customer_note();
        $checkoutNotes = get_post_meta($order_obj->get_id(),'_billing_myfield12', true);
        $orderNotesData = array_filter(array($orderNotes, $checkoutNotes));
        if(!empty($orderNotesData)){
            echo '<h2>Order Notes</h2>';
            echo '<ul>';
            foreach($orderNotesData as $notes)
            {
                echo  '<li>'.$notes.'</li>';
            }
            echo '</ul>';
        }
        
    }
}
