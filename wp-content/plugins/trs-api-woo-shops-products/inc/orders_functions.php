<?php
function apply_coupon_in_order($obj){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    WC()->session->destroy_session();
    $postData = $obj['payload'];
    $user = get_user_by('id', $postData['user_id']);
    wp_set_current_user( $postData['user_id'], $user->user_login );

    $products = $postData['products'];
    $coupon_code = $postData['coupon_code'];
    if(empty($products)){
        die('not valid data');  
    }
    foreach($products as $product){
        WC()->cart->add_to_cart( $product['product_id'], $product['quantity'] );
    }
    
    $cart_data = array();
    $coupon_status = WC()->cart->add_discount(sanitize_text_field($coupon_code));
    $cart_data['coupon_status'] = $coupon_status;
    WC()->cart->check_customer_coupons(array('billing_email'=>$user->user_email));
    WC()->cart->calculate_totals();
    
    $allapplied_coupons = WC()->cart->get_applied_coupons();
    $is_free_shipping = false;
    $cart_data['applied_coupons'] = array();
    foreach($allapplied_coupons as $code){
        $coupon_details = new WC_Coupon( $code );
        $couponData = $coupon_details->get_data();
        $cart_data['applied_coupons'][] = array(
            'coupon_code' => $code,
            'discount_amount' => WC()->cart->get_coupon_discount_amount($code),
            'free_shipping' => $couponData['free_shipping'],
        );
        if($couponData['free_shipping']){
            $is_free_shipping = true;
        }
    }
    /* 
    if(!empty($allapplied_coupons)){
        $cart_data['applied_coupons_discount'] = WC()->cart->get_coupon_discount_totals();
    }
     */
    $cart_data['cart_notices'] = wc_get_notices();
    $cart_totals = WC()->cart->get_totals();
    $cart_data['cart_contents_total'] = $cart_totals['cart_contents_total'];
    $cart_data['cart_subtotal'] = $cart_totals['subtotal'];
    $cart_data['discount_total'] = $cart_totals['discount_total'];
    $cart_data['is_free_shipping'] = $is_free_shipping;
    $cart_data['fee_total'] = $cart_totals['fee_total'];
    $cart_data['total'] = $cart_totals['total'];
    
    return json_encode(array('order_data'=>$cart_data));
}
function createNewOrder($obj){
	if(!empty($obj["product"]) && !empty($obj["address"]) && !empty($obj["user_id"])){
		
		$fname		= $obj["address"]["first_name"];
		$address1 	= $obj["address"]["address_1"];
		$city 		= (!empty($obj["address"]["city"]))?$obj["address"]["city"]:'';
		$state 		= (!empty($obj["address"]["state"]))?$obj["address"]["state"]:'';
		$postcode 	= (!empty($obj["address"]["postcode"]))?$obj["address"]["postcode"]:'';
		$country 	= (!empty($obj["address"]["country"]))?$obj["address"]["country"]:'UK';
		$email 		= $obj["address"]["email"];
		$phone 		= $obj["address"]["phone"];
		
		$applied_coupons = $obj['applied_coupons'];
		$is_free_shipping_from_coupon = $obj['is_free_shipping'];
        
		$address = array(
			'first_name' => $fname,
			'email'      => $email,
			'phone'      => $phone,
			'address_1'  => $address1,
			'city'       => $city,
			'state'      => $state,
			'postcode'   =>$postcode,
			'country'    => $country
			);
		
		$order = wc_create_order();
		$order->set_address( $address, 'billing' );
		$order->set_address( $address, 'shipping' );
		
		
		
		foreach($obj["product"] as $product_item){
			$order->add_product( wc_get_product($product_item['product_id']), $product_item['quantity']); 
		}
		
        WC()->shipping->load_shipping_methods();
        $shipping_methods = WC()->shipping->get_shipping_methods();

        $selected_shipping_method = $shipping_methods['devoshipping'];
        
        /* $order->add_shipping((object)array (
            'id' => $selected_shipping_method->id,
            'label'    => $selected_shipping_method->title,
            'cost'     => 3.50,
            'taxes'    => array(),
            'method_id'  => $selected_shipping_method->id,
        )); */
        
		$ship_rate_ob = new WC_Shipping_Rate('devoshipping');
		$ship_rate_ob->id       =   $selected_shipping_method->id;
		$ship_rate_ob->label    =   $selected_shipping_method->title;
		$ship_rate_ob->taxes    =   array();
                
        $order_shipping_fees = (!empty($obj["delivery_fees"]))?(float)$obj["delivery_fees"]:0;
        if(!empty($is_free_shipping_from_coupon)){
            $order_shipping_fees = 0;
        }
		$ship_rate_ob->cost     =   $order_shipping_fees;        
		$order->add_shipping($ship_rate_ob);
        
        $coupon_status = array();
        foreach($applied_coupons as $coupon){
            $coupon_status[] = $order->apply_coupon($coupon['coupon_code']);
        }

		$order->calculate_totals();
		
		//print_r($order);
		
		$suborder_price =  $order->get_subtotal(); 
		
		//print_r($order->get_total());
		
		if($suborder_price <= 10){
			//$order->add_fee( 'Surcharge', '2.0', false, '' );
			$order_fee = new stdClass(); 
			$order_fee->id = sanitize_title( 'Service Fee (Items below  £10.00)' ); 
			$order_fee->name = 'Service Fee (Items below  £10.00)'; 
			$order_fee->amount = get_option('service_fee_text'); 
			$order_fee->taxable = false; 
			$order_fee->tax = 0; 
			$order_fee->tax_data = array(); 
			$order_fee->tax_class = ''; 
			$fee_id = $order->add_fee( $order_fee );
		}
		
		$order->calculate_totals();
		
		if ( is_numeric( (int)$obj["user_id"] ) ) {
			update_post_meta( $order->id, '_customer_user', $obj["user_id"] );
		  }
		  
		if(!empty($obj["card_type"])){
			update_post_meta( $order->id, 'stripe_card_type', $obj["card_type"] );
		}
		if(!empty($obj["service_fees"]))
		{
			   update_post_meta($order->id, 'service_fee_text' ,$obj["service_fees"]);
   		}
	
		//    update_post_meta($order -> id, 'service_fee_text' ,$obj["service_fees"])
		
		/* $posted=array(
			'terms' => 1,
            'createaccount' => 0,
            'payment_method' => 'stripe',
            'shipping_method' => array('devoshipping'),
            'ship_to_different_address' => '',
            'billing_phone' 	=> $phone,
            'billing_country' 	=> $country,
            'billing_address_1' => $address1,
            'billing_address_2' => '',
            'billing_city' 		=> $city,
            'billing_postcode'	=> $postcode,
            'order_comments' 	=> '',
		);
		$objectdata = new YITH_Orders;
		$objectdata->check_suborder ( $order->id, $posted, $return = false );
		 */
		if(!empty($obj['address_id'])){
			$address_id=$obj['address_id'];
			$storedAddress = get_user_meta($obj["user_id"],'checkout_saved_address',true);
			if(!empty($storedAddress[$address_id])){
				$storedAddress[$address_id]['default']=true;
			}
			update_user_meta($obj["user_id"],'checkout_saved_address',$storedAddress);
		}
		
		return json_encode(array('id'=>$order->id, 'coupon_status' => $coupon_status)); // changing here
	}
}


function check_promocode($code,$uid){
	
	//echo "<pre>";
	
	$coupon_details = new WC_Coupon( $code );
	$couponData = $coupon_details->get_data();
	// echo "<pre>";print_r($couponData);die;
	
	$customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => $uid,
        'post_type' => 'shop_order',
        'post_status' => array('wc-approved','wc-deliveryprocessing','wc-deliveryshipped','wc-completed','wc-cancelled'),
    ) );
    
	//print_r($couponData);
	$ordersCount =  (!empty($customer_orders))?count($customer_orders):0;
	$val = array(
		'code' => $code ,
		'id' => (!empty($couponData['id'])) ? $couponData['id'] : 0,
		'discount_type' => (!empty($couponData['discount_type'])) ? $couponData['discount_type'] : "",
		'coupon_amount' =>  (!empty($couponData['amount'])) ? $couponData['amount'] : "",
		'expiry_date' =>  (!empty($couponData['date_expires'])) ? $couponData['date_expires'] : "",
		'free_shipping' => (!empty($couponData['free_shipping'])) ? $couponData['free_shipping'] : "",
		'usage_limit_per_user' =>  (!empty($couponData['usage_limit_per_user'])) ? $couponData['usage_limit_per_user'] : "",
		'user_orders_count' =>  $ordersCount,
	
	);
	
	
	$return_data = json_encode(array('promocode'=>$val));
	
	return $return_data;
	
}

function getuserorders($uid){
	
	// Get all customer orders
	$customer_orders = get_posts( array(
			'numberposts' => -1,
			'meta_key'    => '_customer_user',
			'meta_value'  => $uid,
			'post_type'   => wc_get_order_types(),
			'post_status' => array_keys( wc_get_order_statuses() ),
			'post_parent'      => '0',
		)
	);
	
	$order_statuses = array(
		'nothing'		=> "Coming",
		'wc-pending'    => "Pending",
		'wc-processing' => "Coming",
		'wc-on-hold'    => "Rejected",
		'wc-completed'  => "Delivered",
		'wc-approved'	=> "Coming",
		'wc-cancelled'  => "Cancelled",
		'wc-refunded'   => "Cancelled",
		'wc-failed'     => "Cancelled",
	);
	
	$ord = array();			
	$arks = array();		
	$orders_array=array();
    
    foreach($customer_orders as $suborders){
        $ello = new WC_Order($suborders->ID);
        $total = $ello->get_total();
        $items = $ello->get_items();
        $new_order = array();
        /* foreach ( $items as $item_id => $item ) {
            $vendor = yith_get_vendor ( $item[ 'product_id' ], 'product' );
        } */
        /* $category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$vendor->term->term_id);
        $address = $ello->get_address();
        $shipping=0;
        if(!empty($category_postcodes))
        {
            foreach($category_postcodes as $postcode){
                $post_code = $postcode['postcode']; 
                if($post_code ==  $address['postcode']){
                    $shipping = $postcode['delivery_group'];
                }
            }
            
        }	 */
        $status_keys = array_keys($order_statuses);
        
        $orders_array[] = array(
            "id" => $suborders->ID,
            "main_id" => $suborders->ID,
            "status" => $order_statuses[$status_keys[array_search($ello->post_status, $status_keys)]],
            "shop" => 'Select & Save',
            "price" => $ello->get_total(),
            "date" => date('d F Y', strtotime($suborders->post_date)),
        );
    }
	/* foreach($customer_orders as $order){
		
		$customer_sub_orders = get_posts( array(
				'numberposts' => -1,
				'post_type'   => wc_get_order_types(),
				'post_status' => array_keys( wc_get_order_statuses() ),
				'post_parent' => $order->ID,
			)
		);
		foreach($customer_sub_orders as $suborders){
			$ello = new WC_Order($suborders->ID);
			$total = $ello->get_total();
			$items = $ello->get_items();
			$new_order = array();
			foreach ( $items as $item_id => $item ) {
				$vendor = yith_get_vendor ( $item[ 'product_id' ], 'product' );
			}
			$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$vendor->term->term_id);
			$address = $ello->get_address();
			$shipping=0;
			if(!empty($category_postcodes))
			{
				foreach($category_postcodes as $postcode){
					$post_code = $postcode['postcode']; 
					if($post_code ==  $address['postcode']){
						$shipping = $postcode['delivery_group'];
					}
				}
				
			}	
			$status_keys = array_keys($order_statuses);
			
			$orders_array[] = array(
				"id" => $suborders->ID,
				"main_id" => $order->ID,
				"status" => $order_statuses[$status_keys[array_search($ello->post_status, $status_keys)]],
				"shop" => (!empty($vendor->term))?$vendor->term->name:' ',
				"price" => $total+$shipping,
				"date" => date('d F Y', strtotime($order->post_date)),
			);
		}
	} */
	return json_encode(array('Orders'=>$orders_array));
	
}


function orderdetails($oid){
	
		$order = new WC_Order($oid);	
		$parent = wp_get_post_parent_id( $oid );
		$main_order_id = '';
		if(!empty($parent)){
			$main_order_id = $parent;
		}
		
		$shipping = $order->get_total_shipping();
		// echo "<pre>";print_r($shipping);die;
		$address = $order->get_address();
		//$objauto= new WC_Order_Item_Fee($oid);
		//print_r( $order->get_total_shipping()); die();
		//print_r($address);
		$total = $order->get_total();

		$items = ($order->get_items());
		
		$order_statuses = array(
				'nothing'		=> "En route",
				'wc-pending'    => "Pending",
				'wc-processing' => "En route",
				'wc-on-hold'    => "Rejected",
				'wc-completed'  => "Delivered",
				'wc-approved'	=> "En route",
				'wc-cancelled'  => "Cancelled",
				'wc-refunded'   => "Cancelled",
				'wc-failed'     => "Cancelled",
			);
			
		//$order->post_status = "meow";
			
		//echo $order_statuses[$status_keys[array_search($order->post_status, $status_keys)]];
		$vendor=false;
        $items_details = array();
		foreach ( $items as $item_id => $item ) {
            $item_id = $item->get_id();
            $product_id = $item->get_product_id();
            $item_name = $item->get_name(); // Name of the product
            $item_data = $item->get_data();
            $quantity = $item['quantity'];
            $line_total = $item['total'];
			$vendor = get_post_field( 'post_author', $product_id);
			$items_details[] = array("qty"=>$quantity,"name"=> $item_name,"price"=>$line_total);
        }
		$status_keys = array_keys($order_statuses);
		if(empty($vendor)){
			return json_encode(array('Order_Detail'=>array()));
		}
		$address_arr = null;	
		if(!empty($address['address_1']) && is_array($address['address_1'])){
			$address_arr=$address['address_1'];	
		}
		
		$address1=(!empty($address_arr['address_1']))?$address_arr['address_1']:$address['address_1'];
		$address_2=(!empty($address_arr['address_2']))?$address_arr['address_2']:$address['address_2'];
		$city=(!empty($address_arr['city']))?$address_arr['city']:$address['city'];
		$state=(!empty($address_arr['city']))?$address_arr['state']:$address['state'];
		$phone=(!empty($address_arr['phone']))?$address_arr['phone']:$address['phone'];
		
		
		$service_fee = ($total < 10)? get_option('service_fee_text'):'0';
		$myvals = get_post_meta($order->id);
			$service = $order->get_total() - ($order->get_shipping_total() + $line_total); 
	
        $orders_array = array(

			"order_number" => $order->id,
			"main_id" => $main_order_id,
			"status" => $order_statuses[$status_keys[array_search($order->post_status, $status_keys)]] ." ". date('d F Y', strtotime($order->post->post_modified)),
			"shop_name" => 'Select & Save',
			"total_amount" => $total,
			"delivery_fee" => $order->get_shipping_total(),
			"pincode" => $address['postcode'],
			"phone" => $phone,
			"address" => $address1." ".$address_2." ".$city." ".$state,
			"items_details" => $items_details,
		//	"service_fee" => (!empty(get_post_meta( $order->id, 'service_fee_text', true ))) ? get_post_meta( $order->id, 'service_fee_text', true ),
		  	"service_fee" => (string)$service   ,
		  "card_type" => (!empty(get_post_meta( $order->id, 'stripe_card_type', true ))) ? get_post_meta( $order->id, 'stripe_card_type', true ) : "" ,
		
		);
		
	 	return json_encode(array('Order_Detail'=>$orders_array));
	//$order2 = new WC_Shipping_Method($oid);
	
//	return json_encode($order->get_data());
	
}
function update_order_status($oid,$status,$txn,$querydata){
	if($status=='processing'){
		$order = new WC_Order( $oid );
		
		$userid = $order->user_id;
		update_post_meta($oid,'_stripe_charge_id',$txn);
		update_post_meta($oid,'Stripe Payment ID',$txn);
		update_post_meta($oid,'_stripe_charge_captured','no');
		$order->update_status('processing','Stripe Payment Authenticated');
		// update_post_meta($oid,'stripe_mobile_txn_number',$txn);
		
		
		if(!empty($querydata) && !empty($userid)){
			update_user_meta($userid,'_stripe_customer_id',$querydata);
		}
	}
	return json_encode(array('status'=>'success'));
}



################### MMS Code start for send order delivery_status 
################### Last Working Date 17-jan-2018 


function orderdeliverystatus($ods){
	
	
	$status = get_user_meta($ods,'order_read_status',true);
	
	if($status == 'read'){
		
		return json_encode(array('Pending Orders Count'=>0));
		
	}elseif($status == 'unread'){
		
		
		$customer_orders = get_posts( array(
			'numberposts' => -1,
			'meta_key'    => '_customer_user',
			'meta_value'  => $ods,
			'post_type'   => wc_get_order_types(),
			'post_status' => array( 'wc-processing', 'wc-approved' ),
			'post_parent'      => '0',
		)
	);
	$i = 0;
	foreach($customer_orders as $order){
			$i++;
	}
	//echo $i;
	/* echo"asdasdsa"; */
	return json_encode(array('Pending Orders Count'=>$i));
	}else{
		
		//update_user_meta($ods,'order_read_status','unread');
		
		$customer_orders = get_posts( array(
			'numberposts' => -1,
			'meta_key'    => '_customer_user',
			'meta_value'  => $ods,
			'post_type'   => wc_get_order_types(),
			'post_status' => array( 'wc-processing', 'wc-approved' ),
			'post_parent'      => '0',
		)
	);
	$i = 0;
	foreach($customer_orders as $order){
			$i++;
	}
	//echo $i;
	/* echo"asdasdsa"; */
	return json_encode(array('Pending Orders Count'=>$i));
		
	}
	
	
	
}


function notificationstatus($ods,$status){
	
	
	
	update_user_meta($ods,'order_read_status',$status);
	

	return json_encode(array('Pending Orders Count'=>0));
	
} 


################### MMS Code End for send order delivery_status 