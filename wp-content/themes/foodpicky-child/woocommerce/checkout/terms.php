<?php
/**
 * Checkout terms and conditions checkbox
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="clearfix"></div> 
<?php
if ( wc_get_page_id( 'terms' ) > 0 && apply_filters( 'woocommerce_checkout_show_terms', true ) ) : ?>
	<?php do_action( 'woocommerce_checkout_before_terms_and_conditions' ); ?>
	<p class="form-row terms wc-terms-and-conditions">
		<input type="checkbox" class="input-checkbox" name="terms" <?php checked( apply_filters( 'woocommerce_terms_is_checked_default', isset( $_POST['terms'] ) ), true ); ?> id="terms" />
		<label for="terms" class="checkbox"><?php printf( __( 'I&rsquo;ve read and accept the <a href="%s" target="_blank">Terms &amp; Conditions</a>', 'woocommerce' ), esc_url( wc_get_page_permalink( 'terms' ) ) ); ?> <span class="required"></span></label>
		<input type="hidden" name="terms-field" value="1" />
	</p>
	<?php do_action( 'woocommerce_checkout_after_terms_and_conditions' ); ?>
<?php endif; ?>
<style>
/* .add-promo-coupon{
    text-align: left;
    float: left;
	margin: -28px 5% !important;
	font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
    font-size: 12px;
}
.payment_box payment_method_stripe{
	margin:0px !important;
}
.payment_method_stripe{
	padding-top: 0px !important;
} */

.wc-terms-and-conditions label{
	font-weight: 500 !important;
    color: #a6a6a6 !important;
    font-size: 18px;
    font-family: HelveticaNeue-Light,Helvetica Neue Light !important;
    margin-left: 4%; 
	margin-top: 0%;
}
</style>