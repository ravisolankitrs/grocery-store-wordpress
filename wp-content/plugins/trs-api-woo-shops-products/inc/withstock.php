<?php

/**
 *  @brief Brief
 *  returns Search product with matching query and categories
 *  @param [in] $query Parameter_Description : Searched Product text
 *  @param [in] $cid Parameter_Description : Product Category
 *  @return Return_Description : return Products with matching query and matching categorie
 *  
 *  @author TRS Software Solutions
 *  @details Details
 */
function searchProductWithCategoryQuick($query,$cid,$page=1){
	$query=urldecode($query);
	$args = array(
		'post_type'             => 'product',
		'paged'					=> $page,
		'posts_per_page' 		=> 5,
		'post_status'           => 'publish',
		's' 					=> $query,
		'orderby'     			=> 'title', 
		'order'      			=> 'ASC',
		'nopaging'				=> true,
		'ignore_sticky_posts'   => 1,
		'tax_query'             => array(
			array(
				'taxonomy'      => 'yith_shop_vendor',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => $cid,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			),
		),
		'meta_query'            => array(
			array(
				'key'           => '_visibility',
				'value'         => array('catalog', 'visible'),
				'compare'       => 'IN'
			)
		),
	);
	
	$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$cid);
	$shopprice = $category_postcodes[0]['delivery_group'];
	
	$loop = new WP_Query( $args );
	
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();
		global $product; 
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($pid), 'thumbnail_size' );
		$imgurl = $thumb[0];
		$gprice=$product->price;
		$regular_price = $product->regular_price;

		$sale_price = $product->sale_price;
		
		$cat_id=wp_get_post_terms( $loop->post->ID, 'yith_shop_vendor');
		$cat_id=(!empty($cat_id) && isset($cat_id[0]->term_id))?$cat_id[0]->term_id:'';
		
		$e[] = array(
			'Title' => $ptitle,
			'Id' => $pid,
			'Image' => $imgurl,
			'Slug' 	=> (!empty($cslug))?$cslug:'',
			'RegularPrice' => $regular_price,
			'SalePrice' => $sale_price,
			'Cat_id'	=>$cat_id,
			'shopprice'	=>$shopprice,
		);
		
	endwhile;
	$products=json_encode(array('products'=>$e));
	return $products;
}

/**
 *  @brief Brief
 *  returns Search product with matching query and categories
 *  @param [in] $query Parameter_Description : Searched Product text
 *  @param [in] $cid Parameter_Description : Product Category
 *  @return Return_Description : return Products with matching query and matching categorie
 *  
 *  @author TRS Software Solutions
 *  @details Details
 */
function searchProductWithCategory($query,$cid,$page=1){
	$query=urldecode($query);
	$args = array(
		'post_type'             => 'product',
		'paged'					=> $page,
		'posts_per_page' 		=> TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
		'post_status'           => 'publish',
		's' 					=> $query,
		'orderby'     			=> 'title', 
		'order'      			=> 'ASC',
		'nopaging'				=> true,
		'ignore_sticky_posts'   => 1,
		'tax_query'             => array(
			array(
				'taxonomy'      => 'yith_shop_vendor',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => $cid,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			),
		),
		'meta_query'            => array(
			array(
				'key'           => '_visibility',
				'value'         => array('catalog', 'visible'),
				'compare'       => 'IN'
			)
		),
	);
	
	$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$cid);
	$shopprice = $category_postcodes[0]['delivery_group'];
	
	$loop = new WP_Query( $args );
	
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();
		global $product; 
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($pid), 'thumbnail_size' );
		$imgurl = $thumb[0];
		$gprice=$product->price;
		$regular_price = $product->regular_price;

		$sale_price = $product->sale_price;
		
		$cat_id=wp_get_post_terms( $loop->post->ID, 'yith_shop_vendor');
		$cat_id=(!empty($cat_id) && isset($cat_id[0]->term_id))?$cat_id[0]->term_id:'';
		
		$e[] = array(
			'Title' => $ptitle,
			'Id' => $pid,
			'Image' => $imgurl,
			'Slug' 	=> (!empty($cslug))?$cslug:'',
			'RegularPrice' => $regular_price,
			'SalePrice' => $sale_price,
			'Cat_id'	=>$cat_id,
			'shopprice'	=>$shopprice,
		);
		
	endwhile;
	$products=json_encode(array('products'=>$e));
	return $products;
}

/**
 *  @brief Brief
 *  Provides a global search between all stores for any product by product name, and provide all shops
 *  @param [in] $query Parameter_Description : Searched Product text
 *  @param [in] $postcode Parameter_Description : To look in particular post code shops
 *  @return Return_Description : matching products and their shops
 *  
 *  @author TRS Software Solutions
 *  @details Details 
 *  Provides a global search between all stores for any product by product name, and provide all shops
 */
function mod_global_search($query,$postcode){
	
	//echo "<pre>";
	
	$search_code = $postcode;
	$categories = get_terms('yith_shop_vendor', array('hide_empty' => false, 'parent' => 0));
	$sub_cat_array=array();
	$sub_cat_ids=array();
	foreach($categories as $category){
		$sub_cat_ids[]=$category->term_id;
	}
	$products_data=array();
	if(count($sub_cat_ids)>0){
		$args = array(
			'post_type'             => 'product',
			'post_status'           => 'publish',
			's' 					=> $query,
			'orderby'     			=> 'title', 
			'order'      			=> 'ASC',
			'nopaging'				=> true,
			'ignore_sticky_posts'   => 1,
			'tax_query'             => array(
				array(
					'taxonomy'      => 'yith_shop_vendor',
					'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
					'terms'         => $sub_cat_ids,
					'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
				),
			),
			'meta_query'            => array(
				array(
					'key'           => '_visibility',
					'value'         => array('catalog', 'visible'),
					'compare'       => 'IN'
				)
			),
		);

		$loop = new WP_Query( $args );
		
		$fl = 0;
		
		while ( $loop->have_posts() ) : $loop->the_post();global $product; 
			$ptitle = $loop->post->post_title;
			$pid = $loop->post->ID;
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
			$imgurl = $thumb[0];
			$gprice=$product->price;
			$regular_price = $product->regular_price;

			$sale_price = $product->sale_price;
			$cat_id=wp_get_post_terms( $loop->post->ID, 'yith_shop_vendor');
			$cat_id=(!empty($cat_id) && isset($cat_id[0]->term_id))?$cat_id[0]->term_id:'';
			
			$vendor_cat=get_term_by('id', $cat_id, 'yith_shop_vendor');
			
			$thumbnail_id = get_term_meta( $vendor_cat->term_id, 'header_image',true );		
			$image = (!empty($thumbnail_id))?wp_get_attachment_url( $thumbnail_id ):''; 
			$sub_cat_array[]=array(
				'sub_cat_name'=>$vendor_cat->name,
				'sub_cat_id'=>$vendor_cat->term_id,
				'sub_cat_slug'=>$vendor_cat->slug,
				'sub_cat_products'=>$vendor_cat->count,
				'sub_cat_image'=>(!empty($image))?$image:'',
			);
			$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$vendor_cat->term_id);
		
			if($fl == 0){
				
				$shop_id = $vendor_cat->term_id;
				
			}
			
			//print_r($shop_id);
		
			//print"<pre>";print_r($category_postcodes);die;
		
			foreach($category_postcodes as $cat_postcode) {
				$post_code = $cat_postcode['postcode']; 
				if($post_code == $search_code){
					$delivery_charge = $cat_postcode['delivery_group'];		
					$delivery_time = $cat_postcode['delivery_time'];
					$shoptype=get_woocommerce_term_meta( $vendor_cat->term_id, 'custom_shop_type', true );
					
					if($cat_id == $shop_id){
						
						$products_data[] = array(
							'Title' => $ptitle,
							'Id' 	=> $pid,
							'Image' => $imgurl,
							'Slug' 	=> (!empty($cslug))?$cslug:'',
							'RegularPrice' => $regular_price,
							'SalePrice' => $sale_price,
							'Cat_id'	=>$cat_id,
							'time'		=> (!empty($delivery_time))?$delivery_time:'',
							'shoptype'	=> (!empty($vendor_cat->name))?$vendor_cat->name:'',
							'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
						);
						
					}	
				}
					
			}
			
			$fl++;
			
		endwhile;
		
		
		
	}
		
	$aa = array('Products'=>$products_data,'subcats' => array_values(super_unique($sub_cat_array) ));
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}

/**
 *  @brief Brief
 *  Remove Duplicates from Multidimenstional Array
 *  @param [in] $array Parameter_Description
 *  @return Return_Description
 *  Unique multidimensional Array
 *  @author TRS Software Solutions
 *  @since 4:09 PM Tuesday, December 20, 2016
 *  @modification_date 
 *  @details Details 
 */
function super_unique($array)
{
  $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

  foreach ($result as $key => $value)
  {
    if ( is_array($value) )
    {
      $result[$key] = super_unique($value);
    }
  }

  return $result;
}

/**
 *  @brief Brief
 *  
 *  @param [in] $cid Parameter_Description
 *  @return Return_Description
 *  @author Author Name
 *  @details Details
 */
function get_product_by_cat($cid,$postcode,$page=1){
	$search_code = $postcode;
	$args = array( 
		'post_type' => 'product',
		'paged'		=> $page,
		'posts_per_page' => TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
		'tax_query'             => array(
			array(
				'taxonomy'      => 'yith_shop_vendor',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => $cid,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			),
		),
		// 'meta_key' => 'total_sales',
		// 'orderby' => 'meta_value_num',
		'orderby' => 'title',
		'order'   => 'ASC',
		// 'meta_query' => WC()->query->get_meta_query()
	);
	$loop = new WP_Query( $args );
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();global $product; 
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
		$imgurl = $thumb[0];
		
		$stock_status = ($product->stock_status);
		
		//die;
		
		$gprice=$product->price;
		$regular_price = $product->regular_price;

		$sale_price = $product->sale_price;
		
		$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$cid);
		
		foreach($category_postcodes as $cat_postcode) {
			$post_code = $cat_postcode['postcode']; 
			if($post_code == $search_code){
				$delivery_charge = $cat_postcode['delivery_group'];		
				$delivery_time = $cat_postcode['delivery_time'];
				$shoptype=get_woocommerce_term_meta( $cid, 'custom_shop_type', true );
		
				$e[] = array(
					'Title' => $ptitle,
					'Id' 	=> $pid,
					'Image' => $imgurl,
					'Slug' 	=> (!empty($cslug))?$cslug:'',
					'RegularPrice' => $regular_price,
					'SalePrice' => $sale_price,
					'StockStatus' => $stock_status,
					'Cat_id'	=>$cid,
					'time'		=> (!empty($delivery_time))?$delivery_time:'',
					'shoptype'	=> (!empty($shoptype))?$shoptype:'',
					'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
				);
			}
		}
		
	endwhile;
	
	$args = array(
	   'show_option_none' => '',
	   'hide_empty' => 0,
	   // 'parent' => $cid,
	    'parent'	=>0,
	   'taxonomy' => 'product_cat'
	);
	$subcats = get_categories($args);
	$sub_cat_array=array();
	foreach($subcats as $category){
		$thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true );     
		$image = wp_get_attachment_url( $thumbnail_id ); 
		$sub_cat_array[]=array(
			'sub_cat_name'=>$category->cat_name,
			'sub_cat_id'=>$category->term_id,
			'sub_cat_slug'=>$category->slug,
			'sub_cat_products'=>$category->category_count,
			'sub_cat_image'=>(!empty($image))?$image:'',
		);
	}
	$aa = array('Products'=>$e,'subcats' => $sub_cat_array );
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}

/**
 *  @brief Brief
 *  get_product_by_shop_and_cat
 *  @param [in] $shopid   Parameter_Description
 *  @param [in] $cid      Parameter_Description
 *  @param [in] $postcode Parameter_Description
 *  @return Return_Description
 *  @author : TRS
 *  @creation_date : 4:59 PM 23 December, 2016
 *  @modification_date : 4:59 PM 23 December, 2016
 *  @modification_task : 
 *  @details Details
 */
function get_product_by_shop_and_cat($shopid,$cid,$postcode,$page=1){
	// die('tada');
	$search_code = $postcode;
	$args = array(
		'post_type' => 'product',
		'paged'		=> $page,
		'posts_per_page' => TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy'      => 'yith_shop_vendor',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => array($shopid),
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			),
			array(
				'taxonomy'      => 'product_cat',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => array($cid),
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			)
		),
	);
	$loop = new WP_Query( $args );
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();global $product; 
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
		$imgurl = $thumb[0];
		$gprice=$product->price;
		$regular_price = $product->regular_price;

		$sale_price = $product->sale_price;
		
		$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$shopid);
		
		foreach($category_postcodes as $cat_postcode) {
			$post_code = $cat_postcode['postcode']; 
			if($post_code == $search_code){
				$delivery_charge = $cat_postcode['delivery_group'];		
				$delivery_time = $cat_postcode['delivery_time'];
				$shoptype=get_woocommerce_term_meta( $shopid, 'custom_shop_type', true );
		
				$e[] = array(
					'Title' => $ptitle,
					'Id' 	=> $pid,
					'Image' => $imgurl,
					'Slug' 	=> (!empty($cslug))?$cslug:'',
					'RegularPrice' => $regular_price,
					'SalePrice' => $sale_price,
					'Cat_id'	=>$cid,
					'time'		=> (!empty($delivery_time))?$delivery_time:'',
					'shoptype'	=> (!empty($shoptype))?$shoptype:'',
					'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
				);
			}
		}
		
	endwhile;
	
	$args = array(
	   'show_option_none' => '',
	   'hide_empty' => 0,
	   // 'parent' => $cid,
	   'taxonomy' => 'product_cat',
	   'parent' => 0
	);
	$subcats = get_categories($args);
	$sub_cat_array=array();
	foreach($subcats as $category){
		$thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true );     
		$image = wp_get_attachment_url( $thumbnail_id ); 
		$sub_cat_array[]=array(
			'sub_cat_name'=>$category->cat_name,
			'sub_cat_id'=>$category->term_id,
			'sub_cat_slug'=>$category->slug,
			'sub_cat_products'=>$category->category_count,
			'sub_cat_image'=>(!empty($image))?$image:'',
		);
	}
	$aa = array('Products'=>$e,'subcats' => $sub_cat_array );
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}


/**
 *  @brief Brief
 *  get_product_by_shop_and_subcat
 *  @param [in] $shopid   Parameter_Description
 *  @param [in] $cid      Parameter_Description
 *  @param [in] $postcode Parameter_Description
 *  @return Return_Description
 *  @author : TRS sb
 *  @creation_date : 29 December, 2016
 *  @modification_date : 29 December, 2016
 *  @modification_task : 
 *  @details Details
 */
function get_product_by_shop_and_subcat($cid,$shopid,$postcode,$page=1){
	// die('tada');
	
	$search_code = $postcode;
	$args = array( 
		'post_type' => 'product',
		'paged'		=> $page,
		'posts_per_page' => TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy'      => 'yith_shop_vendor',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => array($shopid),
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			),
			array(
				'taxonomy'      => 'product_cat',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => array($cid),
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			)
		),
	);
	$loop = new WP_Query( $args );
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();global $product; 
	
		//print_r($loop->post);
	
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
		$imgurl = $thumb[0];
		$gprice=$product->price;
		$regular_price = $product->regular_price;

		$sale_price = $product->sale_price;
		
		$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$shopid);
		
		foreach($category_postcodes as $cat_postcode) {
			$post_code = $cat_postcode['postcode']; 
			if($post_code == $search_code){
				$delivery_charge = $cat_postcode['delivery_group'];		
				$delivery_time = $cat_postcode['delivery_time'];
				$shoptype=get_woocommerce_term_meta( $shopid, 'custom_shop_type', true );
		
				$e[] = array(
					'Title' => $ptitle,
					'Id' 	=> $pid,
					'Image' => $imgurl,
					//'Slug' 	=> (!empty($cslug))?$cslug:'',
					'RegularPrice' => $regular_price,
					'SalePrice' => $sale_price,
					'Cat_id'	=> $cid,
					'time'		=> (!empty($delivery_time))?$delivery_time:'',
					'shoptype'	=> (!empty($shoptype))?$shoptype:'',
					'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
				);
			}
		}
		
	endwhile;
	
	
	
	//echo "<pre>";
	
	$args = array(
	    'parent' => $cid,
		'show_option_none' => '',
		'hide_empty' => 0,
		'taxonomy'=>'product_cat'
	);
	$subcats = get_categories($args);
	
	//print_r($subcats);
	
	//die;
	
	$sub_cat_array=array();
	foreach($subcats as $category){
		$thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true );     
		$image = wp_get_attachment_url( $thumbnail_id ); 
		$sub_cat_array[]=array(
			'sub_cat_name'=>$category->cat_name,
			'sub_cat_id'=>$category->term_id,
			'sub_cat_slug'=>$category->slug,
			'sub_cat_products'=>$category->category_count,
			'sub_cat_image'=>(!empty($image))?$image:'',
		);
	}
	$aa = array('subcats' => $sub_cat_array,'Products' => $e );
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}

/**
 *  @brief Brief
 *  
 *  @param [in] $uid Parameter_Description
 *  @return Return_Description
 *  @author Author Name
 *  @details Details
 */
function get_products_by_handler($uid){
	$categoryId= get_user_meta($uid, 'woo_delivery_category', true);
	$args = array(
		'post_type'             => 'product',
		'post_status'           => 'publish',
		'ignore_sticky_posts'   => 1,
		'nopaging'				=> true,
		'meta_query'            => array(
			array(
				'key'           => '_visibility',
				'value'         => array('catalog', 'visible'),
				'compare'       => 'IN'
			)
		),
		'tax_query'             => array(
			array(
				'taxonomy'      => 'product_cat',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => $categoryId,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			)
		),
	);
	$products = new WP_Query($args);
	$productsdata = $products->get_posts();
	$pdata=array();
	foreach($productsdata as $item){
		$product=new WC_Product($item);
		$pdata[]=array(
			'id'			=> $item->ID,
			'title'		=> $item->post_title,
			'availablity'	=>($product->is_in_stock()==true)?'In Stock':'Out of Stock',
		);
	}
	return json_encode(array('products'=>$pdata));
}


/**
 *  @brief - gives vendor info
 *  
 *  @param [in] $latitude   - latitude of the place
 *  @param [in] $longitude  - longitude of the place
 *  
 *  @return - returns postcode from get_vendors_by_postcode function
 *  
 *  @author - Trs Software Solutions, India
 *  
 *  @details - gets vendors postcode via user latitude and longitude
 */
function get_vendors_by_geocode($latitude,$longitude){
	$postcode=trswoo_shop_api_getZipcode($latitude,$longitude);
		return get_vendors_by_postcode($postcode);
}


/**
 *  @brief - gives address
 *  
 *  @param [in] $latitude   - latitude of the place
 *  @param [in] $longitude  - longitude of the place
 *  
 *  @return - Return address if anything is true or false
 *  
 *  @author - Trs Software Solutions, India
 *  
 *  @details - This function takes latitude and longitude as input and gets location name from google api. 
 */
function trswoo_shop_api_getZipcode($latitude,$longitude){
    $geocodeFromLatlon = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=true_or_false&key=AIzaSyAPSJ_1awPnPNq4Ki2S713mLQuR9zyKsD4');
	$output2 = json_decode($geocodeFromLatlon);
	if(!empty($output2) && isset($output2->results[0])){
		$addressComponents = $output2->results[0]->address_components;
		foreach($addressComponents as $addrComp){
			if($addrComp->types[0] == 'postal_code'){
				//Return the zipcode
				return $addrComp->long_name;
			}
		}
		return false;
	}else{
		return false;
	}
}

/**
 *  @brief Brief
 *  Get All Nearby vendors From given postcode
 *  @param [in] $postcode Area Postcode
 *  @return Returns nearby stores from given post code
 *  @author TRS
 *  @details Get All Nearby vendors From given postcode
 */
function get_vendors_by_postcode($postcode){
	$search_code = $postcode;
	$categories = get_terms('yith_shop_vendor', array('hide_empty' => false, 'parent' => 0));

	$shops = array();		
	$openedArray=array();
	$closedArray=array();
	$comingArray=array();
	foreach($categories as $category) {
		$id = $category->term_id;
		$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$id);
		
		if(!empty($category_postcodes)) {
			foreach($category_postcodes as $cat_postcode) {
				$post_code = $cat_postcode['postcode']; 
				if($post_code == $search_code){
					$delivery_charge = $cat_postcode['delivery_group'];
					$enable_time = get_term_meta( $id, 'enable_time', true );
					$custom_comingsoon_status = get_term_meta( $id, 'custom_comingsoon_status', true );
					$enable_status=(!empty($custom_comingsoon_status))
									?'coming'
									:(empty($enable_time)
										?'closed'
										:'opened');
										
					$delivery_time = $cat_postcode['delivery_time'];
					$shoptype=get_woocommerce_term_meta( $id, 'custom_shop_type', true );
					$thumbnail_id = get_term_meta( $category->term_id, 'header_image',true );		
					$image = (!empty($thumbnail_id))?wp_get_attachment_url( $thumbnail_id ):''; 
					$name = get_cat_name($id);
					
					if($enable_status=='opened'){
						$openedArray[]=array(
							'id'		=> $id,
							'name'		=> $name, 
							'image'		=> (!empty($image))?$image:'',
							'time'		=> (!empty($delivery_time))?$delivery_time:'',
							'shoptype'	=> (!empty($shoptype))?$shoptype:'',
							'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
							'status'	=> $enable_status,
						);
					}elseif($enable_status=='closed'){
						$closedArray[]=array(
							'id'		=> $id,
							'name'		=> $name, 
							'image'		=> (!empty($image))?$image:'',
							'time'		=> (!empty($delivery_time))?$delivery_time:'',
							'shoptype'	=> (!empty($shoptype))?$shoptype:'',
							'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
							'status'	=> $enable_status,
						);
					
					}elseif($enable_status=='coming'){
						$comingArray[]=array(
							'id'		=> $id,
							'name'		=> $name, 
							'image'		=> (!empty($image))?$image:'',
							'time'		=> (!empty($delivery_time))?$delivery_time:'',
							'shoptype'	=> (!empty($shoptype))?$shoptype:'',
							'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
							'status'	=> $enable_status,
						);					
					}
				}
			}
		}
	}
	usort($openedArray, function($a, $b) {
		return $a['shopprice'] - $b['shopprice'];
	});
	usort($closedArray, function($a, $b) {
		return $a['shopprice'] - $b['shopprice'];
	});
	usort($comingArray, function($a, $b) {
		return $a['shopprice'] - $b['shopprice'];
	});
	$shops=array_merge($openedArray,$closedArray,$comingArray);
	if(count($shops)>0):
		return json_encode(array('shops'=>$shops));
	else:
		return json_encode(array('error'=>'invalid shop code'));
	endif;
}

/**
 *  @brief Brief
 *  Get All Vendors function to fetch all vendors from yith vendors taxonomy
 *  @return All Vendors Array in Json Format
 *  @author TRS
 *  @details Returns All Vendors Array in Json Format
 */
function get_all_vendors(){
	$taxonomy = 'yith_shop_vendor';
	if(!taxonomy_exists($taxonomy)){echo json_encode(array('categories'=>array()));die;}
	$args = array(
		 'taxonomy'     => $taxonomy,
		 'orderby'      => 'name',
		 'show_count'   => 0,
		 'hide_empty'   => 0,
		 'parent'		=>0,
	);
	$all_categories = get_categories( $args );
	$categories=array();

	foreach($all_categories as $category){

		$id = $category->term_id;

		$name = get_cat_name($id);

		$thumbnail_id = get_woocommerce_term_meta( $id, 'thumbnail_id', true );     
		$image = wp_get_attachment_url( $thumbnail_id ); 
		$enable_time = get_term_meta( $id, 'enable_time',true); 
		
		$custom_comingsoon_status = get_term_meta( $id, 'custom_comingsoon_status', true );
		$enable_status=(!empty($custom_comingsoon_status))
									?'coming'
									:(empty($enable_time)
										?'closed'
										:'opened');
		
		$shoptype= get_woocommerce_term_meta( $id, 'custom_shop_type', true ); 
		$shopprice = get_woocommerce_term_meta( $id, 'custom_shop_price', true ); 
		$duration=get_woocommerce_term_meta( $id, 'custom_delivery_duration', true );
		$post_code='';
		$delivery_charge='';
		
		$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$id);
		
		if(!empty($category_postcodes) && count($category_postcodes)>0){
			$post_code = (!empty($category_postcodes[0]['postcode']))
							?$category_postcodes[0]['postcode']:'';
			$delivery_charge = (!empty($category_postcodes[0]['postcode']))
								?$category_postcodes[0]['delivery_time']:'';
		}
		$categories[]=array(
			'id'=>$category->term_id,
			'name'=>$name,
			'image'=>$image,
			'time'=> $duration,
			'shoptype'=>$shoptype,
			'shopprice'=>$shopprice,
			'post_code'=>$post_code,
			'delivery_time'=>$delivery_charge,
			'status'	=> $enable_status,
		);

	}
	$return_data = json_encode(array('categories'=>$categories));
	return $return_data;
}