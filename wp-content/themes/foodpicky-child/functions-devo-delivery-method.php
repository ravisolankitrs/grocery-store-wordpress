<?php
 /**
 *  @file functions-devo-delivery-method.php
 *  @brief Custom Created Devo Delivery Method
 */
/*
 * Check if WooCommerce is active
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

    function devo_shipping_method() {
        if ( ! class_exists( 'Devo_Shipping_Method' ) ) {
            class Devo_Shipping_Method extends WC_Shipping_Method {
                /**
                 * Constructor for your shipping class
                 *
                 * @access public
                 * @return void
                 */
                public function __construct($instance_id = 0) {
                    $this->id                 = 'devoshipping'; 
					$this->instance_id 		 = absint( $instance_id );
                    $this->method_title       = __( 'Devo Shipping', 'devoshipping' );  
                    $this->method_description = __( 'Custom Shipping Method for Devo', 'devoshipping' ); 
					$this->supports            = array(
							'shipping-zones',
							'instance-settings',
					);

                    $this->init();
 
                    $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
                    $this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'Devo Shipping', 'devoshipping' );
                }
 
                /**
                 * Init your settings
                 *
                 * @access public
                 * @return void
                 */
                function init() {
                    // Load the settings API
                    $this->init_form_fields(); 
                    $this->init_settings(); 
 
                    // Save settings in admin if you have any defined
                    add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
                }
 
                /**
                 * Define settings field for this shipping
                 * @return void 
                 */
                function init_form_fields() { 
 
                    $this->form_fields = array(
 
                     'enabled' => array(
                          'title' => __( 'Enable', 'devoshipping' ),
                          'type' => 'checkbox',
                          'description' => __( 'Enable this shipping.', 'devoshipping' ),
                          'default' => 'yes'
                          ),
 
                     'title' => array(
                        'title' => __( 'Title', 'devoshipping' ),
                          'type' => 'text',
                          'description' => __( 'Title to be display on site', 'devoshipping' ),
                          'default' => __( 'Devo Shipping', 'devoshipping' )
                          ),
 
                     );
 
                }
 
                /**
                 * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
                 *
                 * @access public
                 * @param mixed $package
                 * @return void
                 */
                public function calculate_shipping( $package = array() ) {
					
					$cost = 2.50;
					$data = WC()->cart->applied_coupons;
					
					$freeshipping = false;
					foreach($data as $coupon){
						$coupon_details = new WC_Coupon( $coupon );
						if($coupon_details->get_free_shipping()==true){			
							$freeshipping = true;
						}
					}
					
					$outcode = (!empty($_COOKIE['processed_outcode']))?strtolower($_COOKIE['processed_outcode']):null;
					if(!empty($package) && count($package['contents']) > 0 && !empty($outcode)){
						$packageData = array_values($package['contents'])[0];
						$auth = get_post($packageData['product_id']); // gets author from post
						$authid = $auth->post_author; // gets author id for the post
						$args = array(
							'post_type'        => 'azl_profile',
							'author'	   		=> $authid,
						);
						$posts_array = get_posts( $args );
						$shop = $posts_array[0];
						$deliveryData = get_post_meta($shop->ID,'profile_delivery_data',true);
						$savedOutcode = $outcode;
						if(array_key_exists($savedOutcode,$deliveryData)){
							$data = $deliveryData[$savedOutcode];
								$cost = $data[1];
						}
						
					}
					if($freeshipping==true){
						$currentPrice = $cost;
						if($currentPrice >= 2.50){
							$cost = $currentPrice - 2.50;
						}else{
							$cost = 0;
						}
						
					}
                    // $cost = 0.01; // comment this line after testing
                    $rate = array(
                        'id' => $this->id,
                        'label' => $this->title,
                        'cost' => $cost
                    );
 
                    $this->add_rate( $rate );
                    
                }
            }
        }
    } 
    add_action( 'woocommerce_shipping_init', 'devo_shipping_method' );
 
    function add_devo_shipping_method( $methods ) {
        $methods['devoshipping'] = 'Devo_Shipping_Method';
        return $methods;
    } 
    add_filter( 'woocommerce_shipping_methods', 'add_devo_shipping_method' );
	
	function devo_shipping_method_validate_order( $posted )   {
 
		/* global $woocommerce;
		$data = $woocommerce->cart->applied_coupons;
		
		foreach($data as $coupon){
			$coupon_details = new WC_Coupon( $coupon );			
			if($coupon_details->get_free_shipping()==true){			
				WC()->session->set('chosen_shipping_methods', array( 'free_shipping' ) );
			} 
		} */
	}
	 
	add_action( 'woocommerce_review_order_before_cart_contents', 'devo_shipping_method_validate_order' , 10 );
	add_action( 'woocommerce_after_checkout_validation', 'devo_shipping_method_validate_order' , 10 );
}

/* This function has been added to add service fees in cart total if order is below $10 */
//£2 surcharge if order is under £10
function woo_add_cart_fee() {
    global $woocommerce;
    $subt = $woocommerce->cart->subtotal;
    if ($subt < 10 ) {
        $surcharge = number_format(get_option('service_fee_text'),2);
		// $surcharge = 0.01;
    } else {
        $surcharge = 0.00;   
    }
$woocommerce->cart->add_fee( __('Service Fee (Items below  £10.00)', 'woocommerce'), $surcharge );
}
add_action( 'woocommerce_cart_calculate_fees', 'woo_add_cart_fee' );

/**
 *  
 *  To add service fee sb in Total if Product Total < 10 
 *  
 */

add_filter( 'woocommerce_email_order_meta_fields', 'custom_woocommerce_email_order_meta_fields', 10, 3 );

function custom_woocommerce_email_order_meta_fields( $fields, $sent_to_admin, $order ) {
    $fields['meta_key'] = array(
        'label' => __( 'Label' ),
        'value' => get_post_meta( $order->id, 'meta_key', true ),
    );
    return $fields;
}

/* This filter has been added to replace shipping word to Delivery everywhere in site */
if(!is_admin()){
	add_filter('gettext', 'translate_reply');
	add_filter('ngettext', 'translate_reply');
	function translate_reply($translated) {
		$translated = str_ireplace('Shipping', 'Delivery Fee', $translated);
		return $translated;
	}
}


function get_active_shipping_methods() {
	$shipping_methods = WC()->shipping->load_shipping_methods();
	$active_methods = array();	
	foreach ( $shipping_methods as $id => $shipping_method ) {
		if ( isset( $shipping_method->enabled ) && 'yes' === $shipping_method->enabled ) {
			$method_title = $shipping_method->title;
			if ( 'international_delivery' === $id ) {
				$method_title .= ' (International)';
			}
			array_push( $active_methods, $method_title );
		}
	}
	return $active_methods;
}
function reset_default_shipping_method( $method, $available_methods ) {

    // If the shipping method has been chosen don't do anything
    if ( ! empty( $method ) ) {
        return $method;
    }        

    // add code to set 'Table Rate' as the default shipping method 

    $method = 'devoshipping';

    return $method;
}



