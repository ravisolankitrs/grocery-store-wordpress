var postcodeCookie = gtm_getCookie('processed_outcode');

function gtm_getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// Shop Find Executed
jQuery('#notify_btn_id').on('click',function( e ) {
    console.log('Fired find_shop event');
    dataLayer.push({
        event: 'find_shop',
        'postcode': postcodeCookie,
        'user-id': gtm_data.user_id
    });
});
/* ------------------ */
// Notify Me Tapped Executed
jQuery('.notify-class#btnwth').on('click',function( e ) {
    console.log('Fired Notify Me Tapped event');
    dataLayer.push({
        event: 'notify_me',
        'postcode': postcodeCookie,
        'user-id': gtm_data.user_id
    });
});

// Store Tapped Executed
jQuery('.list-profile.type-azl_profile a').on('click',function( e ) {
	console.log('Fired Store Tapped event');
	var shop_data = jQuery(this).closest('.list-profile').find('script').html();
	if(shop_data!=''){
		var store_obj = jQuery.parseJSON(shop_data);
		dataLayer.push({
			event: 'store_tapped',
			'store_id': store_obj.id,
			'user-id': gtm_data.user_id
		});
	}
    
});

//Multi-Store Search Executed 
jQuery('form.search-form input.search-submit').on('click',function( e ) {
	console.log('Fired Multi-Store Search event');
	var searchQuery = jQuery(this).closest('form').find('#searchProfileShop').attr('value');
	if(searchQuery!=''){
		dataLayer.push({
            event: 'multi_store_search_executed',
			'postcode': postcodeCookie,
			'search_query': searchQuery
		});
	}
    
});

//Multi-Store Product Tapped Executed 
jQuery('a.global_search_product').on('click',function( e ) {
	console.log('Fired Multi-Store Product Tapped event');
	var productId = jQuery(this).attr('id');
	if(productId!=''){
		dataLayer.push({
            event: 'multi_store_product_tapped',
			'product_id': productId
		});
	}
    
});

//Multi-Store View All Tapped
jQuery('a.shop_view_more_link').on('click',function( e ) {
	console.log('Fired Multi-Store View All Tapped event');
	var store_id = jQuery(this).attr('data-store-id');
	if(store_id!=''){
		dataLayer.push({
            event: 'multi_store_view_all_tapped',
			'store_id': store_id
		});
	}
    
});

//Store -  Product Tapped
jQuery('a.ajax_add_to_cart').on('click',function( e ) {
	console.log('Fired Product Tapped event');
	var productId = jQuery(this).attr('data-product_id');
	if(productId!=''){
		dataLayer.push({
            event: 'store_product_tapped',
			'productId': productId
		});
	}
    
});

//Store -  Category Tapped
jQuery('li.main_cat_parent').on('click',function( e ) {
	console.log('Fired Category Tapped event');
	var category_id = jQuery(this).attr('id');
	if(category_id!=''){
		dataLayer.push({
            event: 'store_category_tapped',
			'category_id': category_id
		});
	}
    
});

//Store -  Subcategory Tapped
jQuery('li.child_cat_parent').on('click',function( e ) {
	console.log('Fired Subcategory Tapped event');
	var sub_category_id = jQuery(this).attr('id');
	if(sub_category_id!=''){
		dataLayer.push({
            event: 'store_sub_category_tapped',
			'sub_category_id': sub_category_id
		});
	}
    
});

//Store -  Store Search Executed
jQuery('.nothing-search > button.search-submit').on('click',function( e ) {
	console.log('Fired Store Search Executed event');
	var searchQuery = jQuery('#customsearchtrs_id').attr('value');
	if(searchQuery!=''){
		dataLayer.push({
            event: 'store_search_executed',
			'search_query': searchQuery
		});
	}
    
});

//basket -  Minus item tapped
jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {
jQuery('.mini_cart_item_div > #one_minus').on('click',function( e ) {
	console.log('Fired Minus item tapped Executed event');
	
	
	// var searchQuery = jQuery('#customsearchtrs_id').attr('value');
	// if(searchQuery!=''){
		// dataLayer.push({
            // event: 'store_search_executed',
			// 'search_query': searchQuery
		// });
	// }
    
});


});





 //Login -  login with facebook
jQuery('#global_login_form [type="submit"]').on('click',function( e ) {
	console.log('Fired login_tap_executed Executed event');
	
		dataLayer.push({
            event: 'login_tap_executed',
			'user-id': gtm_data.user_id
		});
	
	
    
});

//Login -  login

jQuery('.social-login').find('.devo_fb_button').on('click',function( e ) {
	console.log('Fired Login with facebook Executed event');
	
	
		dataLayer.push({
            event: 'login_with_facebook_tap_executed',
			'user-id': gtm_data.user_id
		});
	
    
}); 

//Login -  forget password
jQuery('.lost_password a').on('click',function( e ) {
	console.log('Fired froget password tapped Executed event');
	dataLayer.push({
        event: 'forget_password_tapped',
        
    });
    
});

//Login -  forget password
jQuery('.woocommerce-FormRow').find('.woocommerce-Button').on('click',function( e ) {
	console.log('Fired froget password Executed event');
	
	console.log('Fired Login with facebook Executed event');
	var email = jQuery('.woocommerce-FormRow').find('#user_login').val();
	
	if(email!=''){
		console.log(email);
		dataLayer.push({
             event: 'forget_password_executed',
			'email_id': email
		});
	}
    
});

//Login -  signup
jQuery('#global_registration_form [type="submit"]').on('click',function( e ) {
	console.log('Fired Signup Executed event');
	
	dataLayer.push({
        event: 'signup_tapped',
        
    });
    
});

//Login -  signup
jQuery('#global_registration_form').find('.devo_fb_button [type="submit"]').on('click',function( e ) {
	console.log('Fired Signup with facebook Executed event');
	//alert('signup_login_with_facebook_tap_executed');
	dataLayer.push({
        event: 'signup_login_with_facebook_tap_executed',
        
    });
    
});


//Ryder -  add
jQuery('.cart_rider_add_tip').on('click',function( e ) {
	console.log('cart_rider_add_tip');
	
	var incre_price = jQuery('#tipsfee').text();
	console.log(incre_price);
	dataLayer.push({
        event: 'basket_rider_tip_increased_tapped',
		'price':incre_price,
        'user-id': gtm_data.user_id
    });
    
});

//Ryder -  minus
jQuery('.cart_rider_minus_tip').on('click',function( e ) {
	console.log('cart_rider_minus_tip');
	
	var decre_price = jQuery('#tipsfee').text();
	console.log(decre_price);
	dataLayer.push({
        event: 'basket_rider_tip_decreased_tapped',
		'price':decre_price,
        'user-id': gtm_data.user_id
    });
    
});


//Logout - 

    
jQuery('.root li:last-child').on('click',function( e ) {

dataLayer.push({
        event: 'logout_tapped',
		
        'user-id': gtm_data.user_id
    });

});



jQuery('.col_basket_price').on('click','.checkoutcenter', function(e) {
	
	var currency = jQuery('#check_out').find('.woocommerce-Price-amount').text();
	
	//console.log(jQuery(this).find('.woocommerce-Price-amount'));
	
	//alert(currency);
	
	// console.log(decimals);
	// console.log(currency);

	// return false;
	
	
});
 
 
    
jQuery('.root li:first-child').on('click',function( e ) {

dataLayer.push({
        event: 'my_details',
		
        'user-id': gtm_data.user_id
    });

});

     
jQuery('#addrs-add').on('click',function( e ) {

dataLayer.push({
        event: 'delivery_address_my_account',
		
        'user-id': gtm_data.user_id
    });

});

 // jQuery('.actions > ul > li:last-child').on('click',function( e ) {

 // alert('sdfdfdsf');
// dataLayer.push({
        // event: 'payment_method_post_order',
        // payment_type: 'Credit Card',
		// 'user-id': gtm_data.user_id
    // });

// });

 // jQuery('#wizard a[href="#finish"]').on('click',function( e ) {

// dataLayer.push({
        // event: 'payment_method_post_order',
        // payment_type: 'Credit Card',
		// 'user-id': gtm_data.user_id
    // });

// });
 
     
jQuery('#faq_m').on('click',function( e ) {

dataLayer.push({
        event: 'faq_tapped',
		
    });

});

 jQuery('#tcm').on('click',function( e ) {

dataLayer.push({
        event: 'terms_and_conditions_tapped',
		
    });

});

 jQuery('#cookies_m').on('click',function( e ) {

dataLayer.push({
        event: 'cookies_tapped',
		
    });

}); 

jQuery('#privacy_m').on('click',function( e ) {

dataLayer.push({
        event: 'privacy_tapped',
		
    });

});

 
 

//Ajax Handles
jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {

    //Out of Area Shown
    var out_of_area_action = "postcode_ajax_process_request_js";
    if(settings.url.indexOf("admin-ajax.php") >= 0 && settings.type == "POST" && settings.data.indexOf('postcode_ajax_process_request_js') >= 0 && xhr.responseText == 'empty' ){
        console.log('Out of Area Shown');
        dataLayer.push({
            event: 'out_of_area_shown',
            'postcode': postcodeCookie,
            'user-id': gtm_data.user_id
        });
    }
    /* --------------------- */
    
    
});
