<?php
global $woocommerce;
/* This constant is for products listing page to control how many products is to be displayed in frontend listing */
if(!defined('PRODUCTS_PER_PAGE')){
	define('PRODUCTS_PER_PAGE',90);
}

/* External Files Inclusion */

require_once('functions-checkout.php');
		//Checkout Related Functions and calls
		
require_once('functions-cart.php');
		//Cart Related Functions and calls
		
require_once('functions-admin.php');
		//Admin end Related Functions and calls

require_once('functions-devo-delivery-method.php');
		// This file is used to implement custom delivery fee functionality
require_once('functions-autocomplete.php');
		
/* External inclusions ends here */

//enque main css file
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );

}

function wpdocs_theme_name_scripts() {


	wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'child-script', get_stylesheet_directory_uri() . '/js/fdpickychild.js', array(), '1.0.0', true );
	wp_enqueue_script( 'custom_devo_script', get_stylesheet_directory_uri() . '/js/main.js', array(), '1.0.0', true );
	wp_localize_script('child-script', 'custom_child_script_ajax', array(
			'ajaxurl' => admin_url('admin-ajax.php'),
            'cached_outcodes' => get_option('cached_geo_data'),
	));
    //To track Activity and send to GTM on 2:45 PM 25 January, 2018 by PJ
    wp_enqueue_script( 'devo-gtm-analytics', get_stylesheet_directory_uri() . '/js/gtm-analytics.js', array( 'jquery' ), '1.0.0', true );
    wp_localize_script('devo-gtm-analytics', 'gtm_data', array(
			'user_id' => get_current_user_id(),
	));

	wp_enqueue_style( 'main-style', get_stylesheet_directory_uri() .'/css/main.css',false,'1.1','all');

	/* This Code applies on myaccount page only */
	if(function_exists('is_account_page') && is_account_page()){
		wp_enqueue_style( 'myaccount-style', get_stylesheet_directory_uri() .'/css/myaccount.css',false,'1.1','all');
		wp_enqueue_script( 'custom_devo_myaccount_script', get_stylesheet_directory_uri() . '/js/myaccount-js.js', array(), '1.0.0', true );

		wp_enqueue_script( 'stripe', 'https://js.stripe.com/v2/', '', '1.0', true );
		wp_enqueue_script( 'woocommerce_stripe', '/wp-content/plugins/woocommerce-gateway-stripe/assets/js/stripe.js', array( 'stripe' ), WC_STRIPE_VERSION, true );
        
        $stripe = new WC_Gateway_Stripe();
        $stripe_params = array(
			'key'                    => $stripe->publishable_key,
		);
        wp_localize_script( 'woocommerce_stripe', 'wc_stripe_params', apply_filters( 'wc_stripe_params', $stripe_params ) );

		wp_enqueue_script( 'jquery_validation', get_stylesheet_directory_uri() . '/js/jquery.validate.min.js', array( 'jquery' ), '1.0.0', true );
		wp_enqueue_script( 'jquery_validation_additional', get_stylesheet_directory_uri() . '/js/additional-methods.min.js', array( 'jquery' ), '1.0.0', true );
	}
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

/*****ajax request for store hone post code input value and match post code for shop address*****/

add_action('wp_ajax_postcode_ajax_process_request_js', 'postcode_ajax_process_request');
add_action('wp_ajax_nopriv_postcode_ajax_process_request_js', 'postcode_ajax_process_request');

function postcode_ajax_process_request(){
	global $wpdb;
    $outcode = '';
	$latitude = (!empty($_POST['latitude']))?$_POST['latitude']:null;
	$longitude = (!empty($_POST['longitude']))?$_POST['longitude']:null;
	if(empty($latitude) || empty($longitude)){
		die('empty');
	}
    $cookie_longitude = $_COOKIE['longitude'];
    $cookie_latitude = $_COOKIE['latitude'];
    
    $cookie_longitude = $_COOKIE['longitude'];
    $cookie_latitude = $_COOKIE['latitude'];
    $cached_geodata = array(); //it is used to cache geo data in WP Options so that we don't hit API for known geo-code
    // Comparing if Searched location is same as last entered location then use last provided outcode
    if($cookie_longitude == $longitude && $cookie_latitude == $latitude){
        $outcode = $_COOKIE['processed_outcode'];
    }else{
        $cached_geodata = get_option('cached_geo_data');
        if(array_key_exists("loc_".$longitude."_".$latitude, $cached_geodata)){
            $outcode = $cached_geodata["loc_".$longitude."_".$latitude]['processed_outcode'];
        }else{
            $outcode = match_geocode_with_shops($longitude, $latitude);
        }        
    }
	
	if(!empty($outcode)){
		setcookie('processed_outcode', $outcode, time() + (86400 * 30), "/"); // 86400 = 1 day
		die('notempty');
	}
    die('empty');
}



//notify me input email
add_action('wp_ajax_notifyme_ajax_process_request_js', 'notifyme_ajax_process_request');
add_action('wp_ajax_nopriv_notifyme_ajax_process_request_js', 'notifyme_ajax_process_request');
function notifyme_ajax_process_request(){
	global $wpdb;

	$email = (!empty($_POST['email']))?$_POST['email']:null;

	$email=$_POST['data'];
	$locations=$_POST['locations'];
	$emailsList=get_option('shop_request_clients_emails_list');

	if(!empty($emailsList)){
		$emailsList[] = array('email'=>$email,'locations'=>$locations);
	}else{
		$emailsListArray = array('email'=>$email,'locations'=>$locations);
		update_option( 'shop_request_clients_emails_list', $emailsListArray);
	}

		$to = 'help@devo.co.uk';
		$subject = 'NOTIFY ME | POST CODE NEEDED';
		$body = 'A new user has submitted request for Shop availability in his area.<br/> User Email : '.$email.'<br/>User locations : '.$locations;
		$headers = array('Content-Type: text/html; charset=UTF-8');

	wp_mail( $to, $subject, $body, $headers );

	die('success');
	/* if(empty($email)){
		die('empty');
	}
	die('notempty'); */
	//die('notempty');
}

//1. Add a new form element...
add_action( 'register_form', 'myplugin_register_form' );
function myplugin_register_form() {

    $first_name = ( ! empty( $_POST['first_name'] ) ) ? trim( $_POST['first_name'] ) : '';
    $last_name = ( ! empty( $_POST['last_name'] ) ) ? trim( $_POST['last_name'] ) : '';

        ?>
        <p class="form-row form-row-wide">
            <label for="first_name"><?php _e( 'First Name *', 'woocommerce' ) ?></label>
             <input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>"/></label>
        </p>
        <p class="form-row form-row-wide">
            <label for="first_name"><?php _e( 'Last Name *', 'woocommerce' ) ?></label>
             <input type="text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" /></label>
        </p>
        <?php
}

//2. Add validation. In this case, we make sure first_name and lastname is required.
add_action( 'woocommerce_register_post', 'devo_validate_extra_register_fields', 10, 3 );
function devo_validate_extra_register_fields( $username, $email, $validation_errors ) {

    if ( isset( $_POST['first_name'] ) && empty( $_POST['first_name'] ) ) {
        $validation_errors->add( 'first_name_error', __( 'You must include a first name.', 'woocommerce' ) );
    }

    if ( isset( $_POST['last_name'] ) && empty( $_POST['last_name'] ) ) {
        $validation_errors->add( 'last_name_error', __( 'You must include a last name.', 'woocommerce' ) );
    }
}





//3. Finally, save our extra registration user meta.
add_action( 'user_register', 'myplugin_user_register' );
function myplugin_user_register( $user_id ) {
	if ( ! empty( $_POST['first_name'] ) ) {
		update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
	}
	if ( ! empty( $_POST['last_name'] ) ) {
		update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
	}
	wp_set_current_user($user_id);
	wp_set_auth_cookie($user_id);
		// You can change home_url() to the specific URL,such as
	//wp_redirect( 'http://www.wpcoke.com' );
    if(!empty($_POST['_wp_http_referer'])){
        $redirect_to = $_POST['_wp_http_referer'];
        wp_redirect( $redirect_to );
        exit;
    }
	wp_redirect(home_url());
	exit;
}

/**
 * Add HTML5 theme support.for product search box
 */
function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );


// define the woocommerce_credit_card_form_fields callback

function filter_woocommerce_credit_card_form_fields( $default_fields, $this_id ) {


	$default_fields['card-number-field']='<div class="col-md-12 col-sm-12 col-xs-12"><input id="stripe-card-number" class="input-text wc-credit-card-form-card-number" type="text" maxlength="20" autocomplete="off" placeholder="Card Number"  name="stripe-card-number"/></div>';

	$default_fields['card-expiry-field']='<div class="col-md-12 col-sm-12 col-xs-12"><input id="stripe-card-expiry" class="input-text wc-credit-card-form-card-expiry" type="text" maxlength="7" autocomplete="off" placeholder="MM/YY" name="stripe-card-expiry"/></div>';

	$default_fields['card-cvc-field']='<div class="col-md-12 col-sm-12 col-xm-12"><input id="stripe-card-cvc" class="input-text wc-credit-card-form-card-cvc" type="text" maxlength="3" autocomplete="off" placeholder="CVC" name="stripe-card-cvc"/></div>';

	return $default_fields;
};

// add the filter
add_filter( 'woocommerce_credit_card_form_fields', 'filter_woocommerce_credit_card_form_fields', 10, 2 );


/* set cookies check postcode exist or not */
/* $location = isset($_GET['location']) ?  $_GET['location'] : ''; */

	 if ( !empty($_GET["location"]) && !empty($_GET["post_type"]) && !empty($_GET["role"])) {
		$cookie_name = "location";
		$cookie_value = $_GET["location"];
		setcookie("location", $cookie_value, time() + 86400, "/");   // 86400 = 1 day
	}

/* End cookies */


add_action( 'woocommerce_email_before_order_table', 'add_order_email_instructions', 10, 2 );

function add_order_email_instructions( $order, $sent_to_admin ) {

	$user_info = get_userdata($order->customer_user);
	$first_name = $user_info->first_name;

	echo "<h3 mc:edit='header' style='color:#737373; line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;'>Hi $first_name,</h3>";
	echo '<div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:10px;color:#737373;line-height:135%;">Your order has been received and is now being prepared. Your order details are shown below.</div>';

}

/*-------International Formate Number Start here-------*/

function formatPhoneNumber($p_num) {
    $p_num = preg_replace('/[^0-9]/','',$p_num);

    if(strlen($p_num) > 12) {
        $countryCode = substr($p_num, 0, strlen($p_num)-12);
        $areaCode = substr($p_num, -12, 4);
        $nextThree = substr($p_num, -7, 3);
        $lastFour = substr($p_num, -5, 5);

        $p_num = '+'.$countryCode.''.$areaCode.''.$nextThree.'&nbsp;'.$lastFour;
    }
    else if(strlen($p_num) == 12) {
        $areaCode = substr($p_num, 0, 4);
        $nextThree = substr($p_num, 3, 3);
        $lastFour = substr($p_num, 6, 5);

        $p_num = '+'.$areaCode.'&nbsp;'.$nextThree.'&nbsp;'.$lastFour;
    }
    else if(strlen($p_num) == 7) {
        $nextThree = substr($p_num, 0, 3);
        $lastFour = substr($p_num, 3, 5);

        $p_num = $nextThree.'&nbsp;'.$lastFour;
    }

    return $p_num;
}


/**
 * Changes the redirect URL for the Return To Shop button in the cart.
 *
 * @return string
 */
function wc_empty_cart_redirect_url() {
	extract($_COOKIE);
	$urlAppend = get_site_url();
	if(!empty($latitude) && !empty($longitude) && !empty($location)){
		$urlAppend = get_site_url()."?location={$location}&latitude={$latitude}&longitude={$longitude}&use_radius=on&radius=30&post_type=azl_profile&role=vendor";
	}
	return $urlAppend;
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );

add_filter( 'woocommerce_price_trim_zeros', 'wc_hide_trailing_zeros', 10, 1 );
function wc_hide_trailing_zeros( $trim ) {
    // set to false to show trailing zeros
    return false;
}

add_filter( 'registration_redirect', '__my_registration_redirect',100 );
function __my_registration_redirect(){
	$query_string = (!empty($_SERVER['QUERY_STRING']))?'?'.$_SERVER['QUERY_STRING']:'';

	$redirect_to = $_SERVER['REQUEST_URI'].$query_string;
    if(!empty($_POST['_wp_http_referer'])){
        $redirect_to = $_POST['_wp_http_referer'];
    }
	return $redirect_to;

}

add_filter('woocommerce_login_redirect', 'wc_login_redirect');
add_filter('woocommerce_registration_redirect', 'wc_login_redirect');
function wc_login_redirect( $redirect_to ) {
	
	/* if(isset($_COOKIE['redirect_to'])){
		setcookie("redirect_to", "", time() - 3600);
		return $_COOKIE['redirect_to'];
	}
	
	$query_string = (!empty($_SERVER['QUERY_STRING']))?'?'.$_SERVER['QUERY_STRING']:'';

	$redirect_to = $_SERVER['REQUEST_URI'].$query_string; */
    if(!empty($_POST['_wp_http_referer'])){
        $redirect_to = $_POST['_wp_http_referer'];
    }
	return $redirect_to;
}

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
	wp_redirect( home_url() );
	exit();
}

function get_outcode_from_geocode($longitude, $latitude,$raw = false,$failsafe=false){
	$responseData = array();
	$url = "https://api.postcodes.io/postcodes?lon={$longitude}&lat={$latitude}";
   
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	$data = json_decode($response,true);
	
	if(!empty($data) && $data['status']==200 && count($data['result']) > 0){
        $postData = $data['result'][0];
		$responseData['processed_postcode'] = (!empty($postData['postcode']))?$postData['postcode']:$postData['outcode'];
		$responseData['processed_outcode'] = $postData['outcode'];
		$responseData['processed_incode'] = (!empty($postData['incode']))?$postData['incode']:$postData['outcode'];
	}else{
        $url = "https://api.postcodes.io/outcodes?lon={$longitude}&lat={$latitude}";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $data = json_decode($response,true);
        
        if(!empty($data) && $data['status']==200 && count($data['result']) > 0){
            $postData = $data['result'][0];
            $responseData['processed_postcode'] = (!empty($postData['postcode']))?$postData['postcode']:$postData['outcode'];
            $responseData['processed_outcode'] = $postData['outcode'];
            $responseData['processed_incode'] = (!empty($postData['incode']))?$postData['incode']:$postData['outcode'];
        }
    }
    // echo "<pre>";print_r($responseData);die;
	if($raw==true){
		return $data;
	}
	// file_put_contents(__FILE__.'postcode.txt',print_r($responseData,true));
	return $responseData;
}

function match_geocode_with_shops($longitude, $latitude){
    $responseData = array();

    
    $responseData = get_outcode_from_geocode($longitude, $latitude);
    array_map('strtolower',$responseData);
    
    //it is used to cache geo data in WP Options so that we don't hit API for known geo-code
    $cached_geo_datoptions = get_option('cached_geo_data');
    $cached_geodata = (!empty($cached_geo_datoptions))?$cached_geo_datoptions:array();
    
    $cached_geodata["loc_".$longitude."_".$latitude] = $responseData;
    update_option('cached_geo_data', $cached_geodata);	

	if(!empty($responseData) && !empty($responseData['processed_outcode'])){
        // We have cached postcodes of locations where we serve, so if at any situation we don't find this Option
        // as a fail safe we regenerate it
		$available_postcodes = get_option('cached_postcodes');
		if(empty($available_postcodes)){
			$dbpostcodes=array();
			global $wpdb;
			$sql = $wpdb->prepare("
					SELECT $wpdb->postmeta.meta_value
						FROM $wpdb->postmeta
						WHERE 1=1
							AND $wpdb->postmeta.meta_key = %s
						",'profile_delivery_data'
					);
			$post_values = $wpdb->get_results($sql, OBJECT_K);
			foreach($post_values as $items){
				$metadata = array_keys(unserialize($items->meta_value));
				foreach($metadata as $postcode){
					$dbpostcodes[] = $postcode;
				}
			}
			$available_postcodes = array_unique($dbpostcodes);
			update_option( 'cached_postcodes', $available_postcodes);
		}
		$all_postcodes = $available_postcodes;
		$all_postcodes = array_map('strtolower',$all_postcodes);

		/* Fetch All Postcodes here in array format */
		$outcode = strtolower($responseData['processed_outcode']);
		if(in_array($outcode,$all_postcodes)){
			return $outcode;
		}
	}
}

/* this function is called in top of header. it is used to fetch Outcode from Postcodes.io on Geo Location Basis */
function do_postcodes_fetch(){
	if(empty($_COOKIE['processed_postcode'])){
		if(empty($_COOKIE['latitude']) || empty($_COOKIE['longitude']) || empty($_COOKIE['location']))
		return;
	}else{
		return;
	}
	$latitude = $_COOKIE['latitude'];
	$longitude = $_COOKIE['longitude'];
	$responseData = get_outcode_from_geocode($longitude, $latitude);
	if(!empty($responseData)){
		foreach($responseData as $key=>$value){
			setcookie($key, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
		}
	}
	return;

}

/* Rebuild Cache on Post Change */
add_action( 'save_post', 'rebuild_cache_postcode' );
add_action( 'delete_post', 'rebuild_cache_postcode', 10 );
function rebuild_cache_postcode( $post_id){
	$post_type = get_post_type($post_id);

    // If this isn't a 'book' post, don't update it.
    if ( "azl_profile" != $post_type ) return;
	$dbpostcodes=array();
	global $wpdb;
	$sql = $wpdb->prepare("
			SELECT $wpdb->postmeta.meta_value
				FROM $wpdb->postmeta
				WHERE 1=1
					AND $wpdb->postmeta.meta_key = %s
				",'profile_delivery_data'
			);
	$post_values = $wpdb->get_results($sql, OBJECT_K);
	foreach($post_values as $items){
		$metadata = array_keys(unserialize($items->meta_value));
		foreach($metadata as $postcode){
			$dbpostcodes[] = $postcode;
		}
	}
	$available_postcodes = array_unique($dbpostcodes);
	update_option( 'cached_postcodes', $available_postcodes);
	
}

/* 11:14 AM 24 January, 2018 by PJ to fix Login bug after adding product to cart */

add_action( 'init', 'process_profile_login_fix' );

function process_profile_login_fix() {
    if ( ! empty( $_POST['login'] )) {
        $nonce_value = isset( $_POST['_wpnonce'] ) ? $_POST['_wpnonce'] : '';
        $nonce_value = isset( $_POST['woocommerce-login-nonce'] ) ? $_POST['woocommerce-login-nonce'] : $nonce_value;
        
        if(!wp_verify_nonce( $nonce_value, 'woocommerce-login' ) ){
            devo_custom_process_login();
        }
        
    }
    
    if ( ! empty( $_POST['register'] )) {
        $nonce_value = isset( $_POST['_wpnonce'] ) ? $_POST['_wpnonce'] : '';
		$nonce_value = isset( $_POST['woocommerce-register-nonce'] ) ? $_POST['woocommerce-register-nonce'] : $nonce_value;

        if(!wp_verify_nonce( $nonce_value, 'woocommerce-register' ) ){
            devo_custom_process_register();
        }
        
    }
}

function devo_custom_process_login(){
    try {
        $creds = array(
            'user_password' => $_POST['password'],
            'remember'      => isset( $_POST['rememberme'] ),
        );

        $username         = trim( $_POST['username'] );
        $validation_error = new WP_Error();
        $validation_error = apply_filters( 'woocommerce_process_login_errors', $validation_error, $_POST['username'], $_POST['password'] );

        if ( $validation_error->get_error_code() ) {
            throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . $validation_error->get_error_message() );
        }

        if ( empty( $username ) ) {
            throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'Username is required.', 'woocommerce' ) );
        }

        if ( is_email( $username ) && apply_filters( 'woocommerce_get_username_from_email', true ) ) {
            $user = get_user_by( 'email', $username );

            if ( ! $user ) {
                $user = get_user_by( 'login', $username );
            }

            if ( isset( $user->user_login ) ) {
                $creds['user_login'] = $user->user_login;
            } else {
                throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'A user could not be found with this email address.', 'woocommerce' ) );
            }
        } else {
            $creds['user_login'] = $username;
        }

        // On multisite, ensure user exists on current site, if not add them before allowing login.
        if ( is_multisite() ) {
            $user_data = get_user_by( 'login', $username );

            if ( $user_data && ! is_user_member_of_blog( $user_data->ID, get_current_blog_id() ) ) {
                add_user_to_blog( get_current_blog_id(), $user_data->ID, 'customer' );
            }
        }

        // Perform the login
        $user = wp_signon( apply_filters( 'woocommerce_login_credentials', $creds ), is_ssl() );

        if ( is_wp_error( $user ) ) {
            $message = $user->get_error_message();
            $message = str_replace( '<strong>' . esc_html( $creds['user_login'] ) . '</strong>', '<strong>' . esc_html( $username ) . '</strong>', $message );
            throw new Exception( $message );
        } else {

            if ( ! empty( $_POST['redirect'] ) ) {
                $redirect = $_POST['redirect'];
            } elseif ( wc_get_raw_referer() ) {
                $redirect = wc_get_raw_referer();
            } else {
                $redirect = wc_get_page_permalink( 'myaccount' );
            }

            wp_redirect( wp_validate_redirect( apply_filters( 'woocommerce_login_redirect', remove_query_arg( 'wc_error', $redirect ), $user ), wc_get_page_permalink( 'myaccount' ) ) );
            exit;
        }
    } catch ( Exception $e ) {
        wc_add_notice( apply_filters( 'login_errors', $e->getMessage() ), 'error' );
        do_action( 'woocommerce_login_failed' );
    }
}

function devo_custom_process_register(){
    $username = 'no' === get_option( 'woocommerce_registration_generate_username' ) ? $_POST['username'] : '';
    $password = 'no' === get_option( 'woocommerce_registration_generate_password' ) ? $_POST['password'] : '';
    $email    = $_POST['email'];

    try {
        $validation_error = new WP_Error();
        $validation_error = apply_filters( 'woocommerce_process_registration_errors', $validation_error, $username, $password, $email );

        if ( $validation_error->get_error_code() ) {
            throw new Exception( $validation_error->get_error_message() );
        }

        $new_customer = wc_create_new_customer( sanitize_email( $email ), wc_clean( $username ), $password );

        if ( is_wp_error( $new_customer ) ) {
            throw new Exception( $new_customer->get_error_message() );
        }

        if ( apply_filters( 'woocommerce_registration_auth_new_customer', true, $new_customer ) ) {
            wc_set_customer_auth_cookie( $new_customer );
        }

        if ( ! empty( $_POST['redirect'] ) ) {
            $redirect = wp_sanitize_redirect( $_POST['redirect'] );
        } elseif ( wc_get_raw_referer() ) {
            $redirect = wc_get_raw_referer();
        } else {
            $redirect = wc_get_page_permalink( 'myaccount' );
        }

        wp_redirect( wp_validate_redirect( apply_filters( 'woocommerce_registration_redirect', $redirect ), wc_get_page_permalink( 'myaccount' ) ) );
        exit;

    } catch ( Exception $e ) {
        wc_add_notice( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . $e->getMessage(), 'error' );
    }
}


/* Store page Code */

/* Store page Code */

function devo_custom_geolocation_process($field) {
    $use_radius = isset($_GET['use_radius']) && 'on' == $_GET['use_radius'];
    $lat = isset($_GET[$field['lat_meta_key']]) ? (float) $_GET[$field['lat_meta_key']] : false;
    $lng = isset($_GET[$field['lng_meta_key']]) ? (float) $_GET[$field['lng_meta_key']] : false;
    $radius = isset($_GET['radius']) ? (int) $_GET['radius'] : false;

	if(isset($_GET['latitude']) && isset($_GET['longitude']) && !empty($_COOKIE['processed_outcode'])){
		$raw_postcode = strtolower($_COOKIE['processed_outcode']);		
		$outcode = "%".$_COOKIE['processed_outcode']."%";
		global $wpdb;
		$sql = $wpdb->prepare("
				SELECT $wpdb->posts.ID
					FROM $wpdb->posts
					INNER JOIN $wpdb->postmeta 
						AS pm 
						ON $wpdb->posts.ID = pm.post_id
					WHERE 1=1
						AND ($wpdb->posts.post_status = 'publish' ) 
						AND pm.meta_key = %s
						AND pm.meta_value LIKE %s
					ORDER BY $wpdb->posts.menu_order ASC",'profile_delivery_data', $outcode
				);
        $post_ids = $wpdb->get_results($sql, OBJECT_K);
        $sorted_ids = array();
        if(!empty($post_ids)){
            $args = array(
                'post_type' => 'azl_profile',
                'post__in' =>array_keys($post_ids),
            );
            
            $active_shops = array();
            $closed_shops = array();
            $coming_soon = array();
            $all_posts = get_posts($args);
            foreach($all_posts as $post){
                $hours = get_post_meta($post->ID, 'working-hours-' . date('N', time() + get_option('gmt_offset') * HOUR_IN_SECONDS) . '-hours');
                $postcode_data = get_post_meta($post->ID,'profile_delivery_data',true);
                if(!empty($postcode_data[$raw_postcode])){
                    $deliveryData = $postcode_data[$raw_postcode];
                    $deliveryPrice = $deliveryData[1];
                    $deliveryTime = $deliveryData[2];
                    if(in_array(date('G', time() + get_option('gmt_offset') * HOUR_IN_SECONDS), $hours)) {
                        $active_shops[] = array(
                            'id'				=>$post->ID,
                            'delivery_charge'	=>$deliveryPrice,
                            'delivery_time'		=>$deliveryTime
                        );
                    } else {
                        $closed_shops[] = array(
                            'id'				=>$post->ID,
                            'delivery_charge'	=>$deliveryPrice,
                            'delivery_time'		=>$deliveryTime
                        );
                    }
                }else{
                    $coming_soon[] = array(
                        'id'				=>$post->ID,
                        'delivery_charge'	=>0.0,
                        'delivery_time'		=>00
                    );
                }
                
            }
            usort($active_shops, function($a, $b) {
                return $a['delivery_charge'] - $b['delivery_charge'];
            });
            usort($closed_shops, function($a, $b) {
                return $a['delivery_charge'] - $b['delivery_charge'];
            });
            
            $allshops=array_merge($active_shops,$closed_shops,$coming_soon);
            $final_shops = array_column($allshops, 'id');
            $sorted_ids = $final_shops;
        }
			
			
			
        if (is_array($post_ids)) {
            if (empty($post_ids)) {
                return array(0);
            } else {
                if(!empty($sorted_ids)){
                    return $sorted_ids;
                }else{
                   return array_keys($post_ids);
                }
            }
        }
        return array(0);
	}
	
}

add_filter( 'woocommerce_product_subcategories_args', 'devo_remove_uncategorized_category' );
/**
 * Remove uncategorized category from shop page.
 *
 * @param array $args Current arguments.
 * @return array
 **/
function devo_remove_uncategorized_category( $args ) {
  $uncategorized = get_option( 'default_product_cat' );
  $args['exclude'] = $uncategorized;
  return $args;
}
