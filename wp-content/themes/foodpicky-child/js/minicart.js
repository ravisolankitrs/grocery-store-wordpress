function add_cart_list(pro_id,changeOne,current){
	
	if(changeOne == -1){
		dataLayer.push({
            event: 'basket_minus_item_tapped',
            'productId': pro_id,
            
        });
	}
	
	if(changeOne == 1){
		dataLayer.push({
            event: 'basket_increase_item_tapped',
            'productId': pro_id,
            
        });
	}
	
 
	
    /* Change quantity inline starts */
    var qty2 = jQuery("#quant_"+pro_id).val();
    var quantity = Number(changeOne)+Number(qty2);
    if( quantity < 1 ){
        console.log('quantity is less');
        jQuery('#pro_id_'+pro_id).css('display','none');
    }else{
        jQuery('#pro_id_'+pro_id).css('display','block');
    }
    jQuery('#quan_text_'+pro_id).html(quantity);
    /* Change quantity inline ends */
      
    /* Change inline total inline starts */
    var lineTotal = jQuery("#dataLine_"+pro_id).val();
    var dataPrice = jQuery("#dataPrice_"+pro_id).val();
    //var subTotal = jQuery("#sub_hid_ajx").val();
    
    var tTotal = jQuery("#total_hid_ajx").val();
    if(changeOne==1){
        var lineVal = (parseFloat(lineTotal)+parseFloat(dataPrice)).toFixed(2);
        
        var tTAjx = (parseFloat(tTotal)+parseFloat(dataPrice)).toFixed(2);
    }else{
        var lineVal = (parseFloat(lineTotal)-parseFloat(dataPrice)).toFixed(2);
        
        var tTAjx = (parseFloat(tTotal)-parseFloat(dataPrice)).toFixed(2);
    }
    jQuery('#line_total_'+pro_id).html(lineVal);
    
    jQuery('#t_amt_ajx').html(tTAjx);
    /* Change inline total inline ends */
    
    doClosestRemoveaction(current,quantity);
    var base_url = window.location.origin;
    jQuery.ajax({
        url: custom_child_script_ajax.ajaxurl,
        type: "GET",
        data: {'action':'changeCartQuantity_js','pro_id':pro_id,'quantity':quantity,'changeOne':changeOne},
        dataType:'json',
        success:function(response) {
            //console.log(response);
            jQuery('#quan_text_'+pro_id).html(response[0]);
            jQuery('#quant_'+pro_id).val(response[0]); //change value of quntity num hidden field
            jQuery('#line_total_'+pro_id).html(response[1]);
            jQuery('#dataLine_'+pro_id).val(response[1]); //change value of line total hidden field
            jQuery('#sub_ajx').html(response[2]);
            jQuery('#cart-head_total').html(response[2]);
            //jQuery("#sub_hid_ajx").val(response[2]); //change value of sub total hidden field
            jQuery('.minicart_subtotal').html(response[4]);
            refresh_fragments();
            if(response==''){
                // jQuery('#sidebar-1').css('display','none');
            }
        },
        error: function(errorThrown){
            console.log(errorThrown);
            // jQuery('#sidebar-1').css('display','none');
        }
    });
	doEmptyCartInspect();
	// alert(pro_id);
		 jQuery('#product_loader_'+pro_id).addClass('product_loader_m');
		 jQuery('#pro_id_'+pro_id).addClass('main_div_disable');
		 jQuery('.custom_mini_cart_div').addClass('main_div_disable');
		
			jQuery('#one_plus').addClass('disabled');
			jQuery('#one_minus').addClass('disabled'); 
 
 
	
	
	//}
}
function doEmptyCartInspect(){
	try{
		var storeCount = jQuery('.mini_cart_item_div').length;
		if(storeCount < 1){
			jQuery('.widget_shopping_cart').html('<div class="widget-title"><h3>My Basket</h3></div><div class="widget_shopping_cart_content"><ul class="cart_list product_list_widget "><li class="empty">No products in the cart.</li></ul><!-- end product list --></div>');
			/* jQuery('.upper-white').addClass('upper_white_ajax');	
			jQuery('.cart_contents_head_ajax > #basket').hide(); */			
		}
		/* else{
			jQuery('#sidebar-1').show();
			jQuery('.upper-white').removeClass('upper_white_ajax');
			jQuery('.cart_contents_head_ajax > #basket').show();
		} */
	}catch(e){
		
	}
}
function doClosestRemoveaction(current,quantity){
	var closestProduct = jQuery(current).closest('.mini_cart_item_div');
	if( quantity < 1 ){
		jQuery(closestProduct).remove();
		// jQuery(current).closest('.mini_cart_item_div').remove();
	}
	/* var itemsCount =  parseInt(jQuery( "#"+closestShop ).find( ".mini_cart_item_div" ).length);
	if( itemsCount < 1){
		
		jQuery("#"+closestShop).remove();
	} */
}

/* Force Woocommerce cart refresh */
function refresh_fragments() {
    console.log('fragments refreshed!');
    jQuery( document.body ).trigger( 'wc_fragment_refresh' );
}

/* Following Functions are intended for Main Cart Page */
function main_add_cart_list(pro_id,changeOne,current){
	
		var qty2 = jQuery("#main_quant_"+pro_id).val();
		var quantity = Number(changeOne)+Number(qty2);
		if( quantity < 1 ){
			console.log('quantity is less');
			jQuery('#main_pro_id_'+pro_id).css('display','none');
		}else{
			jQuery('#main_pro_id_'+pro_id).css('display','block');
		}
		jQuery('#main_quan_text_'+pro_id).html(quantity);
		/* Change quantity inline ends */
		
		/* Change inline total inline starts */
		var lineTotal = jQuery("#main_dataLine_"+pro_id).val();
		var dataPrice = jQuery("#main_dataPrice_"+pro_id).val();
		//var subTotal = jQuery("#sub_hid_ajx").val();
		
		var tTotal = jQuery("#total_hid_ajx").val();
		if(changeOne==1){
			var lineVal = (parseFloat(lineTotal)+parseFloat(dataPrice)).toFixed(2);
			
			var tTAjx = (parseFloat(tTotal)+parseFloat(dataPrice)).toFixed(2);
		}else{
			var lineVal = (parseFloat(lineTotal)-parseFloat(dataPrice)).toFixed(2);
			
			var tTAjx = (parseFloat(tTotal)-parseFloat(dataPrice)).toFixed(2);
		}
		jQuery('#main_line_total_'+pro_id).html(lineVal);
		
		jQuery('#t_amt_ajx').html(tTAjx);
		/* Change inline total inline ends */
		
		// doClosestRemoveaction(current,quantity);
		var base_url = window.location.origin;
		jQuery.ajax({
			url: custom_child_script_ajax.ajaxurl,
			type: "GET",
			data: {'action':'changeCartQuantity_js','pro_id':pro_id,'quantity':quantity,'changeOne':changeOne},
			dataType:'json',
			success:function(response) {
				//console.log(response);
				jQuery('#main_quan_text_'+pro_id).html(response[0]);
				jQuery('#main_quant_'+pro_id).val(response[0]); //change value of quntity num hidden field
				jQuery('#main_line_total_'+pro_id).html(response[1]);
				jQuery('#main_dataLine_'+pro_id).val(response[3]); //change value of line total hidden field
				jQuery('#main_line_total_'+pro_id).html(response[3]); //change value of line total hidden field
				// jQuery('#sub_ajx').html(response[2]);
				// jQuery('#cart-head_total').html(response[2]);
				//jQuery("#sub_hid_ajx").val(response[2]); //change value of sub total hidden field
				// jQuery('.minicart_subtotal').html(response[4]);
				refresh_fragments();
				if(response==''){
					// jQuery('#sidebar-1').css('display','none');
				}
			},
			error: function(errorThrown){
				console.log(errorThrown);
				// jQuery('#sidebar-1').css('display','none');
			}
		});
	// doEmptyCartInspect(); abs
	//} 
	
	
	jQuery('#product_loader_'+pro_id).addClass('product_loader_main_cart');
		 jQuery('#main_pro_id_'+pro_id).addClass('main_div_disable');
		  jQuery('.panel-body').find('.woocommerce-cart-form').addClass('main_div_disable');
			jQuery('#one_plus').addClass('disabled');
			jQuery('#one_minus').addClass('disabled'); 
	
}
 