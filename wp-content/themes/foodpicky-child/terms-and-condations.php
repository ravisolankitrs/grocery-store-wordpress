<?php 
/* 
 *Template Name: Terms And Conditions
 */ 
get_header(); ?>
<style>
body{	
	font-family: "Neutraface Text","Zona Pro",Helvetica,Arial,Serif;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    color: #2D2D2D;
}
.header_text{
	text-align:left;
	padding-left:0px;
}
.terms_header, h3{
	font-weight: 700;
    font-size: 24px;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    line-height: 1.1;
    color: inherit;
    box-sizing: border-box;
}
p{
	font-size: 14px;
    line-height: 1.42857143;
}
a{
	color: #FF6C61;
}
a:hover {
    text-decoration: underline;
}
a:visited {
   color: #FF6C61;
}
ul {
    list-style-type: disc;
	padding-left: 3%;
	color: #2D2D2D;
}
ol{
	padding-left: 3%;
}
.custom_strong strong,strong{
	color: #2D2D2D;
    font-weight: 700;
    font-size: 14px;
}
.custom_ol_alpha{
	list-style: lower-alpha;
	padding-left: 0px;
}
ol ol ol {
	list-style: lower-roman;
	padding-left: 0px;
}

</style>
<div id="primary" class="content-area 123">
	<main id="main" class="site-main testing" role="main">
		<article id="post-13544" class="post-13544 page type-page status-publish hentry">
			<header class="entry-header">
				<div class="dev-page-header-inner dev-outer">
					<div class="dev-inner dev-text-centered">
					<h2 class="text-head"> Terms & Conditions </h2> 
			
					</div>
				</div>
				</header>
				<div class="container">
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							<div class="entry-content">
								<h3 class="header_text">Terms and Conditions of Service&nbsp;</h3>
								<p>These terms of use (together with the documents referred to in them) tell you the terms on which you may make use of our website URLs <a href="http://www.devo.co.uk">www.devo.co.uk</a> and the iOS and Android mobile application versions (our Service), whether as a guest or as a registered user. Use of our Service includes accessing, browsing, or registering to use our Service either as a consumer or as a seller.</p>
								<p>Please read these terms of use carefully before you start to use our Service, as these will apply to your use of our Service. We recommend that you print a copy for future reference.</p>
								<p>By using our Service, you confirm that you accept these terms of use and that you agree to comply with them.</p>
								<p>If you do not agree to these terms of use, you must not use our Service.</p>
								<p><strong>Other applicable terms</strong></p>
								<p>These terms of use refer to the following additional terms, which also apply to your use of our Service:</p>
								<ul>
									<li>Our <a href="https://www.devo.co.uk/privacy">Privacy Policy</a>, which sets out the terms on which we process any personal data we collect from you, or that you provide to us. By using our Service, you consent to such processing and you warrant that all data provided by you is accurate.</li>
									<li>Our <a href="#acceptable-policy">Acceptable Use Policy</a>, which sets out the permitted uses and prohibited uses of our Service. When using our Service, you must comply with this Acceptable Use Policy.</li>
									<li>Our <a href="http://www.devo.co.uk/cookies">Cookie Policy </a>, which sets out information about the cookies on our Service.</li>
									<li>If you visit our Service as a consumer, our <a href="#terms-consumers">Terms and Conditions for Consumers</a> will apply.</li>
									<li>If you sell goods on our Service, our Seller Participation Agreement will apply.</li>
									<li>If you are a courier, our Courier Agreement will apply.</li>
								</ul>
								<p><strong>Information about us</strong></p>
		                        <p>Our Service is operated by Devo, a trading name of Devo Technologies Ltd  (we or us). Devo Technologies Ltd  is a company a company incorporated in England and Wales under company number 9996712 whose registered office is at 20-22 Wenlock Road, London N1 7GU.</p>																								<p><strong>Changes to these terms</strong></p>	                            <p>We may revise these terms of use at any time by amending this page.</p>		                            <p>Please check this page from time to time to take notice of any changes we have made, as they are legally binding on you.</p>
								
								<p><strong>Changes to our Service</strong></p>
								<p>We may update our Service from time to time, and may change the content at any time. However, please note that any of the content on our Service may be out of date at any given time, and we are under no obligation to update it.</p>
								<p>We do not guarantee that our Service, or any content on it, will be free from errors or omissions.</p>
								<p><strong>Accessing our Service</strong></p>
								<p>Our Service is made available free of charge.</p>
								<p>We do not guarantee that our Service, or any content on it, will always be available or be uninterrupted. Access to our Service is permitted on a temporary basis. We may suspend, withdraw, discontinue or change all or any part of our Service without notice. We will not be liable to you if for any reason our Service is unavailable at any time or for any period.</p>
								<p>You are responsible for making all arrangements necessary for you to have access to our Service.</p>
								<p>You are also responsible for ensuring that all persons who access our Service through your internet connection are aware of these terms of use and other applicable terms and conditions, and that they comply with them.</p>
								<p><strong>Your account and password</strong></p>
								<p>If you choose, or you are provided with, a password or any other piece of information as part of our security procedures, you must treat such information as confidential. You must not disclose it to any third party.</p>
								<p>We have the right to disable any password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these terms of use.</p>
								<p>If you know or suspect that anyone other than you knows your password, you must promptly notify us at info@devo.co.uk</p>
								<p><strong>Intellectual property rights</strong></p>
								<p>We are the owner or the licensee of all intellectual property rights in our Service, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.</p>
								<p>You may print off one copy, and may download extracts, of any page(s) from our Service for your personal use.</p>
								<p>You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text.</p>
								<p>Our status (and that of any identified contributors) as the authors of content on our Service must always be acknowledged.</p>
								<p>You must not use any part of the content on our Service for commercial purposes without obtaining a licence to do so from us or our licensors where appropriate.</p>
								<p>If you print off, copy or download any part of our Service in breach of these terms of use, your right to use our Service will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</p>
								<p><strong>No reliance on information</strong></p>
								<p>The content on our Service is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on our Service.</p>
								<p>Although we make reasonable efforts to update the information on our Service, we make no representations, warranties or guarantees, whether express or implied, that the content on our Service is accurate, complete or up-to-date.</p>
								<p><strong>Limitation of our liability</strong></p>
								<p>Nothing in these terms of use excludes or limits our liability for death or personal injury arising from our negligence, or our fraud or fraudulent misrepresentation, or any other liability that cannot be excluded or limited by English law.</p>
								<p>To the extent permitted by law, we exclude all conditions, warranties, representations or other terms which may apply to our Service or any content on it, whether express or implied.</p>
								<p>We will not be liable to any user for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:</p>
								<ul>
									<li>use of, or inability to use, our Service; or</li>
									<li>use of or reliance on any content displayed on our Service.</li>
								</ul>
								<p>If you are a business user, please note that in particular, we will not be liable for:</p>
								<ul>
									<li>loss of profits, sales, business, or revenue;</li>
									<li>business interruption;</li>
									<li>loss of anticipated savings;</li>
									<li>loss of business opportunity, goodwill or reputation; or</li>
									<li>any indirect or consequential loss or damage.</li>
								</ul>
								<p>We will not be liable for any loss or damage caused by a virus, distributed denial-of-service attack, or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of our Service or to your downloading of any content on it, or on any website linked to it.</p>
								<p>We assume no responsibility for the content of websites linked on our Service. Such links should not be interpreted as endorsement by us of those linked websites. We will not be liable for any loss or damage that may arise from your use of them.</p>
								<p>Different limitations and exclusions of liability will apply to liability arising as a result of orders placed on our Service, which are set out in our 
									<a href="#terms-consumers">Terms and Conditions for Consumers</a> and in our Seller Participation Agreement.
								</p>
								<p><strong>Uploading content to our Service</strong></p>
								<p>Whenever you make use of a feature that allows you to upload content to our Service, you must comply with our 
									<a href="#acceptable-policy">Acceptable Use Policy</a>.
								</p>
								<p>You warrant that any such use complies with those standards, and you will be liable to us and indemnify us for any breach of that warranty. This means you will be responsible for any loss or damage we suffer as a result of your breach of warranty.</p>
								<p>Any content you upload to our Service will be considered non-confidential and nonproprietary. You retain all of your ownership rights in your content, but you are required to grant us a limited licence to use, store and copy that content and to distribute and make it available to third parties.</p>
								<p>We also have the right to disclose your identity to any third party who is claiming that any content posted or uploaded by you to tour Service constitutes a violation of their intellectual property rights, or of their right to privacy, or to the police or other legal authority if they reasonably believe that you have committed a criminal offence or breached any rules or regulations in respect of your obligations under these terms of website use.</p>
								<p>We will not be responsible, or liable to any third party, for the content or accuracy of any content posted by you or any other user of our Service.</p>
								<p>You are solely responsible for securing and backing up your content.</p>
								<p><strong>Viruses</strong></p>
								<p>We do not guarantee that our Service will be secure or free from bugs or viruses.</p>
								<p>You are responsible for configuring your information technology, computer programmes and platform in order to access our Service. You should use your own virus protection software.</p>
								<p>You must not misuse our Service by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorised access to our Service, the server on which our Service is stored or any server, computer or database connected to our Service. You must not attack our Service via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will cooperate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our Service will cease immediately.</p>
								<p><strong>Linking to our Service</strong></p>
								<p>You may link to the home page of our Service, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it.</p>
								<p>You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</p>
								<p>You must not establish a link to our Service in any website that is not owned by you.</p>
								<p>We reserve the right to withdraw linking permission without notice.</p>
								<p>If you wish to make any use of content on our Service other than that set out above, please contact info@devo.co.uk</p>
								<p><strong>Third party links and resources in our Service</strong></p>
								<p>Where our Service contains links to other sites and resources provided by third parties, these links are provided for your information only.</p>
								<p>We have no control over the content of those sites or resources.</p>
								<p><strong>Applicable law</strong></p>
								<p>If you are a consumer, please note that these terms of use, their subject matter and their formation, are governed by English law. You and we both agree that the courts of England and Wales will have non-exclusive jurisdiction. However, if you are a resident of Northern Ireland you may also bring proceedings in Northern Ireland, and if you are resident of Scotland, you may also bring proceedings in Scotland.</p>
								<p>If you are a business, these terms of use, their subject matter and their formation (and any non-contractual disputes or claims) are governed by English law. We both agree to the exclusive jurisdiction of the courts of England and Wales.</p>
								<p><strong>Contact us</strong></p>
								<p>To contact us, please email info@devo.co.uk</p>
								<p>Thank you for visiting our Service.</p>
								<p id="acceptable-policy"><strong>Acceptable Use Policy</strong></p>
								<p>This acceptable use policy sets out the terms between you and us under which you may access the website URLs www.devo.co.uk including the iOS and android mobile application versions (our Service). This acceptable use policy applies to all users of, and visitors to, our Service.</p>
								<p>Your use of our Service means that you accept, and agree to abide by, all the policies in this acceptable use policy, which supplement our terms of 
									<a href="http://www.devo.co.uk/terms-conditions">website use</a>.
								</p>
								<p>Our Service is operated by Devo, a trading name of Devo Technologies Ltd  (we or us). Devo Technologies Ltd  is a company a company incorporated in England and Wales under company number 9996712 whose registered office is at 20-22 Wenlock Road, London N1 7GU.</p>
								
								<p><strong>Prohibited uses</strong></p>
								<p>You may use our Service only for lawful purposes. You may not use our Service:</p>
								<ul>
									<li>In any way that breaches any applicable local, national or international law or regulation.</li>
									<li>In any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect.</li>
									<li>For the purpose of harming or attempting to harm minors in any way.</li>
									<li>To upload, download, use or reuse any material for which you have not been given express written permission from us to do so.</li>
									<li>To transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar solicitation (spam).</li>
									<li>To knowingly transmit any data, send or upload any material that contains viruses, Trojan horses, worms, time-bombs, keystroke loggers, spyware, adware or any other harmful programs or similar computer code designed to adversely affect the operation of any computer software or hardware.</li>
								</ul>
									<p>You also agree:</p>
								<ul>
									<li>Not to reproduce, duplicate, copy or resell any part of our Service in contravention of the provisions of our terms of 
										<a href="http://www.devo.co.uk/terms-conditions">website use</a>.
									</li>
									<li>Not to access without authority, interfere with, damage or disrupt:</li>
									<li>any part of our Service;</li>
									<li>any equipment or network on which our Service is stored;</li>
									<li>any software used in the provision of our Service; or</li>
									<li>any equipment or network or software owned or used by any third party.</li>
								</ul>
								<p><strong>Suspension and termination</strong></p>
								<p>We will determine, at our discretion, whether there has been a breach of this acceptable use policy through your use of our Service. When a breach of this policy has occurred, we may take such action as we deem appropriate.</p>
								<p>Failure to comply with this acceptable use policy constitutes a material breach of the 
									<a href="http://www.devo.co.uk/terms-conditions">terms of use </a>upon which you are permitted to use our Service, and may result in our taking all or any of the following actions:
								</p>
								<ul>
									<li>Immediate, temporary or permanent withdrawal of your right to use our Service.</li>
									<li>Issue of a warning to you.</li>
									<li>Suspension or termination of your subscription to our Service.</li>
									<li>Legal proceedings against you for reimbursement of all costs on an indemnity basis (including, but not limited to, reasonable administrative and legal costs) resulting from the breach.</li>
									<li>Further legal action against you.</li>
									<li>Disclosure of such information to law enforcement authorities as we reasonably feel is necessary.</li>
								</ul>
								<p>We exclude liability for actions taken in response to breaches of this acceptable use policy. The responses described in this policy are not limited, and we may take any other action we reasonably deem appropriate.</p>
								<p><strong>Changes to this acceptable use policy</strong></p>
								<p>We may revise this acceptable use policy at any time by amending this page. You are expected to check this page from time to time to take notice of any changes we make, as they are legally binding on you. Some of the provisions contained in this acceptable use policy may also be superseded by provisions or notices published elsewhere on our Service.</p>
								<p id="terms-consumers"><strong>Terms and Conditions for Consumers</strong></p>
								<p>Devo is a digital marketplace where consumers (you) can purchase products (Products) directly from selected sellers (Sellers) through the website URLs 
									<a href="http://www.devo.co.uk">www.devo.co.uk</a> including the iOS and android mobile application versions (our Service). It is important to note that Devo is not part of that transaction.
								</p>
								<ol>
									<li>you are not purchasing any Products or services from Devo but directly from Sellers (including the delivery of those Products to you);</li>
									<li>Devo does not offer any guarantee or warranty on the Products, their description on our Service or their delivery;</li>
									<li>any order you place through our Service will only become binding on the relevant Seller(s) when accepted by them; and</li>
									<li>you agree to be bound by these terms and conditions, (together with our 
										<a href="http://www.devo.co.uk/privacy">Privacy Policy</a>, 
										<a href="http://www.devo.co.uk/terms-conditions">Terms of Website Use </a>and 
										<a href="https://www.devo.co.uk/terms-conditions">Acceptable Use Policy</a>) (Terms) when you purchase Products using our Service.
									</li>
								</ol>
								<p>Please note that these Terms may be revised from time to time. You will be subject to the Terms in force at the time of your registration to use Our Service and at the time of any order made thereafter.</p>
								<p>If you do not accept these Terms you should not register to use our Service or order any Products from it.</p>
								<p>You may wish to print and save a copy of these Terms for your future reference.</p>
								<ol>
									<li>
									<p><strong>These Terms</strong></p>
									</li>
								</ol>
								<ol>
									<li>
									<ol class="custom_ol_alpha">
										<li>
										<p><strong>What these Terms cover:</strong> These are the terms and conditions on which we provide a digital marketplace for you to purchase Products directly from Sellers. Once you register to use our Service you will be able to purchase Products directly from Sellers and Sellers shall be able to accept your order and receive your payment for Products using a digital wallet. We engage a network of third party drivers (Couriers) and the Seller(s) will instruct a Courier(s) to deliver your order to you. We will manage your order on the Seller's behalf but at no time shall we be responsible for fulfilling your order or for any issue that you have with the Products once delivered, this responsibility shall remain the Seller's at all times</p>
										</li>
										<li>
										<p><strong>Why you should read them:</strong> Please read these Terms carefully before you register to use our Service and before you submit any order for Products on our Service. These Terms tell you who we are, how you can purchase Products via our Service, what to do if there is a problem and other important information. If you think that there is a mistake in these Terms, please contact us to discuss.</p>
										</li>
										<li>
										<p><strong>Who we are:</strong> We are Devo Technologies Ltd  (trading as Devo), a company registered in England and Wales under company number 9996712 (we or us). Our registered office is at 20-22 Wenlock Road, London N1 7GU. Our registered VAT number is 244 7980 73.</p>
										</li>
										<li>
										<p><strong>How to contact us</strong> You can contact us by writing to us by email to info@devo.co.uk or by post to Devo, 20-22 Wenlock Road, London N1 7GU.</p>
										</li>
										<li>
										<p><strong>How we may contact you:</strong> If we have to contact you we will do so by telephone or by writing to you using the telephone number or the email address or postal address you provided to us when you registered to use our Service (or have updated in your registration details on our Service since) or submitted an order, or if you have contacted us in accordance with clause 1.4 above and asked for a reply.</p>
										</li>
										<li>
										<p><strong>"Writing" includes emails</strong>: When we use the words "writing" or "written" in these Terms, this includes emails.</p>
										</li>
										<li>
										<p><strong>Consumers only:</strong> Our Service is only intended for use by consumers (that means people who want to buy Products for their personal use and not for any business purposes). If you are a business we reserve the right to cancel your registration to our Service without recourse and/or any orders you place through our Service.</p>
										</li>
										<li>
										<p><strong>Age restrictions:</strong> We are committed to upholding our legal and social obligations in terms of ensuring that we comply with age-related restrictions on the supply of Products sold through our Service and to prevent fraud and money laundering. We may therefore ask you to confirm your age and you may be asked to provide photo identification (such as a passport or driving licence) to confirm your identity and/or age. This is in addition to any identification that you may be asked to produce on the delivery of your order by Couriers. Please note that it is your responsibility to ensure that you are legally entitled to place your order on our Service. Devo accepts no responsibility for any fraudulent use of identification or payment details in registering to use our Service or in purchasing Products from it and no responsibility for checking your identification, confirming that you are legally entitled to purchase the Products or checking that you are not purchasing Products for anyone under the age of 18.</p>
										</li>
										<li>
										<p>Confirmation of personal status By registering to use our Service, you are confirming to us that you are a consumer and that you are at least 18 years old.</p>
										</li>
										<li>
										<p>Registering on our Service. In order to place an order for Products on our Service you must register to use our Service. Please see our 
										<a href="http://www.devo.co.uk/privacy">Privacy Policy</a> for further details of how we will process your information when you register to use our Service and place orders through it.</p>
										</li>
									</ol>
									</li>
									<li>
									<p><strong>Your contract with Sellers</strong></p>

									<ol class="custom_ol_alpha">
										<li>Each time you place an order on our Service, you are offering to purchase the Product(s) for the amount displayed on our Service from the Seller(s) including delivery of the Product(s) for the amount displayed on our Service. Your payment shall be processed via a digital wallet on our Service (which is operated by Stripe, see clause 4.3 for more details) and we will provide confirmation of receipt of payment for your order by email. Please note that this confirmation of receipt is not our acceptance of your order or confirmation that we have any implied or express contract with you for the supply of the Product(s) to you. If the Seller(s) accept(s) your order then a contract will be created between you and the Seller(s) in respect of that order.</li>
																			<li>Details of the Seller(s) will be made available to you on our Service before checkout, however you must not contact the Seller(s) directly. If you have a question about any Products, an order you are yet to place or an order that you have placed then please contact us (see clause 1.4).</li>
																			<li>If the Seller(s) is/are unable to accept your order (in part or in full) we will inform you of this on their behalf by email or by telephone and you will not be charged for the relevant part of your order. This might be because a Product is out of stock, because it cannot be delivered to your specified address either within the delivery deadline specified or at all, because of unexpected limits on the Seller's resources, because the Seller has identified an error in the price or description of the Product or because there has been a problem in the processing of your payment.</li>
																			<li>
																				<strong>Your order number:</strong> We will assign an order number to each order you place via our Service. Please refer to this number when contacting us. Please note that the assignment of an order number is not an indication that the order has been accepted by the Seller(s).
																			</li>
																			<li>We only operate in England. Our Service is solely for the use of consumers in England. We reserve the right to refuse requests for registration to our Service or delete such registrations from addresses outside England.</li>
								</ol>
								</li>
								<li>
								<p><strong>The Products</strong></p>

								<ol class="custom_ol_alpha">
									<li>
										<strong>Product images:</strong> The images of Products on our service are for illustrative purposes only. Devo does not accept any responsibility where the packaging of Products may vary from that shown in images on our Service.
									</li>
									<li>
										<strong>Product information:</strong> The Product information contained on our Service has been published in good faith and we will do our best to ensure that it is accurate. However, it may occasionally be incorrect, incomplete or out of date due to human error, circumstances beyond our control or where incorrect information has been provided to us. We accept no responsibility for any losses that you incur either directly or indirectly as a result of such inaccuracies.
									</li>
									<li>
										<strong>Delivery of Products</strong>: Delivery of Products is solely the responsibility of the Seller(s) including their engagement of Couriers to deliver your order to you.
									</li>
								</ol>
								</li>
								<li>
								<p><strong>Price and payment</strong></p>

								<ol class="custom_ol_alpha">
									<li>
										<strong>Where to find the price for the Products:</strong> The price of Products (which includes VAT) will be the price indicated on the order summary page when you place your order on our Service. We take all reasonable care to ensure that these prices are correct. If they are incorrect then we will contact you to resolve this with you.
									</li>
									<li>
										<strong>Delivery costs: </strong>The costs of delivery will be notified to you on our Service before you place your order. The Seller(s) shall remain(s) solely responsible at all times for the delivery of Products to you.
									</li>
									<li>
										<strong>How your payment is processed:</strong> All payments made on our Service are processed using a digital wallet operated by Stripe (a trading name of Stripe Inc). In order to process payment for your order and to prevent fraudulent or money laundering activities, Stripe may perform identity verification and/or fraud analysis or any other check in accordance with accepted industry best practice and regulatory compliance. Stripe may also impose transaction limits on consumers or Sellers and/or suspend access to the digital wallet either for payments or withdrawals if it deems there to be a security risk in any transactions.
									</li>
									<li>
										<strong>What happens to your payment</strong>: When you submit an order on our Service and provide payment via the digital wallet, your payment will be processed by Stripe. The Seller(s) will be able to access payments accepted by Stripe once your order has been delivered, subject to the deduction of any fees or charges of Stripe, delivery costs and our fees. At no point will your payment details (ie your bank or credit card details) be accessible to Sellers or provided to them. For more information about what data is shared with Stripe and Seller(s) by us please see our 
										<a href="http://www.devo.co.uk/privacy">Privacy Policy</a>.
									</li>
									<li>
										<strong>When you must pay and how you must pay:</strong> Payments via the digital wallet on our Service can be made using the following methods of payment: Visa, MasterCard, American Express, Discover, JCB, and Diners Club cards CB. Sellers are under no obligation to accept orders without payment and may not arrange for your order to be delivered until full payment for your order has been paid via the digital wallet.
									</li>
									</ol>
								</li>
									<li>
									<p><strong>Your rights to make changes</strong></p>

									<ol class="custom_ol_alpha">
										<li>If you wish to make a change to your order or cancel it (for whatever reason) once it has been placed please contact us and we will deal with your query on behalf of the Seller(s).</li>
										<li>If the Seller(s) agrees to your request then we will facilitate processing further payment from you or issuing a partial or full refund to you through the digital wallet. We can make no guarantee in respect of any refund as they are processed by Stripe via the digital wallet.</li>
									</ol>
									</li>
									<li>
									<p><strong>Our rights to make changes</strong></p>

									<ol class="custom_ol_alpha">
										<li>
										<strong>Minor changes to the Products:</strong> We may change the Products available to purchase on our Service:

										<ol>
											<li>to reflect changes in relevant laws and regulatory requirements;</li>
											<li>to reflect changes to the Product branding or information provided by the manufacturer or licensors of their intellectual property rights;</li>													<li>to implement minor technical adjustments and improvements, for example to address a security threat; or</li>													<li>in response to a product recall.</li>
										</ol>
										</li>
										<li>We reserve the right to make changes to our Service and to suspend or terminate use of our Service in accordance with our 
											<a href="http://www.devo.co.uk/terms-conditions">Website Terms and Conditions</a>.
										</li>
									</ol>
									</li>
									<li>
										<strong>If there is a problem with a Product</strong>
									</li>
								</ol>
								<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If you have any questions or complaints about a Product, please contact us and we will deal with your query on behalf of the Seller(s).</p>
								<ol class="custom_strong">
									<li>
										<strong>Our liability to you</strong>
										<ol class="custom_strong custom_ol_alpha">
											<li>
												<strong>We do not exclude or limit in any way our liability to you where it would be unlawful to do so.</strong> This includes liability for death or personal injury caused by our negligence; for fraud or fraudulent misrepresentation and for breach of your legal rights in relation to the Products to the extent that we are responsible for those Products under these Terms.
											</li>
											<li>
												<strong>We are not liable for business losses</strong>: Our Service is intended for domestic and private use only. We will have no liability to you for any loss of profit, loss of goodwill, loss of business, business interruption or loss of business opportunity howsoever caused.
											</li>
											<li>
												<strong>We do not give any guarantee about the Products</strong>. We are not responsible for the condition, quality or suitability of the Products or for their delivery to you.
											</li>
											<li>
												<strong>Limit on our liability to you.</strong> If we are found to be liable to you for any losses you have sustained then our total aggregate liability to you shall be limited to the price of your relevant order.
											</li>
										</ol>
									</li>
									<li>
										<strong>How we may use your personal information</strong>
										<ol class="custom_ol_alpha">
											<li>
												<strong>How we will use your personal information We will use the personal information you provide to us:</strong>
									
												<ol>
													<li>to process your payment for the Products to the extent that this is done via the digital wallet;</li>
													<li>to engage with the Seller(s) (and other third parties engaged by them such as Couriers) in relation to your order; and</li>
													<li>for the uses explained in our 
														<a href="http://www.devo.co.uk/privacy">Privacy Policy</a>.
													</li>
												</ol>
											</li>
											<li>
												<strong>We will only give your personal information to other third parties where the law either requires or allows us to do so.</strong>
											</li>
										</ol>
									</li>
								</ol>
								<ol class="custom_strong">
									
								</ol>
								<ol start="3" class="custom_strong">
									<li>
										<strong>Other important terms</strong>
										<ol class="custom_strong custom_ol_alpha">
											<li>
												<strong>We may transfer these Terms to someone else</strong>. We may transfer our rights and obligations under these Terms to another organisation. If you have registered to use our Service we will contact you to let you know if we plan to do this.
											</li>
											<li>
												<strong>You need our consent to transfer your rights to someone else</strong>. You may only transfer your rights or your obligations under these Terms to another person if we agree to this in writing.
											</li>
											<li>
												<strong>Nobody else has any rights under these Terms</strong>. These Terms are between you and us. No other person shall have any rights to enforce any of these Terms, except as explained in these Terms. We will not need to get the agreement of any other person to make any changes to these Terms.
											</li>
											<li>
												<strong>If a court finds part of these Terms illegal, the rest will continue in force</strong>. Each of the paragraphs of these Terms operates separately. If any court or relevant authority decides that any of them are unlawful, the remaining paragraphs will remain in full force and effect.
											</li>
											<li>
												<strong>Even if we delay in enforcing any of these Terms, we can still enforce them later</strong>. If we do not insist immediately that you do anything you are required to do under these Terms, or if we delay in taking steps against you in respect of your breaking these Terms, that will not mean that you do not have to do those things and it will not prevent us taking steps against you at a later date.
											</li>
											<li>
												<strong>Which laws apply to these Terms and where you may bring legal proceedings.</strong> These Terms are governed by English law and you can bring legal proceedings in respect of these Terms in the English courts. If you live in Scotland you can bring legal proceedings in respect of these Terms in either the Scottish or the English courts. If you live in Northern Ireland you can bring legal proceedings in respect of these Terms in either the Northern Irish or the English courts.
											</li>
										</ol>
									</li>
								</ol>
							</div>
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>
		</article>				
	</main>
</div>
<?php
get_footer();
?>