<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// ...Call the database connection settings
// require( "../../../../wp-config.php");

/* $servername = DB_HOST;
$username = DB_USER;
$password = DB_PASSWORD;
$dbname = DB_NAME; */
$servername = "localhost";
$username = "devoco_main_devo";
$password = "vXSR(*4gQ*+E";
$dbname = "devoco_temp_clone";


function createConnection(){
    // Create connection
    global $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    return $conn;

}

function destroyConnection($conn){
    $conn->close();
}

$conn = createConnection();

$action = (isset($_GET['action']))?$_GET['action']:'';
$query = (isset($_GET['query']))?$_GET['query']:'';
$shopid = (isset($_GET['shopid']))?$_GET['shopid']:'';
$aid = (isset($_GET['aid']))?$_GET['aid']:'';
$postcode = (isset($_GET['postcode']))?$_GET['postcode']:'';
$cid = (isset($_GET['cid']))?$_GET['cid']:'';
$cpage = (isset($_GET['cpage']))?$_GET['cpage']:1;
if($action=='shopProductSearchQuick' and $query!='' && !empty($shopid) && !empty($aid)){
    echo searchProductWithCategoryQuick( $conn , $query , $shopid , $aid , $postcode , $cid , $cpage );die;
}
if($action=='modglobalsearchnewquick' and $query!='' && !empty($postcode)){
    echo mod_global_search_new_quick($conn, $query,$postcode);die;
}
destroyConnection($conn);


function searchProductWithCategoryQuick($conn, $query,$shopid,$aid,$postcode,$cid,$page=1){
    $query=urldecode($query);
	$query = str_replace("’", "'", $query);
    $sql = "
        SELECT
            ID, post_title , (SELECT result.guid AS featuredImage
                FROM wp_posts AS p 
                INNER JOIN wp_postmeta AS pm ON p.ID=pm.post_id
                INNER JOIN wp_posts as result ON pm.meta_value = result.ID
                WHERE pm.meta_key = '_thumbnail_id' and p.ID = wp_posts.ID) as featuredImage
        FROM 
            wp_posts
        WHERE 
            wp_posts.post_author IN ($aid) 
            AND wp_posts.post_title LIKE '%$query%'
            AND wp_posts.post_type = 'product'
            AND ((wp_posts.post_status = 'publish'))
            GROUP BY wp_posts.ID
            ORDER BY wp_posts.post_title ASC LIMIT 0, 10
        ";
    $result = $conn->query($sql);
    
    $productData = array();
    if ($result->num_rows > 0) {
        
        $data = $result->fetch_all (MYSQLI_ASSOC );
        foreach($data as $product){
            $product_id = $product['ID'];
            $productTitle = $product['post_title'];
            $productImage = $product['featuredImage'];
            $productData[] = array(
                'Title' => $productTitle,
                'Id' => $product_id,
                'Image' => $productImage,
            );
        }
    }
    
    $products=json_encode(array('products'=>$productData));
	return $products;
}

function mod_global_search_new_quick($conn, $query,$postcode){
    $search_code = strtolower($postcode);
    $postcode_shops_author = get_shops_by_postcode_array($conn, $search_code);
    if(empty($postcode_shops_author)){
        $aa = array('products'=>array());
        $mnb = json_encode($aa);
        return $mnb;
    }
    $allAuthors = implode(',',$postcode_shops_author);
    $sql = "
        SELECT
            ID, post_title , (SELECT result.guid AS featuredImage
                FROM wp_posts AS p 
                INNER JOIN wp_postmeta AS pm ON p.ID=pm.post_id
                INNER JOIN wp_posts as result ON pm.meta_value = result.ID
                WHERE pm.meta_key = '_thumbnail_id' and p.ID = wp_posts.ID) as featuredImage
        FROM 
            wp_posts
        WHERE 
            wp_posts.post_author IN ($allAuthors) 
            AND wp_posts.post_title LIKE '%$query%'
            AND wp_posts.post_type = 'product'
            AND ((wp_posts.post_status = 'publish'))
            GROUP BY wp_posts.ID
            ORDER BY wp_posts.post_title ASC LIMIT 0, 10
        ";
        
    $result = $conn->query($sql);
    $productData = array();
    if ($result->num_rows > 0) {
        
        $data = $result->fetch_all (MYSQLI_ASSOC );
        foreach($data as $product){
            $product_id = $product['ID'];
            $productTitle = utf8_encode($product['post_title']);
            $productImage = $product['featuredImage'];
            $productData[] = array(
                'Title' => $productTitle,
                'Id' => $product_id,
                'Image' => $productImage,
            );
        }
    }
    $products=json_encode(array('products'=>$productData));
    
	return $products;
}

function get_shops_by_postcode_array($conn, $postcode){
	$postcode = "%$postcode%";
    $sql = "
        SELECT wp_posts.ID, wp_posts.post_author
        FROM wp_posts
        INNER JOIN wp_postmeta 
            AS pm 
            ON wp_posts.ID = pm.post_id
        WHERE 1=1
            AND (wp_posts.post_status = 'publish' ) 
            AND (wp_posts.post_type = 'azl_profile' ) 
            AND pm.meta_key = 'profile_delivery_data'
            AND pm.meta_value LIKE '$postcode'
        ORDER BY wp_posts.ID = 1044 desc
    ";
    $result = $conn->query($sql);
    $shopData = array();
    if ($result->num_rows > 0) {
        
        $data = $result->fetch_all (MYSQLI_ASSOC ); 
        $shopData = array_column($data, 'ID');
        $AuthorData = array_column($data, 'post_author');
        return $AuthorData;
    }
    return $shopData;
}