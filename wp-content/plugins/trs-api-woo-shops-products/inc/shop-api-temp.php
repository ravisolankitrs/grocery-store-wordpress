<?php
/* 
 ini_set('display_errors', 1);

ini_set('display_startup_errors', 1);

error_reporting(E_ALL);  */
global $wpdb, $siteurl;

/* include all support Files */
include(TRSWOO_SHOP_API_INC.'products_functions.php');
include(TRSWOO_SHOP_API_INC.'orders_functions.php');
include(TRSWOO_SHOP_API_INC.'user_functions.php');


/** Loads the WordPress Environment and Template */
require ('./wp-blog-header.php');

$json = file_get_contents('php://input');
$obj = json_decode($json, true);

/* if(!empty($obj["card_details"])) {

	echo add_app_card_details($obj);
	die;	
} */
/* if(!empty($obj["app_card_delete"])) {
	echo add_app_card_delete($obj);
	die;	
} */
if(!empty($obj["address_details"])) {
	echo user_update_address_details($obj); //Working
	die;	
}
if(!empty($obj["address_delete"])) {
	echo user_update_address_delete($obj);  //Working
	die;	
}
if(!empty($obj["login"])) {
	echo make_user_login($obj);die;	        //Working
}
if(!empty($obj["signup"])){
	echo make_user_register($obj);die;      //Working
}
if(!empty($obj["action"]) && $obj["action"] == 'apply_coupon' && !empty($obj["payload"])){
	echo apply_coupon_in_order($obj);die;          //Pending
}
if(!empty($obj["action"]) && $obj["action"] == 'store_fcm_token' && !empty($obj["payload"])){
	echo check_and_store_token($obj);die;          //Pending
}
if(!empty($obj["product"]) && !empty($obj["address"])){
	echo createNewOrder($obj);die;          //Pending
}
if(!empty($obj["resetpassword"])) {
	echo make_user_reset_password($obj);die;//UnTested	
}
if(!empty($obj["updateuser"])) {
	echo update_user_details($obj);die;     //Working
}

if(!empty($obj["add_stripe_customer"])) {
	echo add_stripe_customer($obj["add_stripe_customer"]);die;  //UnTested
}

if(!empty($obj["help_mail"])){
	echo mail_devo_help_dask($obj);die;  //UnTested
}

if(!empty($obj["addvendor"]) && !empty($obj["key"])){
	$response='';
	$key = $obj["key"];
	if($key==="SVKTWAOoicz8Is5rTjTPmogGiKiyrbd9"){
		$vendor_id = $obj["addvendor"]['vendor_id'];
		$product_id = $obj["addvendor"]['product_id'];
		
		
		$user = get_userdata( $vendor_id );
		if ( !empty($user) ) {
			$arg = array(
				'ID' => $product_id,
				'post_author' => $vendor_id,
			);
			wp_update_post( $arg );
		}
		$response = array(
			'response'=>array(
				'status'	=>'200',
				'message'	=>'Success',
			),
		);
	}else{
		$response = array(
			'response'=>array(
				'status'	=>'100',
				'message'	=>'Authentication Failed',
			),
		);
	}
	echo json_encode($response);exit;
	
}

/* Following code is for get queries calls */



$gett = (isset($_GET['q']))?$_GET['q']:'';

$query = (isset($_GET['query']))?$_GET['query']:'';

$oid = (isset($_GET['oid']))?$_GET['oid']:'';

$action = (isset($_GET['action']))?$_GET['action']:'';

$uid = (isset($_GET['uid']))?$_GET['uid']:'';
//$uid = (isset($_GET['status']))?$_GET['status']:'';

$status = (isset($_GET['status']))?$_GET['status']:'';

$shopid = (isset($_GET['shopid']))?$_GET['shopid']:'';

$cid = (isset($_GET['cid']))?$_GET['cid']:'';

$cslug = (isset($_GET['cslug']))?$_GET['cslug']:'';

$pid = (isset($_GET['pid']))?$_GET['pid']:'';

$sid = (isset($_GET['sid']))?$_GET['sid']:'';

$aid = (isset($_GET['aid']))?$_GET['aid']:'';

$scid = (isset($_GET['scid']))?$_GET['scid']:'';

$postcode = (isset($_GET['postcode']))?$_GET['postcode']:'';

$lat = (isset($_GET['lat']))?$_GET['lat']:'';
$long = (isset($_GET['long']))?$_GET['long']:'';

$code = (isset($_GET['code']))?$_GET['code']:'';

$cpage = (isset($_GET['cpage']))?$_GET['cpage']:1;
$txn = (isset($_GET['txn']))?$_GET['txn']:'';

try {
	if($gett == 'get-customer-cards' && !empty($query)){
        echo getUserCards($query);
		die;
    }
    
	if($gett == 'bypostcode' && !empty($query)) {
		echo get_vendors_by_postcode($query);
		die;
	}
	if($gett == 'bygeocode' && !empty($lat) && !empty($long)) {
		echo get_vendors_by_geocode($lat,$long);
		die;
	}
	if($gett == 'allcategories'){
		echo get_all_vendors();
		die;
	}
	
	if($gett == 'products'){     //Working
		if(empty($action) && !empty($uid)){
			echo get_products_by_handler($uid);die;
		}
		if($action=='get_product_by_cat' && $cid!='' && $sid!='' && !empty($postcode)){
			echo get_product_by_cat($cid,$sid,$postcode,$cpage);die;
		}
		if($action=='get_product_by_shop_and_cat' && !empty($shopid) && !empty($sid)  && $cid!='' && !empty($postcode)){
			echo get_product_by_shop_and_cat($shopid,$sid,$cid,$postcode,$cpage);die;
		}
		if($action=='get_product_by_shop_and_subcat' && $cid!='' && !empty($sid) && $scid!='' && !empty($shopid)){
			echo get_product_by_shop_and_subcat($cid,$shopid,$sid,$scid,$postcode,$cpage);die;
		}
		if($action=='modglobalsearch' and $query!='' && !empty($postcode)){
			echo mod_global_search($query,$postcode);die;
		}
        if($action=='modglobalsearchnew' and $query!='' && !empty($postcode)){
			echo mod_global_search_new($query,$postcode);die;
		}
		if($action=='shopProductSearch' and $query!='' && !empty($shopid) && !empty($aid)){
			echo searchProductWithCategory($query,$shopid,$aid,$postcode,$cid,$cpage);die;
		}
		if($action=='shopProductSearchQuick' and $query!='' && !empty($shopid) && !empty($aid)){
			echo searchProductWithCategoryQuick($query,$shopid,$aid,$postcode,$cid,$cpage);die;
		}
		if($action=='modglobalsearchnewquick' and $query!='' && !empty($postcode)){
			echo mod_global_search_new_quick($query,$postcode);die;
		}
	}

	if($gett == 'promocode' && !empty($code) && !empty($uid)){
		
		echo check_promocode($code,$uid);die;
		
	}
	
	
	if($gett == 'orders'){
		if($action == 'getuserorders' && !empty($uid))
		{
			echo getuserorders($uid);die;
		}
		
		if($action == 'orderdetail' && !empty($oid)){
			
			echo orderdetails($oid);
			
			die;
			
		}
		if($action == 'updateorderstatus' && !empty($oid) && !empty($status) && !empty($txn)){
			
			echo update_order_status($oid,$status,$txn,$query);			
			die;			
		}
		#### TRS MMS
		
		if($action == 'orderdeliverystatus' && !empty($uid) ){
			
			//echo"helloooo";
			
			echo orderdeliverystatus($uid);	
			//echo orderdeliverystatus($status);	
			
			die;			
		}
		
		if($action == 'notificationstatus' && !empty($uid) && !empty($status) ){
			
			//echo"helloooo";
			
			echo notificationstatus($uid,$status);	
			//echo orderdeliverystatus($status);	
			
			die;			
		}
	}
	
	if($gett == 'test'){
		if($action == 'getstripecards' && !empty($query))
		{
			echo get_stripe_cards_for_user($query);die;
		}
	}
	
	
	if($gett == 'cmt')
	{
		if(!empty($obj['comment']['pid'])){
		
			$obj = json_decode($json, true); 
			//print_R($obj);die;	
			$cmt_user_id = get_userdata($obj['comment']['uid']);
			$commentdata = array(
					'comment_post_ID' => $obj['comment']['pid'], 
					'comment_author' => $obj['comment']['name'], 
					'comment_author_email' =>$cmt_user_id->data->user_email,
					'comment_content' => $obj['comment']['content'], 
					'comment_date' => date(), 
					'comment_parent' => 0, 
					'user_id' => $obj['comment']['uid'], 
				);
			$comment_id = wp_new_comment( $commentdata );
					
			if($comment_id){
				$aa = array('status'=>'1'); 
				$mnb = json_encode($aa);
				echo $mnb;
			} else{
				$aa = array('status'=>'0');
				$mnb = json_encode($aa);
				echo $mnb;
			}
		}
	}

} catch ( Exception $e ) {
	echo $e->getMessage() . PHP_EOL;

	echo $e->getCode() . PHP_EOL;
}
?>