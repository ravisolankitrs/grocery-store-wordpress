<?php
$path = preg_replace('/wp-content.*$/','',__DIR__);
include($path.'wp-load.php');
global $woocommerce;
global $wpdb;
$user_id = get_current_user_id();

if( isset($_POST['gen_data']) ) {

	$id = wp_update_user(array('ID'=>$user_id,'first_name'=>$_POST['f_name'],'last_name'=>$_POST['l_name'],'user_email'=>$_POST['e_mail']));
	if(isset($_POST['p_nume']))
		update_user_meta($user_id, 'phone', $_POST['p_nume']);
	
	if(!empty($id)){ echo "1"; }else{ echo "0"; }
}

if( isset($_POST['pwd_data']) ) {
	$user=wp_get_current_user();
	$current_password=$_POST['curpwd_data'];
	$new_pwd=$_POST['password'];
	$auth=wp_authenticate($user->user_login, $current_password);
	if(!is_wp_error($auth)){
		wp_set_password( $new_pwd, $user_id );
		echo 1;
	}else{
		echo "Invalid Current Password";
	}
	die;
}

if( isset($_POST['add_adrs_data']) ) {
	
	$user_id = get_current_user_id();
	
	$meta_key = "checkout_saved_address";
	$addresses = get_user_meta($user_id, $meta_key,true);
	
	$newaddress= array(
		'address_1' => $_POST['add_1'],
		'address_2' => $_POST['add_2'],
		'phone' =>  $_POST['p_num'],
		'postcode' => $_POST['p_cde'],
		'rider' => '',
		'user_id' => $user_id,
		'default' => false,
	);
	$uniqueid = uniqid();
	$address_name= 'checkout_add_'.$uniqueid;
	
	if(!empty($addresses) && count($addresses) > 0){
		$addresses[$address_name] = $newaddress;
		update_user_meta($user_id, $meta_key,$addresses);
	}else{
		$newadd = array($address_name => $newaddress);
		update_user_meta($user_id, $meta_key,$newadd); 
	}

	if(!empty($user_id)){ echo "1"; }else{ echo "0"; }
}

if( isset($_POST['adrs_data']) ) { 

	$user_id = get_current_user_id();
	
	$meta_key = "checkout_saved_address";
	$addresses = get_user_meta($user_id, $meta_key,true);
	
	$newaddress= array(
		'address_1' => $_POST['adrs_1'],
		'address_2' => $_POST['adrs_2'],
		'phone' =>  $_POST['p_numb'],
		'postcode' => $_POST['p_code'],
		'rider' => '',
		'user_id' => $user_id,
		'default' => false,
	);
	$uniqueid = uniqid();
	$address_name= 'checkout_add_'.$uniqueid;
	
	if(!empty($addresses) && count($addresses) > 0){
		$addresses[$address_name] = $newaddress;
		update_user_meta($user_id, $meta_key,$addresses);
	}else{
		$newadd = array($address_name => $newaddress);
		update_user_meta($user_id, $meta_key,$newadd); 
	}
	
	if(!empty($id)){ echo "1"; }else{ echo "0"; }
}

if( isset($_POST['u_card_data']) ) {
	
	$stripe_cus_id = get_user_meta( get_current_user_id(), '_stripe_customer_id', true);
	
	$stripe_token = $_POST['stripe_token'];
	
	if(!empty($stripe_cus_id)){
		$stripe = new WC_Gateway_Stripe();
		$response = $stripe->add_card( $stripe_cus_id, $stripe_token);
        
		if ( is_wp_error( $response ) ) {
            $data = (array)$response;
            $error_string = $response->get_error_message();
            $data['message'] = $error_string;
            echo json_encode($data);die;
        }
		echo "1";die;
	}
	die;
}
if( isset($_POST['delete_card']) ) {

	delete_user_meta($user_id, 'card_number_1');
	delete_user_meta($user_id, 'expire_date_1');
	delete_user_meta($user_id, 'cvv_numb_1');
	delete_user_meta($user_id, 'bil_pstcde_1');
	
	if(!empty($id)){ echo "1"; }else{ echo "0"; }
}
?>