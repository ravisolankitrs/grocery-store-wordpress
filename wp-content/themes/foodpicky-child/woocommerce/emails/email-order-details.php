<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
<style>

#cont-foot2 ul {
    display: inline;
    /* text-align: center; */
    list-style: none;
    padding: 0px 0;
}

#cont-foot2 ul li {
    height: 45px;
    width: 45px;
    padding: 0 1% 0 1%;
    display: inline-block;
	font-family: arial,sans-serif;
	    
}

#cont-foot2 ul li a {
    padding: 10px;
    display: inline-block;
    text-decoration: none;
}
</style>


<hr style="width: 100%; ">

<table class="td tr_width" cellspacing="0" cellpadding="6" data-test="5"  style="width: 100%;font-family: arial,sans-serif;     border: 1px solid white !important;">
	<thead >
		<tr>
			<th colspan="4" style="padding:0 !important;">
			
						<?php if ( ! $sent_to_admin ) : ?>
							<h2><span style="color:#FC2534"><center><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?><center></span></h2>
						<?php else : ?>
						     <h2><span style="color:#FC2534"><center><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?><center></span></h2>
							<!--<h2 style="text-align:center;><a class="link" href="<?php // echo esc_url( admin_url( 'post.php?post=' . $order->id . '&action=edit' ) ); ?>" style="color:#fd0e35"><?php // printf( __( 'Order #%s', 'woocommerce'), $order->get_order_number() ); ?></a> 
							(<?php// printf( '<time datetime="%s">%s</time>', date_i18n( 'c', strtotime( $order->order_date ) ), date_i18n( wc_date_format(), strtotime( $order->order_date ) ) ); ?>) </h2>-->
						<?php endif; ?>
				
			</th>
		</tr>
	</thead>     
	<tbody  style="background: ;">
					<tr>
					<td align="left" valign="top" style=" <?php echo 'padding:0px 0px !important;'; ?> color:#a2a2a2; font-family: arial,sans-serif;  "  class="textContent"><p style="font-size: 15px; margin-top: 0;  <?php echo 'padding:0px 0px !important;'; ?> "> From </p> 
					<span style="color:#1a1a1a; font-family: arial,sans-serif;  <?php echo 'padding:0px 0px !important;'; ?>  "> 
					Select&Save
					</span></td>
					
						<td align="right"  valign="top" colspan="3" class="textContent" style="padding-bottom: 35px; padding-right: 15px;">
						<?php 

							$address=(is_array($order->billing_address_1))?$order->billing_address_1['address_1']:$order->billing_address_1;
							$address2=((is_array($order->billing_address_2))?$order->billing_address_2['address_2']:$order->billing_address_2)." ".((is_array($order->billing_postcode))?$order->billing_postcode['postcode']:$order->billing_postcode);
								
							$phone = (is_array($order->billing_phone))?$order->billing_phone['address_1']:$order->billing_phone;
							
							$user_info = get_userdata($order->customer_user);
							
							$first_name = $user_info->first_name;
							//$last_name = $user_info->last_name;
 
					?>   
						<div style="color:##1a1a1a;;line-height:125%;  font-family: arial,sans-serif;    font-size: 15px;font-weight:normal;margin-top:0;margin-bottom:15px;text-align:right; ">To</div>
						
						<span style="color:#1a1a1a;;size:15px;"><?php echo $first_name  ?></span><br/>
						<span style="color:#1a1a1a; size:15px;"><?php echo $address; ?></span><br/>
						<span style="color:#1a1a1a; size:15px;"><?php echo $address2; ?></span><br/>
						<span style="font-weight:normal; size:15px;color:#1a1a1a;"><?php echo $phone; ?></span>
							
						</td>
					</tr>  
                    <br/>					
				
		<?php  echo $order->email_order_items_table( array(
			'show_sku'      => $sent_to_admin,
			'show_image'    => false,
			'image_size'    => array( 32, 32 ),
			'plain_text'    => $plain_text,
			'sent_to_admin' => $sent_to_admin 
		) ); ?>
	</tbody>
	<br/><br/>
	<tfoot border="0" id="bottom_tfoot">
		<!-- Code has been moved from here. in desktop test moved code -->
		<?php
		$custom_totalsArray = array();
			if ( $totals = $order->get_order_item_totals() ) {
				if(!empty($totals['shipping'])){
					$totals['shipping']['label'] = 'Delivery Fee:';
				}
				
				file_put_contents(__DIR__.'/tada.txt',print_r($totals,true));
				
				//$check_service = serialize($totals);
				//$new_total_service = $totals;
				
				$all_keys = serialize(array_keys($totals));
				
				/* if(stripos($check_service,'Service Fee') !== false){
					$check_service = str_ireplace("Service Fee","Service Fee(Items below £10.00):",$check_service);
					$totals = unserialize($check_service);
					if (!is_array($totals)) {
						// something went wrong, initialize to original total
						$totals = $new_total_service;
					}
				}
  */
				if(strpos($all_keys,'fee')===false){
					$totals['fee_1432'] = array(
						'label'=> 'Service Fee (Items below £10.00):',
						'value'=> '<span class="woocommerce-Price-amount amount"><span class="currency"><span class="woocommerce-Price-currencySymbol">&pound;</span></span>0<span class="decimals">.00</span></span>',
					);
				}
				if(!empty($totals['shipping'])){
					$totals['shipping']['label'] = 'Delivery Fee:';
				}
				$custom_totalsArray = $totals ;
				$i = 0;
				/* $order->get_total_shipping(); */
				foreach ( $totals as $key=>$total ) {
					$i++;
					$array_to_skip = array('order_total', 'payment_method');
					if(in_array($key,$array_to_skip )){
						continue;
					}
					if($key=='shipping'){
						file_put_contents(__DIR__.'/tada1.txt',print_r($total,true));
						if($total['value']=='Devo Delivery Fee'){
							$total['value'] = get_woocommerce_currency_symbol().number_format(00,2); 
						}else{
							$shipping_value = $total['value'];
							$total['value'] = explode('&nbsp;',$shipping_value)[0];
						}
					}
					file_put_contents(__DIR__.'/tada2.txt',print_r($total,true), FILE_APPEND);
					?>   
					<tr>
						<td scope="row" colspan="4" style="<?php echo 'padding:0px 0px !important;'; ?> text-align:left; font-weight: normal; color:#737373;font-family: arial,sans-serif;">
							<span><?php echo $total['label']; ?></span>
							<span style="text-align:right;  font-family: arial,sans-serif;  padding-right: 15px; float:right; color:#737373; padding-top:0px;">
							<?php echo $total['value']; ?></span>
						</td>
					
					</tr>
					
					<?php
					
					
				}
				
			}
		?>
		
	</tfoot>
					

</table>

<hr style="width: 100%; ">

				<table class=" tr_width " style="height:50px; width:100%; background: ; ">
					<tr  style="background: ;  " >
						<td scope="row" colspan="2" style="text-align:left;  font-family: arial,sans-serif;   font-size:23px; padding: 15px; color:#4d4d4d;">
							<?php echo str_ireplace(":","",$custom_totalsArray['order_total']['label'])." <span style='font-size: small; color: #999;'> (Including VAT): </span>"; ?></td>  
						<td style="text-align:right;  font-family: arial,sans-serif;   color:#FC2534;  font-size:23px;  padding-right: 20px;   ">
							<?php echo $custom_totalsArray['order_total']['value']; ?></td>
					</tr>
				</table>
			
			<!--<table class="tr_width" style="width:100%; height:50px;">
			  
				<tr>   
										
				   <td>
				   <span  class="left_thanks"  style="align:center; margin-left:167px">Thanks for choosing Devo !</span>
				   
				   </td>
				    
				</tr>		
   
			</table>-->
			
			<center>
			
					<span   style="align:center !important; width:100%; line-height:5;font-weight:bold;">Thanks for choosing Devo!</span>
			
			<div id="cont-foot2">
			<a href="https://itunes.apple.com/us/app/devo-local-shops-delivery/id1317731607?ls=1&mt=8">
<img class="app-button" style="height:45px;width:135px;" src="https://devo.co.uk/wp-content/uploads/2017/10/applestore.png" alt="Play Market">
</a>
			  <a style="margin-left: 20px;" href="https://play.google.com/store/apps/details?id=com.trs.devo&amp;hl=en">
                  <img class="app-button" src="https://devo.co.uk/wp-content/uploads/2016/09/play.png" alt="Play Market" >
              </a><br>
                  <ul>
                      <li style=" padding-right: 50px; ">
                          <a class="page-footer--icon icon-tw social_images_new" href="https://twitter.com/DevoDelivery" target="_blank" rel="noopener noreferrer">
                              <img src="<?php echo get_site_url().'/wp-content/uploads/2017/07/twitter-logo-button-1.png'; ?>" style=" width: 35px; float: left; margin-right: 60px; margin-left: -40px !important; padding-left: 0;  "></img>
                          </a>
                      </li>
                      <li style=" padding-right: 50px; ">
                          <a class="page-footer--icon icon-fb social_images_new" href="https://www.facebook.com/DevoDelivery/" target="_blank" rel="noopener noreferrer">
                              <img src="<?php echo get_site_url().'/wp-content/uploads/2017/07/facebook-logo-button-3.png'; ?>" style=" width: 35px; float: left; margin-right: 60px; margin-left: -15px; padding-left: 0; "></img>
                          </a>
                      </li>
                      <li style="padding-right: 50px; ">
                          <a class="page-footer--icon icon-in social_images_new" href="https://www.instagram.com/devodelivery/" target="_blank" rel="noopener noreferrer">
                              <img src="<?php echo get_site_url().'/wp-content/uploads/2017/07/instagram-logo-1.png'; ?>" style="width: 35px; float: left; margin-right: 60px;   "></img>
                          </a>
                      </li>
                  </ul>
              </div>
	
			</center>
 
<style type="text/css">

@media screen and (min-device-width: 375px)and (max-device-width:667px) {
	
			.tr_width{
				    
					width: 98% !important;
			}
			  .left_thanks{
				
				margin-left:70px !important;
	
			} 
			
			}
  
@media screen and (min-device-width: 412px)and (max-device-width: 732px) {
	
			.tr_width{
				    
					width: 98% !important;
			}
			.left_thanks{
				
	          margin-left:70px !important;
	
			}			
			
			}
			/* end IOS targeting */
			
</style>			
			