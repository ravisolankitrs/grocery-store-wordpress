<?php
/*
Plugin Name: TRS API Woo Ecommerce Shop for devo
Description: This is custom made plugin.This plugin is for ecommerce app of Devo.
Author: Team Trs
Version: 2.0.0
Author URI: http://trssoftwaresolutions.com
*/
// adding plugin menu in wordpress home
// Set the plugin prefix to be later used in code
	
if (!defined('TRSWOO_SHOP_API_PREFIX'))  
define('TRSWOO_SHOP_API_PREFIX', 'trswoo_shop_api', true);

// Set the plugin FILE Constant
if (!defined('TRSWOO_SHOP_API_FILE_CONSTANT'))  
	define('TRSWOO_SHOP_API_FILE_CONSTANT', __FILE__, true);
// Set constant path to the plugin directory to be later used by plugin.

if (!defined('TRSWOO_SHOP_API_DIR')) 
	define('TRSWOO_SHOP_API_DIR', plugin_dir_path(__FILE__));

// Set constant URI to the plugin URL to be later used for refrence.
if (!defined('TRSWOO_SHOP_API_URI'))   
	define('TRSWOO_SHOP_API_URI', plugin_dir_url(__FILE__));

// Set the path to the plugin's javascript directory in constant.
if (!defined('TRSWOO_SHOP_API_JS'))  
	define('TRSWOO_SHOP_API_JS', TRSWOO_SHOP_API_URI . trailingslashit('js'), true);

// Set the path to the plugin's CSS directory.
if (!defined('TRSWOO_SHOP_API_CSS')) 
	define('TRSWOO_SHOP_API_CSS', TRSWOO_SHOP_API_URI . trailingslashit('css'), true);

// Set the path to the plugin's includes directory.
if (!defined('TRSWOO_SHOP_API_INC'))  
	define('TRSWOO_SHOP_API_INC', TRSWOO_SHOP_API_DIR . trailingslashit('inc'), true);

// Set the path to the plugin's includes directory.
if (!defined('TRSWOO_SHOP_API_PUSH_NOTIFICATION'))  
	define('TRSWOO_SHOP_API_PUSH_NOTIFICATION', TRSWOO_SHOP_API_INC . trailingslashit('push-notification'), true);

// Set the path to the plugin's log file.
if (!defined('TRSWOO_SHOP_API_LOG_FILE'))  
	define('TRSWOO_SHOP_API_LOG_FILE', TRSWOO_SHOP_API_DIR . trailingslashit('log'), true);

if (!defined('TRSWOO_SHOP_API_LOG_FILE_EXT'))  
	define('TRSWOO_SHOP_API_LOG_FILE_EXT', '.txt', true);
	
if (!defined('TRSWOO_SHOP_API_PRODUCTS_PER_PAGE'))  
	define('TRSWOO_SHOP_API_PRODUCTS_PER_PAGE', 120, true);

// Set the MANDRILL FILE Constant
if (!defined('TRSWOO_SHOP_API_FILE_MANDRILL_API_KEY'))  
	define('TRSWOO_SHOP_API_FILE_MANDRILL_API_KEY','eunBC6Fr9DRbwc6aD_BvRQ');

// Set the MANDRILL FILE PATH Constant
if (!defined('TRSWOO_SHOP_API_MANDRILL_FILE_PATH'))  
	define('TRSWOO_SHOP_API_MANDRILL_FILE_PATH', plugin_dir_path(__FILE__).'mailchimp-mandrill/src/Mandrill.php');


if (!defined('TRSWOO_SHOP_API_STORE_URL')) { 
   define('TRSWOO_SHOP_API_STORE_URL', 'http://www.trssoftwaresolutions.com', TRUE);
   }
   
if (!defined('TRSWOO_SHOP_API_TEXT_DOMAIN'))
{    
    define('TRSWOO_SHOP_API_TEXT_DOMAIN', 'TRSWOO_DELIVERY_API');
}
   
   
/* ini_set("log_errors", 1);ini_set("error_log", TRSWOO_SHOP_API_LOG_FILE . "plugin-logs" . TRSWOO_SHOP_API_LOG_FILE_EXT); */

/* Added on 2:31 PM 13 February, 2018 by PJ */
// require_once(TRSWOO_SHOP_API_PUSH_NOTIFICATION.'notification-pusher.php');
   
   function trswoo_shop_api_activation_script() {
	   $my_page = array(
				'post_title' => 'TRS Woo SHOP API by trs new',	
				'post_content' => 'Do not change, delete or update this page. This page is made by API plugin for API to run properly',	
				'post_status' => 'publish',	
				'post_type' => 'page',	
				'post_name'  => TRSWOO_SHOP_API_PREFIX,	
				);		

				$post_id = wp_insert_post($my_page);	
				update_post_meta($post_id, 'trswoo_shop_pid', $post_id);
		}
	// run the install scripts upon plugin activation
	register_activation_hook(__FILE__,'trswoo_shop_api_activation_script');
	
	// run the deactivate scripts upon plugin deactivation
	
	register_deactivation_hook( __FILE__, 'trswoo_shop_api_deactivatation_script' );
	
	function trswoo_shop_api_deactivatation_script()
	{	
		global $wpdb;	
		$results = $wpdb->get_results( "select post_id, meta_value from $wpdb->postmeta where meta_key = 'trswoo_shop_pid'", ARRAY_A );	
		$pid = $results[0]['post_id'];	
		wp_delete_post($pid, true);
	}
	
	
	add_filter( 'page_template', 'trswoo_shop_api_static_page_template' );
	
	function trswoo_shop_api_static_page_template( $page_template )	{	
		if( is_page( 'my-custom-page-slug' ) )
			{	
				$page_template = dirname( __FILE__ ) . 'includes/shop-api-temp.php';	
			}
		
		return $page_template;	
	}
	
	add_action("template_redirect", 'trswoo_shop_api_my_theme_redirect');
	
	function trswoo_shop_api_my_theme_redirect() {	
		global $wp;	
		$plugindir = dirname( __FILE__ );	
		
		
		if (isset($wp->query_vars["pagename"]) && $wp->query_vars["pagename"] == TRSWOO_SHOP_API_PREFIX) {			$templatefilename = 'shop-api-temp.php';	
		if (file_exists(TEMPLATEPATH . '/' . $templatefilename)) {	
		$return_template = TEMPLATEPATH . '/' . $templatefilename;	
		}	
		else{	
		$return_template = TRSWOO_SHOP_API_INC . $templatefilename;		
		}		
		trswoo_shop_api_do_theme_redirect($return_template);
		
		}
	}
	
	function trswoo_shop_api_do_theme_redirect($url) 
	{	
		global $post, $wp_query;	
		if (have_posts())
			{	
				include($url);	
				die();
			}
		else {	
			$wp_query->is_404 = true;
		}
	}
	
	/* function trswoo_shop_api_log($message){  
		$currentTime = date('y-m-d h:i:s', time());	
		$backtrace=debug_backtrace(); 
		error_log("$currentTime  -->   " . "$message  Log Generation Details :      Filename: ".$backtrace[0]['file']."   at Line number : ".$backtrace[0]['line']."\n\n", 3, TRSWOO_SHOP_API_LOG_FILE . "plugin-logs" . TRSWOO_SHOP_API_LOG_FILE_EXT);
	} */
	
	
	function trswoo_shop_api_mandrill_mailer($dataarr) {
		require_once TRSWOO_SHOP_API_MANDRILL_FILE_PATH;
		try {
			$mandrill = new Mandrill(TRSWOO_SHOP_API_FILE_MANDRILL_API_KEY); 

			$to_emails = (!empty($dataarr['to_email']))?$dataarr['to_email']:'help@devo.co.uk';
			$to_names = (!empty($dataarr['to_names']))?$dataarr['to_names']:'';
			
			$message = array(
				
				'subject' 		=> (!empty($dataarr['subject']))?$dataarr['subject']:'Subject',
				'from_email' 	=> (!empty($dataarr['from_email']))?$dataarr['from_email']:'help@devo.co.uk',
				'from_name' 	=> (!empty($dataarr['from_name']))?$dataarr['from_name']:'Help Devo',
				'to' => array(
					array(
						'email' => $to_emails,
						'name' =>  $to_names,
						'type' => 'to'
					)
				),
				'global_merge_vars' => (!empty($dataarr['global_merge_vars']))?$dataarr['global_merge_vars']:array(),
			);
			
			$template_name = (!empty($dataarr['template_name']))?$dataarr['template_name']:'devo_sample_testing_template';
			
			$template_content = array(
				array(
					'name' => 'main',
					'content' => 'Template content'),
				array(
					'name' => 'footer',
					'content' => 'Copyright 2017.')

				); 
			
			$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message);
		} catch(Mandrill_Error $e) {
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			throw $e;
		}
	
	}
?>