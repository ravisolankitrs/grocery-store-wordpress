<?php
/**
 *  @file functions-cart.php
 *  @brief Cart Specific functions
 */
/* function woo_add_custom211_fee( ) {
        $tips=5;
        global $woocommerce;
	
        $woocommerce->cart->add_fee( 'Customfee4567',  $tips, false );
     }
	 add_action( 'woocommerce_cart_calculate_fees', 'woo_add_custom211_fee');  */
 function woo_add_rider_tips() {
		global $woocommerce;	
		
        if(isset($_SESSION['cart']['addtips']))
		{
			$tips = $_SESSION['cart']['addtips'];
		}
		else{
			$tips = 0.00;
		}
	//	echo "<pre>";print_r($_POST);die();
        
        $woocommerce->cart->add_fee( 'Rider Fee',  $tips, false );
     }
	 add_action( 'woocommerce_cart_calculate_fees', 'woo_add_rider_tips' );
function cart_enqueue_scripts_theme() {
	/* This script is intended for Cart page and mini cart in site */
	wp_enqueue_script( 'custom-minicart', get_stylesheet_directory_uri() . '/js/minicart.js', array( 'jquery'), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'cart_enqueue_scripts_theme' );



/* AJAX Mini Cart Processing to add or delete quantity of the product */
add_action('wp_ajax_nopriv_changeCartQuantity_js', 'changeMiniCartQuantityRequest');
add_action('wp_ajax_changeCartQuantity_js', 'changeMiniCartQuantityRequest');
add_action('wp_ajax_rider_tips_add_js','ridersTipAdd');
add_action('wp_ajax_rider_tips_min_js','ridersTipsMinus');
function ridersTipAdd()
{
	global $woocmmerce;
	global $tips;
   
		if(isset($_SESSION['cart']['addtips']))
		{
				$_SESSION['cart']['addtips']+=1.00;
				echo $_SESSION['cart']['addtips'];
        	do_action( 'woocommerce_cart_calculate_fees');
		}
		else {
	     	$_SESSION['cart']['addtips'] = 1.00;
			echo $_SESSION['cart']['addtips'];
	    	do_action( 'woocommerce_cart_calculate_fees');
         }

	die('done Plus');		 
}
function ridersTipsMinus()
{
	global $woocommerse;
	global $tips;
	
		 if(isset($_SESSION['cart']['addtips']))
		{
			if( $_SESSION['cart']['addtips'] > 0 ){
			   $_SESSION['cart']['addtips'] -= 1.00;
        	   do_action( 'woocommerce_cart_calculate_fees');
		    }
		}
		else {
	     	$_SESSION['cart']['addtips'] = 1.00;
	    	do_action( 'woocommerce_cart_calculate_fees');
         }
	die('done Minus');
}

function changeMiniCartQuantityRequest(){

	global $woocommerce;
	$pro_id = $_GET['pro_id'];
	$quantity = $_GET['quantity'];
	$changeOne = $_GET['changeOne'];

	if($changeOne==1){

		$response = $woocommerce->cart->add_to_cart($pro_id,$changeOne);
	}else {

		$qty = $quantity;

		if( $quantity == 0 ) {
			foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) {
				if ($cart_item['product_id'] == $pro_id) {

					$response = $woocommerce->cart->remove_cart_item($cart_item_key);
				}
			}
		}else {
			foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) {

				if ($cart_item['product_id'] == $pro_id) {
					$product = wc_get_product($pro_id);
					if ( $product->stock_status=='instock' && $product->managing_stock()==false) {
							$response = $woocommerce->cart->set_quantity($cart_item_key, $qty);
					}elseif($product->stock_status=='instock' && $product->managing_stock()==true && $product->get_stock_quantity() >= $qty){
						$response = $woocommerce->cart->set_quantity($cart_item_key, $qty);
					}


				}
			}
		}
	}

	if(!empty($response)) {

		foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) {
			if ($cart_item['product_id'] == $pro_id) {
				$quantity = $cart_item['quantity'];
				$line_total = $cart_item['line_total'];
			}
		}

		$arr = array();
		$arr[0] = $quantity;
		$arr[1] = number_format($line_total,2);
		$arr[2] = $woocommerce->cart->get_cart_subtotal();
		$arr[3] = number_format($quantity * $cart_item['data']->get_price(),2);

		$amount2 = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) );

		$delevery = "0.00";
		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
		$packagedata = WC()->session->get( 'shipping_for_package_0' );
		$currentMethod = (!empty($chosen_methods[0]))?$chosen_methods[0]:null;

		if(!empty($currentMethod) && !empty($packagedata['rates']) && !empty($packagedata['rates'][$currentMethod])){
			$delevery = $packagedata['rates'][$currentMethod]->cost;
		}

		// $t_amount = $amount2+$delevery+$e_fee;
		$t_amount = WC()->cart->get_cart_subtotal();
		$arr[4] = $t_amount;
		$arr[5] = $pro_id;
		$arr[6] = $delevery;

		echo json_encode($arr);die;
	}else{
		$woocommerce->cart->empty_cart();
		echo 'error';die;
	}

	die;
}




/* This code is added to auto refresh the header cart Icon on cart Change */
// Ensure header cart contents update when products are added to the cart via AJAX
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	$displayCarticon="display:none;";
	if ( !WC()->cart->get_cart_contents_count() == 0 ){
			$displayCarticon="display:block;";
	}
	ob_start();
	?>
	<span class="cart_head_total hide_cartprice" style="float:left;padding:0 46px 0 0;color:red;font-size: 20px;<?php echo $displayCarticon; ?>">

		<a onclick="return header_checkuserlogin();" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">

			<span class="glyphicon glyphicon-shopping-cart">

				<div class="badge">
					<div class="message-count"><?php echo WC()->cart->get_cart_contents_count(); ?></div>
				</div>
			</span>
			<span class="s_spn" style="font-size: 14px;font-weight: 600;"><?php echo WC()->cart->get_cart_total(); ?></span>
		</a>
	</span>
	<?php

	$fragments['span.cart_head_total'] = ob_get_clean();

	return $fragments;
}

/* This code is added to auto refresh the main cart cart Change */
// Ensure main cart contents update when products are added to the cart via AJAX
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_main_cart_fragment' );
function woocommerce_main_cart_fragment( $fragments ) {
	$amount2 = floatval( preg_replace( '#[^\d.]#', '', WC()->cart->get_cart_total() ) );
	$service_fee = number_format(get_option('service_fee_text'), 2);
	if($amount2 <= '10.00'){
		$service_fee = number_format(get_option('service_fee_text'), 2);
		// $service_fee = number_format(0.01, 2);
	}
	else{ $service_fee = 0.00; }
	ob_start();
	?>

    <div class="main_cart_side_details">
		<div class="row">
			<div class="col-md-7 rescol2 ">
				<p class="s_spn">&nbsp;<span><?php echo WC()->cart->get_cart_contents_count(); ?></span><span class="s_spn">&nbsp;&nbsp;Items:</span></p>
			</div>
			<div class="col-md-4 col-md-offset-1 rescol2">
				<p class="s_spn sss left_delivery left_content">&nbsp;&nbsp;&nbsp;<?php echo WC()->cart->get_cart_total(); ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7 rescol2">
				<p class="s_spn">&nbsp;Service Fee:
				</p>
			</div>
			<div class="col-md-4 col-md-offset-1  rescol2">
			   <p class="s_spn woocommerce-Price-amounts left_sevice left_content">&nbsp;&nbsp;&nbsp;<?php echo get_woocommerce_currency_symbol(); ?><?php echo @number_format($service_fee,2); ?>
					<input type="hidden" id="extFee" value="<?=$service_fee?>" />
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-9 rescol2">
				<p class="" style="margin: 0 0;padding-bottom: 2px;font-size: 11px;">&nbsp;(Items below <?php echo get_woocommerce_currency_symbol(); ?>10 )</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7 rescol2">
				<p class="s_spn">&nbsp;Delivery Fee:</p>
			</div>
			<div class="col-md-4 col-md-offset-1 rescol2">
				<p class="s_spn woocommerce-Price-amount sss left_delivery_free left_content">&nbsp;&nbsp;&nbsp;<?php echo WC()->cart->get_cart_shipping_total(); ?></p>
			</div>
		</div>
		<!--<div class="row">
			<div class="col-md-7 rescol2">
				<p  class="s_spn " >&nbsp;Total:</p>
			</div>
			<div class="col-md-4 col-md-offset-1 rescol2">
				<p class="s_spn usetotal sss left_total left_content">&nbsp;&nbsp;&nbsp;<?php // echo WC()->cart->get_total(); ?></p>
			</div>
		</div>  -->
	</div>
	<?php

	$fragments['div.main_cart_side_details'] = ob_get_clean();
	ob_start();
     ?>
        <a href="/checkout" onclick="return checkuserlogin();" data-total="<?php echo WC()->cart->cart_contents_total; ?>" class="button checkout wc-forward custom_minicart_checkout_btn min-check_btn btn-block checkoutcenter">
		<b>
			<span style="float:left;">
				Total : <?php echo WC()->cart->get_total(); ?>
			</span>
			<span style="float:right">Checkout</span>
		</b>
	</a>
	<?php
	$fragments['a.custom_minicart_checkout_btn'] = ob_get_clean();
	ob_start();
	?>
	 <span id="tipsfee">
        <?php  $val = WC()->cart->get_fees();
              echo @number_format($val[0]->amount,2);
        ?><?php  
        ?></span>
      <?php
	  $fragments['span#tipsfee'] = ob_get_clean();
	  return $fragments;
}


/* This function is used for micro cart to display just below the products */
// add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_micro_cart_fragment' );
function woocommerce_micro_cart_fragment( $fragments ) {
	$displayCarticon="display:none;";
	if ( !WC()->cart->get_cart_contents_count() == 0 ){
			$displayCarticon="display:block;";
	}
	ob_start();
	?>
	<div class="row custom_micro_cart" style="<?php echo $displayCarticon; ?>">
		   <div  class="col-md-3 table_style">
		   <table>
			<thead class="table_top">
			   <tr>
				<td  class="caret_td_width td_font_color"><?php echo WC()->cart->get_cart_contents_count(); ?></td>
				<td  class="mybasket_td_width td_font_color"><a href="<?php echo WC()->cart->get_cart_url(); ?>" class="td_font_color" >My Basket</a></td>
				<td  class="td_font_color td_amount"> <?php echo WC()->cart->get_cart_total(); ?></td>
			   </tr>
			</thead>

		  </table>
		 </div>
	</div>
	<?php

	$fragments['div.custom_micro_cart'] = ob_get_clean();

	return $fragments;
}



/* This code is added to remove Undo Message after removing a product from mini cart */
add_filter('woocommerce_cart_item_removed_title','hide_removed_from_cart_message');
function hide_removed_from_cart_message($title){
	return '';
}

function devo_cart_totals_coupon_html( $coupon ) { 
    if ( is_string( $coupon ) ) { 
        $coupon = new WC_Coupon( $coupon ); 
    } 
 
    $discount_amount_html = ''; 
 
    if ( $amount = WC()->cart->get_coupon_discount_amount( $coupon->get_code(), WC()->cart->display_cart_ex_tax ) ) { 
        $discount_amount_html = '-' . wc_price( $amount ); 
    } elseif ( $coupon->get_free_shipping() ) { 
        $discount_amount_html = __( 'Free shipping coupon', 'woocommerce' ); 
    } 
 
    $discount_amount_html = apply_filters( 'woocommerce_coupon_discount_amount_html', $discount_amount_html, $coupon ); 
    
    // $coupon_html = $discount_amount_html . ' <a href="' . esc_url( add_query_arg( 'remove_coupon', urlencode( $coupon->get_code() ), defined( 'WOOCOMMERCE_CHECKOUT' ) ? wc_get_checkout_url() : wc_get_cart_url() ) ) . '" class="devo-woocommerce-remove-coupon" data-coupon="' . esc_attr( $coupon->get_code() ) . '">' .'<i class="fa fa-times" aria-hidden="true"></i>' . '</a>'; 

    $coupon_html = ' <a href="' . esc_url( add_query_arg( 'remove_coupon', urlencode( $coupon->get_code() ), defined( 'WOOCOMMERCE_CHECKOUT' ) ? wc_get_checkout_url() : wc_get_cart_url() ) ) . '" class="devo-woocommerce-remove-coupon" data-coupon="' . esc_attr( $coupon->get_code() ) . '">' .'<i class="fa fa-times" aria-hidden="true"></i>' . '</a>'; 
 
    echo wp_kses( apply_filters( 'woocommerce_cart_totals_coupon_html', $coupon_html, $coupon, $discount_amount_html ), array_replace_recursive( wp_kses_allowed_html( 'post' ), array( 'a' => array( 'data-coupon' => true ) ) ) ); 
} 