<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<style>
.fix_theme_row_margin{
    margin:0px !important;
}
.res{
	margin-left: -12px;
}
.pull-left {
    float: left !important;
    margin-left: 13px !important;
}

.input-group-addon {
    padding: 0px 0px !important;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    color: #555;
    text-align: center;
    background-color: #eee;
    border: none !important;
    border-radius: 4px;
   }
.linkproduct{

	    font-size: 15px;
    line-height: 24px;
    font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
    margin: 0px 0 !important;
    color: black;
}

.panel-default>.panel-heading {
    color: #333;
    background-color: #f5f5f5;
    border-color: #fff !important;
}
	.s_spn {

		font-size: 15px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: gray;
		padding-bottom:8px;
	}
	headingColor1 {
		    color: #ededed;
            border-top: 3px dotted;
            padding: 1px 6px 2px 3px;
			border-width: 2px;
			display: inline-block;
            height: 1px;
            width: 95%;
			margin-top: 18px;

	}
	.headingColor{
    /* background: #ccc; */
	border-top: 1px dashed #ccc;
	border-width: 2px;
    display: inline-block;
    height: 1px;
   width: 100%;

}


	.atext{

		color: #0e62e4;
		margin-left: 15px;
		font-weight: bold;
	}
	.usetotal{

		font-size:15px;
		font-weight:bold;
	}

	.usetotall{

		font-size:15px;
		font-weight:bold;
	}
	.promorow{
		margin-top: -21px;
	}

	.checkoutcenter{
		text-align:center;

	}
	.basketd_text{
		background-color:white;
		font-size: 18px;
		text-align: center;
		font-weight: 600;
		font-family: AvenirLTStd-Roman,
	}

	.basketd_textt{
		font-size: 18px;
		text-align: center;
		font-weight: 600;
		font-family: AvenirLTStd-Roman,
	}
	.basketd_texttt{
		font-size: 18px;

		font-weight: 600;
		font-family: AvenirLTStd-Roman,
	}

	.pborder{
		border:1px solid #cdcdcd;
	}
	.maindiv{
		margin-top: 35px;
		margin-left:12px;
	}
	.bg{
		background-color:#ffffff !important;
	}
	.selectsave1{
		font-weight:bold;
	}
	.pclose{
		color:red !important;
	}


	#bk-cartimg img{
		width:110px;
		height:110px;
	}
    .left_row{

	margin-left: 47px;
 }
 .pricediv{
			margin-left: 50px

 }

 
.errorpayment {
    background-color: #fd0e35;
    border-radius: 3px;
    width: 100%;
    margin-left: 0px;
    margin-right: 120px;
    min-height: 46px;
}


.gen-name-cls span, .gen-name-cls-right span, .gen-name-full span, .gen-name-cls-mid span {
    float: left;
    font-size: 16px;
}
 

.fea_control {
    padding: 0px;
}

.errortext {
    font-size: 12px !important;
    line-height: 24px;
    font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
    margin: 0px 0 !important;
    color: white;
}

.errortext1 {
    font-size: 11px !important;
    line-height: 18px;
    font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
    margin: 14px 0px !important;
    color: white;
}

.img_tringle {
    width: 35%;
    margin-top: 12px;
    margin-left: 15px;
}
 
 
/* This has been added to hide product Removed Messages and Coupon Errors in Cart Page */
.woocommerce-error,.woocommerce-message{
	display:none;
}
</style>
<script>
$(document).ready(function(){
    $(".variation").addClass('hide');

});




	jQuery(document).ready(function(){
		
		
			
			var error_data = (jQuery(".site-main").find(".woocommerce-error").html());
		 
			//console.log(error_data);
			
			//alert(error_data);
		 
			var before_error = '<div class="col-md-12 col-sm-12 col-xs-12 gen-name-full bbccc fea_control style="width: 98%; margin-left: 2px"><span class="errorpayment" id="Error_msg"><span class="errortext" style="width:13%;"><img src="https://devo.co.uk/wp-content/themes/foodpicky-child/css/error-triangle.png" class="img_tringle" alt="image"></span><span class="errortext1" >';
		 
			var after_error = '</span> </span> </div>';
			
			 if(error_data !== undefined){
				//jQuery("#login-register-toggle").trigger("click");
				
						 
				 if (~error_data.indexOf("does not exist!")){
					jQuery(".site-main").find(".promorow").append(before_error+'Promo code you have enter does not exist.'+after_error);
					jQuery(".img_tringle").css("margin-top","20px");
				 }
									
						 
				 if (~error_data.indexOf("This coupon has expired.")){
					jQuery(".site-main").find(".promorow").append(before_error+'Promo code you have entered has expired.'+after_error);
					jQuery(".img_tringle").css("margin-top","20px");
				 }
												
						 
				 if (~error_data.indexOf("Coupon code already applied!")){
					jQuery(".site-main").find(".promorow").append(before_error+'Promo code already applied!'+after_error);
					jQuery(".img_tringle").css("margin-top","20px");
				 }
                 
                if (~error_data.indexOf("cannot be used in conjunction with other coupons")){
					jQuery(".site-main").find(".promorow").append(before_error+'Sorry, another coupon is already applied!'+after_error);
					jQuery(".img_tringle").css("margin-top","20px");
                }
						 
/* 				 if (~error_data.indexOf("Please enter an account password.")){
					jQuery('.register').trigger("click");
					jQuery(".header-my-account").find(".woocommerce-error").html(before_error+'Password is required.'+after_error);
				 }
				 
						 
				 if (~error_data.indexOf("Username is required.")){
					jQuery(".header-my-account").find(".woocommerce-error").html(before_error+'Email is required.'+after_error);
				 }
				 
						 
				 if (~error_data.indexOf("Please provide a valid email address.")){
					 jQuery('.register').trigger("click");
					jQuery(".header-my-account").find(".woocommerce-error").html(before_error+'Email is required.'+after_error);
				 }
				 
						 
				 if (~error_data.indexOf("The password field is empty.")){
					jQuery(".header-my-account").find(".woocommerce-error").html(before_error+'Password is required.'+after_error);
				 }
				 
				 if (~error_data.indexOf("An account is already registered with your email address. Please login.")){
					jQuery('.register').trigger("click");
					jQuery(".header-my-account").find(".woocommerce-error").html(before_error+"Sorry, that email id is already registered."+after_error);
					
				 } */

				
			 }

		
		$( ".login, .register, #login-register-toggle" ).on( "click", function() {
			jQuery(".header-my-account").find(".woocommerce-error").html("").hide();
		});
		
		
		
				
	});
	






</script>
<!-- start main container -->
<?php wc_print_notices();?>
 <?php do_action( 'woocommerce_before_cart' );  ?>
	<div class="container">
	<div class="row">
	 <div class="col-sm-2 col-sm-offset-4">
	   <h5 class="basketd_textt">
	     My Basket
	   </h5>
	 </div>
	</div><br/>


	<div class="row">
	  <div class="col-sm-10 col-offset-2">
     	<div class="row left_row">
		    <!-- start product div-->
			<div class="col-sm-8 col_left_basket">
				<div class="panel panel-default pborder">
				<div class="panel-body" style="background-color: white;">
					<div class="row">
						<div class="col-sm-12">
						   <span class="s_spn">Delivered from</span><br/>
						   <span>

							<h5 class="basketd_texttt">
								<a href="/profile/selectandsave/" style="color: #fd0e35;">
									Select & Save
								</a>
							</h5>



						   </span>
						   <h6 class="headingColor">&nbsp;</h6>
						</div>
					</div>
                    <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
                    <?php do_action( 'woocommerce_before_cart_table' ); ?>
					<div class="row">
						<?php do_action( 'woocommerce_before_cart_contents' ); ?>
						<?php
						   foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
						   $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
						   $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

						   if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						   $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
						   ?>
                   <div id="main_pro_id_<?php echo $product_id;?>" data-product_id = "<?php echo $product_id;?>" class="mini_cart_item_div <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>" style="float:left;">
						<div class="col-sm-8" style="margin-bottom: 4%;">
						   <div class="pull-left media-object"  id="bk-cartimg" >
							  <?php
								 $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

								 if ( ! $product_permalink ) {
									echo $thumbnail;
								 } else {
									//printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
                                     printf( '<a href="%s">%s</a>', esc_url( '#' ), $thumbnail );
								 }
								 ?>
						    </div>
						    <div class="media-body">
							<div id="product_loader_<?php echo $product_id; ?>"></div>
								<div  class="maindiv">

										<?php
										   if ( ! $product_permalink ) {
											echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
										   } else {
											echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a class="linkproduct" href="%s">%s</a>', esc_url( '#' ), $_product->get_name() ), $cart_item, $cart_item_key );
										   }

										   // Meta data
										   echo WC()->cart->get_item_data( $cart_item );

										   // Backorder notification
										   if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
											echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
										   }
										   ?>
                                    <div>
                                       <input type="hidden" id="main_quant_<?=$product_id?>" value="<?=$cart_item['quantity']?>"/>
                                        <input type="hidden" id="main_dataPrice_<?=$product_id?>" value="<?=$cart_item['data']->get_price();?>"/>
                                        <?php $productPrice = number_format($cart_item['quantity'] * $cart_item['data']->get_price(),2); ?>
                                        <input type="hidden" id="main_dataLine_<?=$product_id?>" value="<?php echo $productPrice;?>"/>
                                        <span  class="s_spn">
                                            Qty
                                        </span>&nbsp;&nbsp;
                                        <span  class="s_spn">
                                            <a id="one_minus" href="javascript:void(0);" onclick="main_add_cart_list(<?php echo $product_id; ?>,-1,this);" ><i class="fa fa-minus-circle" style="color:#e85353" ></i></a>
                                        </span>&nbsp;&nbsp;
                                        <span  class="s_spn">
                                            <span id="main_quan_text_<?php echo $product_id; ?>" class="exact_quantity"><?php echo $cart_item['quantity']; ?></span>
                                        </span>&nbsp;&nbsp;
                                        <span  class="s_spn">
                                            <?php if(wc_get_product($product_id)->get_stock_status()=='instock'){ ?>
                                            <a id="one_plus" href="javascript:void(0);" onclick="main_add_cart_list(<?php echo $product_id; ?>, 1,this);" ><i class="fa fa-plus-circle" style="color:#41abb1" ></i></a>
                                            <?php }else{ ?>
                                            <a id="one_plus" href="javascript:void(0);" >+</a>
                                            <?php } ?>
                                        </span>
                                    </div>
									<input type="hidden" id="clicked_id" value=""/>
								</div>
						    </div>
						</div>
						<div class="col-sm-3">
						   <div class="media-body">
							  <div class="maindiv pricediv">
								 <p class="s_spn rescol1 main_line_total_p_element_<?=$product_id;?>" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
                                    <?php echo get_woocommerce_currency_symbol(); ?>
                                    <!--<span id="main_line_total_<?=$product_id;?>">
                                        <?php echo number_format($cart_item['line_total'],2); ?>
                                    </span>-->
                                    <span id="main_line_total_<?=$product_id;?>">
                                    <?php                                    
                                        echo $productPrice;
                                    ?>
                                    </span>
                                    <?php
									  // echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
									   ?>
								 </p>
							  </div>
						   </div>
						</div>
						<div class="col-sm-1">
						   <div class="media-body ">
							  <div class="maindiv">
								 <p style="color:red;" class="rescol res">
									<?php
									   echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(

										'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-trash" aria-hidden="true"></i></a>',
										esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
										__( 'Remove this item', 'woocommerce' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() )
									   ), $cart_item_key );
									   ?>
								 </p>
							  </div>
						   </div>
						</div>
                    </div>
						<?php
						   }
                       }
                   ?>
                    <?php do_action( 'woocommerce_cart_contents' ); ?>

                    <?php do_action( 'woocommerce_after_cart_contents' ); ?>
					</div>
                 <br/>
				</div>
			   </div>
                <div class="coupons-area">
                    <?php $coupons = WC()->cart->get_coupons();?>
                    <?php if(!empty($coupons)) : ?>
                        <div class="panel panel-default pborder">
                            <div class="panel-heading basketd_text bg" style="background-color:#fcfbf9 !important;">Applied Coupons
                                 <h6 class="headingColor" style=" margin-top: 11px; margin-left: 0px;background-color:#fcfbf9 !important;">&nbsp;</h6>
                            </div>
                            <div class="panel-body" style="margin-top: -31px ;background-color: #fcfbf9;">
                                <div class="main_cart_coupon_details">
                                    <div class="row fix_theme_row_margin">
                                        <?php foreach ( $coupons as $code => $coupon ) : ?>
                                            <div class="row fix_theme_row_margin">
                                                <div class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                                                    <span>
                                                        <?php wc_cart_totals_coupon_label( $coupon ); ?>
                                                    </span>
                                                    &nbsp;
                                                    <span data-title="<?php echo esc_attr( wc_cart_totals_coupon_label( $coupon, false ) ); ?>">
                                                        <?php devo_cart_totals_coupon_html( $coupon ); ?>
                                                    </span>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>		
                        </div>
                    <?php endif; ?>
                </div>
			</div>
			<!-- end product div-->
			<!-- start product card div-->
			<div class="col-sm-4 col_basket_price">
			    <div class="panel panel-default pborder">
					<div class="panel-heading basketd_text bg" style="background-color:#fcfbf9 !important;">Basket Details

					 <h6 class="headingColor" style=" margin-top: 11px; margin-left: 0px;background-color:#fcfbf9 !important;">&nbsp;</h6>
					</div>
					<?php
					$amount2 = floatval( preg_replace( '#[^\d.]#', '', WC()->cart->get_cart_total() ) );
					$service_fee = number_format(get_option('service_fee_text'), 2);
					if($amount2 <= '10.00'){
						$service_fee = number_format(get_option('service_fee_text'), 2);
					}
					else{ $service_fee = 0.00; }
					?>
					<div class="panel-body" style="margin-top: -31px ;background-color: #fcfbf9;">
                        <div class="main_cart_side_details">
                            <div class="row">
                                <div class="col-md-7 rescol2 ">
                                    <p class="s_spn">&nbsp;<span><?php echo WC()->cart->get_cart_contents_count(); ?></span><span class="s_spn">&nbsp;&nbsp;Items:</span></p>
                                </div>
                                <div class="col-md-4 col-md-offset-1 rescol2">
                                    <p class="s_spn sss left_delivery left_content">&nbsp;&nbsp;&nbsp;<?php echo WC()->cart->get_cart_total(); ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 rescol2">
                                    <p class="s_spn">&nbsp;Service Fee:
									</p>
                                </div>
                                <div class="col-md-4 col-md-offset-1  rescol2">
                                   <p class="s_spn woocommerce-Price-amounts left_sevice left_content">&nbsp;&nbsp;&nbsp;<?php echo get_woocommerce_currency_symbol(); ?><?php echo number_format($service_fee,2); ?>
										<input type="hidden" id="extFee" value="<?=$service_fee?>" />
									</p>
                                </div>
                            </div>

							<div class="row">
								<div class="col-md-9 rescol2">
									<p class="" style="margin: 0 0;padding-bottom: 2px;font-size: 11px;">&nbsp;(Items below <?php echo get_woocommerce_currency_symbol(); ?>10 )</p>
								</div>
							</div>
                            <div class="row">
                                <div class="col-md-7 rescol2">
                                    <p class="s_spn">&nbsp;Delivery Fee:</p>
                                </div>
                                <div class="col-md-4 col-md-offset-1 rescol2">
                                    <p class="s_spn woocommerce-Price-amount sss left_delivery_free left_content">&nbsp;&nbsp;&nbsp;<?php echo WC()->cart->get_cart_shipping_total(); ?></p>
                                </div>
                            </div>
                            <!--<div class="row">
                                <div class="col-md-7 rescol2">
                                    <p  class="s_spn " >&nbsp;Total:</p>
                                </div>
                                <div class="col-md-4 col-md-offset-1 rescol2">
                                    <p class="s_spn usetotal sss left_total left_content">&nbsp;&nbsp;&nbsp;<?php echo WC()->cart->get_total(); ?></p>
                                </div>
                            </div>-->
                        </div>
                        <!--<div class="cart-collaterals">
                            <?php //do_action( 'woocommerce_cart_collaterals' ); ?>
                        </div>-->
                        <br/>
						<h6 class="headingColor s_spn" style="margin-left:15px;width: 100%;">&nbsp;</h6>
						 <div class="row promorow">
							<div class="col-md-12 col-sm-offset-3" style="margin: 0 0 0 0;">
								<a href="#"  data-toggle="modal" data-target="#myModal1"   class="left_promo" style="color:#8686e2; font-weight:bold;margin-left:58px;">Enter Promo code</a>
						   </div>
						</div>
						<!-- modal--->
						<div class="modal fade" id="myModal1" role="dialog">
							<div class="modal-dialog">
                                <!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<a href="#"><i class="fa fa-times" class="close" data-dismiss="modal" style="color:red; float:right; aria-hidden="true"></i></a>

									</div>
									<div class="modal-body">
									<div class="row">
									<div class="col-sm-2"></div>

									<div class="col-md-8 ">

								<!--<a href="#" class="atext">Enter Promo Code</a>-->
                                <?php if ( wc_coupons_enabled() ) { ?>

									<div class="coupon input-group">
                                        <!--<label for="coupon_code"><?php _e( 'Coupon:', 'woocommerce' ); ?></label>-->
										<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" />
										<span class="input-group-addon"><input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>" />
                                        <?php do_action( 'woocommerce_cart_coupon' ); ?></span>
                                    </div>
                                <?php } ?>
						   </div>
						   </div>
									</div>
									<div class="modal-footer">

									</div>
								</div>
					        </div>
				        </div>



						<!-- end modal -->




					</div>

					<?php
						// $data = get_active_shipping_methods();
						// echo "<pre>";print_r($data);
						?>

				</div>
				<div class="row rider_tip_row">
                    <div class="col-md-6">
                        <span style="font-size:18px;color:#272727;margin-left:5px;">
                            <i class="fa fa-smile-o" aria-hidden="true"></i>
                        </span>
                        &nbsp;Rider Tip
                    </div>
                    <div class="col-md-6 pull-right">
                        <p style="float:right;margin-right:5px;">
                            <a href="javascript:void(0)" onclick="minustips()" class="cart_rider_minus_tip">
                                <span style="color:#e85353"><i class="fa fa-minus-circle" aria-hidden="true"> </i></span>
                            </a>
                            &nbsp;<?php echo get_woocommerce_currency_symbol(); ?><span id="tipsfee"><?php echo $service_fee; ?></span>&nbsp;
                            <a href="javascript:void(0)" onclick="addtips();" class="cart_rider_add_tip" >
                                <span style="color:#41abb1;"><i class="fa fa-plus-circle" aria-hidden="true"> </i></span>
                            </a> 
                        </p>
                    </div>
				
				</div>
                <?php do_action( 'woocommerce_cart_actions' ); ?>
                <?php wp_nonce_field( 'woocommerce-cart' ); ?>
                <?php do_action( 'woocommerce_after_cart_contents' ); ?>
				<div class="row">
					<div class="col-md-12 col-md-12">
					<?php //global $woocommerce; echo $woocommerce->cart->get_cart_total();  ?>
					<?php //echo  ?>
						<p class="buttons" id="check_out">
							<a href="/checkout" onclick="return checkuserlogin();" data-total = "<?php echo "WC()->cart->cart_contents_total"; ?>"  class="button checkout wc-forward custom_minicart_checkout_btn min-check_btn btn-block checkoutcenter"><b><span style="float:left;">Total : <?php echo WC()->cart->get_total(); ?></span> <span style="float:right">Checkout</span></b></a>
							
						</p>
					</div>
				</div>
				<?php
					/* $data = get_active_shipping_methods();
					echo "<pre>";print_r($data); */
				?>
			</div>
            <script>

					/* function checkuserlogin(){


						var user_in = "<?php echo is_user_logged_in();	?>";

						if(user_in == 1){
							return true;
						}

						jQuery(".login").attr("action","<?php echo WC()->cart->get_cart_url(); ?>");
						jQuery(".register").attr("action","<?php echo WC()->cart->get_cart_url(); ?>");
						jQuery("input[name='_wp_http_referer']").attr("value","<?php echo WC()->cart->get_cart_url(); ?>");

						jQuery("#login-register-toggle").trigger("click");

						return false;
					} */

			</script>
			<!-- start product card div-->
		</div>
		</div>
		</div>
 	</div>
<?php do_action( 'woocommerce_after_cart_table' ); ?>



<?php do_action( 'woocommerce_after_cart' ); ?>


<style>



 @media only screen   and  (min-device-width : 412px)and  (max-device-width : 732px){
	.pricediv {
		    margin-left: 545px ;
    margin-top: -46px ;
		z-index: 1111 ;
		position: absolute;
	}

	.res {
		    margin-left:607px ;
    margin-top: -116px ;
		position: absolute ;
		z-index: 1111;
	}

	.left_row{
		margin-left: -15px !important;
   }

   .rescol2{
		width:50% !important;
		float:left !important;
	}
	.margin_free{
	  margin-left: 80px !important;
	}
   .left_content{
	 float:right;

   }


}
@media screen  and (max-width:414px) {
	.left_row{
		margin-left: -15px !important;
   }
   .woocommerce-Price-amount{
	    float: right !important;

}
  .woocommerce-Price-amounts{
	    float: right !important;

}

   #bk-cartimg img {
		width: 75px !important;
		height: 85px !important;
		margin-left: -20px;
		margin-top: 12px;
	}

	.linkproduct {
		font-size: 12px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: black;
	}
	.s_spn {
		font-size: 12px;
	}


	.res {
		    margin-left: 303px ;
			margin-top: -99px ;
		position: absolute ;
		z-index: 1111;
	}

	.pricediv {
    margin-left: 270px;
    margin-top: -36px;
    position: absolute;
    z-index: 1111;
}
	.rescol2{
		width:50% !important;
		float:left !important;
	}
	.margin_free{
	  margin-left: 80px !important;
	}


}

@media screen  and (max-width:375px) {
	.left_row{
		margin-left: -15px !important;
   }
   .woocommerce-Price-amount{
	    float: right !important;

}
  .woocommerce-Price-amounts{
	    float: right !important;

}

   #bk-cartimg img {
		width: 75px !important;
		height: 85px !important;
		margin-left: -20px;
		margin-top: 12px;
	}

	.linkproduct {
		font-size: 10px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: black;
	}
	.s_spn {
		font-size: 10px;
	}


	.res {
		    margin-left: 249px !important;
			margin-top: -99px !important;
		position: absolute !important;
		z-index: 1111;
	}

	.pricediv {
    margin-left: 235px !important;
    margin-top: -36px;
    position: absolute;
    z-index: 1111;
}
	.rescol2{
		width:50% !important;
		float:left !important;
	}
	.margin_free{
	  margin-left: 80px !important;
	}


}


@media screen  and (max-width:412px) {
	.left_row{
		margin-left: -15px !important;
   }
   .woocommerce-Price-amount{
	    float: right !important;

}
  .woocommerce-Price-amounts{
	    float: right !important;

}

   #bk-cartimg img {
		width: 75px !important;
		height: 85px !important;
		margin-left: -20px;
		margin-top: 12px;
	}

	.linkproduct {
		font-size: 12px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: black;
	}
	.s_spn {
		font-size: 12px;
	}


	.res {
		    margin-left: 303px ;
			margin-top: -99px ;
		position: absolute ;
		z-index: 1111;
	}

	.pricediv {
    margin-left: 270px;
    margin-top: -36px;
    position: absolute;
    z-index: 1111;
}
	.rescol2{
		width:50% !important;
		float:left !important;
	}
	.margin_free{
	  margin-left: 80px !important;
	}


}


@media screen  and (max-width:360px) {
	.left_row{
		margin-left: -15px !important;
   }
   .woocommerce-Price-amount{
	    float: right !important;

}
  .woocommerce-Price-amounts{
	    float: right !important;

}

   #bk-cartimg img {
		width: 75px !important;
		height: 85px !important;
		margin-left: -20px;
		margin-top: 12px;
	}

	.linkproduct {
		font-size: 11px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: black;
	}
	.s_spn {
		font-size: 11px;
	}


	.res {
		    margin-left: 249px !important;
			margin-top: -99px !important;
		position: absolute !important;
		z-index: 1111;
	}

	.pricediv {
    margin-left: 230px !important;
    margin-top: -36px;
    position: absolute;
    z-index: 1111;
}
	.rescol2{
		width:50% !important;
		float:left !important;
	}
	.margin_free{
	  margin-left: 80px !important;
	}


}


@media screen  and (max-width:320px) {
	.left_row{
		margin-left: -15px !important;
   }
   .woocommerce-Price-amount{
	    float: right !important;

}
  .woocommerce-Price-amounts{
	    float: right !important;

}

   #bk-cartimg img {
		width: 75px !important;
		height: 85px !important;
		margin-left: -20px;
		margin-top: 12px;
	}

	.linkproduct {
		font-size: 10px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: black;
	}
	.s_spn {
		font-size: 10px;
	}


	.res {
		    margin-left: 215px !important;
			margin-top: -99px !important;
		position: absolute !important;
		z-index: 1111;
	}

	.pricediv {
    margin-left: 189px !important;
    margin-top: -36px;
    position: absolute;
    z-index: 1111;
}
	.rescol2{
		width:50% !important;
		float:left !important;
	}
	.margin_free{
	  margin-left: 80px !important;
	}


}

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px){
	.left_row{
		margin-left: -15px !important;
   }
   #bk-cartimg img {
		width: 75px !important;
		height: 85px !important;
		margin-left: -8px;
		margin-top: 12px;
	}

	.linkproduct {
		font-size: 10px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: black;
	}
	.s_spn {
		font-size: 10px;
	}

	.pricediv {
		margin-left: 25px !important;
		margin-top: 62px !important;
		z-index: 1111 !important;
		position: absolute;
	}

	.res {
		margin-left: -25px !important;
		margin-top: 3px !important;
		position: absolute !important;
		z-index: 1111;
	}
	.rescol2{
		width:50% !important;
		float:left !important;
	}

	.col_basket_price{
   position: absolute;
    width: 47%;
    margin-left: 422px;

	}

	.left_content{
	 float:right;

   }
}
@media only screen and (min-device-width : 1024px) and (max-device-width : 1200px){


   #bk-cartimg img {
		width: 75px !important;
		height: 85px !important;
		margin-left: -8px;
		margin-top: 12px;
	}

	.linkproduct {
		font-size: 12px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: black;
	}
	.s_spn {
		font-size: 12px;
	}

	.pricediv {
		margin-left: 25px !important;
		margin-top: 62px !important;
		z-index: 1111 !important;
		position: absolute;
	}

	.res {
		margin-left: -25px !important;
		margin-top: 3px !important;
		position: absolute !important;
		z-index: 1111;
	}
	.rescol2{
		width:50% !important;
		float:left !important;
	}

	.col_basket_price{
   position: absolute;
    width: 47%;
    margin-left: 553px;

	}

	.left_delivery{

		margin-left: 243px !important;
    margin-top: -23px !important;;
    position: absolute !important;;
	}
	.left_sevice {
    margin-left: 243px !important;
    margin-top: -17px !important;
    z-index: 1111;
    position: fixed;

	}

	.left_delivery_free{

		    margin-left: 243px !important;
			margin-top: -25px !important;
			position: absolute !important;
	}

	.left_total{

		    margin-left: 243px !important;
            position: absolute;
            margin-top: -23px !important;
	}
	.left_promo{

		    margin-left: 81px !important;
	}

}




</style>
<script>
jQuery(document).ready(function(){

			setTimeout(function(){  //Beginning of code that should run AFTER the timeout
				jQuery( document.body ).trigger( 'wc_fragment_refresh' );
				console.log("100");
			},100);
			/* setTimeout(function(){  //Beginning of code that should run AFTER the timeout
				jQuery( document.body ).trigger( 'wc_fragment_refresh' );
				console.log("250");
			},250);
			setTimeout(function(){  //Beginning of code that should run AFTER the timeout
				jQuery( document.body ).trigger( 'wc_fragment_refresh' );
				console.log("500");
			},500); */
	});
    
/* 
Created BY SJ
Mod 12:52 PM 29 January, 2018 by PJ
 */
function addtips()
{   
    jQuery('.rider_tip_row').addClass('rider_main_div_disable');
    jQuery('.rider_tip_row').addClass('rider_tip_change_loader');
    jQuery('.cart_rider_add_tip').addClass('disabled');
    jQuery('.cart_rider_minus_tip').addClass('disabled');
    var val= parseInt($('#tipsfee').text());
    jQuery('#tipsfee').text( (val + 1) +".00");
    jQuery.ajax({
        url: custom_child_script_ajax.ajaxurl,
        type: "GET",
        data: {'action':'rider_tips_add_js','tips_val':1,},
        dataType: 'text' ,
        //async = false,
        success: function (response) {
            console.log(response.text);		
            refresh_fragments();
            jQuery('.cart_rider_add_tip').removeClass('disabled');
            jQuery('.cart_rider_minus_tip').removeClass('disabled');
            jQuery('.rider_tip_row').removeClass('rider_main_div_disable');
            jQuery('.rider_tip_row').removeClass('rider_tip_change_loader');
        },
        error: function(errorThrown){
            console.log(errorThrown);
            jQuery('.cart_rider_add_tip').removeClass('disabled');
            jQuery('.cart_rider_minus_tip').removeClass('disabled');
            jQuery('.rider_tip_row').removeClass('rider_tip_change_loader');
        }
    });
    return false;
}
function minustips()
{
    jQuery('.rider_tip_row').addClass('rider_main_div_disable');
    jQuery('.rider_tip_row').addClass('rider_tip_change_loader');
    jQuery('.cart_rider_minus_tip').addClass('disabled');
    jQuery('.cart_rider_add_tip').addClass('disabled');
    var val= parseInt($('#tipsfee').text());
    if(val > 0)
    {
         $('#tipsfee').text( (val - 1) +".00");
    }
    var val='<?php echo $_SESSION['cart']['addtips']; ?>';
    var val = parseInt(val);
    jQuery.ajax({
        url:custom_child_script_ajax.ajaxurl,
        type:"GET",
        data:{'action':'rider_tips_min_js',},
        dataType:'text',
        success: function(response){
            console.log('sucess minus');		
            refresh_fragments();
            jQuery('.cart_rider_add_tip').removeClass('disabled');
            jQuery('.cart_rider_minus_tip').removeClass('disabled');
            jQuery('.rider_tip_row').removeClass('rider_main_div_disable');
            jQuery('.rider_tip_row').removeClass('rider_tip_change_loader');
        },
        error: function(errorThrown){
            console.log(errorThrown);
            jQuery('.cart_rider_add_tip').removeClass('disabled');
            jQuery('.cart_rider_minus_tip').removeClass('disabled');
            jQuery('.rider_tip_row').removeClass('rider_tip_change_loader');
        }
    });  
    return false;
}
	
</script>
