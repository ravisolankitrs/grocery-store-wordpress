<?php 


/*
Plugin Name: Trs advance category widget
Description: This adds category widget
Author: trs
*/


register_activation_hook( __FILE__ , 'custom_widget_plugin_install');

	function custom_widget_plugin_install() {
  
		$the_page_title = 'profile-search-product';
		$the_page_slug = 'profile-search-product';
		
		$mypage = array(
			'post_title' => $the_page_title,
			'post_name' => $the_page_slug,
			'post_content' => 'This is my Custom-Search page, Please Don\'t delete it',
			'post_status' => 'publish',
			'post_type' => 'page',
			'comment_status' => 'closed',
		);
		
		/* Check for Page is all ready inserted aur not( to Stop add Duplicate file) */
		
		if(empty(get_page_by_path( $the_page_slug ))){			
			$the_page_id = wp_insert_post( $mypage );
		}
	}

	/* Assign Page As a Template */

add_filter( 'page_template', 'custom_form' );
function custom_form( $page_template )
{
    if ( is_page( 'profile-search-product/' ) ) {
        $page_template = dirname( __FILE__ ) . '/page-profile-search-product.php';
    }
    return $page_template;
}




// Register and load the widget
function wpb_cat_load_widget() {
	register_widget( 'trs_custom_cat_widget_sb' );
	
}
add_action( 'widgets_init', 'wpb_cat_load_widget' );

// Register and load the widget
function wpb_trs_load_widget() {
	register_widget( 'trs_advance_search_sb' );
	
}
add_action( 'widgets_init', 'wpb_trs_load_widget' );

// Creating the widget 
class trs_custom_cat_widget_sb extends WP_Widget {

function __construct() {
parent::__construct(

// Base ID of your widget
'trs_custom_cat_widget_sb', 

// Widget name will appear in UI
__('Trs advance category widget ', 'trs_custom_cat_widget_sb_domain'), 

// Widget description
array( 'description' => __( 'Advance custom category to show', 'trs_custom_cat_widget_sb_domain' ), ) 
);
}

// Creating widget front-end

public function widget( $args, $instance ) {
	
	global $wp_query;
$post_type = isset($wp_query->query['post_type']) ? $wp_query->query['post_type'] : 'post';
if (is_array($post_type)) {
    $post_type = reset($post_type);
}
	
?>

<style>

.site-main{
	margin-top: -40px;
}

.qwerty{
	margin-top: 30px;
}

.nothing-search {
    float: right;
    padding-top: 6px;
}  

.loader {
    background: url(http://4.bp.blogspot.com/-hO_3iF0kUXc/Ui_Qa3obNpI/AAAAAAAAFNs/ZP-S8qyUTiY/s200/pageloader.gif);
}

.main-maa-muu{
	color:black;
}  
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus
	 {
		 background-color:transparent !important;	
	}
  .nav-tabs { 
	border-bottom: 1px solid #DDD;
  }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a {
		border: none; color: grey; //#fd0e35
		}
        .nav-tabs > li.active > a, .nav-tabs > li > a:hover {
			border: none; color: #fd0e35 !important; background: transparent;
			}
        .nav-tabs > li > a::after { content: ""; background: grey; height: 3px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after { transform: scale(1); }
.tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
.tab-pane { padding: 0px 0; }
.tab-content{padding:0px}

.card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }
.widget-area{	padding:0px;	margin-bottom: -30px;	}


@media screen and  (max-width:360px) {
	.overflow_div{	
    overflow-x: auto;
    overflow-x: scroll;
    white-space: nowrap;
    display: inline-flex;
    overflow-y: hidden;
    width: 362px;
    overflow-x: auto;
    margin-left: -32px;
	}	   
	.widget-area{
		padding:0px;
		margin-bottom: -30px !important;
	}
	
	.entry-thumbnail .image {
    background-repeat-x: no-repeat;
    background-repeat-y: no-repeat;
	background-size: 134px;
    margin-left: 1px;
}	

.entry-header {
    margin-bottom: 20px;
    margin-left: -13px;
    z-index: 111;
}
.margin_left{
	    float: left;
    width: 165px;
}		
	
.title {
    color: #fd0e35;
    font-size: 11px;
    font-weight: bold;
}	
	
	   
	
	
}	
@media screen and (min-width:412px)and (max-width: 414px) {
	.overflow_div{	
    overflow-x: auto;
    overflow-x: scroll;
    white-space: nowrap;
    display: inline-flex;
    overflow-y: hidden;
       width: 430px;
    overflow-x: auto;
    margin-left: -32px;
	}	   
	.widget-area{
		padding:0px;
		margin-bottom: -30px !important;
	}

		.entry-thumbnail .image {
    background-repeat-x: no-repeat;
    background-repeat-y: no-repeat;
	background-size: 161px;
  
}	

.entry-header {
    margin-bottom: 20px;
    
    z-index: 111;
}
.margin_left{
	    float: left;
        width: 191px;
}		
	
.title {
    //color: grey;
    color: #fd0e35;
    font-size: 10px !important;
    font-weight: bold;
}	
	
	
	
	





	
	}	
	
@media screen and  (max-width:320px) {
	.overflow_div{	
    overflow-x: auto;
    overflow-x: scroll;
    white-space: nowrap;
    display: inline-flex;
    overflow-y: hidden;
    width: 338px !important;
    overflow-x: auto;
    margin-left: -33px !important;
	}	   
	.widget-area{
		padding:0px;
		margin-bottom: -30px !important;
	}

   .entry-thumbnail .image {
    background-repeat-x: no-repeat;
    background-repeat-y: no-repeat;
	background-size: 134px;
    margin-left: 1px;
}	

.entry-header {
    margin-bottom: 20px;
    margin-left: -13px;
    z-index: 111;
}
.margin_left{
	    float: left;
       width: 144px !important;
}	
.title {
    //color: grey;
    color: #fd0e35;
    font-size: 9px !important;
    font-weight: bold;
}

	
	}	
	
	    
	
	@media screen and  (max-width:375px) {
	.overflow_div{	
    overflow-x: auto;
    overflow-x: scroll;
    white-space: nowrap;
    display: inline-flex;
    overflow-y: hidden;
    width: 377px;
    overflow-x: auto;
    margin-left: -33px;
	}	   
	.widget-area{
		padding:0px;
		margin-bottom: -30px !important;
	}



	.entry-thumbnail .image {
    background-repeat-x: no-repeat;
    background-repeat-y: no-repeat;
	background-size: 134px;
    margin-left: 1px;
}	

.entry-header {
    margin-bottom: 20px;
    margin-left: -13px;
    z-index: 111;
}
.margin_left{
	    float: left;
    width: 165px;
}		

.title {
    //color: grey;
    color: #fd0e35;
    font-size: 11px;
    font-weight: bold;
}
	}	

@media screen and  (max-width:640px)and  (min-width:540px) {
	
	
	.overflow_div{	
    overflow-x: auto;
    overflow-x: scroll;
    white-space: nowrap;
    display: inline-flex;
    overflow-y: hidden;
    width: 540px;
    overflow-x: auto;
    margin-left: -33px;
	}	   
	.widget-area{
		padding:0px;
		margin-bottom: -30px !important;
	}

	
	
.entry-thumbnail .image {
    background-repeat-x: no-repeat;
    background-repeat-y: no-repeat;
	background-size: 150px;
   
}

	

.entry-header {
    margin-bottom: 20px;
    margin-left: -13px;
    z-index: 111;
}
.margin_left{
	 float: left;
     width: 177px;
    margin-left:7px;
}	



    

		
.title {
    //color: grey;
    color: #fd0e35;
    font-size: 11px;
    font-weight: bold;
}	
	
}	
		
	
	
	
}
@media screen and  (min-width:415px) and  (max-width:732px) {
	
  

	.entry-thumbnail .image {
    background-repeat-x: no-repeat;
    background-repeat-y: no-repeat;
	background-size: 150px;
   
}	

.entry-header {
    margin-bottom: 20px;
    margin-left: -13px;
    z-index: 111;
}
.margin_left{
	 float: left;
     width: 180px;
     margin-left: 39px;
}			
.title {
    //color: grey;
    color: #fd0e35;
    font-size: 11px;
    font-weight: bold;
}	
	
}	
	@media screen and  (min-width:768px)and  (max-width:1024px) {
		
		
		
	}
	
</style>


<script>
jQuery(document).ready(function(){
	var type = window.location.hash.substr(1);
	console.log(type);
	if(type!=''){
		var obj = {};			                     
		var searchData = type.split('&');
		jQuery.each(searchData, function (i, value) {
			var attributes = value.split('=');
			obj[attributes[0]] = attributes[1];
		}); 
		if(('action' in obj) &&  obj.action=='search'){
			customsearchtrs(obj.search, obj.shop);
		}
	}
});
$(".main_cat_parent").on('click',function(event) {
	event.preventDefault();
    alert("hello");
});

function isDefined(function_name){
    return function_name !== undefined;
}
if(!isDefined('getCookie')){
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
}


function dishoom(id, page = 1){
		console.log('cat_clicked');
		var vendor_profile = "<?php

				$url = ($_SERVER[REQUEST_URI]);
				
				if (stripos($url, 'profile') !== false) {
					$url = str_ireplace('/profile/',"",$url);
					$url = str_ireplace('/',"",$url);
				}
				
				echo $url;
	?>";
    var tobaccoCookie = getCookie('tobacco_disclaimer_displayed');
    if(id == 52 && tobaccoCookie != 'tobacco'){
        jQuery('.tab-content').hide();
        // jQuery('.nav-tabs a[href="#cigarettes"]').tab('hide');
        console.log('returned by policy');
       return false;
    }else if(id == 52 && tobaccoCookie=='tobacco'){
        jQuery('.tab-content').show();
        jQuery('.tab-content>.tab-pane#cigarettes').show();
        jQuery('.tab-content>.tab-pane#cigarettes').css('display','block');
        console.log('inside elseif');
    }else{
        jQuery('.tab-content').show();
        console.log('Tab Content Displayed');
    }
	jQuery('.tab-pane').removeClass('active');
	jQuery('.child_cat_parent').removeClass('active');
	jQuery.post(
		'<?php echo get_site_url(); ?>/profile-search-product/',
		{

			cat_id:id,vendor_profile:vendor_profile,page:page,
		},
		function(data,success) {
			if (success) {
				jQuery(".content-area").html(data);
			}
			else{
				jQuery(".content-area").html("<h4>Error Getting data.</h4>");
			}	
		}
	);	
	
	return false;
}


function returnofchild(id){
	//alert(id);
	//jQuery(".content-area").html('<div id="preloader" ><div id="status" ></div></div>');
	
	
		var vendor_profile = "<?php

				$url = ($_SERVER[REQUEST_URI]);
				
				if (stripos($url, 'profile') !== false) {
					$url = str_ireplace('/profile/',"",$url);
					$url = str_ireplace('/',"",$url);
				}
				
				echo $url;
	?>";
	
	jQuery.post(
		'<?php echo get_site_url(); ?>/profile-search-product/',
		{

			cat_id:id,vendor_profile:vendor_profile,
		},
		function(data,success) {
			//alert(data);
			if (success) {
				//alert("yes");
				jQuery(".content-area").html(data);
			}
			else{
				jQuery(".content-area").html("<h4>Error Getting data.</h4>");
			}	
		}
	);	
	
	return false;
}

</script>

<!---start search box--->	

<div class="main-maa-muu">
	<div class="trs-custom-cat-widget">
		<div class="qwerty"></div>
			<div class="before-list">
				<?php
					//echo "<pre>";
					$terms = get_terms( array(
						'taxonomy' => 'product_cat',
						'hide_empty' => false,
						
					) );
					
					$main_arr = array();
					
					foreach($terms as $main_cat){
						
						//print_r($main_cat);
						$arr_main = (array) $main_cat;
						
						
						
						$get_parent = $main_cat->parent;
						
						if($main_cat->parent == 0){
							$parents[$arr_main['term_id']] = $arr_main;
						}
						$part_val = (!empty($main_arr[$get_parent]))?$main_arr[$get_parent]:'';
						$data_arry = array();
						if(!empty($part_val)){
							$data_arry = $part_val;
							$data_arry[] = $arr_main;
						}
						else{
							$data_arry[] = $arr_main;
						}
						
						$main_arr[$get_parent] = $data_arry;
						
					}
					
					//print_r($parents);
					
					echo '<div class="container">
								<div class="row">
									<div class="col-md-12">
										
										<ul class="nav nav-tabs main_cat_parent overflow_div" role="tablist">';
					
						foreach($parents as $running_to_the_time){
						?>
						
							
											<li onclick="return dishoom(<?php echo $running_to_the_time['term_id']; ?>);" role="presentation" class="main_cat_parent topper" id="<?php echo $running_to_the_time['term_id']; ?>"><a  class="main_cat_parent" href="#<?php echo $running_to_the_time['slug']; ?>" aria-controls="<?php echo $running_to_the_time['slug']; ?>" role="tab" data-toggle="tab"><?php echo $running_to_the_time['name']; ?></a></li>
								
						
						<?php
						}
						  
									echo "</ul>";  
						//echo "<pre>";
						unset($main_arr[0]);
						echo '<div class="tab-content">';
						
						foreach($main_arr as $par => $valalala){
							echo '<div role="tabpanel" class="tab-pane" id="'.$parents[$par]['slug'].'">';
							echo '<ul class="nav nav-tabs overflow_div " role="tablist " >';
							
								foreach($valalala as $lets_go){
									echo '<li role="presentation"  class="child_cat_parent" onclick="return returnofchild('.$lets_go['term_id'].');" id="'.$lets_go['term_id'].'" class=""><a href="#'.$lets_go['slug'].'" style="color:grey;"  aria-controls="'.$lets_go['slug'].'" role="tab" data-toggle="tab">'.$lets_go['name'].' </a></li>';
								}
							
							echo '</ul>';
							echo '</div>';
						}
							
						
						echo '		
									
									</div>			
								</div>			
							</div>			
						</div>		';
						  
					

				?>
			</div>
	</div>
</div>


<?php

}
		


} // Class trs_custom_cat_widget_sb ends here



// Creating the widget 
class trs_advance_search_sb extends WP_Widget {

function __construct() {
parent::__construct(

// Base ID of your widget
'trs_advance_search_sb', 

// Widget name will appear in UI
__('Trs advance search widget', 'trs_advance_search_sb_domain'), 

// Widget description
array( 'description' => __( 'Advance search to show on', 'trs_advance_search_sb_domain' ), ) 
);
}

// Creating widget front-end

public function widget( $args, $instance ) {
	
	global $wp_query;
$post_type = isset($wp_query->query['post_type']) ? $wp_query->query['post_type'] : 'post';
if (is_array($post_type)) {
    $post_type = reset($post_type);
}
	
?>

<style>

.site-main{
	margin-top: -40px;
}

.qwerty{
	margin-top: 30px;
}

.nothing-search {
    float: right;
    padding-top: 6px;
}

.loader {
    background: url(http://4.bp.blogspot.com/-hO_3iF0kUXc/Ui_Qa3obNpI/AAAAAAAAFNs/ZP-S8qyUTiY/s200/pageloader.gif);
}



</style>


<script>

function customsearchtrs(search_term = '', shop_name=''){
	
	var url      = window.location.href;     // Returns full URL
	var check_url = url.indexOf("/profile/");
	var vendor_profile = "<?php

				$url = ($_SERVER[REQUEST_URI]);
				
				if (stripos($url, 'profile') !== false) {
					$url = str_ireplace('/profile/',"",$url);
					$url = str_ireplace('/',"",$url);
				}
				
				echo $url;
	?>";
	var search_value = jQuery('#customsearchtrs_id').val();
	
	search_term = (search_term=='')?search_value:search_term;
	shop_name = (shop_name=='')?vendor_profile:shop_name;
	shop_name = $('.entry-data .entry-header .entry-title a').text();
	if(check_url != "-1"){
		jQuery.post(
            '<?php echo get_site_url(); ?>/profile-search-product/',
            {
               
				
				search_product: search_term,vendor_profile:shop_name,
            },
            function(data,success) {
				//alert(data);
                if (success) {
					//alert("yes");
					jQuery(".content-area").html(data);
                }
				else{
					alert("no");
				}	
            }
        );	
		
		return false;
		
	} 
	else{
		console.log("meow");
	}
	
	return false;
	
}

</script>

<!---start search box--->	
<div class="qwerty"></div>
	<div class="before-list">
		<p class="result-count"><?php

		
				$paged = max(1, $wp_query->get('page'));
				$per_page = $wp_query->get('posts_per_page');
				$total = $wp_query->found_posts;
				$first = ( $per_page * $paged ) - $per_page + 1;
				$last = min($total, $wp_query->get('posts_per_page') * $paged);

				if ($total <= $per_page || -1 === $per_page){ 
					//show postcode user enter value  
					if(isset($_COOKIE['location'])) {
					echo "Delivering to <a href=".esc_url(home_url('/')).">".$_COOKIE['location']."</a>" ;
					}else{
						printf(_n('Showing the single result', 'Delivering to %d results', $total, 'foodpicky'), $total);
					}
				} else {
					printf(_nx('Showing the single result', 'Showing %1$d&ndash;%2$d of %3$d results', $total, '%1$d = first, %2$d = last, %3$d = total', 'foodpicky'), $first, $last, $total);
				}
				
				$queried_object = get_queried_object();
				$title = (!empty($queried_object) && !empty($queried_object->post_type) && $queried_object->post_type=='azl_profile')?$queried_object->post_title:'shop';
				?>			
				<div class="nothing-search">
					<label>
						<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
						<input type="search" class="search-field" id="customsearchtrs_id" size="30"
							placeholder="<?php echo esc_attr_x( "Search {$title} for product..", 'placeholder' ) ?>"
							value="<?php echo get_search_query() ?>" name="s"
							title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" onkeyUp="vendorsearchkey($(this).val())" autocomplete="off"/>
					<div  id="searchprodinvendor"><ul> </ul></div>
					</label>
					<button type="submit" onclick="return customsearchtrs();" class="search-submit "><mannu id="custom_search_mannu"><?php echo esc_attr_x( 'Search', 'submit button' ) ?></mannu> &nbsp;&nbsp;&nbsp;<i class="fa fa-search fa-rotate-90"></i></button>
				</div>
	</div>



<?php

}
		


} // Class trs_advance_search_sb ends here

// Auto complete ajax fill data for search product according to author
           //      add_action('autocompleteAllprodlist','autocompleteAllprodlist');
	            add_action('autocompleteAllprodlist','autocompleteAllprodlist');
	 function autocompleteAllprodlist(){
	global $wpdb;
     $qry = "SELECT $wpdb->posts.post_title,$wpdb->posts.ID FROM $wpdb->posts WHERE 
                                              $wpdb->posts.post_status ='publish' AND 
                                              $wpdb->posts.post_type ='product'";
     $posts = $wpdb->get_results($qry);
	 ?><script type='text/javascript'> var allprod = new Array(); var imgprod = new Array();<?php
	 foreach($posts as $post){ 
       if(has_post_thumbnail($post->ID)){
		   $imgsrc =wp_get_attachment_image_src(get_post_thumbnail_id($post->ID));
	   }
       else{ $imgsrc ="NO"; }?>
		  allprod.push("<?php echo $post->post_title; ?>"); 
		  imgprod.push("<?php echo $imgsrc[0]; ?>");
	<?php } ?></script><?php
}	 
 	add_action('wp_enqueue_scripts','autocompscriptadd');
		function autocompscriptadd(){
		wp_enqueue_style( 'autocomplstyle', plugins_url('custom-cat-widget/customstyle.css') );
		wp_enqueue_script('autocompscript',plugins_url('custom-cat-widget/autocompscript.js'), array(), '1.0.0', true );
		wp_localize_script('autocompscript','autocompturl',array('ajax_url'=>admin_url('admin-ajax.php'),)); 
	}
add_action('loadallproductforautocomplete','loadallproductforautocomplete',10,1);

 function loadallproductforautocomplete($url){
     global $product;
     global $woocommerce;
     global $wpdb;
		  $url = "%".$url."%";
				$qr ="SELECT $wpdb->posts.post_author FROM $wpdb->posts where 
				                                            $wpdb->posts.guid LIKE '$url' AND 
                                                            $wpdb->posts.post_type ='azl_profile' AND
                                                            $wpdb->posts.post_excerpt='Convenience Store'";
				$auth_id = $wpdb->get_results($qr);
       //           print_r($aut_id);
	 /*   $qry = "SELECT $wpdb->posts.post_author FROM $wpdb->posts where 
				                                     $wpdb->posts.post_title  = '$shop' and 
													 $wpdb->posts.post_type='azl_profile' and
													 $wpdb->posts.post_excerpt='Convenience Store'";
              $auth_id = $wpdb->get_results($qry);  */
	    $auth = $auth_id[0]->post_author; 
		$querystr = "    SELECT $wpdb->posts.post_title,$wpdb->posts.ID     FROM $wpdb->posts     WHERE 
                              	$wpdb->posts.post_status = 'publish' AND
 								$wpdb->posts.post_type = 'product' AND
                                $wpdb->posts.post_author = 	$auth";
		$posts =  $wpdb-> get_results($querystr);
		?><script type="text/javascript">var parr = new Array();  var imgarr = new Array();	             
		<?php
		foreach($posts as $post)
		{
			if(has_post_thumbnail($post->ID)){
				 $imgsrc  = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ));
			} else{ $imgsrc ="No"; }
		   ?>  parr.push("<?php echo  (string)($post->post_title); ?>"); 
		       imgarr.push("<?php echo $imgsrc[0]; ?>");
		   <?php	
		  
		} 
		?></script><?php
    wp_reset_postdata();	
	 }
	 







