<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

?>
<div id="custom-div_add">
	<div class="col-md-6 col-sm-6 col-xs-6">
		<div class="img-check">
			<div onClick="dispatch_previousstep();" role="menuitem" style="cursor: pointer;">
				<img src="/wp-content/uploads/act_home.png" class="icon_width" style="float:right; "/>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<div class="img-check">
			<div onClick="dispatch_nextstep();" role="menuitem" style="cursor: pointer;">
				<img src="/wp-content/uploads/dact_cards.png" class="icon_width" style="float:left;"/>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<p class="check-h1">DELIVERY ADDRESS</p>
		<p class="check-text">An adult (18+) with valid ID must be present to sign for delivery.</p>
	</div>

	<hr class="hr_line" style="width:36%;height:1px;display:inline-block;margin:0; background-color:#aaa;"/>

	<?php
		$user_id = get_current_user_id();
		$add_1 = get_user_meta($user_id, 'billing_address_1');
		$add_2 = get_user_meta($user_id, 'billing_address_2');
		$p_cde = get_user_meta($user_id, 'billing_postcode');
		$p_num = get_user_meta($user_id, 'billing_phone');

		$meta_key = "checkout_saved_address";

		$addresses = get_user_meta($user_id, $meta_key);
		 // print"<pre>";print_r($addresses);
		if(!empty($addresses[0]) && count($addresses[0]) > 0) :
	?>
<script>
jQuery(document).ready(function(jQuery) {
	jQuery('#address-checkout').addClass('hidesheak_bc');
});
</script>
	<div id="address-add-div">
	    <div id="over_add_flow">
			<div class="row">
				<?php $i = 0;$uniqe = uniqid(); ?>
				<?php foreach($addresses[0] as $key=>$addr): ?>
					<div class="row checkout_saved_addresses_row" id="fm_row" style="margin: 0 15px !important;">
						<?php $check = $key; ?>
						<div class="col-md-1 col-sm-1 col-xs-1" style="padding: 0 0;">
							<input data-addressid="addVal<?=$check?>" class="selectRadio radio_box1" id="<?=$i?>" type="radio" name="browser" value="<?php echo $check;?>" <?php echo ($i==0) ? "checked" : ""; ?>><br>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8" style="padding: 0 0;">
							<div>
								<div class="add_input" id="addVal<?=$check?>">
									<input class="<?=$i?>add1" name="hadd_1" type="hidden" value="<?=$addr['address_1']?>" />
									<input class="<?=$i?>add2" name="hadd_2" type="hidden" value="<?=$addr['address_2']?>" />
									<input class="<?=$i?>phne" name="hphone" type="hidden" value="<?=$addr['phone']?>" />
									<input class="<?=$i?>pstc" name="hpostc" type="hidden" value="<?=$addr['postcode']?>" />
								</div>
								<span class="ca_spn_h1"><?php echo $addr['address_1']; ?></span>
								<br/>
								<span class="ca_spn"><?php echo $addr['address_2']; ?></span>
								<br/>
								<span class="ca_spn"><?php echo $addr['postcode']; ?></span>
								<br/>
								<span class="ca_spn">
									<span class="custom_phone_number_span"><?php echo formatPhoneNumber($addr['phone']); ?></span>
								</span>
								<br/>
							</div>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2 col-sm-offset-1" style="padding: 0 0;">
							<span class="del"><a class="btn-dlt" id="<?=$check?>" href="javascript:void(0);">Delete</a></span>
						</div>
						<?php $i++; ?>
					</div>
				<?php endforeach; ?>
				<?php
				/* This code has been changed to accomodate postcode validation in stripe from new textbox
				* and to fix address implementation on checkout
				*/
				$defaulthidden=0;
				if(!empty($addresses[0])){
					$defaulthidden=1;
				}else{
					$defaulthidden=0;
				}
				?>
				<input type="hidden" id="hidden_selected" name="hidden_selected" value="<?php echo $defaulthidden; ?>">
			</div>
	</div>
	</div>

	<div class="clearfix"></div>
	<div id="a_div">
	  <div class="col-md-6 col-xs-12 col-sm-4" class="saved_add_address_anchor" style="padding-left: 0px;padding-right: 0px;color: #fd0e35;font-size: 16px;">
		<span style="text-align:left;"><a style="margin-right: 57px;float:right;text-decoration:none;color:#424242;"class="a_margin" id="add-new-add" href="javascript:void(0);">Add New Address</a></span>
	  </div>
	</div>

	<?php else :?>

	<style>
		.hr_line{
			display:none !important;
		}
	</style>
	<?php endif; ?>
</div>

<script>
jQuery("#add-new-add").click(function() {

	var $el = jQuery(this);

	if($el.text() == "Add New Address"){
		$("#add-new-add").addClass('abrakadabra');
		

		/* This code has been changed to accomodate postcode validation in stripe from new textbox */
		jQuery('#hidden_selected').val(0);
		
		
		// jQuery("#billing_address_1").val('');
		jQuery("#billing_address_1").attr('value','');
		jQuery("#billing_address_2").attr('value','');
		jQuery("#billing_postcode").attr('value','');
		jQuery("#billing_phone").attr('value','');
		
		console.log('Clear the address fields here');

	}
	/* This code has been changed to accomodate postcode validation in stripe from new textbox */
	if($el.text() == "Saved address"){
		$("#add-new-add").removeClass('abrakadabra');
		jQuery('#hidden_selected').val(1);
		var radio_selector = jQuery('input.selectRadio:checked');
		var address_section = radio_selector.attr('data-addressid');
		var bil_add1 = jQuery('#'+address_section).find('input[name="hadd_1"]').val();
		var bil_add2 = jQuery('#'+address_section).find('input[name="hadd_2"]').val();
		var bil_phn = jQuery('#'+address_section).find('input[name="hphone"]').val();
		var bil_post = jQuery('#'+address_section).find('input[name="hpostc"]').val();

		jQuery("#billing_address_1").attr('value',bil_add1);
		jQuery("#billing_address_2").attr('value',bil_add2);
		jQuery("#billing_postcode").attr('value',bil_post);
		jQuery("#billing_phone").attr('value',bil_phn);
		
		var hidden_address_form = jQuery('#address-checkout').find('input');
		
		
		jQuery(hidden_address_form).each(function( index ) {
			if(jQuery(this).hasClass('error')){
				jQuery(this).removeClass('error');
			}
		});
		console.log('Fields are filled magically');
	}


	if($el.text() == "Add New Address"){
		$el.text($el.text() == "Add New Address" ? "Saved address": "Add New Address");
		$el.css('right','80px');

	}else{
		$el.text($el.text() == "Add New Address" ? "Saved address": "Add New Address");
		$el.css('right','80px');
	}
	// alert('tada');
	jQuery('#address-checkout').toggleClass('hidesheak_bc');
	jQuery('#address-add-div').toggle();
});

</script>


<div class="clearfix"></div>

<div id="address-checkout" class="woocommerce-billing-fields ">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php _e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<h3 class="bill-dets"><?php _e( 'Billing Details', 'woocommerce' ); ?></h3>

	<?php endif; ?>

	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>

	<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>

		<?php if ( $checkout->enable_guest_checkout ) : ?>

			<!-- <p class="form-row form-row-wide create-account"> -->
				<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'woocommerce' ); ?></label>
			<!-- </p> -->

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>

			<div class="create-account">

				<p><?php _e( 'Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'woocommerce' ); ?></p>

				<?php $acc=array();$akey="";$accp=array();$apkey="";$accp2=array();$ap2key=""; foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>

					<?php

							if($field['label']=="Account username")
							{
								$acc=$field;
								$akey=$key;
							}
							else if($field['label']=="Account password")
							{
								$accp=$field;
								$apkey=$key;
							}
							else
							{

					 			woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );

							}
					?>

				<?php endforeach;
					woocommerce_form_field( $akey, $acc, $checkout->get_value( $akey ) );
					woocommerce_form_field( $apkey, $accp, $checkout->get_value( $apkey ) );

				?>



				<div class="clear"></div>

			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>
	
	<?php $contry=array();$ckey=""; foreach ( $checkout->checkout_fields['billing'] as $key => $field ) : ?>
		<?php
			 if($field['label']=="Country")
			{
				$contry=$field;
				$ckey=$key;
			}
			else{
					woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
				}	 ?>


	<?php endforeach;

	woocommerce_form_field( $ckey, $contry, $checkout->get_value( $ckey ) );


	?>


</div>

<script>
function dispatch_previousstep(){
	jQuery('form.checkout a[href="#previous"]').click();

}
function dispatch_nextstep(){
	jQuery('form.checkout a[href="#next"]').click();
}
jQuery(document).ready(function(){
	var radio_selector = jQuery('input.selectRadio:checked');
	if(radio_selector){
		var address_section = radio_selector.attr('data-addressid');
		var bil_add1 = jQuery('#'+address_section).find('input[name="hadd_1"]').val();
		var bil_add2 = jQuery('#'+address_section).find('input[name="hadd_2"]').val();
		var bil_phn = jQuery('#'+address_section).find('input[name="hphone"]').val();
		var bil_post = jQuery('#'+address_section).find('input[name="hpostc"]').val();

		jQuery("#billing_address_1").attr('value',bil_add1);
		jQuery("#billing_address_2").attr('value',bil_add2);
		jQuery("#billing_postcode").attr('value',bil_post);
		jQuery("#billing_phone").attr('value',bil_phn);
	}
	

});

</script>
