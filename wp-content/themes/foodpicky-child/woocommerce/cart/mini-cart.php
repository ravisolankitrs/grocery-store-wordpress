<?php

/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<style>

.table_top{
    position: absolute;
    margin-top: 15px;
}
.table_style{
 background-color:#fd0e35;
 height:54px;
 border-radius:3px;
}
.caret_td_width{
 width:83px;
 
}
.mybasket_td_width{
width:127px;

}
.td_font_color{
    color:white;
	font-size: 15px !important;
    font-weight: 600 !important;
}

@media only screen and (max-width:414) {
	.product_list_widget {
		display:none;
		
	}
	
	.widget_shopping_cart .widget_shopping_cart_content .total strong{
		
		display:none !important;
	}
   .woocommerce-Price-amount{
		
	display:none !important;
	 }
	
	 .mini_cart_item_div{
		
		display:none !important;
	}
}

/* This code is added to remove Undo Message after removing a product from mini cart */
.custom_mini_cart_div > .woocommerce-message{
    display: none;
}

</style>
<div class="custom_mini_cart_div">
<?php do_action( 'woocommerce_before_mini_cart' ); ?>


<ul class="cart_list product_list_widget display_none <?php echo esc_attr( $args['list_class'] ); ?>" >

	<?php if ( ! WC()->cart->is_empty() ) : ?>

		<?php do_action( 'woocommerce_before_mini_cart_contents' ); ?>

		<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
					
					$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
					
					$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
					
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					
					?>
					<li id="pro_id_<?php echo $product_id; ?>" class="mini_cart_item_div <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
						<?php
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="remove" onclick="return delete_button_custom_delete('.$product_id.');" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
							esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
							__( 'Remove this item', 'woocommerce' ),
							esc_attr( $product_id ),
							esc_attr( $_product->get_sku() )
						), $cart_item_key );
						?>
						<?php if ( ! $_product->is_visible() ) : ?>
							<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . $product_name . '&nbsp;'; ?>
						<?php else : ?>
							<a href="<?php echo esc_url( '#' ); ?>">
								<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . $product_name . '&nbsp;'; ?>
							</a>
						<?php endif; ?>
					
						<div class="row" >
							<div class="col-md-12"><?php echo WC()->cart->get_item_data( $cart_item ); ?> </div>
							<input type="hidden" id="quant_<?=$product_id?>" value="<?=$cart_item['quantity']?>"/>
							<input type="hidden" id="dataPrice_<?=$product_id?>" value="<?=$cart_item['data']->get_price()?>"/>
							<input type="hidden" id="dataLine_<?=$product_id?>" value="<?=number_format($cart_item['line_total'],2);?>"/>
						</div>
						<div id="product_loader_<?php echo $product_id; ?>"></div>
						<div class="row" >
							<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7" style="padding-right:0px"><div style="display:inline-block;margin-top: 6%;"> 
								<a id="one_minus" href="javascript:void(0);" onclick="add_cart_list(<?php echo $product_id; ?>,-1,this);" style="float:left;margin-right: 10px !important; padding: 2px 0 0 0;"><i class="fa fa-minus-circle" style="color:#e85353"></i></a>
								
								<span id="quan_text_<?php echo $product_id; ?>" class="exact_quantity" style="float: left;"><?php echo $cart_item['quantity']; ?></span>
								
								<?php if(wc_get_product($product_id)->get_stock_status()=='instock'){ ?>
						   		<a id="one_plus" href="javascript:void(0);" onclick="add_cart_list(<?php echo $product_id; ?>, 1,this);" style="float:left;margin-left: 8px !important;padding: 2px 0 0 0;"><i class="fa fa-plus-circle" style="color:#41abb1" ></i></a>
								<?php }else{ ?>
								<a id="one_plus" href="javascript:void(0);" style="float:left;margin-left: 8px !important;padding: 2px 0 0 0;"><i class="fa fa-plus-circle" style="color:#41abb1" ></i></a>
								<?php } ?>
							</div> </div>
							
							
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="padding-right:0px"> <span class="quantity">
							<span class="woocommerce-Price-amount amount" id="line_pro_<?php echo $product_id; ?>">
							<span class="currency">
								<span class="woocommerce-Price-currencySymbol">
									<?php echo get_woocommerce_currency_symbol(); ?>
								</span>
							</span>
							<span id="line_total_<?=$product_id;?>">
								<?php 
                                echo number_format($cart_item['quantity'] * $cart_item['data']->get_price(),2); ?>
							</span>
						</span>
						</span> </div>
						
						</div>
					 
						
						
						
						
						<?php // echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
				
					
					</li>
					<?php
				}
			}
		?>

		<?php do_action( 'woocommerce_mini_cart_contents' ); ?>

	<?php else : ?>

		<li class="empty"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></li>

	<?php endif; ?>
	
	
	

</ul><!-- end product list -->

<?php if ( ! WC()->cart->is_empty() ) : ?>
	<?php
	

	WC()->cart->calculate_totals();
	$amount2 = WC()->cart->cart_contents_total;
	$t_amount = number_format(floatval(WC()->cart->cart_contents_total),2);
	?>
	<input type="hidden" id="total_hid_ajx" value="<?=$t_amount;?>" />
	<p class="total"><strong><?php _e( 'Subtotal', 'woocommerce' ); ?>:</strong> <span class="minicart_subtotal"><?php echo WC()->cart->get_cart_subtotal(); ?></span></p>

	<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

	<p class="buttons">
		<a onclick="return checkuserlogin();" href="<?php echo WC()->cart->get_cart_url(); ?>" class="button checkout wc-forward">Checkout</a>		
		<!-- this code has been hidden to custom change the URL-->
		<?php //do_action( 'woocommerce_widget_shopping_cart_buttons' ); ?>
	</p>

<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
</div>
 
<div class="row custom_micro_cart">
	<div  class="col-md-3 table_style">
		<table>
			<thead class="table_top">  
			    <tr>  
					<td  class="caret_td_width td_font_color"><?php echo WC()->cart->get_cart_contents_count(); ?></td>
					<td  class="mybasket_td_width td_font_color"><a onclick="return checkuserlogin();" href="<?php echo WC()->cart->get_cart_url(); ?>" class="td_font_color">My Basket</a></td>
					<td  class="td_font_color"> <?php echo WC()->cart->get_cart_total(); ?></td>
			    </tr>
			</thead>
		</table>
	</div>  
</div>





