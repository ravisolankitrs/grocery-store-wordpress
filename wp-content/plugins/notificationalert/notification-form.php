 <form method="post" action="" id="frmnotificationsend" enctype="multipart/form-data">

	
    <table id="tbldevonotidication">
        <thead>
            <tr>
                <td>
                    <label><input type="checkbox" name="user_type" id="specific-user" class="selectdeviceforuser" value ="specific-user" >Selected user</label>
                </td>
                <td>
                    <label><input type="checkbox" name="choosealluser"  value ="alluser" class="selectdevicealluser" id="alluserforspecifichk" >All User</label>
                </td>
                <td>
                    <label><input type="radio" name="chooseandroiduser" value ="allandroid" class="selectdeviceforuser" id="allandroidapps" >All Android</label>
                </td>
                <td>
                    <label><input type="radio" name="chooseiosuser" id="alliosapps" class="selectdeviceforuser" value ="allios" >All Ios</label>
                </td>
                <td>
                    <label><input type="radio" name="choosewebuser" id="allwepapps" class="selectdeviceforuser" value ="allweb" >All Web</label>
                </td>
             </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4" class="user_list_row" style="display:none;">
                        
                    <div class="notification-user-list">
                        <div class="all-user-list-notify-title" ><span id ="all-user-list-label">User List :</span>
                        </div>
                        <select name="usrselectlist[]" id="selbxuserlst" multiple>
                            <option></option>
                            <?php getAllUserList(); ?>
                        </select>
                    </div>                    
                </td>
                <tr>
                <td colspan="4">
                    <div style="border:1px solid #ccc;"><label  class="all-usr-list-title"> Message title:						     <input type="text" id="msgtitlenotification" name="notificationtitle" placeholder="Message title type here" class="notification-msg-tb"></label>
                    </div>
                </td>
                </tr>
                <td colspan="4">
                    <div style="padding:5px;">
                     Short Description :<br>
                    <textarea id="msgshortdescription" name="notificationshortdesc"  cols="104" rows="2"></textarea>
                    </div>
                </td>
            </tr>
            
            
            <td colspan="4">
                <span class="notify-heading">Send notification message</span><br>
                <textarea name="ntapinotificationmessage" id="tanotifytitlemsg" rows="10" cols="50" placeholder="Type your message here"></textarea>
            </td>
            <tr>
                <td colspan="2">
                    <input type="hidden" name="ntapuploadnotificationimage" id="uploadnotifyimage">
                     <button class="set_custom_images button">Choose Image </button>
                     <br><br>
                     <label >Button text:  <input type="text" id="msgbtntext" name="notificationbttext" placeholder="Message button type here" class="notification-msg-tb"></label>
                     
                    
                </td>
                <td colspan="2">
                    <img id="choosenotifyimage">
                </td>
            </tr>
            <tr>
                <td colspan="4"><span style="color:red;font-size:12px;" id="error_msg_notifation"> </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:center">	
                    <label>
                        <input type="checkbox" name="isTestMode"  value ="1" class="selectdevicealluser execution_mode" id="notification_test_mode">
                        TestMode
                    </label> 
                    &nbsp;&nbsp;
                    <label>
                        <input type="checkbox" name="isLiveMode"  value ="1" class="selectdevicealluser execution_mode" id="notification_live_mode">
                        Live Mode
                    </label>                        
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:center">	               
                     <input type="submit" name="sendmsgnotification" id="sendmsgnotification" value="Send Notification">  
                </td>
            </tr>
         </tbody>
    </table>
 </form>
<?php 
 
function getAllUserList()
{
    $args = array(
        'meta_query'   => array(
			array(
				'key'     => 'fcm_token_data',
	 			'compare' => 'EXISTS'
			),
        ),
        'orderby'      => 'login',
        'order'        => 'ASC',
     ); 
    $usrobj = get_users($args);
    
    foreach($usrobj as $obj): ?>
	   <option class="user-listin-notification" id= "<?php echo $obj->ID;  ?>" value="<?php echo $obj->ID;  ?>"><?php echo $obj->user_email; ?></option>       
	<?php endforeach;
}
		
?>
<script type="text/javascript">
 
    // jQuery(function() { jQuery("#selbxuserlst").select2({
        // placeholder: 'Select an option',
        // multiple: true
    // }).focus(); } );
</script>
	 
