<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_edit_account_form' ); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<style>
#edit-accnt{
	float: right !important;
	
}
.errortext1{
	font-size:12px; 
	
}
.margin_left{
	margin-left:201px !important;
	
}
@media only screen and (max-width: 320px) {
   .errortext1 {
    font-size: 9px !important;
    line-height: 19px;
    }
	
	.ca_spn{
		margin-left:12px !important;
		
	}
}
</style>
<div id="gen-div">
<?php
$user_id = get_current_user_id();
$u_pnum = get_user_meta($user_id, 'phone');
?>
<h2 class="head-account">ACCOUNT DETAILS</h2>

<form class="woocommerce-EditAccountForm edit-account custom-form-css"  id="gen-input" action="" method="post">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-6 gen-name-gen">
			<span id="general_head">General</span>
		</div>
		<div id="show-div_gen" class="col-md-6 col-sm-6 col-xs-6 gen-name-edit">
			<span  id="edit_link_gen"><a id="gen-link" class="change-color">Change</a></span>
		</div>
	  <hr id="edit-fm-line"/>
	  <div class="clearfix"></div>
	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
	  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
		<input type="text" class="custom-input" name="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" id="account_first_name" readonly="true" placeholder="First Name" />
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls-right">
		<input type="text" class="custom-input" name="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" id="account_last_name" readonly="true" placeholder="Last Name" />
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
		<input type="email" class="custom-input" name="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" id="account_email" readonly="true" placeholder="Email" />
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls-right">
		<div class="input-group">
				<div class="input-group-btn">
					<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height: 40px;">
					  +44
					</button>
				</div>
				<input type="text" class="custom-input custom_phone_number_input" name="account_phone" value="<?php echo formatPhoneNumber($u_pnum[0]); ?>" id="account_phone" readonly="true" placeholder="Phone Number"/>
			</div>
	
	  </div>
	 
	  <div id="edit-div_gen">
		  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
			<input type="button" value="Cancel" id="cancel_edit_btn" class="custm-btn btn-csl" />
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
			<input type="button" value="Save" onclick="save_gen_data();" name="save_gen-info" class="custm-btn btn-save" />
		  </div>
	  </div>
	</div>
	  
	 

</form>
</div>
<script>
jQuery(document).ready(function() {
	jQuery( ".custom_phone_number_input" ).each(function( index ) {
			var phone_val = jQuery( this ).val();
			var phone_val = format_my_number(phone_val);
			jQuery( this ).val(phone_val);
	});
	jQuery( ".custom_phone_number_span" ).each(function( index ) {
		var phone_val = jQuery( this ).html();
		var phone_val = format_my_number(phone_val);
		jQuery( this ).html(phone_val);
	});
	
	jQuery("#gen-link").click(function() {		
		jQuery("#show-div_gen").hide();
		jQuery("#edit-div_gen").show();	
		
		jQuery('#gen-input input').each(
		function(index){  
			var input = jQuery(this).prop('readonly', false);
		});
	});
	
	jQuery("#cancel_edit_btn").click(function() {
		jQuery("#edit-div_gen").hide();
		jQuery("#show-div_gen").show();
		
		jQuery('#gen-input input').each(
		function(index){  
			var input = jQuery(this).prop('readonly', true);
		});
	});		
	
	
	jQuery(".custom_phone_number_input").keypress(function(key) {
		//console.log(key.charCode + "\n\n");
		
		if(key.charCode < 48 || key.charCode > 57) return false;
		// $('#account_phone').val($('#account_phone').val().replace(/[^\d]/g, ""));
		
		var value = jQuery(this).val();
		var length = value.length;
		switch(length){
			case 4:
				jQuery(this).val(value+' ');
				break;
				
			case 8:
				jQuery(this).val(value+' ');
				break;
				
			case 14:
				return false;
		}	
		
		
	});	
		


	
});

$(".card_numb_class").keyup(function(){
	
	alert("asdas");
        var $this = $(this);
        if ((($this.val().length+1) % 5)==0){
            $this.val($this.val() + " ");
        }
    });  

function save_gen_data(){
	var f_name = jQuery("#account_first_name").val();
	var l_name = jQuery("#account_last_name").val();
	var e_mail = jQuery("#account_email").val();
	var p_nume =  jQuery("#account_phone").val();
	/* if(p_nume !== ""){
		p_nume = "+44" + p_nume;
	} */
	var base_url = window.location.origin; 
	jQuery.ajax({
		url : base_url+"/wp-content/themes/foodpicky-child/update_myaccount_ajax.php",
		type : "POST",
		data : {'gen_data':"gen_data",'f_name':f_name,'l_name':l_name,'e_mail':e_mail,'p_nume':p_nume},
		dataType:'json',
		success : function(data) {              
			location.reload(true);
		}
	});
}
</script>

<div id="pwd-div">
<form class="woocommerce-EditAccountForm edit-account custom-form-css"  id="pwd-input" action="" method="post">
	<div class="row" id="show-div_pwd">
	  <div class="col-md-6 col-sm-6 col-xs-6 gen-name-gen">
		<span id="general_head">Password</span>
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-6 gen-name-edit">
		<span  id="edit_link_gen"><a id="pwd-link" class="change-color">Change</a></span>
	  </div>
	  <hr id="edit-fm-line"/>
	  <div class="clearfix"></div>
	  <div class="col-md-12 col-sm-12 col-xs-12 gen-name-full">
		<input type="password" class="custom-input gen-12" name="password_current" id="password_current" readonly="true" placeholder="Current Password" value="***********"/>
	  </div>
	</div>
	<div class="row" id="edit-div_pwd">
	  <div class="col-md-12 col-sm-12 col-xs-12 gen-name-gen">
		<span id="general_head">Password</span>
	  </div>
	  <hr id="edit-fm-line"/>
	  <div class="clearfix"></div>
	  <div class="gen-name-full">
			<div class="col-md-12 col-sm-12 col-xs-12 gen-name-full bbccc fea_control">
				<span class="hide errorpayment" id="Error_msg">      
					<span class="errortext" style="width:13%;">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/error-triangle.png" class="img_tringle" alt="image"/>					 
					</span>      
					<span class="errortext1" style="width:70%;">Sorry, Current password is not correct.</br>Please re-enter your details and try again.</span>
					<span class="errortext" style="width:15%;">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/close.png" alt="image" class="img-cross"/>
					</span>
				</span>
				<span class="hide errorpayment" id="Error_confirm_msg">      
					<span class="errortext" style="width:13%;">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/error-triangle.png" class="img_tringle" alt="image"/>					 
					</span>      
					<span class="errortext1" style="width:70%;">Sorry, New password does not matched.</br>Please re-enter your details and try again.</span>
					<span class="errortext" style="width:15%;">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/close.png" alt="image" class="img-cross"/>
					</span>
				</span>
			</div>
		        
	        
	

		<div class="col-md-12 col-sm-12 col-xs-12 gen-name-full">
			<input type="password" class="custom-input gen-12" name="current_passwords_1" id="current_passwords_1" placeholder="Current Password" />
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 gen-name-full">
			<input type="password" class="custom-input gen-12" name="password_1" id="passwords_1" placeholder="New Password" />
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 gen-name-full">
			<input type="password" class="custom-input gen-12" name="password_2" id="passwords_2" placeholder="Confirm Password" />
		</div>
	    <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
			<input type="button" value="Cancel" id="cancel_pwd_btn" class="custm-btn btn-csl" />
		</div>
	    <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
		   <input type="button" value="Save" onclick="save_pwd_data();" name="save_pwd-info" class="custm-btn btn-save" />
	    </div>
	 </div> 
	</div>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>
</div>
<script>
jQuery(document).ready(function() {
	jQuery("#pwd-link").click(function() {
		jQuery("#show-div_pwd").hide();
		jQuery("#edit-div_pwd").show();
	});
	
	jQuery("#cancel_pwd_btn").click(function() {
		jQuery("#edit-div_pwd").hide();
		jQuery("#show-div_pwd").show();
	});
});

function save_pwd_data(){
	var curpwd_1 = jQuery("#current_passwords_1").val();
	var pwd_1 = jQuery("#passwords_1").val();
	var pwd_2 = jQuery("#passwords_2").val();
	
	if(pwd_1 == pwd_2) {
		var base_url = window.location.origin; 
		jQuery.ajax({
			url : base_url+"/wp-content/themes/foodpicky-child/update_myaccount_ajax.php",
			type : "POST",
			data : {'curpwd_data':curpwd_1,'pwd_data':"pwd_data",'password':pwd_2},
			success : function(data) {
				if(data!=1){
					// alert('Invalid Current Password');
					$('#Error_confirm_msg').addClass("hide");
					$('#Error_msg').removeClass("hide");
				}else{
					location.reload(true);
				}
			}
		});
	}else {
		$('#Error_msg').addClass("hide");
		$('#Error_confirm_msg').removeClass("hide");
	}
}
</script>

<div id="adr-div">
<?php
$user_id = get_current_user_id();
$add_1 = get_user_meta($user_id, 'billing_address_1');
$add_2 = get_user_meta($user_id, 'billing_address_2');
$p_cde = get_user_meta($user_id, 'billing_postcode');
$p_num = get_user_meta($user_id, 'billing_phone');

	$meta_key = "checkout_saved_address";
	
	$addresses = get_user_meta($user_id, $meta_key); ?>
		
<form class="woocommerce-EditAccountForm edit-account custom-form-css"  id="adrs-input" action="" method="post">
	<div class="row">
	  <div class="col-md-6 col-sm-6 col-xs-6 gen-name-gen">
		<span id="general_head">Address</span>
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-6 gen-name-edit" id="show-div_addrs">
		<!--<span  id="edit_link_gen"><a id="addrs-link" class="change-color">Address</a></span>-->
		<span  id="edit_link_gen"><a id="addrs-add" class="change-color">Add Address</a></span>
	  </div>
	  
	  <hr id="edit-fm-line"/>
	  
	<div id="old-add-div">
		<div id="over_add_flow">
			<div class="row">		
<?php		//print"<pre>";print_r($addresses);die;
		if(!empty($addresses[0]) && count($addresses[0]) > 0) :
	// echo "hello";
?>

				<?php $i = 0;$uniqe = uniqid(); ?>
				<?php //print"<pre>";print_r($addresses); die; ?>
				<?php foreach($addresses[0] as $key => $addr): ?>
					<div class="row" style="margin: 0 20px !important;">
						<?php $check = $key; ?>
						<div class="col-md-1 col-sm-1 col-xs-1">
							<input class="selectRadio radio_box" id="<?=$i?>" type="radio" name="browser" value="address_1" style="margin-top:2px;" <?php echo ($i==0) ? "checked" : ""; ?>><label for="selectRadio" class="selectRadio1"></label><br>
						</div>
						<div style="text-align:justify;" class="col-md-8 col-sm-8 col-xs-8">
							<div>
								<div class="add_input" id="addVal<?=$check?>">
									<input class="<?=$i?>add1" name="hadd_1" type="hidden" value="<?=$addr['address_1']?>" />
									<input class="<?=$i?>add2" name="hadd_2" type="hidden" value="<?=$addr['address_2']?>" />
									<input class="<?=$i?>pstc" name="hpostc" type="hidden" value="<?=$addr['postcode']?>" />
									<input class="<?=$i?>phne" name="hphone" type="hidden" value="<?="".$addr['phone']?>" />
								</div>
								<span class="ca_spn_h1"><?php echo $addr['address_1']; ?></span><br/>
								<span class="ca_spn"><?php echo $addr['address_2']; ?></span><br/> 
								<span class="ca_spn"><?php echo $addr['postcode']; ?></span><br/>
								<span class="ca_spn">+44 <span class="custom_phone_number_span"><?php echo formatPhoneNumber($addr['phone']); ?></span></span><br/>
							</div>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-1" style="padding: 0 0;">
							<span class="del"><a class="btn-dlt chnge-clr sa" id="<?=$check?>" href="javascript:void(0);">Delete</a></span>
						</div>	 
						<?php $i++; ?>
					</div>
				<?php endforeach; endif; ?>				
			</div>
		</div>
	</div>
	 
		<div class="clearfix"></div>
		<div id='add_new_address' style='display:none'>
		  <div class="col-md-12 col-sm-12 col-xs-12 gen-name-full">
			<input type="text" class="custom-input gen-12" name="address_1" id="address_add" value="" readonly="true" placeholder="Address Line 1" />
		  </div>
		  <div class="col-md-12 col-sm-12 col-xs-12 gen-name-full">
			<input type="text" class="custom-input gen-12" name="address_2" id="address_2add" value="" readonly="true" placeholder="Address Line 2" />
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
			<input type="email" class="custom-input woocommerce-Input woocommerce-Input--email input-text" name="post_code" id="post_codeadd" value="" readonly="true" placeholder="Postcode"/>
		  </div>
		  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls-right">
			<div class="input-group">
				<div class="input-group-btn">
					<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height: 40px;">
					  +44
					</button>
				</div>
					<input type="text" class="custom-input woocommerce-Input woocommerce-Input--number input-text custom_phone_number_input" name="address_phone" id="phone_numberadd" value="<?php echo "".$addr[0] ?>" readonly="true" placeholder="Phone Number" />
			</div>
		  </div>	
		  <div class="row" id="edit-div_address">	  
			<div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
				<input type="button" value="Cancel" id="cancel_address_btn" class="custm-btn btn-csl" />
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
				<input type="button" value="Save" onclick="add_address();" name="save_adds-info" id="save_add" class="custm-btn btn-save" />
			</div>
		  </div>		  
		</div>
	</div>
</form>
</div>
<script>



jQuery(document).ready(function() {
	jQuery("#addrs-add").click(function() {
		
		// jQuery("#show-div_addrs").hide();
		jQuery("#edit-div_address").show();
		jQuery("#add_new_address").show(); 
		jQuery("#over_add_flow").hide();
		
		jQuery('#adrs-input input').each(
		function(index){  
			var input = jQuery(this).prop('readonly', false);
		});
	}); 
	
	jQuery("#cancel_address_btn").click(function() {
		jQuery("#edit-div_addrs").hide();
		jQuery("#show-div_address").show();
		jQuery("#add_new_address").hide();
		jQuery("#over_add_flow").show();
		
		jQuery('#adrs-input input').each(
		function(index){  
			var input = jQuery(this).prop('readonly', true);
		});
	});
});

function add_address(){
	var add_1 = jQuery("#address_add").val();
	var add_2 = jQuery("#address_2add").val();
	var p_cde = jQuery("#post_codeadd").val();
	var p_num =  jQuery("#phone_numberadd").val(); 
	/* if(p_num !== ""){
		p_num = "+44" + p_num;
	} */
	// alert("tada");
	var base_url = window.location.origin; 
	jQuery.ajax({
		url : base_url+"/wp-content/themes/foodpicky-child/update_myaccount_ajax.php",
		type : "POST",
		data : {'add_adrs_data':"add_adrs_data",'add_1':add_1,'add_2':add_2,'p_cde':p_cde,'p_num':p_num},
		dataType:'json',
		success : function(data) {              
			location.reload(true);
		}
	});
}



/*****************************************************************/

jQuery(document).ready(function() {
	jQuery("#addrs-link").click(function() {
		jQuery("#show-div_addrs").hide();
		jQuery("#edit-div_addrs").show();
		jQuery("#edit_address_form").show(); 
		jQuery("#over_add_flow").hide();
		
		jQuery('#adrs-input input').each(
		function(index){  
			var input = jQuery(this).prop('readonly', false);
		});
	});
	
	jQuery("#cancel_addrs_btn").click(function() {
		jQuery("#edit-div_addrs").hide();
		jQuery("#show-div_addrs").show();
		jQuery("#edit_address_form").hide();
		jQuery("#over_add_flow").show();
		
		jQuery('#adrs-input input').each(
		function(index){  
			var input = jQuery(this).prop('readonly', true);
		});
	});
});


function save_adrs_data(){
	var adrs_1 = jQuery("#address_1").val();
	var adrs_2 = jQuery("#address_2").val();
	var p_code = jQuery("#post_code").val();
	var p_numb = jQuery("#phone_number").val();

	var base_url = window.location.origin; 
	jQuery.ajax({
		url : base_url+"/wp-content/themes/foodpicky-child/update_myaccount_ajax.php",
		type : "POST",
		data : {'adrs_data':"adrs_data",'adrs_1':adrs_1,'adrs_2':adrs_2,'p_code':p_code,'p_numb':p_numb},
		dataType:'json',
		success : function(data) {              
			location.reload(true);
		}
	});
}
</script> 

<div id="adr-div">
<?php
$expire_date = get_user_meta($user_id, 'expire_date_1');
$card_number = get_user_meta($user_id, 'card_number_1');
$stripe_token = get_user_meta($user_id, 'stripe_token_1');

/* Stripe Cards Synchronisation */

$stripe_cus_id = get_user_meta( get_current_user_id(), '_stripe_customer_id', true);

if(!empty($stripe_cus_id)){
	$stripe = new WC_Gateway_Stripe();
	$response = $stripe->get_saved_cards( $stripe_cus_id );
}

?>
<form class="woocommerce-EditAccountForm edit-account custom-form-css"  id="cards-input" action="" method="post">
	<div class="row" id="show-div_card">
	  <div class="col-md-6 col-sm-6 col-xs-6 gen-name-gen">
		<span id="general_head">Payment</span>
		<?php /* $saved_methods = wc_get_customer_saved_methods_list( get_current_user_id() );
				print"<pre>";print_r($saved_methods);die; */ ?>
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-6 gen-name-edit">
		<span  id="edit_link_gen"><a id="card-link" class="change-color">Add Card</a></span>
	  </div>
	  <hr id="edit-fm-line"/>   
	  <div class="clearfix"></div>
		<?php if(!empty($response)) : foreach ($response as $item) : ?>
		 <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom:6px;">
			<span id="stp_p"><div class="col-md-1 col-sm-1 col-xs-2" style="padding: 0 0;">
													<?php if($item->brand=='Visa'){ ?>
														<img src="/wp-content/uploads/visa-logo.png" style="width: 34px; height: 34px;">
													<?php }else if($item->brand=='MasterCard'){ ?>
														<img src="/wp-content/uploads/mastercard.png" style="width: 34px; height: 34px;">
													<?php }else{ ?>
														<img src="/wp-content/uploads/ame_exe.png" style="width: 34px; height: 34px;">
													<?php } ?>
												</div>
												<?php echo ($item->last4);?></span>
			
			<span id="stp_e " class="fea-margin_left">Expires <?php echo $item->exp_month.'/'.$item->exp_year; ?></span><a id="stp_d" class="chnge-clr" onclick="deleteCard('<?=$item->id?>','<?=$stripe_cus_id?>');">Delete</a>
		 </div>
		<?php endforeach; endif;?>
	</div>
	<div class="row" id="edit-div_card">
	  <div class="col-md-12 col-sm-12 col-xs-12 gen-name-gen">
		<span id="general_head">Payment</span>
	  </div>
	  <hr id="edit-fm-line"/>
	  <div class="clearfix"></div>
	  
		<div id="err-txt_card" class="hide col-md-12 col-sm-12 col-xs-12 gen-name-full bbccc fea_control stripe_card_form"> 
			<span class=" errorpayment" id="Error_msg1">
				<span class="errortext" style="width:13%;">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/error-triangle.png" class="img_tringle" alt="image"/>					 
				</span>      
				<span class="errortext1" style="width:70%;">Sorry, the Card details you've enterd are wrong.</br>Please enter a valid credit card number.</span>

				<span class="errortext" style="width:15%;">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/close.png" alt="image" class="img-cross"/>
				</span>
			</span>
		</div>
		<div id="err-txt1_expiry" class="hide col-md-12 col-sm-12 col-xs-12 gen-name-full bbccc fea_control stripe_card_form">
			<span class=" errorpayment" id="Error_msg1">
				<span class="errortext" style="width:13%;">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/error-triangle.png" class="img_tringle" alt="image"/>					 
				</span>      
				<span class="errortext1" style="width:70%;">Sorry, the Expiry Date you've enterd are wrong.</br>Please enter correct expiry date.</span>

				<span class="errortext" style="width:15%;">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/close.png" alt="image" class="img-cross"/>
				</span>
			</span>
		</div>
		<div id="err-txt2_cvv" class="hide col-md-12 col-sm-12 col-xs-12 gen-name-full bbccc fea_control stripe_card_form">
			<span class=" errorpayment" id="Error_msg1">
				<span class="errortext" style="width:13%;">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/error-triangle.png" class="img_tringle" alt="image"/>					 
				</span>      
				<span class="errortext1" style="width:70%;">Sorry, the CVV details you've enterd are wrong.</br>Please enter correct CVV Code.</span>

				<span class="errortext" style="width:15%;">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/close.png" alt="image" class="img-cross"/>
				</span>
			</span>
		</div>
		<div id="err-txt3_postcode" class="hide col-md-12 col-sm-12 col-xs-12 gen-name-full bbccc fea_control stripe_card_form">
			<span class=" errorpayment" id="Error_msg1">
				<span class="errortext" style="width:13%;">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/error-triangle.png" class="img_tringle" alt="image"/>					 
				</span>      
				<span class="errortext1" style="width:70%;">Sorry, the Postcode you've enterd are wrong.</br>Please re-enter your Postcode and try again.</span>

				<span class="errortext" style="width:15%;">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/close.png" alt="image" class="img-cross"/>
				</span>
			</span>
		</div>
		
		<style>
			input[type=number]::-webkit-outer-spin-button,
				input[type=number]::-webkit-inner-spin-button {
					-webkit-appearance: none;
					margin: 0;
			}
			
			//Supports Mozilla
			input[type=number] {
				-moz-appearance:textfield;
			}


		</style>
		
	  <div class="col-md-12 col-sm-12 col-xs-12 gen-name-full">
		<input type="text" class="custom-input gen-12 card_numb_class" name="card_num" id="card_num" maxlength="19" value="" placeholder="Card Number" />
	  </div>
	  <div class="col-md-4 col-sm-4 col-xs-4 gen-name-cls">
		<input type="text" class="custom-input" name="expire_date" id="expire_date" value="" maxlength="8" placeholder="MM/YYYY" />
	  </div>
	  <div class="col-md-4 col-sm-4 col-xs-4 gen-name-cls-mid gen-name-cls">
		<input type="number" class="custom-input" name="cvv_numb" id="cvv_numb" value="" placeholder="CVV" maxlength="3" />
	  </div>
	  <div class="col-md-4 col-sm-4 col-xs-4 gen-name-cls-right">
		<input type="text" class="custom-input" name="bil_pstcde" id="bil_pstcde" value="" placeholder="Billing Postcode" />
	  </div>
	  <div class="clearfix"></div>
	  <div class="col-md-6 col-sm-6 col-xs-12 gen-name-cls">
		<input type="button" value="Cancel" id="cancel_card_btn" class="custm-btn btn-csl" />
	  </div>
	  <div class="col-md-6 col-sm-6 col-xs-12  gen-name-cls">
		<input type="button" value="Save" onclick="save_card_data();" name="save_adds-info" class="custm-btn btn-save" />
	  </div>
	</div>
</form>
</div>
<script>


jQuery(document).ready(function() {
	jQuery("#card-link").click(function() {
		jQuery("#show-div_card").hide();
		jQuery("#edit-div_card").show();
		
		
		jQuery('#cards-input input').each(
		function(index){  
			var input = jQuery(this).prop('readonly', false);
		});
	});
	
	jQuery("#cancel_card_btn").click(function() {
		jQuery("#edit-div_card").hide();
		jQuery("#show-div_card").show();
		
		jQuery('#cards-input input').each(
		function(index){  
			var input = jQuery(this).prop('readonly', true);
		});
	});
});


/* function save_card_data(){
	
	var card_num 	= jQuery("#card_num").val();
	var expire_date = jQuery("#expire_date").val();
	var expArray = expire_date.split( '/' );
	var exp_month_new = ( expArray[ 0 ] );
	var exp_year_new = ( expArray[ 1 ] );
		
	var cvv_numb	= jQuery("#cvv_numb").val();
	var bil_pstcde	= jQuery("#bil_pstcde").val(); 
	
	Stripe.setPublishableKey('pk_test_mwqOk1G6jZ1h0Kac1WazdRtt');
	var strtoken = '';
	var stripeResponseHandler = Stripe.card.createToken({
		  number: jQuery('#card_num').val(),
		  cvc: 	jQuery('#cvv_numb').val(),
		  exp_month: exp_month_new,
		  exp_year: exp_year_new, 
		  address_zip: jQuery('#bil_pstcde').val(), 
		},function(status, response){
			strtoken = response.id;
			// alert(strtoken);
			//console.log(response.id);
			var base_url = window.location.origin; 
			jQuery.ajax({
				
				url : base_url+"/wp-content/themes/foodpicky-child/update_myaccount_ajax.php",
				type : "POST",
				data : {'stripe_token':strtoken,'u_card_data':"u_card_data",'card_num':card_num,'expire_date':expire_date,'cvv_numb':cvv_numb,'bil_pstcde':bil_pstcde},
				dataType:'json',
				success : function(data) {              
					location.reload(true);
				}
			});
			return response.id;
		});
		
	// console.log(stripeResponseHandler);
	// console.log("token == "+strtoken);
	//alert(strtoken);
	//return false;
	
} */


function delete_card_data(){

	var base_url = window.location.origin; 
	jQuery.ajax({
		url : base_url+"/wp-content/themes/foodpicky-child/update_myaccount_ajax.php",
		type : "POST",
		data : {'delete_card':"delete_card"},
		dataType:'json',
		success : function(data) {              
			location.reload(true);
		}
	});
}

function stripeResponseHandler(status, response) {
	
	// var $form = jQuery('cards-inputs');
	
  if (response.error) { // Problem!

   console.log('Error in Token Generation');
   // jQuery("body").trigger('wc-stripe-error', {response: response, form: $form});
  } else { // Token was created!

    // Get the token ID:
    var token = response.id;
    $form.append($('<input type="hidden" name="stripe_token" value="' + token + '" />').val(token));
    $form.get(0).submit();

  }
}
</script>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
