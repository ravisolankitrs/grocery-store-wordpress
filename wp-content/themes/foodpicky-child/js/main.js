/**
 *
 *  FOR SEARCH PAGE
 *
 *  last modified - 27/oct/2017 mms
 *
 */
jQuery(".cat-parent ").find('> a').after('  <span class="getmylife"> <i class="fa fa-caret-down fa-2" aria-hidden="true"></i> </span>'); //w
jQuery(".cat-parent > .children ").hide();
//jQuery('#alcohalpopup').find('.modal-title b').text('Are you old enough to buy alcohal?');
//jQuery('#alcohalpopup').find('.modal-body p').html('<b>Alcohal is not for sale to anyone under the age of 18. You may need to a valid ID upon delivery.</b>');

jQuery( ".custom_phone_number_span" ).each(function( index ) {
	var phone_val = jQuery( this ).html();
	var phone_val = format_my_number(phone_val);
	jQuery( this ).html(phone_val);
});
function format_my_number(number){
	text = number.replace(/(\d{5})(\d{7})(\d{4})/, "$1 $2 $3");
	return text;
}


/* jQuery('#global_login_form').submit(function(e){
	e.preventDefault();
	console.log('login Submit Prevented');
	var loginData = jQuery("#global_login_form").serialize();
	loginData = btoa(loginData);
	console.log(loginData);
}); */

/* jQuery('#global_registration_form').submit(function(e){
	// e.preventDefault();
	// console.log('Registration Submit Prevented');
}); */

/* This code is Created by SB and modified by PJ to disable click on shops if closed */
var url = document.URL;
if(url.indexOf("https://devo.co.uk/?location") >= 0 || url.indexOf("https://devo.co.uk/stores/?location") >= 0){
	jQuery(".entry-more").find('.closed').addClass('closed_new');
	jQuery(".entry-more").find('.coming-soon').addClass('closed_new');
	jQuery(".status-publish:contains(Closed)").addClass('prevent-events');
}

var ComingSoonShops = jQuery(".status-publish:contains(Closed)").find('.coming-soon').closest('.status-publish').find('.entry-footer');

ComingSoonShops.html('<span class="meta-field minimum-order-amount "><span class="value coming-soon">Coming soon <span class="units"></span></span></span><span class="meta-field delivery-time "></span>');


/* Focus redirection on click in footer Popular Locations and home page load MMS Start  */


 var url = document.URL;
 
 if(url == "https://devo.co.uk/" || url == "https://devo.co.uk/#"){
        jQuery(document).ready(function(){
            setTimeout(function(){  jQuery(".location input[name='location']").focus(); 
                                    }, 1000);
            jQuery('.location [name=location]').css("font-size","17px");
            jQuery('.location [name=location]').css("font-size","17px");
            jQuery('.pac-container').find('.pac-item').css("font-size","17px");
           
        jQuery("#redirect_to_home").html('<li><a class="re_search" href="#">London</a></li><li><a class="re_search" href="#">Manchester - coming soon</a></li><li><a class="re_search" href="#">Birmingham - coming soon</a></li>');
        }); 
 
}else{
    //console.log('else');
    // jQuery("#redirect_to_home").html('');
        jQuery("#redirect_to_home").html('<li><a class="re_search" href="https://devo.co.uk/">London</a></li><li><a class="re_search" href="https://devo.co.uk/">Manchester - coming soon</a></li><li><a class="re_search" href="https://devo.co.uk/">Birmingham - coming soon</a></li>');
}

jQuery(document).ready(function(){
	
	
	jQuery(".widget_shopping_cart_content").on('click', 'a', function() {
		
		var pid = $(this).data('product_id');
		
		// $(this).closest('.entry-thumbnail ').addClass('product_extra_loader');
		
		// alert(pid);

		return false;
	});
	

	

	
	setTimeout(function(){
		
	/* 		$.getScript('https://devo.co.uk/wp-content/themes/foodpicky-child/js/custom.js', function()
	{
		// script is now loaded and executed.
		// put your dependent JS here.
	}); */

	
			
									}, 500);
     
	 	
	// $("#checkout_form_new").on('click', 'a[href="#next"]', function() {
		// $(this).after("<button style='width:356px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		// $(this).hide();

//		return false;
	// });
	 	 	
	$(".coupon input[name='apply_coupon']").on('click', function() {
		$(this).after("<button style='width:120px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide();

		//return false;
	});
	 
	 
	$(".buttons").on('click', '.custom_minicart_checkout_btn.checkoutcenter', function() {
		/* $(this).after("<button style='width:274px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide() */

		// return false;
	});

	
	 
	 
	 jQuery('.re_search').click(function(){
            jQuery('.location [name=location]').focus();

        });
});

 /* Focus redirection on click in footer Popular Locations and home page load MMS Ends  */
 
 
 /* Following Code is to Display Popup in Alcohal and Ciggrates category */
 
jQuery('#primary').on('click','a.add_to_cart_button',function(e){
    
    var alcohalCookie = getCookie('alcohal_disclaimer_displayed');
    var tobaccoCookie = getCookie('tobacco_disclaimer_displayed');
    
    var productDiv = $(this).closest('.profile-products,.product');
    // console.log(productDiv);
    if( (!productDiv.hasClass( "product_cat-alcohol" ) && !productDiv.hasClass("product_cat-cigarettes")))
	{ 
        return true;
    }
    
    if( alcohalCookie!='' && tobaccoCookie != ''){
        return true;
    }

    var cart_url = $(this).attr('href');
	
	if(productDiv.hasClass( "product_cat-alcohol" ) && alcohalCookie != 'alcohal'){
		jQuery('#alcohal_product').val(cart_url);
		jQuery('#alcohalpopup').modal('show');
        
        // console.log('alcohalCookie saved');
        return false;
	}else if(productDiv.hasClass( "product_cat-alcohol" ) && alcohalCookie == 'alcohal'){
        return true;
    }
    
    if( productDiv.hasClass("product_cat-cigarettes")  && tobaccoCookie != 'tobacco' ){
		jQuery('#tobacco_product').val(cart_url);
		jQuery('#tobaccopopup').modal('show');
       // setCookie("tobacco_disclaimer_displayed", 'tobacco', 10);
        // console.log('tobaccoCookie saved');
	}else if(productDiv.hasClass( "product_cat-cigarettes" ) && tobaccoCookie == 'tobacco'){
        return true;
    }
    return false;
    
}); 
 
jQuery(document).on('click', 'a.main_cat_parent',function(e){
    var ciggrateMenu = $(this).closest('li.main_cat_parent.topper');
    if( jQuery(ciggrateMenu).is("#52") )
    {
        var tobaccoCookie = getCookie('tobacco_disclaimer_displayed');
        if( tobaccoCookie != 'tobacco' ){
            jQuery('#tobaccopopup-2').modal('show');
        }
    }
});
 
jQuery('#alcohal_warning_allow').on('click',function(){
    var productUrl = jQuery('#alcohal_product').val();
	setCookie("alcohal_disclaimer_displayed", 'alcohal', 10);
    jQuery.get( productUrl, function( data ) {		
	    
        jQuery( document.body ).trigger( 'wc_fragment_refresh' );
    });
});
jQuery('#tobacco_warning_allow').on('click',function(){
	var productUrl=jQuery('#tobacco_product').val();
	
	jQuery.get(productUrl,function(data){
		
		jQuery(document.body).trigger('wc_fragment_refresh');
	});
}); 

jQuery('#tobacco_warning_allow-2').on('click',function(){
    setCookie("tobacco_disclaimer_displayed", 'tobacco', 10);
    dishoom(52);
});
 
 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
 /* Popup code ends here */
  
 
 
/* Following Code is to Display Popup When Add To cart Product */ 
  
  
  
  // jQuery('.profile_take_it_home').find('.entry-thumbnail').on('click',function(){
    
		// jQuery("#addtocart").modal('show');
 
// });



	jQuery( document ).ajaxSend(function( event, xhr, settings ) {
 
		var raw_data = (settings.data);
	
	// console.log(event);
	// console.log(xhr);
	// console.log(settings.url);
	
		if(raw_data !== undefined){
		
			if(raw_data.indexOf("product_id=") >= 0){
				var new_data = raw_data.split("product_id=");
				var prod_id = (new_data[1]);
				
				// alert(prod_id);
				
				jQuery('.post-'+prod_id).addClass('product_main_loader');
				jQuery('#'+prod_id).addClass('product_main_loader');
			}
 
		}
		
		
		if(settings.url.indexOf("add-to-cart=") >= 0){
				
			// jQuery('#addtocart').modal('show');
			console.log('18+ doc it is');
			var new_data = settings.url.split("add-to-cart=");
			var prod_id = (new_data[1]);
			
			// alert(prod_id);
			
			jQuery('.post-'+prod_id).addClass('product_main_loader');
			jQuery('#'+prod_id).addClass('product_main_loader');
		}
		
	});
 

	jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {		
		if(settings.url.indexOf("wc-ajax=add_to_cart") >= 0){
			
			// jQuery('#addtocart').modal('hide'); 
			console.log('ajaxSuccessSuccessfully');
			return false;
		}
		
		
		
		$('.mini_cart_item_div .remove').each(function() {
		  var pid = $( this ).data( "product_id" );
		  $( this ).attr('onclick','return delete_button_custom_delete('+pid+');')
		});
		 
	});
	
	
	function delete_button_custom_delete(pid){
		
		dataLayer.push({
            event: 'basket_delete_item_tapped',
            'productId': pid
        });
		
		// alert(pid);
		
		// $("#main_pro_id_"+pid).addClass('product_cart_remover');
 
		// $(".mini_cart_item_div .remove").closest('.panel-body').addClass('main_div_disable');
		
		// $("#pro_id_"+pid).addClass('product_cart_remover');
		
		var url      = window.location.href; 
		
		// alert(url);
		
		if(url.indexOf('/profile/') >= 0){
			
			 jQuery('#product_loader_'+pid).addClass('product_loader_m');
			 jQuery('#pro_id_'+pid).addClass('main_div_disable');
			 jQuery('#pro_id_'+pid).closest('.widget_shopping_cart_content').addClass('main_div_disable');
			 
		}
		 
		 
		
	}
	
	jQuery( document ).ajaxSend(function( event, xhr, settings ) {		
		if(settings.url.indexOf("/profile-search-product/") >= 0){
			
			jQuery('#primary').html('');
			jQuery('#primary').html('<div style="text-align:center"><img id="product_loader_m" style="border-radius:10px; text-align:center;" src="http://bigwheelbikes.com/content/Loading.gif"  /></div>');
			//jQuery('#primary').html('<div id="preloader"><div id="status" ></div>');
			
			//jQuery('.before-list').find('#custom_search_mannu').text('Loading...');
			console.log('search product loder ');
			return false;
		}
	});
	
	
	jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {		
		if(settings.url.indexOf("/profile-search-product/") >= 0){
			
			//jQuery('.before-list').find('#custom_search_mannu').text('Search');
			console.log('search product loder ');
			return false;
		}
	});
	
	// jQuery( document ).ajaxSend(function( event, xhr, settings ) {		
		// if(settings.url.indexOf("changeCartQuantity_js") >= 0){
			 
		// 	jQuery('.before-list').find('#custom_search_mannu').text('Search');
			// console.log('cart ajax Working ........ ');
			  
			  
			// return false;
		// }
	// }); 
	
	jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {
		if(settings.url.indexOf("changeCartQuantity_js") >= 0){
			 
			//jQuery('.before-list').find('#custom_search_mannu').text('Search');
			console.log('cart ajax Working ........ ');
			jQuery('.panel-body').find('.woocommerce-cart-form').removeClass('main_div_disable');
			jQuery('.panel-body').find('.mini_cart_item_div').removeClass('main_div_disable');
			jQuery('.media-body').find('.product_loader_main_cart').removeClass('product_loader_main_cart');
			// jQuery('.mini_cart_item_div').removeClass('main_div_disable');
			jQuery('#product_loader').removeClass('product_loader_m');
			jQuery('#one_plus').removeClass('disabled');
			jQuery('#one_minus').removeClass('disabled');
			
			return false;
		}
		
		if(settings.url.indexOf("profile-search-product") >= 0){
			 
			 jQuery(".nothing-search .search-submit ").show();
			 jQuery(".profile_loader_search").remove();
			
			return false;
		}
        
       
		
		jQuery('.product').removeClass('product_main_loader');
		jQuery('.product_type_simple').removeClass('product_main_loader');
		
		
		
		jQuery('.entry-thumbnail').removeClass('product_extra_loader');
		jQuery('.mini_cart_item_div').removeClass('product_extra_loader');
		
		jQuery('.entry-thumbnail').removeClass('product_extra_loader');
		
		
		 jQuery('.custom_mini_cart_div').removeClass('main_div_disable'); 
 
		jQuery(document).removeClass('product_loader_main_cart');
		
	});
	
	jQuery("#global_login_form [name=login]").click(function(){
		
		
		
		$(this).after("<button style='width:66px; margin-right: 30px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide()
		
		// return false;
	});	
		
		
	jQuery(".nothing-search .search-submit ").click(function(){
 
		$(this).after("<button style='width:102px; ' class='button profile_loader_search width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide()
		
		// return false;
	});	
	
	
	jQuery( "#global_registration_form [name=register]").click(function(){
 
		$(this).after("<button style='width:83px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide()
		
		// return false;
	});
		
	
	jQuery( ".search-form .search-submit").click(function(){
 
		$(this).after("<button style='width:74px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide()
		
		// return false;
	});

/* 	jQuery(".product_type_simple").click(function(){
		
		var pid = $(this).data('product_id');
		
		$(this).closest('.entry-thumbnail ').addClass('product_extra_loader');
		
		// alert(pid);
		
		// return false;
		
	});	 */
		
	// jQuery(".finish-btn").click(function(){
		
		// jQuery("#divLoading").remove();
		
		// return false;
		
	// });	
	
	
	jQuery(".mini_cart_item_div .remove").click(function(){
		
		var pid = $(this).data('product_id');
		
		// alert(pid);
		
		$("#main_pro_id_"+pid).addClass('product_cart_remover');
 
		$(".mini_cart_item_div .remove").closest('.panel-body').addClass('main_div_disable');
		
	});
    
    
/* 
5:14 PM 22 January, 2018
 */
 
jQuery( ".list-profile.type-azl_profile").on('click','a',function(){
    console.log('pakda');
    $(this).closest('.type-azl_profile').find('.entry-more').html("<button style='width:83px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
    return true;
 
    // $(this).after("<button style='width:83px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
    // $(this).hide()
    
}); 

// Code for stop button click on shop close
