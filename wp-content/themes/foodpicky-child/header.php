<?php
do_postcodes_fetch();
?>
<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <!--<![endif]-->

    <head>
    
        <?php $options = get_option(AZEXO_FRAMEWORK); ?>

        <meta charset="<?php bloginfo('charset'); ?>">

        <meta name="viewport" content="width=device-width">

        <link rel="profile" href="http://gmpg.org/xfn/11">		
        <link rel="pingback" href="<?php esc_url(bloginfo('pingback_url')); ?>"> 		

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	

	

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script>var dataLayer = []</script>


<!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,

0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.unset people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");

for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//devo.co.uk/wp-content/themes/foodpicky-child/js/mixpanel-2-latest.min.js".match(/^\/\//)?"https://devo.co.uk/wp-content/themes/foodpicky-child/js/mixpanel-2-latest.min.js":"//devo.co.uk/wp-content/themes/foodpicky-child/js/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);

mixpanel.init("bb3e3743c8bc97d8f843a183f7ac4fd6");mixpanel.track("init");</script><!-- end Mixpanel -->

	<!-- cdn.mxpnl.com/libs/mixpanel-2-latest.min.js -->	

<?php

if (!function_exists('has_site_icon') || !has_site_icon()) {

    if (isset($options['favicon']['url']) && !empty($options['favicon']['url'])) {

        print '<link rel="shortcut icon" href="' . esc_url($options['favicon']['url']) . '" />';

    }

}

?>

<?php wp_head(); ?>
<style>
<?php if(!is_checkout()): ?>
.woocommerce-error,.woocommerce-message{
	display:none !important;
}
<?php endif; ?>
</style>
<script>
    jQuery( document).on('click',".login, .register, #login-register-toggle",function( event ) {
        console.log('modal triggered');
        <?php $full_actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
        jQuery("input[name='_wp_http_referer']").each(function( index ) {
            var refrer_data = jQuery(this).attr("value");
            if(refrer_data == ''){
                jQuery(this).attr("value","<?php echo $full_actual_link; ?>");
                jQuery(this).val("<?php echo $full_actual_link; ?>");
            }
        });
        
        
    });
    
</script>
<?php     


$actual_link = "$_SERVER[REQUEST_URI]";

//echo "$actual_link";

if (strpos($actual_link, '/profile/') !== false) {
?>
    <!-- css use for profile page url--  <?php echo $actual_link ?>  -->

    <style>

    .content-area{
        width:100% !important;
    }

    .entry-thumbnail  .image {
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-size: 122px;
    }

    .card {
        min-height: 260px !important;
        max-height: 404px !important;
    }

    @media screen and (max-width:768px) and (min-width: 1024px)
        .margin_left{
            width: 222px  !important;
            float: left !important;
        }
    }

    .margin_left{
        width: 222px  !important;
        float: left !important;
    }
    </style>

<?php
}

$actual_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if ( $actual_link == "digitaltriggers.online/" || $actual_link == "digitaltriggers.online" ||  $actual_link == "devo.co.uk/" || $actual_link == "devo.co.uk" ) {
	//echo "$actual_link";
?>
    <script>
        jQuery(document).ready(function(){

            jQuery(document).on('click','.type-azl_profile',function(event){

                event.preventDefault();

                jQuery("html, body").animate({ scrollTop: 0 }, "slow");

                jQuery( "input[name='location']" ).focus();

                return false;

            });

        });

    </script>

    <!-- use for home page  for search page responsive page url  http://api.devo.co.uk/  -->

    <style>
    @media only screen and (max-width:375px){
        .entry-header{
            margin-left: 99px;
        }
        
        .badge {
            right: 24px !important;
            top: -1px  !important;
        }

        .entry-additions{
            margin-left:105px !important;
        }
    }



    @media only screen and (max-width: 360px){

        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
            width: 35%;
            margin-top: -13px !important;
            z-index: 1111;
            position: absolute;
            margin-left: 0px !important;
        }
		
        .static-header .container .azqf-query-form .wrapper .submit::after {
            content: "\f002";
            position: absolute !important;
            font-family: FontAwesome;
            /* top: 59% !important; */
            left: 23% !important;
            padding: 38px 14px 11px 17px !important;
            z-index: 1111 !important;
            -webkit-transform: translate(260%, -50%) scale(-1, 1);
            -khtml-transform: translate(260%, -50%) scale(-1, 1);
            -moz-transform: translate(260%, -50%) scale(-1, 1);
            -ms-transform: translate(260%, -50%) scale(-1, 1);
            transform: translate(260%, -50%) scale(-1, 1);
        }
        
        .profile-item .entry .entry-data .entry-title a {
            color: #414551;
            text-align: center !important;
            margin-left: 72px;
        }

        .entry-summary{
            text-align:center !important;
        }

        .entry-footer{
            text-align:center !important;
        }

        .entry-title{
            margin-left: -50px!important;
			text-align:left!important;
        }

        .container h3{
           text-align:center;
        }
        
        .profile-item .entry .entry-data .entry-title a {
            color: #414551;
            margin-left: -107px !important;
        }
        
        .badge {
            right: 24px !important;
            top: -1px  !important;
        }

        .entry-additions{
            margin-left: 98px !important;
        }

        .entry-more {
            margin-left: 2px !important;
        }
        
        #btnwth{
            margin-left: -153px!important;
            margin-top: 1px !important;
            width: 92% !important;
            position: absolute;
            z-index: 1111;
        }

        button.close {
            -webkit-appearance: none;
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            float: right;
            margin-left: 239px !important;
        }

        .modal-body {
            position: relative;
            padding: 15px;
            height: 287px;
        }

    }

    /* use for mobile */

    @media only screen and (max-width:412px){
        
        .container h3{
            text-align:center;
        }

        .entry-header{
            margin-left: 161px !important;
        }
        
        .entry-summary{
               margin-right: 50px!important;
			text-align: center;
        }

       .entry-footer{
            text-align:center !important;
       }
       
        .profile-item .entry .entry-data .entry-title a {
            color: #414551;
            margin-left: -168px;
        }



        .badge {
            right: 24px !important;
            top: -1px  !important;
        }



        .entry-additions{
            margin-left: 123px;
        }

        .entry-more {
            margin-left: 2px !important;
        }
    }
@media only screen and (max-width: 414px) and (min-width: 413px){
	
	
	.entry-summary{
            margin-right: 50px!important;
			text-align: center;
        }
		.custom_mms_t a{
			text-align: center!important;
			 margin-left: 17%;
		}
} 


    /* use for mobile */
    @media only screen and (max-width:320px){
        
        .container h3{
            text-align:center;
        }
        .entry-header{
            margin-left: -2px!important;
        }
        .badge {
            right: 24px !important;
            top: -1px  !important;
        }

        #btnwth{
            margin-left: -137px!important;
            margin-top: 1px !important;
            width: 92% !important;
            position: absolute;
            z-index: 1111;
        }

        button.close {
            -webkit-appearance: none;
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            float: right;
            margin-left: 239px !important;
        }
        
        .modal-body {
            position: relative;
            padding: 15px;
            height: 287px;
        }

        .entry-additions{
            margin-left: 81px !important;
        }

        .entry-more {
            margin-left: 2px !important;
        }

        .entry-header{        
            margin-left: 184px !important;
        }
    }


    @media only screen and (max-width:414px){
        
        .container h3{
            text-align:center;
        }

        .profile-item .entry .entry-thumbnail, .profile-item .entry .entry-data, .profile-item .entry .entry-additions {
            display: block !important;
           // text-align: center !important;
        }

        .badge {
            right: 24px !important;
            top: -1px  !important;
        }
        .entry-additions{
            margin-left: 123px;
        }

        .entry-more {
            margin-left: 2px !important;
        }
    }

    @media only screen and (max-width:732px) (min-width: 415px){
        .app-section .app{
            margin-top: -64% !important;
        }    
        #form_scroll{
            max-height: 401px !important;
            overflow-y: scroll !important;
        }
    }

    @media only screen and (max-width:320px)and (min-width:318px){ 

        .more-link{
            margin-left: 100px !important;
            width: 159px;
        }
    }

    @media only screen and (max-width:370px)and (min-width:360px){ 
        .more-link{

                margin-left: 99px !important;

               width: 157px;

        }
    }

    @media only screen and (max-width:732px)and (min-width:415px){
        .app {
            /*  padding: 143px!important; */
			 
        }
    }

    </style>

<?php
}

?>
<script>
    /* 11:46 AM 22 January, 2018 Modified by PJ */
    function checkuserlogin(){
        jQuery( document.body ).trigger( 'wc_fragment_refresh' );
        
        jQuery(document).find('a.wc-forward').html("<img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' />");
        
        var user_in = '<?php echo is_user_logged_in();?>';
        console.log(user_in);
    <?php
        if(is_user_logged_in()){
    ?>
            console.log('Returning in');
            window.location.href= '<?php echo WC()->cart->get_cart_url(); ?>';
            return true;
    <?php
        }
    ?>
        console.log('not returned');
        //jQuery(".login").attr("action","<?php echo WC()->cart->get_cart_url(); ?>");
        //jQuery(".register").attr("action","<?php echo WC()->cart->get_cart_url(); ?>");
        jQuery("input[name='_wp_http_referer']").attr("value","<?php echo WC()->cart->get_cart_url(); ?>");
        
        document.cookie = 'redirect_to=<?php echo WC()->cart->get_cart_url(); ?>; expires=Fri, 3 Aug 2020 20:47:11 UTC; path=/';        
        jQuery("#login-register-toggle").trigger("click");
        return false;
    }

</script>
<?php
//Autocomplete all product list
     $url = $_SERVER["REQUEST_URI"];
	  if (stripos($url, 'profile') !== false) {
		do_action('loadallproductforautocomplete' , $url);  
	  }
	  if(strpos($url,'?location=') != false){
      		 do_action('autocompleteAllprodlist');	   
	  }
	  if(strpos($url,'?s=') != false){
      		 do_action('autocompleteAllprodlist');	   	 
	} 
//End All Product List
?><!--
<script type="text/javascript" id="cookieinfo"	src="//cookieinfoscript.com/js/cookieinfo.min.js"></script><script type="text/javascript" id="cookieinfo"	src="//cookieinfoscript.com/js/cookieinfo.min.js"	data-bg="#645862"	data-fg="#FFFFFF"	data-link="#F1D600"	data-cookie="CookieInfoScript"	data-text-align="left"    data-close-text="Got it!"	data-moreinfo="https://devo.co.uk/cookies/"	>	              	</script>-->
				        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" /><script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script><script>// window.addEventListener("load", function(){// window.cookieconsent.initialise({  // "palette": {    // "popup": {      // "background": "#efefef"    // },    // "button": {      // "background": "#efefef"    // }  // },  // "content": {                // "message": "We use cookies to provide a personalized and secure experience for our users. You can learn more in our ",				// "dismiss": "Close(X)",								                                   // "link": "Cookies Policy",                // "href": "https://devo.co.uk/cookies/"              // }// })});</script>
        <script>
           window.addEventListener("load", function(){
            window.cookieconsent.initialise({
              "palette": {
                "popup": {
                  "background": "#efefef",
                  "text": "#404040"
                },
                "button": {
                  "background": "#efefef",
                  "text": "#404040"
                }
              },
              "theme": "edgeless",
             // "type": "opt-in",
              "content": {
                "message": "We use cookies to provide a personalized and secure experience for our users. You can learn more in our ",
                "dismiss": "Close (X)",
               // "allow": "X",
                "link": "Cookies Policy",
                "href": "https://devo.co.uk/cookies/"
              }
            })});

        /* 
        6:24 PM 23 January, 2018 by PJ Jugaad to Change shops page title
         */
        function changeTitle() {
            var title = jQuery(document).prop('title'); 
            if (title.indexOf('Profiles Archive') != -1) {         
                console.log('changed');
                jQuery(document).prop('title', 'Local shops delivery near me | Devo');
            }else{
                console.log('not changed');
            }
        }

        changeTitle();
        </script>
    </head>



    <body <?php body_class(); ?>>   
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNLVXKZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
     

        <div id="preloader"><div id="status"></div></div>
		
		<img class="hide" src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' />

        <div id="page" class="hfeed site">

           <header id="masthead" class="site-header clearfix">

                <?php

                get_sidebar('header');

                ?>   				

                <div class="header-main clearfix">

				<?php //echo"<pre>";print_R($options);die; ?>

                    <div class="header-parts <?php print ((isset($options['header_parts_fullwidth']) && $options['header_parts_fullwidth']) ? '' : 'container'); ?>">

                        <?php

                        if (isset($options['header'])) {

                            foreach ((array) $options['header'] as $part) {

								

                                $template_part = azexo_is_template_part_exists('template-parts/header', $part);

                                if (!empty($template_part)) {

                                    get_template_part('template-parts/header', $part);

                                } else {

                                    switch ($part) {

                                        case 'logo':

                                            ?>

                                            <a class="site-title" href="<?php print esc_url(home_url('/')); ?>" rel="home">

                                                <?php if (isset($options['logo']['url']) && !empty($options['logo']['url'])): ?>

                                                    <img src="<?php print esc_url($options['logo']['url']); ?>" alt="logo">

                                                <?php else: ?>

                                                    <span class="title"><?php print esc_html(get_bloginfo('name')); ?></span>

                                                <?php endif; ?>

                                            </a>

                                            <?php

                                            break;

                                        case 'search':

                                            azexo_get_search_form();

                                            break;

                                        case 'mobile_menu_button':

                                            ?>

                                            <div class="mobile-menu-button"><span><i class="fa fa-bars"></i></span></div>                    

                                            <?php

                                            break;

                                        case 'mobile_menu':

                                            ?><nav class="site-navigation mobile-menu"><?php

                                                        if (has_nav_menu('primary')) {

                                                            wp_nav_menu(array(

                                                                'theme_location' => 'primary',

                                                                'menu_class' => 'nav-menu',

                                                                'menu_id' => 'primary-menu-mobile',

                                                                'walker' => new AZEXO_Walker_Nav_Menu(),

                                                            ));

                                                        }

                                                        ?></nav><?php

                                            break;

                                        case 'primary_menu':

                                            ?><nav class="site-navigation primary-navigation"><?php

                                                if (has_nav_menu('primary')) {

                                                    wp_nav_menu(array(

                                                        'theme_location' => 'primary',

                                                        'menu_class' => 'nav-menu',

                                                        'menu_id' => 'primary-menu',

                                                        'walker' => new AZEXO_Walker_Nav_Menu(),

                                                    ));

                                                }

                                                ?></nav><?php

                                            break;

                                        case 'secondary_menu':

                                            ?><nav class="secondary-navigation"><?php

                                                if (has_nav_menu('secondary')) {

                                                    wp_nav_menu(array(

                                                        'theme_location' => 'secondary',

                                                        'menu_class' => 'nav-menu',

                                                        'menu_id' => 'secondary-menu',

                                                        'walker' => new AZEXO_Walker_Nav_Menu(),

                                                    ));

                                                }

                                                ?></nav><?php

                                            break;

                                        default:

                                            break;

                                    }

                                }

                            }

                        }

                        ?>                        

                    </div>

                </div>				

				

                <?php

                get_sidebar('middle');

                ?>

				

			</header><!-- #masthead -->	

			



			<!----notify me model----->

			<div class="container">

				<div class="modal fade" id="myModal" role="dialog">	

					<div class="modal-dialog modal-sm">						

						<div class="modal-content">														

							<div class="modal-body">								

								<button type="button" class="close" data-dismiss="modal">&times;</button>

								<div class="notify-header">									

									<img src="wp-content/uploads/2017/05/placeholder-.png" alt="Notify Me!" class="img-responsive img-style">													

									<p id="notify_content_id">Sorry, we currently can't travel that far. Please enter your email address and we'll let you know when we do.</p>

									

																

									<form role="form">									

										<div class="form-group pdn-bottom">										

											<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"/>					

										</div>									

										<button type="button" id="btnwth" class="btn btn-default notify-class">Notify Me!</button>					

									</form>

								</div>

								<div id="notify_email_id" style="display:none;">

									<p class="success_class">Thanks! We'll get in touch as soon as we start delivering to .</p>

								</div>

							</div>							

							<!--<div class="modal-footer">							  

							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>							

							</div>-->						

						</div>					

					</div>				

				</div>			

			</div>

            



<!-- Warning Popup Modal -->

<div id="alcohalpopup" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->

        <div class="modal-content" style="border-radius:0px;width:75%;">

      

            <div class="modal-body">

                <h5 class="modal-title" style="padding:3%;"><b>Are you old enough to buy alcohol?</b></h5>      

			  <p style="padding: 0px 2% 0px 3%;"><b>Alcohol is not for sale to anyone under the age of 18. You may need to provide a valid ID upon delivery.</b></p>

                <input type="hidden" name="alcohal_product" id="alcohal_product" value=""/>

                <!-- Fake dynamically change product -->

            </div>

            <div class="modal-footer">

                <div class="col-md-5 col-sm-6 col-xs-12 gen-name-cls">

                     <a href="javascript:void(0);" data-dismiss="modal" class="devo_large_buttons" style="background: white;color:#fd0e35f5;border:1px solid #fd0e3559;border-radius: 3px;">Cancel</a>

                </div>

                <div class="col-md-7 col-sm-6 col-xs-12 gen-name-cls">

                    <a href="javascript:void(0);" id="alcohal_warning_allow" data-dismiss="modal" class="devo_large_buttons">Yes, I'm 18 or over</a>

                </div>

            </div>

        </div>

    </div>

</div>
<div id="addtocart" class="modal fade" style="text-align:center; line-height: 30;" role="dialog">

    <img style="border-radius:10px" src="https://devo.co.uk/wp-content/themes/foodpicky-child/css/again_new.png"  />
 
</div>


<!-- Modal -->

<div id="tobaccopopup" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->

        <div class="modal-content" id="alcohal-model"  style="border-radius:0px;width:75%;">

         

            <div class="modal-body">

                <!--<button type="button" class="close" data-dismiss="modal">&times;</button> -->

				<p style="text-align:center;"><img src="https://devo.co.uk/wp-content/uploads/2018/01/no-waiting-1.png" width="50" height="50"></p>

                <h6 style="line-height:30px;text-align:center;"><b>IT IS ILLEGAL TO SELL TOBACCO PRODUCTS TO ANYONE UNDER THE AGE OF 18</b></h6>

                 <input type="hidden" name="tobacco_product" id="tobacco_product" value=""/>

                <!-- Fake dynamically change product -->

            </div>

            <div class="modal-footer">

                <div class="row">

                    <div class="col-md-5 col-sm-6 col-xs-12 gen-name-cls">

                        <a href="javascript:void(0);" data-dismiss="modal" class="devo_large_buttons" style="background: white;color:#fd0e35f5;border:1px solid #fd0e3559; border-radius: 3px;">Cancel</a>

                    </div>

                    <div class="col-md-7 col-sm-6 col-xs-12 gen-name-cls">

                        <a href="javascript:void(0);" id="tobacco_warning_allow" data-dismiss="modal" class="devo_large_buttons">Yes, I'm 18 or over</a>                       

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<!-- Modal -->

<div id="tobaccopopup-2" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->

        <div class="modal-content" id="alcohal-model"  style="border-radius:0px;width:75%;">

         

            <div class="modal-body">

                <!--<button type="button" class="close" data-dismiss="modal">&times;</button> -->

				<p style="text-align:center;"><img src="https://devo.co.uk/wp-content/uploads/2018/01/no-waiting-1.png" width="50" height="50"></p>

                <h6 style="line-height:30px;text-align:center;"><b>IT IS ILLEGAL TO SELL TOBACCO PRODUCTS TO ANYONE UNDER THE AGE OF 18</b></h6>

                <!-- Fake dynamically change product -->

            </div>

            <div class="modal-footer">

                <div class="row">

                    <div class="col-md-5 col-sm-6 col-xs-12 gen-name-cls">

                        <a href="javascript:void(0);" data-dismiss="modal" class="devo_large_buttons" style="background: white;color:#fd0e35f5;border:1px solid #fd0e3559; border-radius: 3px;">Cancel</a>

                    </div>

                    <div class="col-md-7 col-sm-6 col-xs-12 gen-name-cls">

                        <a href="javascript:void(0);" id="tobacco_warning_allow-2" data-dismiss="modal" class="devo_large_buttons">Yes, I'm 18 or over</a>                       

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>            

<style>

.devo_large_buttons{

    width: 100%;

    height: 40px;

    font-size: 12px !important;

    text-align: center;

    background: #FD0E35;

    color: #fff;
    font-weight: bold;

    border-radius: 3px;

    border: 1px solid #ea1010;

    float: left;

	line-height:40px;

}

.devo_large_buttons {
	opacity:0.7;
	color:#fff;
}

</style>

            

<div id="main" class="site-main">

			

			