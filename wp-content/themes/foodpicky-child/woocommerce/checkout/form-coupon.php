<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $woocommerce;
$checkout_url = $woocommerce->cart->get_checkout_url();
if ( ! wc_coupons_enabled() ) {
	return;
}

/* if ( empty( WC()->cart->applied_coupons ) ) {
	$info_message = apply_filters( 'woocommerce_checkout_coupon_message', __( 'Have a coupon?', 'woocommerce' ) . ' <a href="#" class="showcoupon">' . __( 'Click here to enter your code', 'woocommerce' ) . '</a>' );
	wc_print_notice( $info_message, 'notice' );
} */
?>

<!--<a href="javascript:void(0);" data-toggle="modal" data-target="#myPromoCode">Add a Prome Code</a>-->
<div class="modal fade" id="myPromoCode" role="dialog">
	<div id="code-modal" class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content">
		<div class="modal-header">
			<a style="float: left;position: relative;left: 0px;" href="javascript:void(0);" data-dismiss="modal" class="close_form"><img src="/wp-content/uploads/unchecked.png"></a>
		</div>
		<div class="modal-body">
		  <div id="coupon_div">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" name="coupon_code" class="input-text promo-code" placeholder="<?php esc_attr_e( 'Enter Promo Code', 'woocommerce' ); ?>" id="coupon_code" value="" />
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<input type="button" class="button " id="ccp" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" onclick="set_coupon();" />				
			</div>
			<div id="cr" style=""></div>
			<div class="clear"></div>
		  </div>
		</div>
		<div class="modal-footer">
		</div>
	  </div>
	  
	</div>
</div>


<!--<div id="coupon_div">
	
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
		</div>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<input type="button" class="button " id="ccp" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" onclick="set_coupon();" />
			
		</div>

	<div id="cr" style="display:none;"></div>

	<div class="clear"></div>
</div>-->

<div class="clearfix"></div>
<script>

function set_coupon(){
	
	var data = {};
	data.coupon_code = jQuery('#coupon_code').val();
	data.security = wc_checkout_params.apply_coupon_nonce;

	
	jQuery.ajax({
		type: "POST",
		url: "<?php echo $checkout_url;?>?wc-ajax=apply_coupon",
		data: data,
		cache: false,
		success: function (response) {
			console.log(response);
			jQuery("#cr").show();
			jQuery("#cr").html(response);
			jQuery(".woocommerce-message").show();
			jQuery('body').trigger('update_checkout');
			jQuery( document.body ).trigger( 'wc_fragments_refreshed' );
			refresh_minicart_template_section();
		}
	});
	
	
}

function refresh_minicart_template_section(){

	var data = {
		'action': 'mode_theme_update_mini_cart'
	};
	jQuery.post(
		woocommerce_params.ajax_url, // The AJAX URL
		data, // Send our PHP function
		function(response){
		 jQuery('.widget_shopping_cart_content').html(response); // Repopulate the specific element with the new content
		}
	);
}

jQuery(document).ready(function(){
	// jQuery('form.checkout a[href="#previous"]').hide();
	// jQuery('form.checkout a[href="#next"]').hide();
});
</script>

<style>
#code-modal{
	top:165px;
}
#ccp {
    display: inline-block;
	margin: 10px;
}
#hr_div {
    padding: 0px 55px 0px 55px;
}
.udhr {	
	width:100%;
	padding:0px;
	margin:0px;	
}
#cr{
	text-align:left;
	margin-top:0px;
	padding-bottom: 20px;
	padding-left: 20px;
	float: left;
}
#cr .woocommerce-message{
	display: block !important;
	margin-bottom: 0px !important;
}
.entry-title {	
	display:none;	
}
.product-name {	
	width: 60%;	
}
#coupon_div {	
	padding: 0 0%;
	width: 55%;
	display: inline-block;	
	font-size: 12px;
	font-family: AvenirLTStd-Roman, "Helvetica Neue Light", "Helvetica Neue", sans-serif !important;
}
#coupon_code {
    width: 100%;
	display: inline-block;
}
.woocommerce .woocommerce-error {
    width: auto;
}
/* #payment {	
    margin-left: -58px;
    width: auto !important;	
} */
#s4wc_save_card {	
	margin-left: 155px;	
}
p.wc-terms-and-conditions{
	text-align:center !important;
}
.promo-code{
	border-top: none !important;
    border-left: none !important;
    border-right: none !important;
    border-bottom: solid 2px red !important;	
    outline:none !important;
	font-size:12px !important;
}
.woocommerce-checkout-payment{
	margin-top:-20px !important;
}
.card-list-div{
	margin-top:-16px !important; 
}
</style>