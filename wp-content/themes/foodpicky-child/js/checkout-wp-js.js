jQuery(document).ready(function(jQuery) {

	jQuery(document).on('click','.btn-dlt',function(){
		var id = jQuery(this).attr('id');
		
		//alert(id);
		
		$("#"+id).closest('.row').addClass('edit_account_delete');
		
		var base_url = window.location.origin;
		jQuery.ajax({
			url: custom_child_script_ajax.ajaxurl,
			type: "GET",
			data: {'action':'delete_delevery_address_request_js','checkoutAdd':id},
			success:function(response) {
				// alert(response);
				window.location.reload();
				// console.log(response);
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});

	});

	jQuery(document).on('click','.finish-btn',function(event){
		jQuery( ".payment_method_stripe input[type='text']" ).each(function( index ) {
				if(jQuery(this).val()==''){
					jQuery(this).addClass('error');
				}else if(jQuery(this).val()!=''){
					jQuery(this).removeClass('error');
				}
		});
		jQuery( "p.wc-terms-and-conditions" ).css('border','0px');
		var checkedStatus = jQuery( "input#terms" ).attr('checked');
		if(!checkedStatus){
			jQuery( "p.terms-error" ).hide();
			// jQuery( "#terms-error" ).html('');
			jQuery( ".wc-terms-and-conditions label").css('margin-left','0%');
			var error_message = 'Please accept our terms and conditions to proceed.';
			var message = "<ul class='woocommerce-error'><li>"+error_message+"</li></ul>";
			jQuery('#custom-stripe-error').html(message);
			jQuery('#custom-stripe-error').show();
		}
		console.log('clicked');
		return false;

	});

	jQuery(document).on('change','.selectRadio',function(){

		var id = jQuery(this).attr('id');

		var add1 = jQuery("input."+id+"add1").val();
		var add2 = jQuery("input."+id+"add2").val();
		var phne = jQuery("input."+id+"phne").val();
		var pcde = jQuery("input."+id+"pstc").val();

		jQuery("#billing_address_1").val(add1);
		jQuery("#billing_address_2").val(add2);
		jQuery("#billing_postcode").val(pcde);
		jQuery("#billing_phone").val(phne);

		document.cookie = "billing_address_1="+add1;
		document.cookie = "billing_address_2="+add2;
		document.cookie = "billing_postcode="+pcde;
		document.cookie = "billing_phone="+phne;

	});

	jQuery(document).on('click','#registerUser',function(){

		var username 	= jQuery("#reg_firstname").val();
		var last_name 	= jQuery("#reg_lastname").val();
		var email 		= jQuery("#reg_email_new").val();
		var paswd 		= jQuery("#reg_password_new").val();
		var mobile		= jQuery("#reg_mobile").val();
		var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
		var base_url 	= window.location.origin;

		if( username=='' || last_name=='' || email=='' || paswd=='' || mobile==''){
			alert('Plese submit fields.');
		}else if(username==''){
			alert("Firstname field is empty");
		}else if(last_name==''){
			alert("Lastname field is empty");
		}else if(email=='' || !pattern.test(email)){
			alert("Invalid Email Address");
		}else if(paswd==''){
			alert("Password field is empty");
		}else if(mobile==''){
			alert("Mobile field is empty");
		}
		else{
			//jQuery("#loading").css('display','block');
			jQuery.ajax({
				url: custom_child_script_ajax.ajaxurl,
				type: "POST",
				data: {'action':'register_user_from_checkout','username':username,'last_name':last_name,'email':email,'paswd':paswd,'mobile':mobile},
				success:function(response) {
					//alert(response);
					window.location.reload();
				},
				error: function(errorThrown){
					//jQuery("#loading").css('display','none');
					console.log(errorThrown);
				}
			});
		}
	});

	jQuery("#stripe-card-cvc").attr('maxlength','3');
	
	jQuery("#billing_phone_field > input#billing_phone").attr('type','text');
	jQuery("#billing_phone_field > input#billing_phone").keypress(function(key) {
		//console.log(key.charCode + "\n\n");

		if(key.charCode < 48 || key.charCode > 57) return false;
		// $('#account_phone').val($('#account_phone').val().replace(/[^\d]/g, ""));

		var value = jQuery(this).val();
		var length = value.length;
		switch(length){
			case 4:
				jQuery(this).val(value+' ');
				break;

			case 8:
				jQuery(this).val(value+' ');
				break;

			case 14:
				return false;
		}


	});

});

function deleteCard(cid,cus_id){

	//var id = jQuery(this).attr('id');
	var base_url = window.location.origin;
	
	jQuery('.btn-dlt').closest('.over_add_flow').addClass('edit_account_delete_disable');
	 jQuery('.btn-dlt').closest('.col-md-12').addClass('edit_account_delete');
	
	jQuery.ajax({
		url: custom_child_script_ajax.ajaxurl,
		type: "GET",
		data: {'action':'delete_delevery_card_request_js','cardId':cid,'customerId':cus_id},
		success:function(response) {
			//alert(response);
			window.location.reload(true);
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
	});
}


jQuery( document ).ajaxSend(function(event, xhr, settings) {
	if(settings.url == "/checkout/?wc-ajax=checkout"){
		// jQuery("div#divLoading").addClass('show');
		
		var cardNumber = jQuery('#stripe-card-number').val();
		var cardExp = jQuery('#stripe-card-expiry').val();
		var cardCvv = jQuery('#stripe-card-cvc').val();
		if(cardNumber != null && cardExp != null && cardCvv != null){
			
		jQuery('#checkout_form_new a[href="#finish"]').after("<button style='width:356px; height: 49.78px;' class='button confirm_and_pay_button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		jQuery('#checkout_form_new a[href="#finish"]').hide();
			
		}
		
		
		
		// return false;
		
		console.log('checkout loader displayed')
		var terms = jQuery('#terms');
		if(terms.is(":checked")==false){
			jQuery('p.wc-terms-and-conditions').html('<input type="checkbox" class="input-checkbox" name="terms" id="terms"><label for="terms" class="checkbox">I’ve read and accept the <a href="/terms-conditions/" target="_blank">Terms &amp; Conditions</a> <span class="required" aria-required="true"></span></label><input type="hidden" name="terms-field" value="1">');
            
		}
	}

});
/* 
4:42 PM 29 January, 2018 Added by PJ to Remove Extra Label in Accept Terms and Conditions Checkbox
 */
jQuery("body").on('DOMSubtreeModified', "p.wc-terms-and-conditions", function() {
    console.log('on changed');
    jQuery('#terms-error').remove()
});

jQuery( document ).ajaxSuccess(function( event, xhr, settings ) {
	if(settings.url == "/checkout/?wc-ajax=checkout" && xhr.responseText.indexOf('result":"failure') >= 0){
		
		jQuery('#checkout_form_new a[href="#finish"]').show();
		jQuery('.confirm_and_pay_button').hide();
		
		var data = xhr.responseJSON.messages;
		var reloadFlag = false;
	// 	<ul class="woocommerce-error">
	// 		<li>Please make sure your card details have been entered correctly and that your browser supports JavaScript.</li>
	// </ul>
		if(data.indexOf('your browser supports JavaScript') >= 0){
			data='<ul class="woocommerce-error"><li>Please fill payment details correctly.</li></ul>';
		}else if(data.indexOf('card was declined') >= 0){
			data='<ul class="woocommerce-error"><li>Please check your card information.</li></ul>';
		}else if(data.indexOf('card has expired.') >= 0){
			data='<ul class="woocommerce-error"><li>Please check your card information.</li></ul>';
		}else if(data.indexOf('must accept our Terms') >= 0){
			data='<ul class="woocommerce-error"><li>Please accept our terms and conditions to proceed.</li></ul>';
		}else if(data.indexOf('security code is') >= 0){
			data='<ul class="woocommerce-error"><li>Please check your card information.</li></ul>';
		}else if(data.indexOf('postcode you supplied') >= 0){
			data='<ul class="woocommerce-error"><li>Please check your card information.</li></ul>';
			reloadFlag = true;
		}else if(data.indexOf('cannot use a Stripe token more than') >= 0){
			data='<ul class="woocommerce-error"><li>You have entered wrong details more than once. Page will reload now</li></ul><script>window.location.reload();</script>';
			reloadFlag = true;
		}
		//console.log(data);
		jQuery('#custom-stripe-error').html(data);
		//console.log(xhr);
		jQuery('#custom-stripe-error').show();
		
		if(data.indexOf('terms and conditions') >= 0){
			jQuery('p.wc-terms-and-conditions').html('<input type="checkbox" class="input-checkbox" name="terms" id="terms"><label for="terms" class="checkbox">I’ve read and accept the <a href="/terms-conditions/" target="_blank">Terms &amp; Conditions</a> <span class="required" aria-required="true"></span></label><input type="hidden" name="terms-field" value="1">');
		}
		
		if(reloadFlag==true){
			console.log('reloading successfull');
			window.location.reload();
		}
		console.log('intruption completed')
	}
	if(settings.url == "/checkout/?wc-ajax=checkout"){
		jQuery("div#divLoading").removeClass('show');
		// jQuery(".confirm_and_pay_button").hide();
		// jQuery('#checkout_form_new a[href="#finish"]').show();
		console.log('checkout loader Hidden')
	}
});

function handle_stripe_checkout_error(response){
	var message_text = response.error.message;
	message_text = 'Please check your card information.';
	/* if(!message_text.indexOf('postcode you supplied') >= 0){
		message_text = 'Please check your card information.';
	} */
	var message = "<ul class='woocommerce-error'><li>"+message_text+"</li></ul>";
	jQuery('#custom-stripe-error').html(message);
	jQuery('#custom-stripe-error').show();
}



