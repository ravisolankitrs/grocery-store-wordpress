<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'devoco_main_devo');
define('DB_NAME', 'devoco_temp_clone');

/** MySQL database username */
define('DB_USER', 'devoco_main_devo');

/** MySQL database password */
define('DB_PASSWORD', 'vXSR(*4gQ*+E');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Dynq2O0j6waNcDH2d6O29y5YreoMvoNoVf3q6MkL0eekHh2ZQHxZznixbJ3OsE0p');
define('SECURE_AUTH_KEY',  't1fmSFza1KinsjkMV8G8RI07oySBdRNwnmC9K0c31jW8Xzx4QvgGMqCp1HoymMZA');
define('LOGGED_IN_KEY',    '35R8tkb1ZaLMBauxMAInMfPVsN2kimqP3honpwrx0kL8VA7OXMUfD9tiOnL9hSMD');
define('NONCE_KEY',        'wEvSnR2oxEpXECeJtLh6dEsQkPtLrPOP8HwaW508aq4o9EJGFv3TgnclfhgdFysR');
define('AUTH_SALT',        'yvZgEG01nivSGmjAVkS79i6wdFEYEIoEtSfhRneQyuCCmVzlAm156EUeA668yLR5');
define('SECURE_AUTH_SALT', 'gjUjQVWe3ZiChOxJ1Q5PqpTFZzpVqFeGemTjtATOMbIZQqM2uO4sGRh1BrrHQgHB');
define('LOGGED_IN_SALT',   'd4RBDLqqF1jPtM2d0FXHdwYMcxqQsC9FfSvdkpTNQd2Dup6sRTHox8L0B0vS9ZyU');
define('NONCE_SALT',       'XalMB088RcvCekZT4S1O377cbhA88M9TmnRUiqOaMMiH4Zs2CkPEa9goo1oJtZW7');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);
// Disable display of errors and warnings
define('WP_DEBUG_DISPLAY', false);
define( 'WP_MEMORY_LIMIT', '512M' );
define( 'WP_MAX_MEMORY_LIMIT', '700M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
