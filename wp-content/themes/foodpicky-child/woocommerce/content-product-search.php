<style>

</style>


<?php


//die('hello');


/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 10.0.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product, $post, $woocommerce_loop;

if (empty($woocommerce_loop['loop'])) {
    $woocommerce_loop['loop'] = 0;
}
if (empty($woocommerce_loop['columns'])) {
    $woocommerce_loop['columns'] = apply_filters('loop_shop_columns', 4);
}

$keys = (array)get_post_custom_keys();
$cached_keys = get_option('azexo_' . get_post_type() . '_meta_keys', array());
$diff = array_diff($keys, $cached_keys);
if(!empty($diff)) {
    $cached_keys = array_merge($cached_keys, $keys);
    $cached_keys = array_unique($cached_keys);
    update_option('azexo_' . get_post_type() . '_meta_keys', $cached_keys);
}

$options = get_option(AZEXO_FRAMEWORK);
if (!isset($product_template)) {
    if (isset($template_name)) {
        $product_template = $template_name;
    } else {
        $product_template = isset($options['default_' . get_post_type() . '_template']) ? $options['default_' . get_post_type() . '_template'] : 'shop_product';
    }
}

$product_template = "profile-products";

$woocommerce_loop['loop'] ++;

if (!isset($azexo_woo_base_tag)) {
    $azexo_woo_base_tag = 'li';
}
$single = ($product_template == 'single_product');
$more_link_text = sprintf(wp_kses(__('Read more<span class="meta-nav"> &rsaquo;</span>', 'foodpicky'), array('span' => array('class' => array()))));
$thumbnail_size = isset($options[$product_template . '_thumbnail_size']) && !empty($options[$product_template . '_thumbnail_size']) ? $options[$product_template . '_thumbnail_size'] : 'large';
azexo_add_image_size($thumbnail_size);
$image_thumbnail = isset($options[$product_template . '_image_thumbnail']) ? $options[$product_template . '_image_thumbnail'] : false;

$images_links = azexo_woo_get_images_links($thumbnail_size);

?>

<<?php print $azexo_woo_base_tag; ?> <?php post_class(array(str_replace('_', '-', $product_template))); ?>>

<div class="col-md-2 margin_left  profile_take_it_home">   
	<?php do_action('azexo_entry_open'); ?>
	<div class="card">
		<meta itemprop="url" content="<?php print "?add-to-cart=".get_the_ID() ?> />
		<meta itemprop="image" content="<?php print esc_url(wp_get_attachment_url(get_post_thumbnail_id($post->ID))); ?>" />
		<?php
		if (!$single) {
			do_action('woocommerce_before_shop_loop_item');
		}
		?>      
		
			<!-- This code here is for Product Thumbnail-->
			<?php if (isset($options[$product_template . '_show_thumbnail']) && $options[$product_template . '_show_thumbnail']): ?>
				<?php if ((count($images_links) > 1) && !$image_thumbnail): ?>
					<div class="entry-gallery">
						<?php
						azexo_woo_product_gallery_field($product_template);
						?>
						<?php if (!azexo_is_empty(azexo_entry_meta($product_template, 'hover'))): ?>
							<div class="entry-hover"><?php print azexo_entry_meta($product_template, 'hover'); ?></div>
						<?php endif; ?>
						<?php print azexo_entry_meta($product_template, 'thumbnail'); ?>
					</div>
				<?php else: ?> 
					<?php
					$url = azexo_get_the_post_thumbnail(get_the_ID(), $thumbnail_size, true);
					if ($url):
						?>   
						<div class="entry-thumbnail">
							<?php
							azexo_woo_product_thumbnail_field($product_template);
							?>
							<?php if (!azexo_is_empty(azexo_entry_meta($product_template, 'hover'))): ?>
								<div class="entry-hover"><?php print azexo_entry_meta($product_template, 'hover'); ?></div>
							<?php endif; ?>
							<?php //print azexo_entry_meta($product_template, 'thumbnail'); ?>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>  
			<!-- Product Thumbnail Code Ends here-->
			<?php
			if ($single) {
				do_action('woocommerce_before_single_product_summary');
			}
			?>
			<div class="entry-data">
				<div class="entry-header">
					<?php
					$extra = trim(azexo_entry_meta($product_template, 'extra'));
					if (!$single) {
						ob_start();
						do_action('woocommerce_before_shop_loop_item_title');
						$extra .= ob_get_clean();
					}
					?>
					<?php if (!empty($extra)) : ?>
						<div class="entry-extra"><?php print azexo_entry_meta($product_template, 'extra'); ?></div>
					<?php endif; ?>
					<?php
					if (isset($options[$product_template . '_show_title']) && $options[$product_template . '_show_title']) {
						if ($single) {
							woocommerce_template_single_title();
						} else {
							?>
							<p class="title entry-title" itemprop="name"><?php the_title(); ?></p>
							<?php
						}
					}
					?>
					<?php
					$meta = trim(azexo_entry_meta($product_template, 'meta'));
					if (!$single) {
						ob_start();
						do_action('woocommerce_after_shop_loop_item_title');
						$meta .= ob_get_clean();
					}
					?>
					<?php if (!empty($meta)) : ?>
						<div class="entry-meta"><?php print azexo_entry_meta($product_template, 'meta'); ?></div>
					<?php endif; ?>
					<?php
					global $woocommerce;
					$currency = get_woocommerce_currency_symbol();
					$price = get_post_meta( get_the_ID(), '_regular_price', true);
					$sale = get_post_meta( get_the_ID(), '_sale_price', true);
					
					if(!empty($price)){
						$price = number_format((float)$price,2);
					}
					if(!empty($sale)){
						$sale = number_format((float)$sale,2);
					}
					?>
					<?php if($sale) : ?>
						<p class="product-price-tickr"><del><?php echo $currency; echo $price; ?></del> <?php echo $currency; echo $sale; ?></p>    
					<?php elseif($price) : ?>
						<p class="product-price-tickr"><?php echo $currency; echo $price; ?></p>    
					<?php endif; ?>
					<?php
					print azexo_entry_meta($product_template, 'header');
					?>
				</div>
				
				<?php if (!azexo_is_empty(azexo_entry_meta($product_template, 'footer'))) : ?>
					<div class="entry-footer"><?php print azexo_entry_meta($product_template, 'footer'); ?></div>
				<?php endif; ?>
				 <?php print azexo_entry_meta($product_template, 'data'); ?>
				<?php
				if ($single) {
					do_action('woocommerce_single_product_summary');
				}
				?>
				<?php
				if (!$single) {
					do_action('woocommerce_after_shop_loop_item');
				}
				?>
			
		    </div>
	</div>
	<?php do_action('azexo_entry_close');?>
</div>

</<?php print $azexo_woo_base_tag; ?>>

<?php
print azexo_entry_meta($product_template, 'next');
?>
