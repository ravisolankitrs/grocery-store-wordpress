<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_orders', $has_orders ); ?>

<?php if ( $has_orders ) : ?>
<?php
    $order_statuses = array(
        'nothing'	    => "Enroute",
        'pending'    => "Pending",
        'processing' => "Enroute",
        'on-hold'    => "Rejected",
        'completed'  => "Delivered",
        'approved'	=> "Enroute",
        'cancelled'  => "Cancelled",
        'refunded'   => "Cancelled",
        'failed'     => "Cancelled",
    );
?>
<h2 class="head-orders">ORDERS</h2>
    <div class="table-responsive">
	<table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
		<thead class="order_title_header">
			<tr>
				<?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
					<th class="woocommerce-orders-table__header woocommerce-orders-table__header-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php foreach ( $customer_orders->orders as $customer_order ) :
				$order      = wc_get_order( $customer_order );
				$item_count = $order->get_item_count();
				
				$order_status = strtolower($order->get_status());
				if($order_status =='failed'){
					continue;
				}
				?>
				<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $order->get_status() ); ?> order">
					<?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>

						<td class="devo_order_table woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
							<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
								<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

							<?php elseif ( 'order-number' === $column_id ) : ?>
								<a class="dev_order_table" href="javascript:void(0);" data-url="<?php echo esc_url( $order->get_view_order_url() ); ?>">
									<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?>
								</a>

							<?php elseif ( 'order-date' === $column_id ) : ?>
								<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>

							<?php elseif ( 'order-status' === $column_id ) : ?>
								<?php echo (array_key_exists($order->get_status(),$order_statuses))?$order_statuses[$order->get_status()]:esc_html( wc_get_order_status_name( $order->get_status() ) );
                                //echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

							<?php elseif ( 'order-total' === $column_id ) : ?>
								<?php
								/* translators: 1: formatted order total 2: total order items */
								printf( _n( '%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count );
								?>

							<?php elseif ( 'order-actions' === $column_id ) : ?>
								<?php
									$actions = array(
										'pay'    => array(
											'url'  => $order->get_checkout_payment_url(),
											'name' => __( 'Pay', 'woocommerce' ),
										),
										'view'   => array(
											'url'  => $order->get_view_order_url(),
											'name' => __( 'View', 'woocommerce' ),
										),
										'cancel' => array(
											'url'  => $order->get_cancel_order_url( wc_get_page_permalink( 'myaccount' ) ),
											'name' => __( 'Cancel', 'woocommerce' ),
										),
									);

									if ( ! $order->needs_payment() ) {
										unset( $actions['pay'] );
									}

									if ( ! in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_cancel', array( 'pending', 'failed' ), $order ) ) ) {
										unset( $actions['cancel'] );
									}
									unset( $actions['pay'] );
									unset( $actions['cancel'] );
									if ( $actions = apply_filters( 'woocommerce_my_account_my_orders_actions', $actions, $order ) ) {
										foreach ( $actions as $key => $action ) {
											echo '<a href="javascript:void(0);" data-toggle="modal" data-target="#myOrder'.$order->id.'" class="button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
										}
									}
									
									## order details ##
									$orderDetails = wc_get_order( $order->id );
									// echo "<pre>"; print_R($orderDetails);die;
									// echo "<pre>"; print_R($orderDetails->get_subtotal());die;
									$order_date = $orderDetails->order_date;
									$show_purchase_note    = $orderDetails->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
									$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
									
									
								?>
								
									<div class="modal fade" id="myOrder<?php echo $order->id?>" role="dialog">
										<div class="modal-dialog orderDilog" >
										  <div class="modal-content" style="top:80px">
											<div class="modal-header">
											  <button type="button" class="close" data-dismiss="modal">&times;</button>
											  <h2 class="modal-title  odheader" >Receipt</h2>
											</div>
											<div class="modal-body" id="myOrderdiv">
											  <div class="row orderiddate">Order : <?=$order->id;?> &nbsp; &nbsp; &nbsp;  <span style=" padding:2px;float: right;"><?php  $o_date = date('F d, Y  g:i A ', strtotime($order_date)); echo $o_date;?></span></div>
									
												<?php
											  
											   // echo $orderDetails->get_shipping();
													 // echo $orderDetails->get_total();
													foreach( $order->get_items() as $item_id => $item ) {
														$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );

														wc_get_template( 'myaccount/custom-order-details-item.php', array(
															'order'			     => $order,
															'item_id'		     => $item_id,
															'item'			     => $item,
															'show_purchase_note' => $show_purchase_note,
															'purchase_note'	     => $product ? get_post_meta( $product->id, '_purchase_note', true ) : '',
															'product'	         => $product,
														) );
													}
												?>
												<?php do_action( 'woocommerce_order_items_table', $order ); 
														$pay_details = $order->get_order_item_totals();
														unset($pay_details['payment_method']);
												?>
													<?php //echo "<pre>"; print_r($pay_details);
														 foreach ( $pay_details as $key => $total ) {
															?>
															<div class="row" <?php if($total['label'] == 'Total:'){ ?> style="font-weight:bold; text-align: left; " <?php } ?>>
															
															 <?php if($total['label'] == 'Total:'){ 
																	$style =  "font-weight:bold;text-align:left; " ;
																} 
															 else{ 
																	$style = "text-align:left";
																} 
															 ?>
															
																<div style="<?=$style;?>" class="col-xs-9" >
																	<?php echo $total['label']; ?>
																</div>
																
																<div  class="col-xs-3" style="text-align: left;">
																<div style="margin-left:20%">
																<?php 
																$total_value = str_replace('via Devo Shipping','', $total['value']);
																$total_value=($total_value=='Devo Delivery Fee')?'Free':$total_value;
																echo $total_value;	?>
																</div>
																</div>
															</div>
															<?php
														}
													?>
												
											</div>
											<div class="modal-footer" style="text-align:center; background: #f1f1f1">
												<button type="button" class="btn btn_done_order" data-dismiss="modal">Done</button>
											</div>
										</div>
										</div>
									</div>
								
								
								
							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
    </div>
	<?php do_action( 'woocommerce_before_account_orders_pagination' ); ?>

	<?php if ( 1 < $customer_orders->max_num_pages ) : ?>
		<div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
			<?php if ( 1 !== $current_page ) : ?>
				<a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( wc_get_endpoint_url( 'orders', $current_page - 1 ) ); ?>"><?php _e( 'Previous', 'woocommerce' ); ?></a>
			<?php endif; ?>

			<?php if ( intval( $customer_orders->max_num_pages ) !== $current_page ) : ?>
				<a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( wc_get_endpoint_url( 'orders', $current_page + 1 ) ); ?>"><?php _e( 'Next', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</div>
	<?php endif; ?>

<?php else : ?>
	<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php _e( 'Go shop', 'woocommerce' ) ?>
		</a>
		<?php _e( 'No order has been made yet.', 'woocommerce' ); ?>
	</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_account_orders', $has_orders ); ?>


	<script>
	var divsToHide = document.getElementsByClassName("shipped_via");

		for(var i = 0; i < divsToHide.length; i++)
		{
		divsToHide[i].style.display="none";
		}
		var divsToHide = document.getElementsByClassName("variation");

		for(var i = 0; i < divsToHide.length; i++)
		{
		divsToHide[i].style.display="none";
		}
	</script>

<style>
.odheader { padding-left: 35% !important; }
.orderiddate{
	text-align:left;
	padding:10px !important;
	font-size:16px;
	background: #f1f1f1;
    margin-left: -14px !important;
    margin-right: -14px !important;
	margin-bottom: 25px;
}
.btn_done_order{
	background:#fd0f33;
	width:60%;
	color:#ffffff;
}
.woocommerce table.shop_table td{
	 border: none !important;
}

.woocommerce table.shop_table tr{
	 border: none !important; 
}





.modal-header {
    border-bottom: 0px solid #e5e5e5 !important;
}
.button a:focus{
	/* color: #fd0e35 !important; */ 
	color: #ffffff !important;
	text-decoration:none !important;
} 

.devo_order_table{
	color:#404040;;
	font-family: sans-serif;
	/* text-decoration-line:none; */
}
.devo_order_table > a.button:visited, .devo_order_table > a.button:focus{
	color:white;
	 /* text-decoration-line:none; */
	font-family: sans-serif;
}
a:hover, a:active{
	color: #fd0e35 !important;
}
.woocommerce a.button:hover {
    color: #fff !important;
    /* background-color: #fd0e35 !important; */
}
.order_title_header{
	/* font-size: 20px; */
    color: #414551;
    font-family: HelveticaNeue-Light,Helvetica Neue Light !important;
}
.order_title_header thead{
	border: none !important;
}
a.dev_order_table:focus, a.dev_order_table:hover{
    color: #fd0e35 !important;
}

</style>