jQuery(document).ready(function(jQuery) {
	
	jQuery(document).on('click','.btn-csl',function(event){
		var parent_error_div = event.target.closest('.container_div');
		var error_div = jQuery(parent_error_div).find('.errorpayment');
		jQuery( error_div ).each(function( index ) {
			if(!jQuery(this).hasClass('hide')){
				jQuery(this).addClass('hide'); 
			}
		});
	});	
	
	jQuery(document).on('click','.woocommerce-button--next, .woocommerce-button--previous',function(event){
		jQuery('.woocommerce-MyAccount-orders').addClass('edit_account_delete_disable');
		jQuery('.woocommerce-button--next').addClass('edit_account_delete_disable');
		jQuery('.woocommerce-button--previous').addClass('edit_account_delete_disable');
		
		$(this).after("<button style='width:93px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide();
	});
 
	   
   /* Delete the address saved in myaccount */
   jQuery(document).on('click','.btn-dlt',function(){
		var id = jQuery(this).attr('id');
		// alert(id);
		
		$("#"+id).closest('.row').addClass('edit_account_delete');
		$("#"+id).closest('#over_add_flow').addClass('edit_account_delete_disable');
		var base_url = window.location.origin;
		jQuery.ajax({
			url: custom_child_script_ajax.ajaxurl,
			type: "GET",
			data: {'action':'delete_delevery_address_request_js','checkoutAdd':id},
			success:function(response) {
				window.location.reload();
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
		
	});
	
});

var input = document.getElementById('card_num');
input.onkeyup = function (e) {
    if (this.value == this.lastValue) return;
    var caretPosition = this.selectionStart;
    var sanitizedValue = this.value.replace(/[^0-9]/gi, '');
    var parts = [];
    
    for (var i = 0, len = sanitizedValue.length; i < len; i += 4) {
        parts.push(sanitizedValue.substring(i, i + 4));
    }
    
    for (var i = caretPosition - 1; i >= 0; i--) {
        var c = this.value[i];
        if (c < '0' || c > '9') {
            caretPosition--;
        }
    }
    caretPosition += Math.floor(caretPosition / 4);
    
    this.value = this.lastValue = parts.join(' ');
    this.selectionStart = this.selectionEnd = caretPosition;
	
}


jQuery("#expire_date").mask("99/99");
jQuery("#expire_date").keyup(function() {
  //get the date
  var datevalue = jQuery(this).val();

  //only if the date is full like this: 'xx/xx' continue
  if (datevalue.length == 5) {
    verifyDate(datevalue);
  } else {
    clean();
  }
});

function clean() {
  jQuery('#err-txt1_expiry').addClass('hide')
}

function verifyDate(datevalue) {

  if (datevalue != null || datevalue != '') {

    //split the date as a tmp var
    var tmp = datevalue.split('/');

    //get the month and year
    var month = tmp[0];
    var year = tmp[1];

    if (month >= 1 && month <= 12) {
      //clean the message
      clean();

    } else {
		jQuery('#err-txt1_expiry').removeClass('hide')
      // $('#msg').html('Month is invalid.');
    }
  }

}

jQuery("#cvv_numb").keypress(function(key) {	
	if(key.charCode < 48 || key.charCode > 57) return false;
	var value = jQuery(this).val();
	if(value.length >= 3){
		return false;
	}
});

function save_card_data(){
	
	// var isValid = form.valid();
	var is_error = false;
	var is_card_error = false;
	var is_expiry_error = false;
	var is_cvv_error = false;
	var is_postcode_error = false;
	
	jQuery("#cards-input").validate();
	
	
	var card_num 	= jQuery("#card_num").val();
	var card_type = GetCardType(card_num);
	if(card_num=='' || card_type==''){
		is_card_error = true;
		is_error = true;
	}else{		
		try{
			card_num = card_num.replace(/ /g,'');
			var is_card_valid = isValidCreditCard(card_type, card_num);
			if(is_card_valid!=true){
				is_card_error = true;
				is_error = true;
			}
		}catch(e){
			is_card_error = true;
			is_error = true;
		}
		
	}
	
	
	
	var exp_month_new = '';
	var exp_year_new = '';
	var expire_date = jQuery("#expire_date").val();
	if(expire_date!=''){
		expArray = expire_date.split( '/' );
		exp_month_new = ( expArray[ 0 ] );
		exp_year_new = ( expArray[ 1 ] );
		if(exp_month_new !='' && exp_year_new !='')
		{
				is_expiry_error = false;
		}else{
			is_expiry_error = true;
			is_error = true;
		}
	}else{
		is_expiry_error = true;
		is_error = true;
	}
	
	var cvv_numb	= jQuery("#cvv_numb").val();
	if(cvv_numb==''){
		is_cvv_error = true;
		is_error = true;
	}
		
	var bil_pstcde	= jQuery("#bil_pstcde").val(); 
	if(bil_pstcde==''){
		is_postcode_error = true;
		is_error = true;
	}
	
    var errors_div = jQuery( "#cards-input" ).find('.errorpayment');
	if(is_error==true){		
		// console.log(errors_div);
		jQuery(errors_div).each(function( index ) {
			jQuery(this).removeClass('hide');
			// console.log(this);
		});
		if(is_card_error){
			jQuery('.stripe_card_form').addClass('hide');
			jQuery('#err-txt_card').removeClass('hide');
		}else if(is_expiry_error){
			jQuery('.stripe_card_form').addClass('hide');
			jQuery('#err-txt1_expiry').removeClass('hide');
			
		}else if(is_cvv_error){
			jQuery('.stripe_card_form').addClass('hide');
			jQuery('#err-txt2_cvv').removeClass('hide');
			
		}else if(is_postcode_error){
			jQuery('.stripe_card_form').addClass('hide');
			jQuery('#err-txt3_postcode').removeClass('hide');
			
		}
		console.log('error');
		return false;
	}
	jQuery('.stripe_card_form').addClass('hide');
	
	Stripe.setPublishableKey(wc_stripe_params.key);
	var strtoken = '';
	var stripeResponseHandler = Stripe.card.createToken({
		  number: jQuery('#card_num').val(),
		  cvc: 	jQuery('#cvv_numb').val(),
		  exp_month: exp_month_new,
		  exp_year: exp_year_new, 
		  address_zip: jQuery('#bil_pstcde').val(), 
		},function(status, response){
			strtoken = response.id;
			var base_url = window.location.origin; 
 
			$("#edit_payment_card_m").after("<button style='width:280px;' class='button width_button custom_add_card_loader'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
			$("#edit_payment_card_m").hide();
			
 
			jQuery.ajax({
				
				url : base_url+"/wp-content/themes/foodpicky-child/update_myaccount_ajax.php",
				type : "POST",
				data : {'stripe_token':strtoken,'u_card_data':"u_card_data",'card_num':card_num,'expire_date':expire_date,'cvv_numb':cvv_numb,'bil_pstcde':bil_pstcde},
				dataType:'json',
				success : function(data) {
                    if(data == 1){
                        location.reload(true);
                    }
                    var errorProp = 'errors';
                    if(data.hasOwnProperty(errorProp)){
                        jQuery(".custom_add_card_loader").remove();
                        $("#edit_payment_card_m").show();                       
                        
                        jQuery('#err-txt3_stripe_general').removeClass('hide');
                        console.log('errormessage', data.message);
                    }
				},
                error: function(errorThrown){
                    console.log(errorThrown);
                }
			});
			return response.id;
		});
		
}

function deleteCard(cid,cus_id){
		
	// var id = jQuery(this).attr('id');
	// alert();id
    jQuery('.chnge-clr').closest('#show-div_card').addClass('edit_account_delete_disable');
    jQuery('.stripe_card_data_'+cid).addClass('edit_account_delete');
    var base_url = window.location.origin;
	jQuery.ajax({
		url: custom_child_script_ajax.ajaxurl,
		type: "GET",
		data: {'action':'delete_delevery_card_request_js','cardId':cid,'customerId':cus_id},
		success:function(response) {
			//alert(response);
			window.location.reload(true);
			// jQuery('#card-list-div').html(response);
		},
		error: function(errorThrown){
			console.log(errorThrown);
		}
	});
}

 
function GetCardType(number)
{
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null)
        return "Visa";

    // Mastercard
    re = new RegExp("^5[1-5]");
    if (number.match(re) != null)
        return "Mastercard";

    // AMEX
    re = new RegExp("^3[47]");
    if (number.match(re) != null)
        return "AMEX";

    // Discover
    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
        return "Discover";

    // Diners
    re = new RegExp("^36");
    if (number.match(re) != null)
        return "Diners";

    // Diners - Carte Blanche
    re = new RegExp("^30[0-5]");
    if (number.match(re) != null)
        return "Diners - Carte Blanche";

    // JCB
    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
        return "JCB";

    // Visa Electron
    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
    if (number.match(re) != null)
        return "Visa Electron";

    return "";
}
function isValidCreditCard(type, ccnum) {
   if (type == "Visa") {
      // Visa: length 16, prefix 4, dashes optional.
      var re = /^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/;
   } else if (type == "MC") {
      // Mastercard: length 16, prefix 51-55, dashes optional.
      var re = /^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/;
   } else if (type == "Disc") {
      // Discover: length 16, prefix 6011, dashes optional.
      var re = /^6011-?\d{4}-?\d{4}-?\d{4}$/;
   } else if (type == "AmEx") {
      // American Express: length 15, prefix 34 or 37.
      var re = /^3[4,7]\d{13}$/;
   } else if (type == "Diners") {
      // Diners: length 14, prefix 30, 36, or 38.
      var re = /^3[0,6,8]\d{12}$/;
   }
   if (!re.test(ccnum)) return false;
   // Remove all dashes for the checksum checks to eliminate negative numbers
   ccnum = ccnum.split("-").join("");
   // Checksum ("Mod 10")
   // Add even digits in even length strings or odd digits in odd length strings.
   var checksum = 0;
   for (var i=(2-(ccnum.length % 2)); i<=ccnum.length; i+=2) {
      checksum += parseInt(ccnum.charAt(i-1));
   }
   // Analyze odd digits in even length strings or even digits in odd length strings.
   for (var i=(ccnum.length % 2) + 1; i<ccnum.length; i+=2) {
      var digit = parseInt(ccnum.charAt(i-1)) * 2;
      if (digit < 10) { checksum += digit; } else { checksum += (digit-9); }
   }
   if ((checksum % 10) == 0) return true; else return false;
}

	$("#edit-div_address input[name='save_adds-info']").on('click', function() {
		$(this).after("<button style='width:280px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide();

		//return false;
	});
	
	
	
	/* $("#edit_payment_card_m").on('click', function() {
		
		var card = $('#card_num').val();
		var exp = $('#expire_date').val();
		var cvv = $('#cvv_numb').val();
		var postcode = $('#bil_pstcde').val();
		
		if( card != '' && exp != '' && cvv != '' && postcode != ''){
			
		$(this).after("<button style='width:280px;' class='button width_button'><img src='https://devo.co.uk/wp-content/themes/foodpicky-child/img/button-loader.gif' /></button>");
		$(this).hide();
		}

		//return false;
	}); */
	
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}	
	 