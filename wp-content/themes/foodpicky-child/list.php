<?php

if(!empty($_GET['latitude'])){
	setcookie("latitude", $_GET['latitude'], time() + (86400 * 90), "/");
}

if(!empty($_GET['longitude'])){
	setcookie("longitude", $_GET['longitude'], time() + (86400 * 90), "/");
}


global $wp_query;

$post_type = isset($wp_query->query['post_type']) ? $wp_query->query['post_type'] : 'post';
if (is_array($post_type)) {
    $post_type = reset($post_type);
}
$options = get_option(AZEXO_FRAMEWORK);
if (!isset($show_sidebar)) {
    $show_sidebar = isset($options['show_sidebar']) ? $options['show_sidebar'] : 'right';
}
if (!isset($template_name)) {
    $template_name = isset($options['default_' . $post_type . '_template']) ? $options['default_' . $post_type . '_template'] : 'post';
}
if (isset($_GET['template'])) {
    $template_name = $_GET['template'];
}
$additional_sidebar = isset($options[$post_type . '_additional_sidebar']) ? (array) $options[$post_type . '_additional_sidebar'] : array();
get_header();
$have_posts = have_posts();
if ($have_posts && (isset($options['before_list_place']) && ($options['before_list_place'] == 'before_container'))) {
    azexo_before_list($post_type);
}
?>

<div class="<?php print ((isset($options['content_fullwidth']) && $options['content_fullwidth']) ? '' : 'container'); ?> <?php print (is_active_sidebar('sidebar') && ($show_sidebar != 'hidden') ? 'active-sidebar ' . esc_attr($show_sidebar) : ''); ?> <?php print (in_array('list', $additional_sidebar) ? 'additional-sidebar' : ''); ?>">
    <?php
    if ($show_sidebar == 'left') {
        get_sidebar();
    } else {
        if (in_array('list', $additional_sidebar)) {
            get_sidebar('additional');
        }
    }
    ?>

<!---start search box--->	
<?php 	
	
	if (is_search()) {
?>	
	<div class="before-list">
		<p class="result-count"><?php

				$paged = max(1, $wp_query->get('paged'));
				$per_page = $wp_query->get('posts_per_page');
				$total = $wp_query->found_posts;
				$first = ( $per_page * $paged ) - $per_page + 1;
				$last = min($total, $wp_query->get('posts_per_page') * $paged);

				if ($total <= $per_page || -1 === $per_page){ 
					//show postcode user enter value  
					if(isset($_COOKIE['location'])) {
					echo "Delivering to <a href=".esc_url(home_url('/')).">".$_COOKIE['location']."</a>" ;
					}else{
						printf(_n('Showing the single result', 'Delivering to %d results', $total, 'foodpicky'), $total);
					}
				} else {
					printf(_nx('Showing the single result', 'Showing %1$d&ndash;%2$d of %3$d results', $total, '%1$d = first, %2$d = last, %3$d = total', 'foodpicky'), $first, $last, $total);
				}
				?>			
				
				<form role="search" method="get" class="search-form search-product" action="<?php echo home_url( '/' ); ?>">
					<label>
						<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
						<input type="search" class="search-field" size="30"
							placeholder="<?php echo esc_attr_x( 'Search shop for product..', 'placeholder' ) ?>"
							value="<?php echo get_search_query() ?>" name="s"
							title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>"  id="searchProfileShop"  onkeyup="showAutocomResult(this.value)" onblur="autocomploutfocus()" autocomplete="off" />
							<div id="searchresshop"><ul></ul></div>
					</label>
					<input type="submit" class="search-submit"
						value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
				</form>
	</div>
<?php } ?><!---End search box--->
		
    <div id="primary" class="content-area">
        <?php
        if ($options['show_page_title']) {
            get_template_part('template-parts/general', 'title');
        }
        if ($have_posts && (isset($options['before_list_place']) && ($options['before_list_place'] == 'inside_content_area'))) {
            azexo_before_list($post_type);
        }
        ?>
        <div id="content" class="site-content <?php print str_replace('_', '-', $template_name); ?> <?php print ((isset($options['infinite_scroll']) && $options['infinite_scroll']) ? 'infinite-scroll' : '') ?>" role="main">
            <?php
            if (isset($options['author_bio']) && $options['author_bio']) {
                if (is_author()) {
                    get_template_part('template-parts/general', 'author-bio');
					
                }
            }
            ?>
            <?php if ($have_posts) : ?>
                <?php while (have_posts()) : the_post(); ?>
				    <?php  include(locate_template('content.php')); ?>
                <?php endwhile; ?>
            <?php else: ?>
                <?php include(azexo_locate_template('content-none.php')); ?>
            <?php endif; ?>

        </div><!-- #content -->
        <?php
        if ($have_posts) {
            azexo_paging_nav();
        }
        ?>
    </div><!-- #primary -->

    <?php
    if ($show_sidebar == 'right') {
        get_sidebar();
    } else {
        if (in_array('list', $additional_sidebar)) {
            get_sidebar('additional');
        }
    }
    ?>
</div>
<?php get_footer(); ?>