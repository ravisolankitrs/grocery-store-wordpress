<?php
/**
 *  @file functions-checkout.php
 *  @brief This file include all functions and actions for checkout page.
 */
function checkout_enqueue_scripts_theme() {
	/* This Code applies on checkout page only */
	if(function_exists('is_checkout') && is_checkout()){
		wp_enqueue_style( 'checkout-style', get_stylesheet_directory_uri() .'/css/checkout.css',false,'1.1','all');
		wp_enqueue_script( 'custom_devo_delivery_script', get_stylesheet_directory_uri() . '/js/checkout-wp-js.js', array(), '1.0.0', true );
	}

}
add_action( 'wp_enqueue_scripts', 'checkout_enqueue_scripts_theme' );


/**
 * Update User Address Details in User meta
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' , 10, 1 );

function my_custom_checkout_field_update_order_meta( $order_id ) {
	file_put_contents(__DIR__.'/tada.txt',print_r($_POST,true));
	$user_id = get_current_user_id( );
	
	
	if(isset($_POST['hidden_selected'])){
		if($_POST['hidden_selected']==0){
			$addresses = get_user_meta($user_id, 'checkout_saved_address',true);
			$newaddress= array(
				'address_1' => $_POST['billing_address_1'],
				'address_2' => $_POST['billing_address_2'],
				'phone' =>  str_replace(' ', '', $_POST['billing_phone']),
				'postcode' => $_POST['billing_postcode'],
				'rider' => '',
				'user_id' => $user_id,
				'default' => false,
			);
			$uniqueid = uniqid();
			$address_name= 'checkout_add_'.$uniqueid;
			
			$newadd = array($address_name => $newaddress);
			
			
			if(!empty($addresses) && count($addresses) > 0){
				$result	=	array_uintersect($newadd,$addresses,'CustomCompareDeepValue');
				if(empty($result)){
					$addresses[$address_name] = $newaddress;
					update_user_meta($user_id, 'checkout_saved_address',$addresses);
				}
				
			}else{
				update_user_meta($user_id, 'checkout_saved_address',$newadd); 
			}
		}
	}
}

/* Custom Compare code.*/
function CustomCompareDeepValue($val1, $val2)
{
	$address_1 = strcmp($val1['address_1'], $val2['address_1']);
	$address_2 = strcmp($val1['address_2'], $val2['address_2']);	
	return array_sum(array($address_1,$address_2));
}


function custom_devo_deleteDeliveryAddressRequest(){
	$checkout = $_GET['checkoutAdd'];
	$user_id = get_current_user_id();
	$meta_key = "checkout_saved_address";
	$addresses = get_user_meta($user_id, $meta_key);
	// echo"<pre>";print_R($addresses);die;
	unset($addresses[0][$checkout]);
	update_user_meta( $user_id, $meta_key, $addresses[0]);
	die('success');
}
/* AJAX Action Processing Calls */
add_action('wp_ajax_delete_delevery_address_request_js', 'custom_devo_deleteDeliveryAddressRequest');

function custom_devo_deleteCardRequest(){

	$cardId = $_GET['cardId'];
	$customerId = $_GET['customerId'];

	$stripe = new WC_Gateway_Stripe();
	$result = $stripe->stripe_request( array(), 'customers/' . $customerId . '/sources/' . sanitize_text_field( $cardId ), 'DELETE' );
	delete_transient( 'stripe_cards_' . $customerId );
	do_action( 'wc_stripe_delete_card', $customerId, $result );
	if ( is_wp_error( $result ) ) {
		wc_add_notice( __( 'Unable to delete card.', 'woocommerce-gateway-stripe' ), 'error' );
	} else {
		wc_add_notice( __( 'Card deleted.', 'woocommerce-gateway-stripe' ), 'success' );
	}
	die;
}
/* AJAX Action Processing Calls */
add_action('wp_ajax_delete_delevery_card_request_js', 'custom_devo_deleteCardRequest');


/* This code has been changed to accomodate postcode validation in stripe from new textbox */
function custom_validate_address_browser() {

	if(!empty($_POST['browser']) && $_POST['hidden_selected' == 1]){
		$addressBrowser = $_POST['browser'];
		$user_id = get_current_user_id();
		$addresses = get_user_meta($user_id, 'checkout_saved_address',true);

		if(array_key_exists($addressBrowser,$addresses)){
			$sel_address = $addresses[$addressBrowser];
			$_POST['billing_address_1'] = (!empty($sel_address['address_1']))?$sel_address['address_1']:$_POST['billing_address_1'];
			$_POST['billing_address_2'] = (!empty($sel_address['address_2']))?$sel_address['address_2']:$_POST['billing_address_2'];
			$_POST['billing_postcode'] = (!empty($sel_address['postcode']))?$sel_address['postcode']:$_POST['billing_postcode'];
			$_POST['billing_phone'] = (!empty($sel_address['phone']))?$sel_address['phone']:$_POST['billing_phone'];

			$_POST['shipping_address_1'] = (!empty($sel_address['address_1']))?$sel_address['address_1']:$_POST['billing_address_1'];
			$_POST['shipping_address_2'] = (!empty($sel_address['address_2']))?$sel_address['address_2']:$_POST['billing_address_2'];
			$_POST['shipping_postcode'] = (!empty($sel_address['postcode']))?$sel_address['postcode']:$_POST['billing_postcode'];
		}
	}else{
		$_POST['shipping_address_1'] = $_POST['billing_address_1'];
		$_POST['shipping_address_2'] = $_POST['billing_address_2'];
		$_POST['shipping_postcode'] = $_POST['billing_postcode'];
	}
}
add_action('woocommerce_checkout_process', 'custom_validate_address_browser');

add_action('woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta2',5);
function my_custom_checkout_field_update_order_meta2( $order_id ) {
	$stripe_card_postcode = (!empty($_POST['stripe-post-code']))?$_POST['stripe-post-code']:null;
	if(empty($stripe_card_postcode) && !empty($_POST['billing_postcode'])){
		$stripe_card_postcode = $_POST['billing_postcode'];
	}
	if (!empty($stripe_card_postcode)) update_post_meta( $order_id, 'stripe_card_postcode', esc_attr($stripe_card_postcode));
}

function my_woocommerce_add_error( $error ) {
    if( 'The zip code you supplied failed validation.' == $error ) {
        $error = 'The postcode you supplied failed validation.';
    }
    return $error;
}
add_filter( 'woocommerce_add_error', 'my_woocommerce_add_error' );

add_filter( 'woocommerce_validate_postcode', 'gpls_woo_rfq_validate_postcode',1000,3 );

function gpls_woo_rfq_validate_postcode($valid, $postcode, $country){
	$valid=true;
	return $valid;

}