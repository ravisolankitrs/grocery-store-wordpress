<?php 
/* 
 *Template Name: Privacy Policy 
 */ 
get_header(); ?>
<style>
body{	
	font-family: "Neutraface Text","Zona Pro",Helvetica,Arial,Serif;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    color: #2D2D2D;
}
.header-text2{
	text-align:left;
	padding-left:0px;
}
.terms_header, h3{
	font-weight: 700;
    font-size: 24px;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    line-height: 1.1;
    color: inherit;
    box-sizing: border-box;
}
p{
	font-size: 14px;
    line-height: 1.42857143;
}
a{
	color: #FF6C61;
}
a:hover {
    text-decoration: underline;
}
a:visited {
   color: #FF6C61;
}
ul {
    list-style-type: disc;
	padding-left: 3%;
	color: #2D2D2D;
}
ol{
	padding-left: 3%;
}
strong{
	color: #2D2D2D;
    font-weight: 700;
}
p strong{
	font-size: 14px;
}
.custom_strong strong,strong{
	color: #2D2D2D;
    font-weight: 700;
    font-size: 14px;
}
.custom_ol_alpha{
	list-style: lower-alpha;
	padding-left: 0px;
}
ol ol ol {
	list-style: lower-roman;
	padding-left: 0px;
}
</style>

<div id="primary" class="content-area 123">
    <main id="main" class="site-main testing" role="main">

        <article id="post-13546" class="post-13546 page type-page status-publish hentry">
            <header class="entry-header">
			<!--<div class="jumbotron">
				<div class="container">
					<h1 class="entry-title">Privacy Policy</h1> </header>
				</div>
			</div>-->
			<div class="dev-page-header-inner dev-outer">
				<div class="dev-inner dev-text-centered">
					<h2 class="text-head">Privacy </h2>
				</div>
			</div>
            <!-- .entry-header -->
			<div class="container">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="entry-content">
							<p><strong>Introduction</strong></p>
							
							<p>This Privacy Policy relates to the website URL <a href="http://www.devo.co.uk">www.devo.co.uk</a> and our mobile application versions (our <strong>"Service"</strong>) which are owned and operated by Devo Technologies Ltd (trading as Devo), a company registered in England and Wales under company number 9996712 whose registered office is at 20-22 Wenlock Road, London N1 7GU (Devo). Any reference to we or us in this Privacy Policy means Devo.</p>

							<p>This Privacy Policy (and any other documents referred to on it) sets out the basis on which any personal information we collect from you, or that you provide to us, will be processed by us and applies to your use of our Service.</p>
							<p>For the purpose of the Data Protection Act 1998 including any subsequent legislation (the Act), the data controller is Devo Technologies Ltd (trading as Devo), a company registered in England and Wales under company number 9996712 whose registered office is at 20-22 Wenlock Road, London N1 7GU.</p>
							<p>This Privacy Policy is regularly reviewed to ensure that we continue to serve your privacy interests. We reserve the right to update this Privacy Policy from time to time, with any updates published on our Service. We therefore encourage you to review our Privacy Policy periodically for the most up to date information on our privacy practices. We will not however substantially change the way we use personal information you have already provided to us without the appropriate prior agreement. By continuing to use our Service after a Privacy Policy change you are indicating your acceptance of the revised Privacy Policy terms.</p>
							<p>Please read this Privacy Policy carefully before you browse our Service and/or register with us. If you do not agree with the ways in which we intend to use your personal information then please do not continue to use our Service.</p>
							<p><strong>Who this Privacy Policy applies to</strong></p>
							
							<p>Devo is a digital marketplace where retailers (Sellers), through our Service, sell their products and couriers (Couriers) deliver those products on the Sellers behalf to consumers. Although Devo does not at any point have any contractual relationship with or liability to consumers regarding their purchase of Sellers products via our Service, it directs and facilitates the relationships between consumers and Sellers, Sellers and Couriers, and Sellers with other third parties such as brand owners of the products sold by Sellers via our Service, social media, analytics and the payment software provider as further described in this Privacy Policy.</p>
							<p>You do not need to register to browse our Service, and this Privacy Policy sets out how we will use your personal information, regardless of whether you register with us, when you are generally browsing and using our Service, but if you want to purchase any products via our Service then you will need to register with us.</p>
							<p><strong>Keeping you informed</strong></p>
							
							<p>Registering with us means that we will use your details based on interests you have shown when browsing our Service or purchasing products via our Service to identify other products, offers, promotions, information and services within the Devo digital marketplace or its wider business portfolio which may be of interest to you. We may contact you about these products and services by email/text/picture/video/social media messaging service, phone call or post using the contact details you provide on registration or update us with or which we obtain or update through third party sources. You can update your preferences by emailing us or writing to us, please see the 'Contact us' section of this Privacy Policy.</p>
							<p><strong>Collecting and using your personal information</strong></p>
							
							<p>We may collect and process the following personal information about you:</p>
							<p><strong>Information you give us. </strong>You may give us information about you by:</p>
							<ul>
								<li>browsing our Service (eg your IP address, operating system and browser type).</li>
								<li>registering on our Service.</li>
								<li>purchasing products via our Service.</li>
								<li>corresponding with us (or our customer support partners) by phone, e-mail or otherwise [(we may record and/or monitor calls for quality checks, staff training and to help us detect, prevent or combat fraud or money laundering)].</li>
							</ul>
							<p>This includes information you provide when you browse our Service, register to use our Service, search for a product, place item(s) in your basket, place an order via our Service, participate in social media functions on our Service or carry out any other activity via our Service, or when you report a problem with our Service or discuss an order or your registration on our Service or use of it with us. The personal information you give us may include your name, billing and delivery address, e-mail address, phone number, date of birth and/or age, financial and credit card information and anything else that you choose to share with us when doing of the activities mentioned in this section.</p>
							<p>We will use this information:</p>
							<p>to provide you with the information and services that you request from us (We provide customer support services for orders placed with Sellers via our Service, please see our <a href="/terms-conditions#terms-consumers">Terms and Conditions for Consumers </a> for more details).</p>
							<p>To check or confirm your identity if you contact us about an order you have placed via our Service or to identify, prevent, detect or tackle fraud, money laundering or other crime.</p>
							<p>to provide you with updates relating to your order placed via our Service.</p>
							<p>to provide you with information about other goods and services available via our Service (please see the 'Keeping you informed' section of this Privacy Policy).</p>
							<p>to remind you about items in your basket that you haven't yet proceeded to purchase.</p>
							<p>to use features of social media platforms, for example if you 'like' a page or product or share a 'tweet' then we may link the information from the marketing features of those social media platforms to information we collect through our Service to help us form a better understanding of your interests and to keep you updated with our Service (please see the 'Keeping you informed' section of this Privacy Policy).</p>
							<p>to notify you about changes to our Service.</p>
							<p>to ensure that content from our Service is presented in the most effective manner for you and for your device (computer, tablet, phone or other device). We may use information from cookies and similar technologies to help us with this</p>
							<p>to create aggregated or statistical data to use for analytical or research purposes to understand and improve customer service, website experience, and your interest in products and services. We may share aggregated or statistical information with third party for their similar purposes.</p>
							<p><strong>Information we collect about you.</strong> Depending on your settings (see our <a href="http://devo.co.uk/cookies">Cookie Policy</a> ) we may automatically collect the following information:</p>
							<p>technical information, including the Internet protocol (IP) address used to connect your computer or other device to our Service, your login information, browser type and version, time zone setting, location data, browser plug-in types and versions, operating system and platform.</p>
							<p>information about your visit to our Service, including the full Uniform Resource Locators (URL) clickstream to, through and from our Service (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our customer support services number.</p>
							<p>We will use this information:</p>
							<p>to administer our Service and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes.</p>
							<p>to improve our Service to ensure that content is presented in the most effective manner for you and your device.</p>
							<p>to allow you to participate in interactive features of our Service with social media platforms.</p>
							<p>as part of our efforts to keep our Service safe and secure.</p>
							<p>to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you.</p>
							<p>to make suggestions and recommendations to you and other users of our Service about goods or services that may interest you or them.</p>
							<p>to report sales, statistics and other analysis to Sellers and brand owners so that they can use that information to better understand consumers' purchasing characteristics generally.</p>
							<p><strong>Information we receive from and share with other sources.</strong>&nbsp;We work closely with the following third parties:</p>
							
							<ul>
								<li>Sellers who sell their products via our Service.</li>
								<li>Couriers who, acting on behalf of Sellers, deliver the orders you place via our Service to you. This includes Proof of Age verifiers provided at point of delivery of orders.</li>
								<li>Our partner Stripe (a trading name of Stripe Inc) who provides the payment software for the processing of all orders through our Service and who may process your personal information for counter fraud and anti money-laundering purposes as well as payment processing.</li>
								<li>Sub-contractors in technical and customer support roles.</li>
								<li>Advertising and marketing, analytics and social media providers.</li>
								<li>Brand owners of the products sold via our Service.</li>
							</ul>
							<p>We may combine this information with information you give to us and information we collect about you. We may use this information and the combined information for the purposes set out above (depending on the types of information we receive).</p>
							<p>We may also share your personal details with any person (and their and our legal and professional advisers for this purpose) with whom we are negotiating any sale, transfer or re-organisation of the business. If this proceeds the acquiring party of the business or part of it, may use your personal information in the same ways as set out in this Privacy Policy.</p>
							<p><strong>Storage of your personal information</strong></p>
							</p>
							<p>All personal information you provide to us will be stored on our secure servers or those of third parties or contractors we engage to help us run our business. We will keep information no longer than we need to for the purpose for which we collected it. The period of time for which we keep personal information depends on any legal retention period to which we are subject and according to business needs. If you have registered with us then we will keep your personal information for at least as long as you are registered with us.</p>
							<p>The personal information that we collect from you may be transferred to and held by us, or third parties/contractors at a destination outside the European Union that does not have equivalent data protection laws to those in the UK. We will put in place appropriate measures for the secure and confidential treatment of such personal information.</p>
							<p>Where we have given you (or where you have chosen) a password which enables you to access certain parts of our Service, for example when you register with us, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>
							<p>Unfortunately, the transmission of information via the internet is not completely secure. Although we take appropriate measures to protect your personal information, we cannot guarantee the security of your personal information transmitted to our Service and any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access. We ask you not to put any details that are particularly private into free text fields.</p>
							<p>Our Service includes social networking and chat room features. Please ensure when using these features that you do not submit any personal information that you do not want to be seen, collected or used by other users.</p>
							<p><strong>Other disclosures of your personal information</strong></p>
							
							<p>We may disclose your personal information to third parties if we are under a duty to disclose or share it in order to comply with any legal obligation, or in order to enforce or apply our <a href="/terms-conditions">Terms of Website Use</a> or our <a href="/terms-conditions#terms-consumers">Terms and Conditions for Consumers</a> or where we think it appropriate to protect the rights, property, or safety of Devo, its staff and contractors and partners, visitor to our Service, our registered consumers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and anti money-laundering.</p>
							<p><strong>Links to other websites</strong></p>
							
							<p>Our Service may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites and any services that may be accessible through them (including those linked to your location) have their own privacy policies and that we do not accept any responsibility or liability for these policies or for the processing of your personal information by those parties. Please check those policies before you submit any personal information to these websites.</p>
							<p><strong>Your rights</strong></p>
							
							<p>You have the right to ask us not to process your personal information for direct marketing purposes. You can exercise this right by contacting us (please see the 'Contact us' section of this Privacy Policy).</p>
							<p>Subject to certain exceptions, the Act gives you the right to access personal information we hold about you. You can exercise this right by emailing or writing to us (please see the Contact us section of this Privacy Policy) to request this and sending us the prescribed <?php echo (function_exists('WC'))?get_woocommerce_currency_symbol():'�';?>10 to meet our costs in providing you with details of the information we hold about you.</p>
							<p>Please help us to keep your personal details up to date by promptly notifying us of any changes, for example if you change address or telephone number. If you need to change your personal details or preferences at any time please login to your account on our Service to change your settings or contact us (please see the 'Contact us' section of this Privacy Policy).</p>
							<p><strong>Changes to our privacy policy</strong></p>
							
							<p>We reserve the right to update or change this Privacy Policy from time to time and you should check back frequently to see any such updates or changes. We may also notify you of any updates or changes by email. If you do not agree to any updates or changes, please stop using our Service.</p>
							
							
							<p><strong>Contact us</strong></p>
							
							
							<p>Questions, comments and requests regarding this privacy policy are welcomed and should be sent</p>
							<p>to us by email at hello@devo.co.uk or in writing to Devo Technologies Ltd, 20-22 Wenlock Road, London N1 7GU.</p>
							<p>This Privacy Policy was last updated on 03 June 2017</p>
							
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
            <!-- .entry-content -->
            <!-- .entry-footer -->
        </article>
        <!-- #post-## -->

    </main>
    <!-- #main -->
</div>
<?php
get_footer();
?>