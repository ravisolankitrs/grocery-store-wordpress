<?php
add_action( 'admin_menu', 'devo_push_notification_dashboard_menu');
function devo_push_notification_dashboard_menu(){
    add_submenu_page( 
        'tools.php', 'Push Notification Dashboard', 'Push Notification Dashboard', 'manage_options', 'devo-pn-dashboard','devo_push_notification_dashboard_body'
    );
}

function devo_push_notification_dashboard_body() {
    echo '<div class="wrap">';
    echo '<h2>Push Notification Dashboard</h2>';
    echo '</div>';
}