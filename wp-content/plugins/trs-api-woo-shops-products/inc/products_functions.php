<?php

/**
 *  @brief Brief
 *  returns Search product with matching query and categories
 *  @param [in] $query Parameter_Description : Searched Product text
 *  @param [in] $cid Parameter_Description : Product Category
 *  @return Return_Description : return Products with matching query and matching categorie
 *  
 *  @author TRS Software Solutions
 *  @details Details
 */
function searchProductWithCategoryQuick($query,$shopid,$aid,$postcode,$cid,$page=1){
	$query=urldecode($query);
	$query = str_replace("’", "'", $query);
	$taxdata = array();
	if(!empty($cid)){
		$taxdata = array(
				 array(
					'taxonomy'      => 'product_cat',
					'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
					'terms'         => array($cid),
					'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
				)
			);
	}
	$args = array(
		'post_type'             => 'product',
        'author'                =>  $aid,
		'paged'					=> $page,
		'posts_per_page' 		=> 10,
		'post_status'           => 'publish',
		's' 					=> $query,
		'orderby'     			=> 'title', 
		'order'      			=> 'ASC',
		// 'nopaging'				=> true,
		'ignore_sticky_posts'   => 1,
		'tax_query'				=>$taxdata,
		'meta_query'            => array(
			array(
				'key'           => '_visibility',
				'value'         => array('catalog', 'visible'),
				'compare'       => 'IN'
			)
		),
	);
	
    
    //$search_code = strtolower($postcode);
   // $shop_id = $shopid;
   // $shop_meta = get_post_meta($shop_id,'profile_delivery_data',true);
    
    // $delivery_time = 0.00;
    // $delivery_charge = 0.00;
    // if(!empty($shop_meta) && !empty($shop_meta[$search_code])){               
        // $delivery_charge    =  $shop_meta[$search_code][1];
        // $delivery_time      =  $shop_meta[$search_code][2];
    // }
    
    
    
	$loop = new WP_Query( $args );
	
	 // echo"<pre>";print_R($loop);die;
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();
		global $product; 
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($pid), 'thumbnail_size' );
		$imgurl = $thumb[0];
		//$gprice=$product->price;
		//$regular_price = $product->regular_price;

		//$sale_price = $product->sale_price;
		
		////$cat_id=wp_get_post_terms( $loop->post->ID, 'product_cat');
		//$cat_id=(!empty($cat_id) && isset($cat_id[0]->term_id))?$cat_id[0]->term_id:'';
        
		$e[] = array(
			'Title' => $ptitle,
			'Id' => $pid,
			'Image' => $imgurl,
			//'Slug' 	=> (!empty($cslug))?$cslug:'',
			//'RegularPrice' => $regular_price,
			//'SalePrice' => $sale_price,
			//'Cat_id'	=>$shop_id,
			//'shop_author_id'    =>$aid,
            //'shopprice'	=>$delivery_charge,
            //'delivery_time'	=>$delivery_time,
		);
      
	endwhile;
	$products=json_encode(array('products'=>$e));
	return $products;
}

/**
 *  @brief Brief
 *  returns Search product with matching query and categories
 *  @param [in] $query Parameter_Description : Searched Product text
 *  @param [in] $cid Parameter_Description : Product Category
 *  @return Return_Description : return Products with matching query and matching categorie
 *  
 *  @author TRS Software Solutions
 *  @details Details
 */
function searchProductWithCategory($query,$shopid,$aid,$postcode,$cid,$page=1){
	$query=urldecode($query);
	$query = str_replace("’", "'", $query);
	$taxdata = array();
	if(!empty($cid)){
		$taxdata = array(
				 array(
					'taxonomy'      => 'product_cat',
					'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
					'terms'         => array($cid),
					'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
				)
			);
	}
	$args = array(
		'post_type'             => 'product',
        'author'                =>  $aid,
		'paged'					=> $page,
		'posts_per_page' 		=> TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
		'post_status'           => 'publish',
		's' 					=> $query,
		'orderby'     			=> 'title', 
		'order'      			=> 'ASC',
		// 'nopaging'				=> true,
		'ignore_sticky_posts'   => 1,
		'tax_query'				=>$taxdata,
		'meta_query'            => array(
			array(
				'key'           => '_visibility',
				'value'         => array('catalog', 'visible'),
				'compare'       => 'IN'
			)
		),
	);
	
    
    $search_code = strtolower($postcode);
    $shop_id = $shopid;
    $shop_meta = get_post_meta($shop_id,'profile_delivery_data',true);
    
    $delivery_time = 0.00;
    $delivery_charge = 0.00;
    if(!empty($shop_meta) && !empty($shop_meta[$search_code])){               
        $delivery_charge    =  $shop_meta[$search_code][1];
        $delivery_time      =  $shop_meta[$search_code][2];
    }
    
    
    
	$loop = new WP_Query( $args );
	
	 // echo"<pre>";print_R($loop);die;
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();
		global $product; 
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;
        
        $terms = wp_get_post_terms( $pid, 'product_cat' );
        $termsArray = array_map(function($e) {
            return is_object($e) ? $e->term_id : $e['term_id'];
        }, $terms);
        
        $isAlcohalProduct = (in_array('49', $termsArray))?1:0;
        $isTobaccoProduct = (in_array('52', $termsArray))?1:0;
        
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($pid), 'thumbnail_size' );
		$imgurl = $thumb[0];
		$gprice=$product->price;
		$regular_price = $product->regular_price;

		$sale_price = $product->sale_price;
		
		$cat_id=wp_get_post_terms( $loop->post->ID, 'product_cat');
		$cat_id=(!empty($cat_id) && isset($cat_id[0]->term_id))?$cat_id[0]->term_id:'';
        
		$e[] = array(
			'Title' => $ptitle,
			'Id' => $pid,
			'Image' => $imgurl,
			'Slug' 	=> (!empty($cslug))?$cslug:'',
			'RegularPrice' => $regular_price,
			'SalePrice' => $sale_price,
			'Cat_id'	=>$shop_id,
			'shop_author_id'    =>$aid,
            'shopprice'	=>$delivery_charge,
            'delivery_time'	=>$delivery_time,
            'isAlcohalProduct'	=> $isAlcohalProduct,
            'isTobaccoProduct'	=> $isTobaccoProduct,
		);
      
	endwhile;
	$products=json_encode(array('products'=>$e));
	return $products;
}

/**
 *  @brief Brief
 *  Provides a global search between all stores for any product by product name, and provide all shops
 *  @param [in] $query Parameter_Description : Searched Product text
 *  @param [in] $postcode Parameter_Description : To look in particular post code shops
 *  @return Return_Description : matching products and their shops
 *  
 *  @author TRS Software Solutions
 *  @details Details 
 *  Provides a global search between all stores for any product by product name, and provide all shops
 */
function mod_global_search($query,$postcode){
    
    $postcode_shops = get_shops_by_postcode_array($postcode);
    $shop_owners = array();
    if(!empty($postcode_shops)){
        foreach($postcode_shops as $shop_id){
            $author_id = get_post_field( 'post_author',$shop_id );
            $shop_owners[$author_id] = $shop_id;
        }
    }
    
    if(empty($shop_owners)){
        $aa = array('Products'=>array(),'subcats' =>array());
        $mnb = json_encode($aa);
        wp_reset_query();
        return $mnb;
    }
    $search_code = strtolower($postcode);
    
    
	$products_data=array();
	$final_subcats = array();
	$sub_cat_array = array();
	if(count($shop_owners)>0){
		$args = array(
			'post_type'             => 'product',
            'author__in'            =>  array_keys($shop_owners),
			'post_status'           => 'publish',
			's' 					=> $query,
			'orderby'     			=> 'title', 
			'order'      			=> 'ASC',
			'nopaging'				=> true,
			'ignore_sticky_posts'   => 1,
			'meta_query'            => array(
				array(
					'key'           => '_visibility',
					'value'         => array('catalog', 'visible'),
					'compare'       => 'IN'
				)
			),
		);

		$loop = new WP_Query( $args );
		
		$fl = 0;
        
        $all_products_of_shop = $count = $loop->post_count;
		
		while ( $loop->have_posts() ) : $loop->the_post();global $product; 
			$ptitle = $loop->post->post_title;
			$pid = $loop->post->ID;
            
            $terms = wp_get_post_terms( $pid, 'product_cat' );
            $termsArray = array_map(function($e) {
                return is_object($e) ? $e->term_id : $e['term_id'];
            }, $terms);
            
            $isAlcohalProduct = (in_array('49', $termsArray))?1:0;
            $isTobaccoProduct = (in_array('52', $termsArray))?1:0;
            
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
			$imgurl = $thumb[0];
			$gprice=$product->price;
			$regular_price = $product->regular_price;
			$sale_price = $product->sale_price;
            
            $author_id = get_post_field( 'post_author',$pid );
            $shop_id = $shop_owners[$author_id];
            $shop_meta = get_post_meta($shop_id,'profile_delivery_data',true);
            
            $delivery_time = 0.00;
            $delivery_charge = 0.00;
            $shop_name =  html_entity_decode(get_post_field( 'post_title',$shop_id )) ;
            $shop_slug =  get_post_field( 'post_name',$shop_id );
            $shop_description = html_entity_decode(get_post_field( 'post_excerpt',$shop_id ));
            
            $timing_opened = false;
            $hours = get_post_meta($shop_id, 'working-hours-' . date('N', time() + get_option('gmt_offset') * HOUR_IN_SECONDS) . '-hours');
            if(!empty($hours) && in_array(date('G', time() + get_option('gmt_offset') * HOUR_IN_SECONDS), $hours)) {
                $timing_opened = true;
            }
            
            $profile_status = '';
                        
            if($all_products_of_shop > 0 && $timing_opened==true){
                $profile_status = 'opened';
            }elseif($all_products_of_shop > 0 && $timing_opened==false){
                $profile_status = 'closed';
            }else if($all_products_of_shop < 1){
                $profile_status = 'coming-soon';
            }
            
            
           
            if(!empty($shop_meta) && !empty($shop_meta[$search_code])){               
                $delivery_charge    =  $shop_meta[$search_code][1];
                $delivery_time      =  $shop_meta[$search_code][2];
            }
            
            
            $shop_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($shop_id), 'thumbnail_size' );
			$shop_image = $shop_thumb[0];
			
			$sub_cat_array[]=array(
				'shop_name' =>$shop_name,
				'shop_id'   =>$shop_id,
				'shop_author_id'    =>$author_id,
				'shop_slug' =>$shop_slug,
				'shop_products' =>count_user_posts( $author_id , 'product' ),
				'shop_image'    =>(!empty($shop_image))?$shop_image:'',
				'shop_status'    =>$profile_status,
				'shop_time'    =>$hours,
			);
            
           
            $products_data[] = array(
                'Title' => html_entity_decode($ptitle),
                'Id' 	=> $pid,
                'Image' => $imgurl,
                'RegularPrice' => $regular_price,
                'SalePrice' =>  $sale_price,
                'shop_id'	=>  $shop_id,
				'shop_author_id'    =>$author_id,
                'time'		=> (!empty($delivery_time))?$delivery_time:'',
                'shoptype'	=> (!empty($shop_name))?$shop_name:'',
                'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
                'isAlcohalProduct'	=> $isAlcohalProduct,
                'isTobaccoProduct'	=> $isTobaccoProduct,
            );
		
		endwhile;
		
		$final_subcats = array_values(super_unique($sub_cat_array) );
	}
	$aa = array('Products'=>$products_data,'subcats' => $final_subcats);
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}


function mod_global_search_new($query,$postcode){
    
    $postcode_shops = get_shops_by_postcode_array($postcode);
    $shop_owners = array();
    if(!empty($postcode_shops)){
        foreach($postcode_shops as $shop_id){
            $author_id = get_post_field( 'post_author',$shop_id );
            $shop_owners[$author_id] = $shop_id;
        }
    }
    
    if(empty($shop_owners)){
        $aa = array('Products'=>array(),'subcats' =>array());
        $mnb = json_encode($aa);
        wp_reset_query();
        return $mnb;
    }
    $search_code = strtolower($postcode);
    
    
	$final_subcats = array();
	$sub_cat_array = array();
	$i=0;
    foreach($shop_owners as $author_id=>$shop_owner){
		$i++;
        $products_data=array();
        
        $shop_id = $shop_owner;
        $shop_name =  html_entity_decode(get_post_field( 'post_title',$shop_id )) ;
        $shop_slug =  get_post_field( 'post_name',$shop_id );
        $shop_description = html_entity_decode(get_post_field( 'post_excerpt',$shop_id ));
        $shop_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($shop_id), 'thumbnail_size' );
        $shop_image = $shop_thumb[0];
        $shop_meta = get_post_meta($shop_id,'profile_delivery_data',true);    
        
        $delivery_time = 0.00;
        $delivery_charge = 0.00;
        if(!empty($shop_meta) && !empty($shop_meta[$search_code])){               
            $delivery_charge    =  $shop_meta[$search_code][1];
            $delivery_time      =  $shop_meta[$search_code][2];
        }
        
        
        $shopdata = array(
            'shop_name' =>$shop_name,
            'shop_id'   =>$shop_owner,
            'shop_author_id'    =>$author_id,
            'shop_slug' =>$shop_slug,
            'shop_products' =>count_user_posts( $author_id , 'product' ),
            'shop_image'    =>(!empty($shop_image))?$shop_image:'',
            'time'		=> (!empty($delivery_time))?$delivery_time:'',
            'shoptype'	=> (!empty($shop_name))?$shop_name:'',
            'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
        );
        /* if($i==1){
			unset($shopdata['shopprice']);
			$shopdata['shop_price'] = (!empty($delivery_charge))?$delivery_charge:'';
		} */
        
        $args = array(
			'post_type'             => 'product',
            'author__in'            =>  array($author_id),
			'post_status'           => 'publish',
			's' 					=> $query,
			'orderby'     			=> 'title', 
			'order'      			=> 'ASC',
			'nopaging'				=> true,
			'ignore_sticky_posts'   => 1,
			'meta_query'            => array(
				array(
					'key'           => '_visibility',
					'value'         => array('catalog', 'visible'),
					'compare'       => 'IN'
				)
			),
		);
        
        $loop = new WP_Query( $args );
        
        while ( $loop->have_posts() ) : $loop->the_post();global $product; 
			$ptitle = $loop->post->post_title;
			$pid = $loop->post->ID;
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
			$imgurl = $thumb[0];
			$gprice=$product->price;
			$regular_price = $product->regular_price;
			$sale_price = $product->sale_price;
            
            $author_id = get_post_field( 'post_author',$pid );
            $shop_id = $shop_owners[$author_id];
            
           
            $products_data[] = array(
                'Title' => html_entity_decode($ptitle),
                'Id' 	=> $pid,
                'Image' => $imgurl,
                'RegularPrice' => $regular_price,
                'SalePrice' =>  $sale_price,
                'shop_id'	=>  $shop_id,
            );
            
            
		
		endwhile;
        $shopdata['products'] = $products_data;
        $sub_cat_array[] = $shopdata;
    }    
    
	$aa = array('shopData'=>$sub_cat_array);
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}

function mod_global_search_new_quick($query,$postcode,$page=1){
    
    $postcode_shops = get_shops_by_postcode_array($postcode);
    $shop_owners = array();
    if(!empty($postcode_shops)){
        foreach($postcode_shops as $shop_id){
            $author_id = get_post_field( 'post_author',$shop_id );
            $shop_owners[$author_id] = $shop_id;
        }
    }
    
    if(empty($shop_owners)){
        $aa = array('products'=>array());
        $mnb = json_encode($aa);
        wp_reset_query();
        return $mnb;
    }
    $search_code = strtolower($postcode);
    
    
	$products = array();
	$allAuthors = array_keys($shop_owners);    
	$args = array(
		'post_type'             => 'product',
		'author__in'            => $allAuthors,
		'paged'					=> $page,
		'posts_per_page' 		=> 10,
		'post_status'           => 'publish',
		's' 					=> $query,
		'orderby'     			=> 'title', 
		'order'      			=> 'ASC',
		'nopaging'				=> false,
		'ignore_sticky_posts'   => 1,
		'meta_query'            => array(
			array(
				'key'           => '_visibility',
				'value'         => array('catalog', 'visible'),
				'compare'       => 'IN'
			)
		),
	);
        
	$loop = new WP_Query( $args );
        
	while ( $loop->have_posts() ) : $loop->the_post();global $product; 
			$ptitle = $loop->post->post_title;
			$pid = $loop->post->ID;
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
			$imgurl = $thumb[0];            
            $products[] = array(
                'Title' => html_entity_decode($ptitle),
                'Id' 	=> $pid,
                'Image' => $imgurl,
            );
	endwhile;
	$aa = array('products'=>$products);
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}

/**
 *  @brief Brief
 *  Remove Duplicates from Multidimenstional Array
 *  @param [in] $array Parameter_Description
 *  @return Return_Description
 *  Unique multidimensional Array
 *  @author TRS Software Solutions
 *  @since 4:09 PM Tuesday, December 20, 2016
 *  @modification_date 
 *  @details Details 
 */
function super_unique($array)
{
  $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

  foreach ($result as $key => $value)
  {
    if ( is_array($value) )
    {
      $result[$key] = super_unique($value);
    }
  }

  return $result;
}

/**
 *  @brief Brief
 *  
 *  @param [in] $cid Parameter_Description
 *  @return Return_Description
 *  @author Author Name
 *  @details Details
 */
function get_product_by_cat($cid,$sid,$postcode,$page=1){
	$search_code = strtolower($postcode);
	 
	$args = array( 
		'post_type' => 'product',
		'author' =>$cid,
		'paged'		=> $page,
		'posts_per_page' => TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
		 /* 'tax_query'             => array(
			array(
				'taxonomy'      => 'product_cat',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => $cid,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			),
		),  */
		// 'meta_key' => 'total_sales',
		// 'orderby' => 'meta_value_num',
		'orderby' => 'title',
		'order'   => 'ASC',
		// 'meta_query' => WC()->query->get_meta_query()
	);
	$loop = new WP_Query( $args );
	// echo "<pre>"; print_R($loop); die;
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();global $product; 
		// print_R($loop->post->post_title); die;
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;
        
        $terms = wp_get_post_terms( $pid, 'product_cat' );
        $termsArray = array_map(function($e) {
            return is_object($e) ? $e->term_id : $e['term_id'];
        }, $terms);
        
        $isAlcohalProduct = (in_array('49', $termsArray))?1:0;
        $isTobaccoProduct = (in_array('52', $termsArray))?1:0;
        
        
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
		$imgurl = $thumb[0];
		$gprice=$product->price;
		$regular_price = $product->regular_price;
		$cslug = $product->slug;
		$sale_price = $product->sale_price;
		//$shoptype = get_the_title( $sid );
         $profile_delivery_record = get_post_meta($sid,'profile_delivery_data',true);
      
         if(array_key_exists($search_code,$profile_delivery_record))
        {
            $get_search_code = $profile_delivery_record[$search_code];
            
            $postcode = (!empty($get_search_code[0]))?$get_search_code[0]:'';
            $delivery_charge = (!empty($get_search_code[1]))?$get_search_code[1]:'';
            $delivery_time = (!empty($get_search_code[2]))?$get_search_code[2]:'';
           
        }	 
        
        
		$e[] = array(
			'Title' => $ptitle,
			'Id' 	=> $pid,
			'Image' => $imgurl,
			'Slug' 	=> (!empty($cslug))?$cslug:'',
			'RegularPrice' => $regular_price,
			'SalePrice' => $sale_price,
			'Cat_id'	=>$cid,
			'time'		=> (!empty($delivery_time))?$delivery_time:'',
			'shoptype'	=> (!empty($shoptype))?$shoptype:'',
			'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
			'isAlcohalProduct'	=> $isAlcohalProduct,
            'isTobaccoProduct'	=> $isTobaccoProduct,
		);
		
		
		// $category_postcodes = get_field('profile_delivery_data', 'product_cat'.$cid);
		
		
		/* foreach($category_postcodes as $cat_postcode) {
			$post_code = $cat_postcode['postcode']; 
			if($post_code == $search_code){
				$delivery_charge = $cat_postcode['delivery_group'];		
				$delivery_time = $cat_postcode['delivery_time'];
				$shoptype=get_woocommerce_term_meta( $cid, 'custom_shop_type', true );
		
				$e[] = array(
					'Title' => $ptitle,
					'Id' 	=> $pid,
					'Image' => $imgurl,
					'Slug' 	=> (!empty($cslug))?$cslug:'',
					'RegularPrice' => $regular_price,
					'SalePrice' => $sale_price,
					'Cat_id'	=>$cid,
					'time'		=> (!empty($delivery_time))?$delivery_time:'',
					'shoptype'	=> (!empty($shoptype))?$shoptype:'',
					'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
				);
			}
		} */
		
	endwhile;
	
	$args = array(
	   'show_option_none' => '',
	   'hide_empty' => 0,
	   // 'parent' => $cid,
	    'parent'	=>0,
	   'taxonomy' => 'product_cat'
	);
	$subcats = get_categories($args);
	$sub_cat_array=array();
	foreach($subcats as $category){
        if($category->term_id == 52){
            continue; // Code was added to hide ciggrate or alcohal product from categories.
        }
        
		$thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true ); 
		$image = wp_get_attachment_url( $thumbnail_id ); 
		$sub_cat_array[]=array(
			'sub_cat_name'=>$category->cat_name,
			'sub_cat_id'=>$category->term_id,
			'sub_cat_slug'=>$category->slug,
			'sub_cat_products'=>$category->category_count,
			'sub_cat_image'=>(!empty($image))?$image:'',
		);
	}
	$aa = array('Products'=>$e,'subcats' => $sub_cat_array );
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}

/**
 *  @brief Brief
 *  get_product_by_shop_and_cat
 *  @param [in] $shopid   Parameter_Description
 *  @param [in] $cid      Parameter_Description
 *  @param [in] $postcode Parameter_Description
 *  @return Return_Description
 *  @author : TRS
 *  @creation_date : 4:59 PM 23 December, 2016
 *  @modification_date : 4:59 PM 23 December, 2016
 *  @modification_task : 
 *  @details Details
 */
function get_product_by_shop_and_cat($shopid,$sid,$cid,$postcode,$page=1){
	// die('tada');
	$search_code = strtolower($postcode);
    $args = array(
        'post_type' => 'product',
        'author' => $shopid,
        'paged'		=> $page,
        'posts_per_page' => TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
        'tax_query'     => array(
            array(
                'taxonomy'  => 'product_cat',
                'field'     => 'id', 
                'terms'     => $cid
            )
        )
    );

	/* $args = array(
		'post_type' => 'product',
		'paged'		=> $page,
		'posts_per_page' => TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
		'author' => $shopid,
		'tax_query' =>  //array(
			//'relation' => 'AND',
			array(
				'taxonomy'      => 'product_cat',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => $cid,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			), 
			 /* array(
				'taxonomy'      => 'product_cat',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => array($cid),
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			)  */
		//),
	//); */
	$loop = new WP_Query( $args );
	//echo $loop->request; die;
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();global $product; 
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;

        $terms = wp_get_post_terms( $pid, 'product_cat' );
        $termsArray = array_map(function($e) {
            return is_object($e) ? $e->term_id : $e['term_id'];
        }, $terms);
        
        $isAlcohalProduct = (in_array('49', $termsArray))?1:0;
        $isTobaccoProduct = (in_array('52', $termsArray))?1:0;

		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
		$imgurl = $thumb[0];
		$gprice=$product->price;
		$regular_price = $product->regular_price;

		$sale_price = $product->sale_price;
		//$shoptype = get_the_title( $sid );
		$profile_delivery_record = get_post_meta($sid,'profile_delivery_data',true);
		//echo "<pre>"; print_R($profile_delivery_record); die;
         if(array_key_exists($search_code,$profile_delivery_record)){
            $get_search_code = $profile_delivery_record[$search_code];
            
            $postcode = (!empty($get_search_code[0]))?$get_search_code[0]:'';
            $delivery_charge = (!empty($get_search_code[1]))?$get_search_code[1]:'';
            $delivery_time = (!empty($get_search_code[2]))?$get_search_code[2]:'';
		}
		
		$e[] = array(
            'Title' => $ptitle,
            'Id' 	=> $pid,
            'Image' => $imgurl,
            'Slug' 	=> (!empty($cslug))?$cslug:'',
            'RegularPrice' => $regular_price,
            'SalePrice' => $sale_price,
            'Cat_id'	=>$cid,
            'time'		=> (!empty($delivery_time))?$delivery_time:'',
            'shoptype'	=> (!empty($shoptype))?$shoptype:'',
            'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
            'isAlcohalProduct'	=> $isAlcohalProduct,
            'isTobaccoProduct'	=> $isTobaccoProduct,
        );
		
		//$category_postcodes = get_field('postocode_details', 'product_cat'.$shopid);
		
		/* foreach($category_postcodes as $cat_postcode) {
			$post_code = $cat_postcode['postcode']; 
			if($post_code == $search_code){
				$delivery_charge = $cat_postcode['delivery_group'];		
				$delivery_time = $cat_postcode['delivery_time'];
				$shoptype=get_woocommerce_term_meta( $shopid, 'custom_shop_type', true );
		
				$e[] = array(
					'Title' => $ptitle,
					'Id' 	=> $pid,
					'Image' => $imgurl,
					'Slug' 	=> (!empty($cslug))?$cslug:'',
					'RegularPrice' => $regular_price,
					'SalePrice' => $sale_price,
					'Cat_id'	=>$cid,
					'time'		=> (!empty($delivery_time))?$delivery_time:'',
					'shoptype'	=> (!empty($shoptype))?$shoptype:'',
					'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
				);
			}
		} */
		
	endwhile;
	
	$args = array(
	   'show_option_none' => '',
	   'hide_empty' => 0,
	   'parent' => $cid,
	   'taxonomy' => 'product_cat',
	   //'parent' => 0
	);
	$subcats = get_categories($args);
	$sub_cat_array=array();
	foreach($subcats as $category){
		$thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true );     
		$image = wp_get_attachment_url( $thumbnail_id ); 
		$sub_cat_array[]=array(
			'sub_cat_name'=>$category->cat_name,
			'sub_cat_id'=>$category->term_id,
			'sub_cat_slug'=>$category->slug,
			'sub_cat_products'=>$category->category_count,
			'sub_cat_image'=>(!empty($image))?$image:'',
		);
	}
	$aa = array('Products'=>$e,'subcats' => $sub_cat_array );
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}


/**
 *  @brief Brief
 *  get_product_by_shop_and_subcat
 *  @param [in] $shopid   Parameter_Description
 *  @param [in] $cid      Parameter_Description
 *  @param [in] $postcode Parameter_Description
 *  @return Return_Description
 *  @author : TRS sb
 *  @creation_date : 29 December, 2016
 *  @modification_date : 29 December, 2016
 *  @modification_task : 
 *  @details Details
 */
function get_product_by_shop_and_subcat($cid,$shopid,$sid,$scid,$postcode,$page=1){
	// die('tada');
	
	$search_code = strtolower($postcode);
	 $args = array(
    'post_type' => 'product',
	'author' => $shopid,  // author id
	'paged'		=> $page,
	'posts_per_page' => TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
    'tax_query'     => array(
        array(
            'taxonomy'  => 'product_cat',
            'field'     => 'id', 
            'terms'     => $scid // sub category id
        )
    )
);
	/* $args = array( 
		'post_type' => 'product',
		'paged'		=> $page,
		'posts_per_page' => TRSWOO_SHOP_API_PRODUCTS_PER_PAGE,
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy'      => 'yith_shop_vendor',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => array($shopid),
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			),
			array(
				'taxonomy'      => 'product_cat',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => array($cid),
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			)
		), */
	//);
	$loop = new WP_Query( $args );
	$e=array();
	while ( $loop->have_posts() ) : $loop->the_post();global $product; 
	
		//print_r($loop->post);
	
		$ptitle = $loop->post->post_title;
		$pid = $loop->post->ID;

        $terms = wp_get_post_terms( $pid, 'product_cat' );
        $termsArray = array_map(function($e) {
            return is_object($e) ? $e->term_id : $e['term_id'];
        }, $terms);
        
        $isAlcohalProduct = (in_array('49', $termsArray))?1:0;
        $isTobaccoProduct = (in_array('52', $termsArray))?1:0;

		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($loop->post->ID), 'thumbnail_size' );
		$imgurl = $thumb[0];
		$gprice=$product->price;
		$regular_price = $product->regular_price;
		$shoptype = get_the_title( $sid );
		 $profile_delivery_record = get_post_meta($sid,'profile_delivery_data',true);
		
         if(array_key_exists($search_code,$profile_delivery_record))
        {
            $get_search_code = $profile_delivery_record[$search_code];
            
            $postcode = (!empty($get_search_code[0]))?$get_search_code[0]:'';
            $delivery_charge = (!empty($get_search_code[1]))?$get_search_code[1]:'';
            $delivery_time = (!empty($get_search_code[2]))?$get_search_code[2]:'';
		} 
		$sale_price = $product->sale_price;
		
		$e[] = array(
            'Title' => $ptitle,
            'Id' 	=> $pid,
            'Image' => $imgurl,
            //'Slug' 	=> (!empty($cslug))?$cslug:'',
            'RegularPrice' => $regular_price,
            'SalePrice' => $sale_price,
            'Cat_id'	=> $cid,
            'time'		=> (!empty($delivery_time))?$delivery_time:'',
            'shoptype'	=> (!empty($shoptype))?$shoptype:'',
            'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
            'isAlcohalProduct'	=> $isAlcohalProduct,
            'isTobaccoProduct'	=> $isTobaccoProduct,
        );		
	endwhile;
	
	$args = array(
	    'parent' => $cid,
		'show_option_none' => '',
		'hide_empty' => 0,
		'taxonomy'=>'product_cat'
	);
	$subcats = get_categories($args);
	
	//print_r($subcats);
	
	//die;
	
	$sub_cat_array=array();
	foreach($subcats as $category){
		$thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true );     
		$image = wp_get_attachment_url( $thumbnail_id ); 
		$sub_cat_array[]=array(
			'sub_cat_name'=>$category->cat_name,
			'sub_cat_id'=>$category->term_id,
			'sub_cat_slug'=>$category->slug,
			'sub_cat_products'=>$category->category_count,
			'sub_cat_image'=>(!empty($image))?$image:'',
		);
	}
	$aa = array('subcats' => $sub_cat_array,'Products' => $e );
	$mnb = json_encode($aa);
	wp_reset_query();
	return $mnb;
}

/**
 *  @brief Brief
 *  
 *  @param [in] $uid Parameter_Description
 *  @return Return_Description
 *  @author Author Name
 *  @details Details
 */
function get_products_by_handler($uid){
	$categoryId= get_user_meta($uid, 'woo_delivery_category', true);
	$args = array(
		'post_type'             => 'product',
		'post_status'           => 'publish',
		'ignore_sticky_posts'   => 1,
		'nopaging'				=> true,
		'meta_query'            => array(
			array(
				'key'           => '_visibility',
				'value'         => array('catalog', 'visible'),
				'compare'       => 'IN'
			)
		),
		'tax_query'             => array(
			array(
				'taxonomy'      => 'product_cat',
				'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => $categoryId,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			)
		),
	);
	$products = new WP_Query($args);
	$productsdata = $products->get_posts();
	$pdata=array();
	foreach($productsdata as $item){
		$product=new WC_Product($item);
		$pdata[]=array(
			'id'			=> $item->ID,
			'title'		=> $item->post_title,
			'availablity'	=>($product->is_in_stock()==true)?'In Stock':'Out of Stock',
		);
	}
	return json_encode(array('products'=>$pdata));
}


/**
 *  @brief - gives vendor info
 *  
 *  @param [in] $latitude   - latitude of the place
 *  @param [in] $longitude  - longitude of the place
 *  
 *  @return - returns postcode from get_vendors_by_postcode function
 *  
 *  @author - Trs Software Solutions, India
 *  
 *  @details - gets vendors postcode via user latitude and longitude
 */
function get_vendors_by_geocode($latitude,$longitude){
	$postcode=trswoo_shop_api_getZipcode($latitude,$longitude);
		return get_vendors_by_postcode($postcode);
}


/**
 *  @brief - gives address
 *  
 *  @param [in] $latitude   - latitude of the place
 *  @param [in] $longitude  - longitude of the place
 *  
 *  @return - Return address if anything is true or false
 *  
 *  @author - Trs Software Solutions, India
 *  
 *  @details - This function takes latitude and longitude as input and gets location name from google api. 
 */
function trswoo_shop_api_getZipcode($latitude,$longitude){
    $geocodeFromLatlon = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=true_or_false&key=AIzaSyAPSJ_1awPnPNq4Ki2S713mLQuR9zyKsD4');
	$output2 = json_decode($geocodeFromLatlon);
	if(!empty($output2) && isset($output2->results[0])){
		$addressComponents = $output2->results[0]->address_components;
		foreach($addressComponents as $addrComp){
			if($addrComp->types[0] == 'postal_code'){
				//Return the zipcode
				return $addrComp->long_name;
			}
		}
		return false;
	}else{
		return false;
	}
}

/**
 *  @brief Brief
 *  Get All Nearby vendors From given postcode
 *  @param [in] $postcode Area Postcode
 *  @return Returns nearby stores from given post code
 *  @author TRS
 *  @details Get All Nearby vendors From given postcode
 */
function get_vendors_by_postcode($postcode){
	$search_code = strtolower($postcode);
	$postcode = "%$search_code%";
	global $wpdb;
		$sql = $wpdb->prepare("
				SELECT $wpdb->posts.ID
					FROM $wpdb->posts
					INNER JOIN $wpdb->postmeta 
						AS pm 
						ON $wpdb->posts.ID = pm.post_id
					WHERE 1=1
						AND ($wpdb->posts.post_status = 'publish' ) 
						AND ($wpdb->posts.post_type = 'azl_profile' ) 
						AND pm.meta_key = %s
						AND pm.meta_value LIKE %s
					ORDER BY $wpdb->posts.ID = 1044 desc, $wpdb->posts.menu_order ASC",'profile_delivery_data', $postcode
				);
			$post_ids = $wpdb->get_results($sql, OBJECT_K);
			if(!empty($post_ids))
			{
			$args = array(
				'post_type' => 'azl_profile',
				'post__in' =>array_keys($post_ids),
			);
			
			$all_posts = get_posts($args);
			}
			else
			{
				return json_encode(array('error'=>'invalid shop code, not found'));
			}
			if(!empty($all_posts))
			{
				foreach($all_posts as $post)
				{
					
					$profile_delivery_record = get_post_meta($post->ID,'profile_delivery_data',true);
					if(array_key_exists($search_code,$profile_delivery_record))
					{
						$get_search_code = $profile_delivery_record[$search_code];
						$thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id',true );
						$image = (!empty($thumbnail_id))?wp_get_attachment_url( $thumbnail_id ):'';
                       
						$postcode = (!empty($get_search_code[0]))?$get_search_code[0]:'';
						$delivery_charge = (!empty($get_search_code[1]))?$get_search_code[1]:'';
						$delivery_time = (!empty($get_search_code[2]))?$get_search_code[2]:'';
						$sql = $wpdb->prepare("SELECT count(*) as count FROM `wp_posts` WHERE `post_author` = $post->post_author AND `post_status` LIKE 'publish' AND `post_type` LIKE 'product'"
						,'');
						$all_products_of_shop = $wpdb->get_results($sql)[0]->count;
                        
                        
                        $timing_opened = false;
                        $hours = get_post_meta($post->ID, 'working-hours-' . date('N', time() + get_option('gmt_offset') * HOUR_IN_SECONDS) . '-hours');
                        if(!empty($hours) && in_array(date('G', time() + get_option('gmt_offset') * HOUR_IN_SECONDS), $hours)) {
                            $timing_opened = true;
                        }
                        
                        $profile_status = '';
                        
                        if($all_products_of_shop > 0 && $timing_opened==true){
                            $profile_status = 'opened';
                        }elseif($all_products_of_shop > 0 && $timing_opened==false){
                            $profile_status = 'closed';
                        }else if($all_products_of_shop < 1){
                            $profile_status = 'coming-soon';
                        }
                        
						
						$shops[]=array(
							'id'		=> $post->ID,
							'author_id' 	=> $post->post_author,
							'name'		=> html_entity_decode($post->post_title), 
							'image'		=> (!empty($image))?$image:'',
							'time'		=> (!empty($delivery_time))?$delivery_time:'',
							'service_fee'=> get_option('service_fee_text'),
							'shoptype'	=> $post->post_excerpt,
							'shopprice'	=> (!empty($delivery_charge))?$delivery_charge:'',
							'status'	=> $profile_status,
							'products'  => (!empty($all_products_of_shop))?$all_products_of_shop:'',
						);
					}	
					else
					{
						$shops[] = array();
					}
				}
			}
			if(count($shops)>0):
				return json_encode(array('shops'=>$shops));
			else:
				return json_encode(array('error'=>'invalid shop code'));
			endif;
}



/**
 *  @brief Brief
 *  Get All Vendors function to fetch all vendors from yith vendors taxonomy
 *  @return All Vendors Array in Json Format
 *  @author TRS
 *  @details Returns All Vendors Array in Json Format
 */
function get_all_vendors(){
	
	$taxonomy = 'product_cat';
	$args = array(
	 'taxonomy'     => $taxonomy,
    'orderby' => 'name',
    'show_count'   => 0,
	'hide_empty'   => 0,
	'parent'		=>0,
	);
	$all_categories = get_categories($args);
	echo "<pre>"; print_R($all_categories); die;
	
	 $args = array(
        'post_type' => 'product',
		'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'cat_ID',
               
            )
        )
    );
	//echo "<pre>"; print_R($categories); die;
	/* $taxonomy = 'yith_shop_vendor';
	if(!taxonomy_exists($taxonomy)){echo json_encode(array('categories'=>array()));die;}
	$args = array(
		 'taxonomy'     => $taxonomy,
		 'orderby'      => 'name',
		 'show_count'   => 0,
		 'hide_empty'   => 0,
		 'parent'		=>0,
	);
	$all_categories = get_categories( $args );
	$categories=array();

	foreach($all_categories as $category){

		$id = $category->term_id;

		$name = get_cat_name($id);

		$thumbnail_id = get_woocommerce_term_meta( $id, 'thumbnail_id', true );     
		$image = wp_get_attachment_url( $thumbnail_id ); 
		$enable_time = get_term_meta( $id, 'enable_time',true); 
		
		$custom_comingsoon_status = get_term_meta( $id, 'custom_comingsoon_status', true );
		$enable_status=(!empty($custom_comingsoon_status))
									?'coming'
									:(empty($enable_time)
										?'closed'
										:'opened');
		
		$shoptype= get_woocommerce_term_meta( $id, 'custom_shop_type', true ); 
		$shopprice = get_woocommerce_term_meta( $id, 'custom_shop_price', true ); 
		$duration=get_woocommerce_term_meta( $id, 'custom_delivery_duration', true );
		$post_code='';
		$delivery_charge='';
		
		$category_postcodes = get_field('postocode_details', 'yith_shop_vendor_'.$id);
		
		if(!empty($category_postcodes) && count($category_postcodes)>0){
			$post_code = (!empty($category_postcodes[0]['postcode']))
							?$category_postcodes[0]['postcode']:'';
			$delivery_charge = (!empty($category_postcodes[0]['postcode']))
								?$category_postcodes[0]['delivery_time']:'';
		}
		$categories[]=array(
			'id'=>$category->term_id,
			'name'=>$name,
			'image'=>$image,
			'time'=> $duration,
			'shoptype'=>$shoptype,
			'shopprice'=>$shopprice,
			'post_code'=>$post_code,
			'delivery_time'=>$delivery_charge,
			'status'	=> $enable_status,
		);

	}
	$return_data = json_encode(array('categories'=>$categories));
	return $return_data; */
}

function get_shops_by_postcode_array($postcode){
    $search_code = strtolower($postcode);
	$postcode = "%$search_code%";
	global $wpdb;
    $sql = $wpdb->prepare("
            SELECT $wpdb->posts.ID
                FROM $wpdb->posts
                INNER JOIN $wpdb->postmeta 
                    AS pm 
                    ON $wpdb->posts.ID = pm.post_id
                WHERE 1=1
                    AND ($wpdb->posts.post_status = 'publish' ) 
                    AND ($wpdb->posts.post_type = 'azl_profile' ) 
                    AND pm.meta_key = %s
                    AND pm.meta_value LIKE %s
                ORDER BY $wpdb->posts.ID = 1044 desc, $wpdb->posts.menu_order ASC",'profile_delivery_data', $postcode
            );
    $post_ids = $wpdb->get_results($sql, OBJECT_K);
    if(!empty($post_ids))
    {
        return array_keys($post_ids);
    }
    return array();
}

function check_and_store_token($obj){

	$postData = $obj['payload'];
    
    $userid  =  $postData['user_id'];
    $fcm_token = $postData['token'];
    
    $saved_fcm_tokens = get_option('saved_fcm_token');
    if(!empty($saved_fcm_tokens )){
        if(!array_key_exists($fcm_token, $saved_fcm_tokens)){
            $saved_fcm_tokens[$fcm_token]=$postData;
        }
    }else{
		$saved_fcm_tokens = [];
        $saved_fcm_tokens[$fcm_token] = $postData;
    }
    update_option('saved_fcm_token', $saved_fcm_tokens);

	return json_encode(array('status'=>'success'));
}