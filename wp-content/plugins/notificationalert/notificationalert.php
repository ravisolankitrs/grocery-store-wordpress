<?php
	/* Plugin Name: Notification Alert 
	   Author: Trssoftware solution
	   Description: This Plugin is used for display all Notification
	*/
	
	add_action( 'admin_menu', 'devo_push_notification_dashboard_menu');
		function devo_push_notification_dashboard_menu(){
			
			add_menu_page('Push Notification Dashboard', 'Push Notification Dashboard', 'manage_options', 'devo-pn-dashboard' ,'devo_push_notification_dashboard_body', 'dashicons-dashboard',11 );
	
		}
		
		function devo_push_notification_dashboard_body() {
			
			require_once('notification-form.php');
			
		}
	
	add_action("admin_enqueue_scripts","add_style_and_css");
	
	
	function add_style_and_css(){
		
		  if(stristr($_SERVER['REQUEST_URI'], 'admin.php?page=devo-pn-dashboard') !== FALSE) {
				
			wp_enqueue_style( 'ntfy_add_notify_css' , plugins_url( 'assets/notify-style.css',__FILE__));
			wp_enqueue_style( 'woocommerce_admin_styles-css' , WC()->plugin_url() . '/assets/css/admin.css');
			
			wp_enqueue_media();
		//	wp_enqueue_script( 'wp-media-uploader', DIR_URL . 'wp_media_uploader.js', array( 'jquery' ), 1.0 );
			
			wp_enqueue_script('select2' ,WC()->plugin_url() . '/assets/js/select2/select2.full.min.js',array('jquery'),'1.2',true);
			wp_enqueue_script('selectWoo' , WC()->plugin_url() . '/assets/js/selectWoo/selectWoo.full.min.js');
			wp_enqueue_script('ntfy_add_notify_js', plugins_url('assets/notify-script.js',__FILE__),array('jquery'),'1.2',true);
			wp_enqueue_script('ntfy_add_emoji_plugin', plugins_url('assets/plug/ckeditor/ckeditor.js',__FILE__),array('jquery'),'1.2',true);
//			wp_enqueue_style( 'woocommerce_admin_styles-css' , plugin_url('assets/plug/ckeditor/style.css'),__FILE__));
			
			
			wp_localize_script('ntfy_add_notify_js' ,'adminAjax', array('ajaxurl' => admin_url( 'admin-ajax.php')));
		}
	}
	
//	add_action("init", "ntapitestimageupload");
	
function ntapitestimageupload(){
	
    if(isset($_POST['sendmsgnotification'])){	
		  
        if (!function_exists('wp_generate_attachment_metadata')){
            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            require_once(ABSPATH . "wp-admin" . '/includes/file.php');
            require_once(ABSPATH . "wp-admin" . '/includes/media.php');
        }
		
			
        $files = $_FILES["uploadnotificationimage"];
        foreach ($files['name'] as $key => $value) {
            if ($files['name'][$key]) {
                $file = array(
                    'name' => $files['name'][$key],
                    'type' => $files['type'][$key],
                    'tmp_name' => $files['tmp_name'][$key],
                    'error' => $files['error'][$key],
                    'size' => $files['size'][$key]
                );
                $_FILES = array("upload_file" => $file);
                $attachment_id = media_handle_upload("upload_file", 0);

                if (is_wp_error($attachment_id)) {
                    
                    // There was an error uploading the image.
                    echo "Error adding file";
                } else {
                    // The image was uploaded successfully!
                    echo "File added successfully with ID: " . $attachment_id . "<br>";
                    echo "<script>alert('".wp_get_attachment_url( $attachment_id )."')</script>";
                    echo wp_get_attachment_image($attachment_id, array(800, 600)) . "<br>"; //Display the uploaded image with a size you wish. In this case it is 800x600
                }
            }
        }
        die();
    }

}

	add_action("wp_ajax_ntapi_ajx_notification_message","ntapi_ajx_notification_message");
		 
	function ntapi_ajx_notification_message(){
		
		define( 'IOS_ACCESS_KEY', 'AAAA45a7D4E:APA91bEjTfPm14VftP46HvB0O4enSWFvuvnXu-7T-3Doqplt-dLNlgLxiqitr8x2Rck1hwYfU_xkwVa7JTyQJv3GbDTizq0kGCtvxE4uvDcE9jXSyEifLZNR5aOvuZWTvZi-dE1sekkx' );
	    
		$userqry = array();
		
		$executionMode = (isset($_POST['isLiveMode']))?'live':'test';

		if(isset($_POST['ntapinotificationmessage'])){
            $msg    = $_POST['ntapinotificationmessage']; 
		}
	
	    if(isset($_POST['ntapuploadnotificationimage'])){	
            $imgurl = $_POST['ntapuploadnotificationimage'];
        }
	
	    if(isset($_POST['notificationtitle'])){	
            $title = $_POST['notificationtitle'];
        }
		
		if(isset($_POST['notificationshortdesc'])){	
            $shrtmsg =$_POST['notificationshortdesc'];
        }
        
		if(isset($_POST['notificationbttext'])){
			$btntext = $_POST['notificationbttext'];
		}
        

		$senderArray = '';
        
        $specific_user_list = false;
        if(isset($_POST['user_type']) && !empty($_POST['usrselectlist'])){
            $senderArray = 'specific';
            $specific_user_list = $_POST['usrselectlist'];
        }
	    if(isset($_POST['chooseandroiduser'])){	
			$chooseandr  =  $_POST['chooseandroiduser'];
			$senderArray = 'android';
	    }
      
		if(isset($_POST['chooseiosuser'])){
			$chooseios = $_POST['chooseiosuser'];
			$senderArray = 'ios';
	    }
		
		if(isset($_POST['choosewebuser']))
		{
			$chooseweb = $_POST['choosewebuser'];
	        $senderArray = 'web';
	    }
		
		if(isset($_POST['choosealluser'])){	
			$choosealluser = $_POST['choosealluser'];
			$senderArray = 'all_user';		 
		}
		
		if(!empty($senderArray) && $senderArray != 'specific'){
			$saved_fcm_token_options = array();
            if($executionMode == 'live'){
                $saved_fcm_token_options = get_option('saved_fcm_token');
            }else{
                $saved_fcm_token_options = get_option('test_saved_fcm_token');
            }			
			if(empty($saved_fcm_token_options)){
				die('tokens empty');
			}
			$senderData = array();
			foreach($saved_fcm_token_options as $tokens){
				if($senderArray=='all_user'){
					$senderData[] = $tokens['token'];
				}elseif($tokens['device_type'] == $senderArray){
					$senderData[] = $tokens['token'];
				}
			}
			if(!empty($senderData)){
				sendnoticationotherdevice($senderData, $title,$shrtmsg, $msg, $btntext, $imgurl, IOS_ACCESS_KEY);
			}
			
		}elseif($senderArray == 'specific' && !empty($specific_user_list)){
            if($executionMode == 'live'){
                $senderData = array();
                $user_ids = $specific_user_list;
                foreach($user_ids as $id){
                    $allTokens = array();
                    $fcm_data = get_user_meta($id, 'fcm_token_data', true);
                    $tokens = array_column($fcm_data,'token');
                    foreach($tokens as $token){
                      $senderData[] =  $token; 
                    }
                }
                if(!empty($senderData)){
                    sendnoticationotherdevice($senderData, $title,$shrtmsg, $msg, $btntext, $imgurl, IOS_ACCESS_KEY);
                }
            }else if($executionMode == 'test'){
                $senderData = array();
                $user_ids = array(1, 29, 2, 211);
                foreach($user_ids as $id){
                    $allTokens = array();
                    $fcm_data = get_user_meta($id, 'fcm_token_data', true);
                    $tokens = array_column($fcm_data,'token');
                    foreach($tokens as $token){
                      $senderData[] =  $token; 
                    }
                }
                if(!empty($senderData)){
                    sendnoticationotherdevice($senderData, $title,$shrtmsg, $msg, $btntext, $imgurl, IOS_ACCESS_KEY);
                }
            }
        }
        wp_die();
	}
		 
add_action("wp_ajax_get_user_list_search","get_user_list_search");
add_action("wp_ajax_no_priv_get_user_list_search","get_user_list_search");

function get_user_list_search($usermetaarr){
		
    global $wpdb;
    $users = get_users(array(
        'meta_key'     => 'fcm_token_data',
    ));
    $result = get_user_meta(211);	
    print_r($result);	  
    die();
}	
		 
function sendnoticationotherdevice($senderData, $title, $shrtmsg, $msg, $btntext, $imgurl, $API_ACCESS_KEY){
    array_filter($senderData);	
    $clearms  = str_replace("<p>","", $shrtmsg);
    $clearms  = str_replace("</p>","", $clearms);
    $clearms  = str_replace("<br />","", $clearms);
    $fields = array	(
        "data" =>  array(
            'buttonname' => $btntext,
            'pnmessage'  => $msg,
            "keyname"=> $imgurl,
        ),				
        'registration_ids' => $senderData,
        'notification'	=> array(
            'title'   => $title,	
            'text' => $clearms ,	
            'sound' =>'default',
            'attachment-url'=>$imgurl,
        ),
        
        
    );
    
    $headers = array(
        'Authorization: key=' . $API_ACCESS_KEY,
        'Content-Type: application/json'
    );
                    
    #Send Reponse To FireBase Server	
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
    echo $result;
    if($result == false){
        echo "false";
    }
    die();			
}
		
		
		
			
add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
		<h3><?php _e("Extra profile information", "blank"); ?></h3>

		<table class="form-table">
			
			<tr>
				<th><label for="devicetype"><?php _e("Device Type"); ?></label></th>
				<td>
				<input type="text" name="devicetype" id="address" value="<?php echo esc_attr( get_the_author_meta( 'devicetype', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e("Please enter your device."); ?></span>
				</td>
			</tr>
	        <tr>
				<th><label for="tokenkey"><?php _e("Token Key"); ?></label></th>
				<td>
				<input type="text" name="tokenkey" id="address" value="<?php echo esc_attr( get_the_author_meta( 'tokenkey', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e("Please enter token key."); ?></span>
				</td>
			</tr>
			
	
		</table>
<?php }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {

    update_user_meta( $user_id, 'devicetype', $_POST['devicetype'] );
    update_user_meta( $user_id, 'tokenkey',   $_POST['tokenkey'] );
    
}
				
			
			