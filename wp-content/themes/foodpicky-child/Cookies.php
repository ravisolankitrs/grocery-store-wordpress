<?php 
/* 
 *Template Name: Cookies
 */ 
get_header(); ?>

<style>
body{	
	font-family: "Neutraface Text","Zona Pro",Helvetica,Arial,Serif;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    color: #2D2D2D;
}
.header-text1{
	text-align:left;
	padding-left:0px;
}
.terms_header, h3{
	font-weight: 700;
    font-size: 24px;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    line-height: 1.1;
    color: inherit;
    box-sizing: border-box;
}
p{
	font-size: 14px;
    line-height: 1.42857143;
}
a, p a{
	color: #FF6C61;
}
a:hover {
    text-decoration: underline;
}
a:visited {
   color: #FF6C61;
}
ul {
    list-style-type: disc;
	padding-left: 3%;
	color: #2D2D2D;
}
ol{
	padding-left: 3%;
}
strong{
	color: #2D2D2D;
    font-weight: 700;
}
p strong{
	font-size: 14px;
}
.custom_strong strong,strong{
	color: #2D2D2D;
    font-weight: 700;
    font-size: 14px;
}
.custom_ol_alpha{
	list-style: lower-alpha;
	padding-left: 0px;
}
.post-edit-link{
	display:none;
}
</style>

<div id="primary" class="content-area 123">
	<main id="main" class="site-main testing" role="main">
		<article id="post-13544" class="post-13544 page type-page status-publish hentry">
			<header class="entry-header">
				<!--<div class="devo-header devo-outer">
					<div class="devo-inner text-center">
						<h1 class="entry-title">Cookies</h1>
					</div>
				</div>-->
				<div class="dev-page-header-inner dev-outer">
					<div class="dev-inner dev-text-centered">
					<h2 class="text-head">Cookies</h2> 
					</div>
				</div>
			</header>
			<!-- .entry-header -->
			<div class="container">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="entry-content">
							<p><strong>Information about our use of&nbsp;cookies</strong></p>
							<p>Our website URL <a href="http://www.devo.co.uk">www.devo.co.uk</a> and our mobile application versions (our <strong>"Service"</strong>) use cookies to distinguish you from other users of our Service. This helps us to provide you with a good experience when you browse our Service and also allows us to improve our Service. By continuing to browse our Service, you are agreeing to our use of cookies.</p>
							<p>A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer, mobile phone or tablet if you agree.</p>
							<p>We use the following cookies:</p>
							<ul>
								<li><strong>Strictly necessary cookies</strong>. These are cookies that are required for the operation of our Service. They include, for example, cookies that enable you to log into secure areas of our Service, use the shopping cart and checkout. We use a third party payment solution for checkout on our Service so we may need to share information with them for this purpose. As we operate our Service as a digital marketplace we may also need to share your information with the sellers that you purchase products from on our Service so that they may process your order and with couriers so that they may deliver your order. Please see our <a href="http://devo.co.uk/privacy">Privacy Policy</a> for further information.</li>
								<li><strong>Analytical/performance cookies.</strong> They allow us to recognise and count the number of visitors and to see how visitors move around our Service when they are using it. This helps us to improve the way our Service works, for example, by ensuring that users are finding what they are looking for easily. We may also share this information with our partners for this purpose, please see our <a href="http://devo.co.uk/privacy">Privacy Policy</a> for further information.</li>
								<li><strong>Functionality cookies.</strong> These are used to recognise you when you return to our Service. This enables us to personalise our content for you, greet you by name, remember your preferences (for example, your choice of language or region), remember your search settings and remember whether you are logged in or not.</li>
								<li><strong>Targeting cookies.</strong> These cookies record your visit to our Service, the pages you have visited and the links you have followed. We will use this information to make our Service and the advertising displayed on it more relevant to your interests. We may also share this information with our partners for this purpose, please see our <a href="http://devo.co.uk/privacy">Privacy Policy</a> for further information.</li>
							</ul>
							<p>Our cookies help us:</p>
							<ul>
								<li>Make our Service work as you'd expect</li>
								<li>Save you having to login every time you visit our Service</li>
								<li>Remember your settings during and between visits on our Service</li>
								<li>Improve the speed/security of our Service</li>
								<li>Allow you to share pages with social networks like Facebook</li>
								<li>Continuously improve our Service for you</li>
								<li>Make our marketing more efficient</li>
								<li>Analyse and report sales data to our digital marketplace sellers and partners</li>
							</ul>
							<p>We do not, without your express permission to do so, use cookies to:</p>
							<ul>
								<li>Collect any personally identifiable information or any sensitive information</li>
								<li>Pass personally identifiable data to third parties (other than as explained in this policy and in our <a href="http://devo.co.uk/privacy">Privacy Policy</a>.</li>
							</ul>
							<p><strong>Granting us permission to use cookies</strong></p>
							<p>If the settings on your software that you are using to view our Service (your browser) are adjusted to accept cookies we take this, and your continued use of our Service, to mean that you agree with this assumption.</p>
							<p>Except for essential cookies, all cookies will expire after 2 years.</p>
							<p><strong>Third Parties</strong></p>
							<p>Please note that third parties (including, for example, advertising networks and providers of external services like web traffic analysis services) including search engines and social media websites may also use cookies, over which we have no control. These cookies are likely to be analytical/performance cookies or targeting cookies.</p>
							<p><strong>Switching cookies off</strong></p>
							<p>You can block cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including essential cookies) you may not be able to access all or parts of our Service.</p>
							<p>You can usually switch cookies off by adjusting your browser settings to stop it from accepting cookies.</p>
							<p>It may be that your concerns around cookies relate to so called "spyware". Rather than switching off cookies in your browser you may find that anti-spyware software achieves the same objective by automatically deleting cookies considered to be invasive.</p>
							<p>&nbsp;</p>
							
						</div><!-- .entry-content -->
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
		</article><!-- #post-## -->

	</main><!-- #main -->
</div>
<?php
get_footer();
?>