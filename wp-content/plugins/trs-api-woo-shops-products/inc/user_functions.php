<?php

/**

 *  @file user_functions.php

 *  @brief Brief

 *  This file is for user related functions.

 */

 

 /**

 *  @brief Brief

 *  Login Function for users

 *  @return Return_Description

 *  

 *  @author TRS Software Solutions

 *  @details Details

 */

function make_user_login($obj){
   /*  ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL); */
  
	global $wpdb;
	$loginpostData=$obj["login"];
	$is_social=false;
	if(!empty($loginpostData["is_social"])){
		$is_social=true;
	}

	$user = null;
	$username = (!empty($loginpostData['username']))?sanitize_user($loginpostData["username"]):'';
	// echo "<pre>";print_r($obj);
	if($is_social==true && !empty($username)){
		$user = get_user_by( 'login', $username );
		if(empty($user)){
			$uemail = (!empty($loginpostData['email']))?sanitize_user($loginpostData["email"]):'';
			$user = get_user_by( 'email', $uemail );
		}
		if(empty($user)){
            // echo "<pre>";print_r('social signup');die;
			$user = trswoo_shop_api_social_signup($loginpostData);        
		}
		$social = "true";

	}
    
	$email = (!empty($loginpostData['email']))?sanitize_email($loginpostData["email"]):'';

	$password = (!empty($loginpostData['password']))?$loginpostData['password']:'';	

	if($is_social == false && empty($username) && !empty($email) && !empty($password)){

		$user = get_user_by( 'email', $email );
		if(!empty($user)){
			$ucheck = wp_check_password( $password, $user->data->user_pass, $user->ID );

			$user =($ucheck==1)?$user:null;
			$social = "false";
		}

	}
    
	if(!empty($user)){

		$user_id = $user->data->ID;
        
		/**
		 *  Fetch Cards Information if available
		 */
		$stripe_customer_id = get_user_meta($user_id,'_stripe_customer_id',true);
		$arr = array();

		if(!empty($stripe_customer_id)){
			$cardsData = get_stripe_cards_for_user($stripe_customer_id);
    		$arr = (!empty($cardsData))?$cardsData:array();
		}

        /* Store FCM Tokens if Available in DB */
		$fcm_token = (!empty($loginpostData['token']))?$loginpostData['token']:false;
		$device_type = (!empty($loginpostData['device_type']))?$loginpostData['device_type']:false;
		$existing_tokens = get_user_meta($user_id, 'fcm_token_data', true);
		  
		if(!empty($fcm_token) && !empty($device_type)){
	        $existing_tokens = get_user_meta($user_id, 'fcm_token_data', true);
		if(!empty($existing_tokens )){
			if(!array_key_exists($fcm_token, $existing_tokens)){
				$existing_tokens[$fcm_token] = array('token'=>$fcm_token, 'device_type'=>$device_type, 'user_id'=>$user_id);
				update_user_meta($user_id, 'fcm_token_data', $existing_tokens);
			}
        }else{
                $existing_tokens = [];
                $existing_tokens[$fcm_token] = array('token'=>$fcm_token, 'device_type'=>$device_type, 'user_id'=>$user_id);
				 update_user_meta($user_id, 'fcm_token_data', $existing_tokens);
            }
           
		}
	    
		/**
		 *  Fetch Address Informations if available
		 */

		$add = array();

		$address=get_user_meta($user_id,'checkout_saved_address',true);
		$add = array();
		if(count($address)>0 && (is_array($address))){
			$address = array_reverse($address,true);
			foreach($address as $key=>$res){
				$add[]=array(
                    'id' 		=> 	$key,
                    'user_id' 	=> 	(!empty($res['user_id']))?$res['user_id']:$user_id,
                    'flat' 		=> 	(!empty($res['address_1']))?$res['address_1']:'',
                    'address' 	=> 	(!empty($res['address_2']))?$res['address_2']:'',
                    'postcode'	=>	(!empty($res['postcode']))?$res['postcode']:'',
                    'phone'		=>	(!empty($res['phone']))?$res['phone']:'',
                    'rider'		=>	(!empty($res['rider']))?$res['rider']:'',
                    'default'	=>	(!empty($res['default']))?$res['default']:false,
                );
			}
		}

		$f_name = get_user_meta($user->data->ID,'first_name');
		$l_name = get_user_meta($user->data->ID,'last_name');
		$phone = get_user_meta($user->data->ID,'phone');
        $phone =  (!empty($phone[0]))?$phone[0]:'';
        
		$e=array(
			'User_Id' => $user->data->ID,
			'username' => ucfirst($user->data->display_name),
			'email' =>  $user->data->user_email,
			'password' => $user->data->user_pass,
			'profile_pic_url'=>($is_social!=false)?get_user_meta($user->data->ID,'social_profile_pic_url',true):'',
			'first_name'=>($f_name[0])?$f_name[0]:'',
			'last_name'=>($l_name[0])?$l_name[0]:'',
			'phone'=>$phone,
			'social'=>($social)?$social:'',
			'stripe_customer_id'=>(!empty($stripe_customer_id))?$stripe_customer_id:'',
		);

		$aa = array('success'=>array('data'=>$e),'user_card'=>$arr,'user_address'=>$add);
		$mnb = json_encode($aa);
		return $mnb;
	}else {
		return '{"error":{"message":"Authentication failed due to invalid Credentials!"}}';
	}

}



 /**
 *  @brief Brief
 *  Signup Function for users
 *  @return Return_Description
 *  
 *  @author TRS Software Solutions
 *  @details Details

 */

function make_user_register($obj){
    global $wpdb;
	$userpostData=$obj["signup"];
	$is_social=false;
	if(!empty($userpostData["is_social"])){
		$is_social=true;
	}
	$email='';
	if(!empty($userpostData["email"])){
		$email=sanitize_email($userpostData["email"]);
	}elseif(empty($userpostData["email"] )&&  !empty($userpostData["username"])){
		$email = trswoo_shop_api_create_email_with_username($userpostData["username"]);
	}

	$username='';
	if(!empty($userpostData["username"])){
		$username=sanitize_user($userpostData["username"]);
	}elseif(!empty($userpostData["email"]) &&  empty($userpostData["username"])){
		$username = trswoo_shop_api_rand_username( $email );
	}

	

	$pass = ($is_social==false)?$userpostData["password"]: wp_generate_password( 12, false );
	$phone = (!empty($userpostData["phone_no"]))?sanitize_user($userpostData["phone_no"]):'';
	$firstname = (!empty($userpostData["firstname"]))?sanitize_text_field($userpostData["firstname"]):'';
	$lastname = (!empty($userpostData["lastname"]))?sanitize_text_field($userpostData["lastname"]):'';
	$profile_pic_url=(!empty($userpostData["picture"]))?$userpostData["picture"]:'';
	$userdata = array(
		// 'user_login'  	=>  $username,
		'user_login'  	=>  $email,
		'user_pass'   	=>  $pass,
		'first_name'  	=>  $firstname,
		'last_name'   	=>  $lastname,
		'user_email'  	=>  $email,
		'display_name'	=>  $firstname,
		'role'			=>	(get_role( 'customer' )!=null || get_role( 'customer' )!=false )?'customer':'subscriber',

	);
    
    $uid = username_exists( $email );
    if ( !$uid and email_exists($email) == false ) {
        $wpdb->insert( 
            'wp_users', 
            array( 
                'user_login' => $email, 
                'user_email' => $email, 
                'user_pass'  =>  wp_hash_password( $pass ), 
            ), 
            array( 
                '%s', 
                '%s', 
                '%s', 
            ) 
        );
        $uid = $wpdb->insert_id;
    }else{
        $uid = null;
    }
    
	// $uid = wp_insert_user($userdata);
	if (is_int($uid)) {
        $user_data = wp_update_user( 
            array( 
                'ID' => $uid,
                'first_name' => $firstname,
                'last_name' => $last_name,
                'display_name' => $firstname,
                'role' => (get_role( 'customer' )!=null || get_role( 'customer' )!=false )?'customer':'subscriber',
            )
        );
        
		/*Update user device and token */
          
    //     print_r($userpostData);	
          
        /* Store FCM Tokens if Available in DB */
		$fcm_token = (!empty($userpostData['token']))?$userpostData['token']:false;
		$device_type = (!empty($userpostData['device_type']))?$userpostData['device_type']:false;
		$existing_tokens = get_user_meta($uid, 'fcm_token_data', true);
		  
		if(!empty($fcm_token) && !empty($device_type)){
	        $existing_tokens = get_user_meta($uid, 'fcm_token_data', true);
			if(!empty($existing_tokens )){
				if(!array_key_exists($fcm_token, $existing_tokens)){
					$existing_tokens[$fcm_token] = array('token'=>$fcm_token, 'device_type'=>$device_type, 'user_id'=>$uid);
					update_user_meta($uid, 'fcm_token_data', $existing_tokens);
				}
			}else{
				$existing_tokens = [];
				$existing_tokens[$fcm_token] = array('token'=>$fcm_token, 'device_type'=>$device_type, 'user_id'=>$uid);
				update_user_meta($uid, 'fcm_token_data', $existing_tokens);
			}
			   
		}	 
			
      //end 
		
		update_user_meta($uid,'is_social_user',$is_social);
		update_user_meta($uid,'social_profile_pic_url',$profile_pic_url);
		$user = get_user_by( 'ID',$uid );
		$f_name = get_user_meta($user->data->ID,'first_name',true);
		$l_name = get_user_meta($user->data->ID,'last_name',true);

		$e=array(
			'User_Id' => $user->data->ID,
			'username' => ucfirst($user->data->display_name),
			'email' =>  $user->data->user_email,
			'password' => $user->data->user_pass,
			'first_name'=>($f_name)?$f_name:'',
			'last_name'=>($l_name)?$l_name:'',
		);

		$aa = array('success'=>array('data'=>$e));
		$mnb = json_encode($aa);
		return $mnb;
		// echo '{"success":{"message":"Registration Successfull"}}';
	}else{
		return '{"error":{"message":"Registration failed"}}';
	}
    return '{"error":{"message":"Registration failed"}}';
 }

 

 /**

 *  @brief Brief

 *  Signup & login function for non registered Social users

 *  @return Return_Description

 *  

 *  @author TRS Software Solutions

 *  @details Details

 */

function trswoo_shop_api_social_signup($userpostData){
    
	$is_social=false;
	if(!empty($userpostData["is_social"])){
		$is_social=true;
	}

	$email='';
	if(!empty($userpostData["email"])){
		$email=sanitize_email($userpostData["email"]);
	}elseif(empty($userpostData["email"] )&&  !empty($userpostData["firstname"])){
		$email = trswoo_shop_api_create_email_with_username($userpostData["firstname"]);
	}
   
	$username='';
    if(!empty($userpostData["email"])){
		$username = trswoo_shop_api_rand_username( $email );
	}
	/* if(!empty($userpostData["username"])){
		$username=sanitize_user($userpostData["username"]);
	}elseif(!empty($userpostData["email"]) &&  empty($userpostData["username"])){
		$username = trswoo_shop_api_rand_username( $email );
	} */

	$pass = ($is_social==false)?$userpostData["password"]: wp_generate_password( 12, false );
	$phone = (!empty($userpostData["phone_no"]))?sanitize_user($userpostData["phone_no"]):'';
	$firstname = (!empty($userpostData["firstname"]))?sanitize_text_field($userpostData["firstname"]):'';
	$lastname = (!empty($userpostData["lastname"]))?sanitize_text_field($userpostData["lastname"]):'';
	$firstname = preg_replace("/[^A-Z a-z0-9]/","",$firstname);
   	$profile_pic_url=(!empty($userpostData["picture"]))?$userpostData["picture"]:'';	

    
    /* $userdata = array(

		// 'user_login'  	=>  $username,

		'user_login'  	=>  $email,

		'user_pass'   	=>  $pass,

		'first_name'  	=>  $firstname,

		'last_name'   	=>  $lastname,

		'user_email'  	=>  $email,

		'display_name'	=>  $firstname,

		'role'			=>	(get_role( 'customer' )!=null || get_role( 'customer' )!=false )?'customer':'subscriber',

	);

	$uid = wp_insert_user( $userdata ); */
    global $wpdb;
    $wpdb->insert($wpdb->users, array(
        'user_login' => $email,
        'user_email' => $email,
        'user_pass' => $pass, // ... and so on
    ));
    $uid = $wpdb->insert_id;
        

	if (is_int($uid)) {
        $updateArray = array(
            'ID'            =>  $uid,
            'first_name'  	=>  $firstname,
            'last_name'   	=>  $lastname,
            'display_name'	=>  $firstname,
            'role'			=>	(get_role( 'customer' )!=null || get_role( 'customer' )!=false )?'customer':'subscriber',
        );
        
        $user_id = wp_update_user( $updateArray );

		update_user_meta($uid,'is_social_user',$is_social);
		update_user_meta($uid,'social_profile_pic_url',$profile_pic_url);
		$user = get_user_by( 'ID',$uid );
		return $user;
	}else{
		return null;
	}

}



/* General Functions for API */

function trswoo_shop_api_create_email_with_username($firstname){

		$siteaddress=get_site_url();

		$domain = preg_replace('#^https?://#', '', $siteaddress);

		$firstname = preg_replace("/[^A-Za-z0-9]/","",$firstname);

		$firstname = strtolower($firstname);

		$user_email=$firstname.'@'.$domain;

		$j = 1;

		while ( email_exists( $user_email ) ){

			$user_email=$firstname. '_' . $j.'@'.$domain;

			// $user_email = $user_email . '_' . $j;

			$j++;	

		}

		// $user_email=$firstname.'@'.$domain;

		return $user_email;		

}

function trswoo_shop_api_rand_username( $username ){

	$username = explode('@', $username);

	$username = $username[0];

	$j = 1;

	while ( username_exists( $username ) ){

		$username = $username . '_' . $j;

		$j++;

	}

	return $username;

}



function make_user_reset_password($obj){



	$loginemailData=$obj["resetpassword"];

	$email = $loginemailData['email'];

	$user = get_user_by( 'email', $email );

	//print_r($user);

	if(!empty($user)){

		

		global $wpdb, $wp_hasher;

		$user_login = $user->user_login;

		$user_login = sanitize_text_field($user_login);



		if ( empty( $user_login) ) {

			return false;

		} else if ( strpos( $user_login, '@' ) ) {

			$user_data = get_user_by( 'email', trim( $user_login ) );

			if ( empty( $user_data ) )

			   return false;

		} else {

			$login = trim($user_login);

			$user_data = get_user_by('login', $login);

		}



		do_action('lostpassword_post');



		if ( !$user_data ) return false;



		// redefining user_login ensures we return the right case in the email

		$user_login = $user_data->user_login;

		$user_email = $user_data->user_email;



		do_action('retreive_password', $user_login);  // Misspelled and deprecated

		do_action('retrieve_password', $user_login);



		$allow = apply_filters('allow_password_reset', true, $user_data->ID);



		if ( ! $allow )

			return false;

		else if ( is_wp_error($allow) )

			return false;



		$key = wp_generate_password( 20, false );

		do_action( 'retrieve_password_key', $user_login, $key );



		if ( empty( $wp_hasher ) ) {

			require_once ABSPATH . 'wp-includes/class-phpass.php';

			$wp_hasher = new PasswordHash( 8, true );

		}

		$hashed = $wp_hasher->HashPassword( $key );

		$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );



		$message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";

		$message .= network_home_url( '/' ) . "\r\n\r\n";

		$message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";

		$message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";

		$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";

		$message .= '<a href="' . network_site_url("my-account/lost-password/?key=$key&login=" . rawurlencode($user_login), 'login') . ">Click here to reset password</a>\r\n";



		if ( is_multisite() )

			$blogname = $GLOBALS['current_site']->site_name;

		else

			$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);



		$title = sprintf( __('[%s] Password Reset'), $blogname );



		$title = apply_filters('retrieve_password_title', $title);

		$message = apply_filters('retrieve_password_message', $message, $key);

		$headers = array('Content-Type: text/html; charset=UTF-8');

		

		if ( $message && !wp_mail($user_email, $title, $message,$headers ) )

			wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function...') );

		

		return '{"success":{"message":"reset"}}';

	

	}else{

		return '{"error":{"message":"Reset failed"}}';

	}

}



function update_user_details($obj){



	$loginemailData=$obj["updateuser"];

	$id = $loginemailData['id'];

	$email = $loginemailData['email'];

	$phone = $loginemailData['phone'];

	$firstname = $loginemailData['first_name'];

	$lastname = $loginemailData['last_name'];

	$user = get_user_by( 'id', $id  );

	if(!empty($user)){

		if(!empty($phone)){

			update_user_meta($user->ID,'phone',$phone); 

		}

		if(!empty($firstname)){

			update_user_meta($user->ID,'first_name',$firstname);

		}

		if(!empty($lastname)){

			update_user_meta($user->ID,'last_name',$lastname);

		}
		if(!empty($email)){
			$usercheck = get_user_by( 'email', $email );
			if(empty($usercheck)){
				$data = wp_update_user( 
					array ( 'ID' => $user->ID, 'user_login' => $email, 'user_email' => $email)
				) ;
			}
			

		}

		$f_name = get_user_meta($user->ID,'first_name',true);

		$l_name = get_user_meta($user->ID,'last_name',true);

		$phone = get_user_meta($user->ID,'phone',true);
		$afterUserData = get_userdata( $user->ID );
		$responsedata = array('detail'=>array(
				'first_name'=>$f_name,
				'last_name'=>$l_name,
				'phone'=>$phone,
				'user_id'=>$user->ID,
				'email'=>$afterUserData->user_email,
			)
		);
		return json_encode($responsedata);

	}else{

		return '{"error":{"message":"update failed"}}';

	}

}



function add_app_card_details($obj)

{

	$postdata	=	$obj["card_details"];

	$user = get_user_by( 'id', $postdata['uid'] );



	if(!empty($user)){		

		global $wpdb;

		$table_name = $wpdb->prefix . "app_order_card_details";

		$userId = $user->ID;

		$old_card = $postdata['oldcard'];

		$row = $wpdb->get_row( "SELECT * FROM $table_name WHERE card_number = $old_card" );

		if(!empty($row)){

			

			$data = array(

					'card_number'	=> 	$postdata['cardnumber'],

					'expire_month'	=> 	$postdata['month'],

					'expire_year' 	=> 	$postdata['year'],

					'cvv'			=>	$postdata['cvv'],

					'postcode'		=>	$postdata['postcode'],

					'type'			=>	$postdata['type'],

				);

			$where = array(

						'card_id'	=>	$row->card_number,

						'user_id'	=>	$userId,

					);

			$order = $wpdb->update( $table_name, $data, $where);

			

		}else{

			$orderData = array(

							'card_id' 		=> 	$postdata['cid'],

							'user_id' 		=> 	$userId,

							'card_number'	=>	$postdata['cardnumber'],

							'expire_month'	=>	$postdata['month'],

							'expire_year'	=>	$postdata['year'],

							'cvv'			=>	$postdata['cvv'],

							'postcode'		=>	$postdata['postcode'],

							'type'			=>	$postdata['type'],

						);

			$order = $wpdb->insert( $table_name, $orderData);

		}

		return '{"success":{"message":"card added"}}';

	}else{

		return '{"error":{"message":"update order failed"}}';

	}

}



function add_app_card_delete($obj) {

	

	$postdata	=	$obj["app_card_delete"];

	$user = get_user_by( 'id', $postdata['uid'] );

	if(!empty($user)){

		

		global $wpdb;

		$table_name = $wpdb->prefix . "app_order_card_details";

		

		

		$wpdb->delete( $table_name, array( 'card_number' => $postdata['cardnumber'], 'user_id' => $user->ID ) );

		

		return '{"success":{"message":"card deleted"}}';

	}else{

		return '{"error":{"message":"delete card failed"}}';

	}

}



function user_update_address_details($obj) {

	$uaddress = $obj["address_details"];

	$user = get_user_by( 'id', $uaddress['uid'] );

	

	if(!empty($user)){

		$storedAddress=get_user_meta($uaddress['uid'],'checkout_saved_address',true);

		

		$count = 0;

		if(empty($storedAddress)){

			$storedAddress=array();

		}

		$count =  uniqid();			

		$address_key = "checkout_add_".$count;

		if(!empty($uaddress['address_id'])){

			$address_key = $uaddress['address_id'];

		}

		

		$storedAddress[$address_key]=array(

			'address_1'	=>	(!empty($uaddress['flat']))?$uaddress['flat']:'',

			'address_2'	=>	(!empty($uaddress['address']))?$uaddress['address']:'',

			'phone'		=>	(!empty($uaddress['phone']))?$uaddress['phone']:'',

			'postcode'	=>	(!empty($uaddress['postcode']))?$uaddress['postcode']:'',

			'rider'		=>	(!empty($uaddress['rider']))?$uaddress['rider']:'',

			'user_id' 	=>  (!empty($uaddress['uid']))?$uaddress['uid']:'',

			'default' 	=>  (!empty($uaddress['default']))?$uaddress['default']:false,

		);

		

		update_user_meta($uaddress['uid'],'checkout_saved_address',$storedAddress);

		return '{"success":{"message":"address added"}}';

			

	}else{		

		return '{"error":{"message":"update address failed"}}';

	}

}

function getUserCards($stripe_customer_id){
    // $stripe_customer_id = get_user_meta($user_id,'_stripe_customer_id',true);
    $arr = array();

    if(!empty($stripe_customer_id)){
        $cardsData = get_stripe_cards_for_user($stripe_customer_id);
        // echo "<pre>";var_dump($cardsData);die;
        $arr = (!empty($cardsData))?$cardsData:array();
        return json_encode($arr);
    }
}



function user_update_address_delete($obj) {

	

	$postdata	= $obj["address_delete"];

	$user 		= get_user_by( 'id', $postdata['uid'] );

	$address_id	= (!empty($postdata['address_id']))?$postdata['address_id']:'';

	if(!empty($user) && !empty($address_id)){

		$storedAddress=get_user_meta($postdata['uid'],'checkout_saved_address',true);	
		if(!empty($storedAddress[$address_id])){

			unset($storedAddress[$address_id]);

			update_user_meta($postdata['uid'],'checkout_saved_address',$storedAddress);

		}

		

		return '{"success":{"message":"address deleted"}}';

	}else{

		return '{"error":{"message":"delete address failed"}}';

	}

}



function get_stripe_cards_for_user($stripe_customer_id) {	

	$stripe = new WC_Gateway_Stripe();

	$response = $stripe->stripe_request( array(
        'limit'       => 100
    ), 'customers/' . $stripe_customer_id . '/sources', 'GET' );
    
	if(!empty($response->data)){
		return $response->data;
	}
	return null;
}



function add_stripe_customer($obj){

	if(!empty($obj['user_id']) && !empty($obj['user_stripe_id'])){

		$user = get_user_by( 'id',$obj['user_id'] );

		if(!empty($user)){

			update_user_meta($obj['user_id'],'_stripe_customer_id',$obj['user_stripe_id']);

		}

	}

		return '{"success":{"message":"customerid added"}}';

}



function mail_devo_help_dask($obj){
	if(!empty($obj['help_mail'])){
		$email=$obj['help_mail']['from'];
		$pcode=$obj['help_mail']['postcode'];
		$emailsList=get_option('custom_postcodes_clients_emails_list');
		if(!empty($emailsList)){
			$emailsList[] = $email;
		}else{
			$emailsListArray = array($email);
			update_option( 'custom_postcodes_clients_emails_list', $emailsListArray);
		}
		$to = 'help@devo.co.uk';
		// $to = 'info@trssoftwaresolutions.com';
		$subject = 'NOTIFY ME | POST CODE NEEDED';
		$body = 'A new user has submitted request for Shop availability in his area. Postcode : '.$pcode.' User Email : '.$email;
		$headers = array('Content-Type: text/html; charset=UTF-8');
		wp_mail( $to, $subject, $body, $headers );
        return '{"success":{"message":"Notification have Sent"}}';
	}else{
		return '{"error":{"message":"Notification failed"}}';
	}
}