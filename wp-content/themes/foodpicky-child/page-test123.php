
<?php get_header(); ?>
<style>
	.s_spn {  
	   
		font-size: 12px;
		line-height: 24px;
		font-family: AvenirLTStd-Roman,"Helvetica Neue Light","Helvetica Neue",sans-serif !important;
		margin: 0px 0 !important;
		color: black;
	}
	.headingColor {
		color: black;
		border-top: 1px dotted;
		padding: 1px 6px 2px 3px;
	}
	.atext{
		
		color: #0e62e4;
		margin-left: 15px;
		font-weight: bold;
	}
	.usetotal{
		
		font-size:15px; 
		font-weight:bold;
	}
	.promorow{
		margin-top: -21px;
	}

	.checkoutcenter{
		text-align:center;
		
	}
	.basketd_text{
		background-color:white;
		font-size: 18px;
		text-align: center;
		font-weight: 600;
		font-family: AvenirLTStd-Roman,
	}
	.pborder{
		border:1px solid grey;
	}
	.maindiv{
		margin-top: 35px;
	}
	.bg{
		background-color:#ffffff !important;
	}
	.selectsave1{
		font-weight:bold;
	}
	.pclose{
		color:red !important;
	}

/* use for mobile */
@media only screen and (max-width: 500px) {
	   .resize{
		
		width: 53px !important;
		height: 85px !important;
		margin-left: -11px;
			margin-top: 12px;
	}
	.rescol{
		
		color: red;
		margin-top: -122px !important;
		margin-left: 237px !important;
		position: absolute !important;
		z-index: 1111 !important;
	}
	.rescol1{
		
			margin-top: -58px !important;
		margin-left: 143px !important;
		z-index: :1111 !important;
		position: absolute !important;
		z-index: 1111;
	}
	.rescol2{
		width:50% !important;
		float:left !important;
	}
	.atext{
		
		margin-left:80px !important;
	}

}

/* use for tablet*/
@media screen and (max-width: 801px) and (min-width: 500px) {
	   .rescol2{
		width:50% !important;
		float:left !important;
	}
	.rescol1{
		margin-left: 216px !important;
		margin-top: -54px !important;
		z-index: 1111;
		position: absolute;
		
	}

	.rescol{
		color: red;
		position: absolute;
		z-index: 1111;
		margin-top: -122px;
		margin-left: 375px;
	}

	.atext{
		
		margin-left:160px !important;
	}
}

/* use for small screen desktop */
@media screen and (max-width: 1001px) and (min-width: 995px) {
	                                                                                                                                              
	.atext{
	    margin-left: 63px !important;
		
	}
	
}
  
</style>
    <!-- strat main container-->
    <div class="container">
     	<div class="row bg">
		    <!-- start product div-->
			<div class="col-sm-8">
				<div class="panel panel-default pborder">
					<div class="panel-body">
					    <div class="row">
							<div class="col-sm-12">
								<span class="s_spn">Delivered from</span><br/>
								<span class="s_spn selectsave1">Select & Save</span><br/>
								<h6 class="headingColor">&nbsp;</h6>
							</div>
						</div>
						<?php 
						for($i=0; $i<10;$i++) { ?>
						<div class="row">
							<div class="col-sm-8">
						    	<a class="pull-left" href="#"> 
									<img src="https://www.devo.co.uk/wp-content/uploads/2017/05/3d5f0db0-0936-22f4-d835-8f4fe8ab5e0d-23-147x147.jpg" class="media-object resize"alt="img" style="width:110px;height:110px;">
								</a>
								<div class="media-body">
									<div  class="maindiv">
										<p class="s_spn">Fanta Fruit Twist 330ml</p>
										<span  class="s_spn">QTY</span>&nbsp;&nbsp;<span  class="s_spn">-</span>&nbsp;&nbsp;<span  class="s_spn">5</span>&nbsp;&nbsp;<span  class="s_spn">+</span>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="media-body">
									<div class="maindiv">
										<p class="s_spn rescol1">$23.2233 </p>
									</div>
								</div>
							</div>
						    <div class="col-sm-2 pull-right">
								<div class="media-body ">
									<div class="maindiv">
										<p style="color:red;" class="rescol"><i class="fa fa-times" aria-hidden="true"></i></p>
									</div>
								</div>
						    </div>
						</div><br/>
						<?php
						} ?>
					</div>
				</div>  
			</div>
			<!-- end product div-->
			<!-- start product card div-->
			<div class="col-sm-4">
			    <div class="panel panel-default pborder" >
					<div class="panel-heading basketd_text bg">Basket Details</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6 rescol2">
								<p class="s_spn">&nbsp;<span>4</span><span>&nbsp;&nbsp; Item</span></p>
							</div>
							<div class="col-md-5 col-md-offset-1 rescol2">
								<p class="s_spn">$23.2233 </p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-7 rescol2">  
								<p class="s_spn">&nbsp;Service Fee</p>
							</div>
							<div class="col-md-5  rescol2">
								<p class="s_spn">$23.2233 </p>
							</div>
						</div>								
						<div class="row">
							<div class="col-md-6 rescol2">
								<p class="s_spn">&nbsp;Delivery</p>
							</div>
							<div class="col-md-5 col-md-offset-1 rescol2">
								<p class="s_spn">$23.2233 </p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 rescol2">
								<p class="s_spn usetotal">Total</p>
							</div>
							<div class="col-md-5 col-md-offset-1 rescol2">
								<p class="s_spn usetotal">$23.2233 </p>
							</div>
						</div><br/>
						<h6 class="headingColor s_spn">&nbsp;</h6>
						<div class="row promorow">
							<div class="col-md-9 col-sm-offset-3">
								<a href="#" class="atext">Enter Promo Code</a>
						   </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-md-12">
						<p class="buttons">
							<a href="#" class="button checkout wc-forward custom_minicart_checkout_btn min-check_btn btn-block checkoutcenter">Checkout</a>
						</p>
					</div>
				</div>					
			</div>
			<!-- start product card div-->
		</div>
 	</div>
	<!-- strat main container-->
	<br/>
<?php get_footer(); ?>