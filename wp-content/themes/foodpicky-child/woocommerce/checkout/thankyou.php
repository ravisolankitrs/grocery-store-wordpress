<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<style>
.entry-header{
	display:none;
}
.entry-content{
	margin-top: 7%;
}
.custom_thankyou{
	color: red;
    font-size: 40px;
    font-weight: 900;
    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
    padding: 0 0 0 0;
    margin: 0 0;
}
.custom_sub_heading{
	color: red;
    font-size: 21px;
    font-weight: 600;
    font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
    padding: 0 0 0 0;
    margin: 22px 0;
}
.custom_hr{
	background-color: #FC0D34;
    height: 1.1px;
}
@media only screen and (max-width: 545px){
	.entry-content{
		margin-top: 15%;
	}	
}
</style>

<?php
if ( $order ) : ?>

	<?php if ( $order->has_status( 'failed' ) ) : ?>

		<p class="woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

		<p class="woocommerce-thankyou-order-failed-actions">
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
				<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>
	<?php 
		$orderdata = new WC_Order( $order->id );
		$items = $orderdata->get_items();
		// global $woocommerce;
		// $cat_aaraay = array();
		// $catterm='';
		// foreach ( $items as $item ) {
			// $productId   = $item['product_id'];
			// $term_list = wp_get_post_terms($productId,'yith_shop_vendor',array('fields'=>'ids'));
			// $cat_id = (int)$term_list[0];
			// $catterm = get_term( $cat_id, 'yith_shop_vendor' );
			// $cat_aaraay[$cat_id] = $term;
			
		//}
		$redirectionLink = get_site_url();
		/* if(count($cat_aaraay) == 1){
			$redirectionLink = '/profile/'.$catterm->slug;
		} */
	?>
	<style>
		.entry-content{
			margin:0 0 !important;
		}
		.rec_box_container{
			float: left;
			border: 1px solid black;
			text-align: center;
		}
		.rec_box_row{
			border-bottom: 1px solid black;
			padding: 10px 0 10px 0;
		}
		.rec_box_padding{
			padding: 6px 0 6px 0;
		}
		.rec_box_row_margin{
			margin: 0 0;
		}
		.rec_box_products_row{
			margin: 0 19px;
		}
		.rec_box_products_name{
			text-align: left;
		}
		.rec_box_products_price{
			text-align: right;
		}
		.rec_box_internal_sec{
			border-bottom: 1px solid black;
			margin: 0 20px;
		}
		.rec_box_title{
			font-weight: 600;
		}
		.rec_box_sub_title{
			color: black;
			font-weight: 400;
		}
		.rec_box_sub_value{
			color: black;
			font-weight: 600;
		}
	</style>
	<div class="col-md-3"></div>
	<div class="container container col-md-6">
		<div class="col-md-12 col-xl-12 col-sm-12 col-xs-12 rec_box_container">
		<div class="row rec_box_row rec_box_title">
			<h3>Order details</h3>
		</div>
		<div class="row rec_box_row">
			<div class="row rec_box_sub_title">
				<span>Your order from</span>
			</div>
			<div class="row rec_box_sub_value">
				<span>Select & Save</span>
			</div>
		</div>
		<div class="row rec_box_row">
			<div class="row rec_box_sub_title">
				<span>Delivering to:</span>
			</div>
			<div class="row rec_box_sub_value">
				<div class="row col-md-12 col-sm-12 col-xs-12 rec_box_row_margin">
					<span><?php echo $order->get_billing_address_1()." , ".$order->get_billing_address_2(); ?></span>
				</div>
				<div class="row col-md-12 col-sm-12 col-xs-12 rec_box_row_margin">
					<span><?php echo $order->get_billing_postcode(); ?></span>
				</div>
				<div class="row col-md-12 col-sm-12 col-xs-12 rec_box_row_margin">
					<span><?php echo $order->get_billing_phone(); ?></span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="row col-md-12 col-sm-12 col-xs-12">
				<div class="row rec_box_products_row rec_box_products_name rec_box_sub_value">
					<span>Order summary:</span>
				</div>
				<div class="row rec_box_products_row rec_box_products_name rec_box_sub_title">
					<span>Order no: #<?php echo $order->get_order_number(); ?></span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row rec_box_internal_sec"></div>
			<div class="row rec_box_sub_title rec_box_padding">
			
				<?php
				// Iterating through each WC_Order_Item objects
				foreach( $order-> get_items() as $item_key => $item_values ):
				?>
				<div class="row rec_box_products_row">
					<?php
					$item_id = $item_values->get_id();
					$item_name = $item_values->get_name(); // Name of the product
					$item_type = $item_values->get_type(); // Type of the order item ("line_item")
					$item_data = $item_values->get_data();
					$product_name = $item_data['name'];
					$product_id = $item_data['product_id'];
					$quantity = $item_data['quantity'];
					$line_subtotal = $item_data['subtotal'];
					$line_total = $item_data['total'];
					?>
					<div class="col-md-8 col-sm-6 col-xs-6 rec_box_products_name">
						<span><?php echo $product_name;?></span>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-6 rec_box_products_price">
						<?php echo get_woocommerce_currency_symbol().number_format($line_total,2);?>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
			<div class="clearfix"></div>
			<div class="row rec_box_internal_sec"></div>
			<div class="row rec_box_sub_title rec_box_padding">
				<div class="row rec_box_products_row">
					<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_name">
						<span>Subtotal</span>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_price">
						<?php echo $order->get_subtotal_to_display(); ?>
					</div>
				</div>
				<div class="row rec_box_products_row">
					<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_name">
						<span>Delivery Fee</span>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_price">
						<?php echo get_woocommerce_currency_symbol().number_format($order->get_shipping_total(),2); ?>
					</div>
				</div>
				<?php
				foreach($order->get_fees() as $item_key => $item_values ):
				?>
					<?php
						$item_data = $item_values->get_data();
						$item_name = $item_data['name']; // Name of the fee
						$item_total = $item_data['total']; // Total fee
					?>
					<div class="row rec_box_products_row">
						<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_name">
							<span><?php echo $item_name; ?></span>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_price">
							<?php echo get_woocommerce_currency_symbol().number_format($item_total,2); ?>
						</div>
					</div>
				<?php endforeach;?>
			</div>
			<div class="clearfix"></div>
			<div class="row rec_box_internal_sec"></div>
			<div class="row rec_box_padding">
				<div class="row col-md-12 col-sm-12 col-xs-12 rec_box_products_row rec_box_products_name rec_box_sub_value">
					<span>Paid By:</span>
				</div>
				<div class="row rec_box_products_row rec_box_sub_title">
					<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_name">
						<span><?php echo $order->payment_method_title; ?></span>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_price">
						<?php echo $order->get_formatted_order_total(); ?>
					</div>
				</div>
				<div class="row rec_box_products_row rec_box_sub_value">
					<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_name">
						<span>Total</span>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 rec_box_products_price">
						<?php echo $order->get_formatted_order_total(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="col-md-3"></div>
	<!--

<div class="container" style="text-align:center;">
		<div class="col-md-4"></div>
		<div class="col-md-5 col-sm-12 col-xs-12">
			<h1 class="custom_thankyou">THANK YOU!</h1>
			<hr class="custom_hr"/>
			<img src = "/wp-content/uploads/thankyoujoker.PNG"/>
			<h2 class="custom_sub_heading">Your order will be with you shortly</h2>
			<a href="<?php echo $redirectionLink;?>/"><img src = "/wp-content/uploads/button_continue.png"/></a>
		</div>
		<div class="col-md-3"></div>
	</div>
	
	
		<p class="woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></p>

		<ul class="woocommerce-thankyou-order-details order_details">
			<li class="order">
				<?php _e( 'Order Number:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_order_number(); ?></strong>
			</li>
			<li class="date">
				<?php _e( 'Date:', 'woocommerce' ); ?>
				<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
			</li>
			<li class="total">
				<?php _e( 'Total:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_formatted_order_total(); ?></strong>
			</li>
			<?php if ( $order->payment_method_title ) : ?>
			<li class="method">
				<?php _e( 'Payment Method:', 'woocommerce' ); ?>
				<strong><?php echo $order->payment_method_title; ?></strong>
			</li>
			<?php endif; ?>
		</ul>
		<div class="clear"></div>

	<?php endif; ?>

	<?php //do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	<?php //do_action( 'woocommerce_thankyou', $order->id ); ?>
-->
<?php else : ?>

	<p class="woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

<?php endif; ?>
