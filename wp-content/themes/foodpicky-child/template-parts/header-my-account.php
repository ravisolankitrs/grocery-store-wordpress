<?php
global $current_user;
wp_get_current_user();
?>

<div class="header-my-account <?php print (is_user_logged_in() ? 'logged-in' : '') ?>">
	<?php 
		$displayCarticon="display:none;";
	?>
	<?php if ( !WC()->cart->get_cart_contents_count() == 0 ){
			$displayCarticon="display:block;";		
		}
	?>
	<span class="cart_head_total hide_cartprice" style="float:left;padding:0 46px 0 0;color:red;font-size: 20px;<?php echo $displayCarticon; ?>">
	   
		<a onclick="return header_checkuserlogin();" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
	
			<span class="glyphicon glyphicon-shopping-cart">
				
				<div class="badge">
					<div class="message-count"><?php echo WC()->cart->get_cart_contents_count(); ?></div>
				</div>
			</span>
			<span class="s_spn" style="font-size: 14px;font-weight: 600;"><?php echo WC()->cart->get_cart_total(); ?></span>
		</a>
	</span>
	<span style="float:right;">  
		<div class="dropdown">
			<input id="login-register-toggle" type="checkbox" style="position: absolute; clip: rect(0, 0, 0, 0);">
			<div class="link">   
				<?php if (is_user_logged_in()): ?>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/menu1.png" alt="image" class="hide_image"/>  
				<?php else: ?>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/U2.png" alt="image" class="hide_image"/>
				<?php endif; ?>
				<a href="<?php print ( function_exists('wc_get_page_permalink') ? esc_url(wc_get_page_permalink('myaccount')) : ''); ?>">
					<span><?php print (is_user_logged_in() ? esc_html(get_user_meta( $current_user->ID, 'first_name', true )) : esc_html__('Login/Register', 'foodpicky')) ?></span>
					<?php print (is_user_logged_in() ? get_avatar($current_user->ID) : '') ?>            
				</a>            
				<label for="login-register-toggle"></label>
			</div>
			<?php if (is_user_logged_in()): ?>
				<?php
				$type = 'AZEXO_Dashboard_Links';
				global $wp_widget_factory;
				if (is_object($wp_widget_factory) && isset($wp_widget_factory->widgets, $wp_widget_factory->widgets[$type])) {
					the_widget($type, array('title' => esc_html__('My account', 'foodpicky')));
				}
				?>
			<?php else: ?>
				<div class="form" id="form_scroll">
					<label for="login-register-toggle"></label>
					<?php if (function_exists('wc_get_page_permalink') && !azexo_is_current_post(wc_get_page_id('myaccount'))): ?>
						<input id="register-toggle" type="checkbox" style="position: absolute; clip: rect(0, 0, 0, 0);">
						<div></div>
						<label for="register-toggle">
							<span class="login"><?php esc_html_e('Already have an account?', 'foodpicky'); ?></span>
							<span class="register"><?php esc_html_e("Don't have an account?", 'foodpicky'); ?></span>
						</label>
						<?php
						// wp_enqueue_script('wc-password-strength-meter');
						if (function_exists('wc_get_template')) {
							wc_get_template('myaccount/form-login.php');
						}
					endif;
					?>
					<div class="social-login">
						<label><?php esc_html_e('Connect with Social Networks:', 'foodpicky'); ?></label> <?php print (function_exists('azsl_social_login_links') ? azsl_social_login_links() : ''); ?>
					</div>
				</div>
			<?php endif; ?>
		</div> 
	</span>	
</div>

<script>
					
		function header_checkuserlogin(){
			
			
			var user_in = "<?php echo is_user_logged_in();	?>";
			
			if(user_in == 1){
				return true;
			}
			
 			//jQuery(".login").attr("action","<?php echo WC()->cart->get_cart_url(); ?>");
			//jQuery(".register").attr("action","<?php echo WC()->cart->get_cart_url(); ?>");
			jQuery("input[name='_wp_http_referer']").attr("value","<?php echo WC()->cart->get_cart_url(); ?>"); 
			
			document.cookie = 'redirect_to=<?php echo WC()->cart->get_cart_url(); ?>; expires=Fri, 3 Aug 2020 20:47:11 UTC; path=/'
			
			jQuery("#login-register-toggle").trigger("click");
			
			return false;
		}

</script>
