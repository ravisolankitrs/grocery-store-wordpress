
jQuery(document).on("submit","#frmnotificationsend",function(e){
    e.preventDefault();
		
    var formData = new FormData(this);
    var cond= true;
    var valid=true;
    var  valid = validatingnotifincation("msgtitlenotification" ,  "Message title is empty");
    var  valid = validatingnotifincationtextarea("msgshortdescription" ,  "Short description  is empty");
    var  valid = validatingnotifincation("msgbtntext" ,  "Button text must be filled");	
    var  valid = validatingnotifincation("uploadnotifyimage" ,  "Please upload image");	
     
    var valid = validatingnotifincationtextareamsg("tanotifytitlemsg" ,  "Message  is empty");
    
    if(valid == true){
        return;
    }
    var frmdata = jQuery("#frmnotificationsend").serialize();
      
    frmdata ="action=ntapi_ajx_notification_message&"+frmdata;
    var id = jQuery(jQuery("#selbxuserlst")).children(":selected").attr("id");
    
    if(jQuery('#notification_live_mode').is(":checked")){
        var livemode_sure = confirm("Are you sure, Send notification to Live Users?");
        if (livemode_sure != true) {
           return false;
        }
    }
    
    
    jQuery.ajax({
        type :"POST",
        url  :adminAjax.ajaxurl,
        data :frmdata,
        success: function(response){           
            jQuery("#error_msg_notifation").text("Successfuly send message");
            // clearTextfields()
            console.log(response);
        }	
    });
    return false;
   
		
});

function clearTextfields(){
    jQuery("#msgtitlenotification").val("");
    jQuery("#tanotifytitlemsg").val("");
    jQuery('#msgbtntext').val("");
    jQuery("#choosenotifyimage").removeAttr("src height width");
    CKEDITOR.instances.tanotifytitlemsg.setData('');
    CKEDITOR.instances.msgshortdescription.setData('');    
    jQuery("#uploadnotifyimage").val();
}

function validatingnotifincation(textboxid, msg){
    
    if(jQuery("#"+textboxid+"").val().length < 1 ){
        jQuery ("#error_msg_notifation").text(msg);
        return false;
    }		
}
function validatingnotifincationtextarea(textboxid, msg){
    
    var  texdata = CKEDITOR.instances.msgshortdescription.getData();
     if(texdata.length <1){
        jQuery ("#error_msg_notifation").text(msg);
     return false; }
     
}
function validatingnotifincationtextareamsg(textboxid, msg){
    
    var  texdata = CKEDITOR.instances.tanotifytitlemsg.getData();
    if (texdata.length < 1 ){
        jQuery ("#error_msg_notifation").text(msg);
      return false;
    }
}




    
jQuery(document).ready(function($){
    
    CKEDITOR.replace( 'tanotifytitlemsg', {
				basicEntities : false,
				entities : false,
				forceSimpleAmpersand : true,
            } );
    CKEDITOR.replace( 'msgshortdescription', {
				height:['120px'],
				basicEntities : false,
				entities : false,
				forceSimpleAmpersand : true,
            } );
    
    $('.set_custom_images').click(function(e) {
        e.preventDefault();
        var image = wp.media({ 
            title: 'Upload Image',
            multiple: false
        }).open()
        .on('select', function(e){
        
        var uploaded_image = image.state().get('selection').first();
            console.log(uploaded_image);
            var image_url = uploaded_image.toJSON().url;
            jQuery('#uploadnotifyimage').val(image_url);
            jQuery('#choosenotifyimage').attr({"src":image_url,"width":"150","height":"150"});
    
        });
    });
});



jQuery(document).on("change","#uploadnotifyimage", function(event){
    
     var output = document.getElementById('choosenotifyimage');
     
     output.src = URL.createObjectURL(event.target.files[0]);
     output.height="150";
     output.width ="150";
     jQuery("#choosenotifyimage").fadeIn();  // height="100" width ="100"
    
    
});




jQuery(document).on("click",".selectdeviceforuser",function(){
    
    var sendqry =  "action=get_user_list_search";
    var msg="";
    var usrlst = "";
    if(jQuery(this).prop('checked')== true){
        
        jQuery(this).closest('label').css({'border':'1px solid #882a2a', 'background':'#fd0e35','color':'#f9b2b2'});	
        }
    if(jQuery(this).prop('checked') != true){
        jQuery(this).closest('label').css({'border':'1px solid #ccc','background':'#eee','color':'#787878'});
        
    }
     if(jQuery("#alluserforspecifichk").prop('checked') == true){
        msg +="All User ";
        sendqry += "&alluser";
    }
    if(jQuery("#allandroidapps").prop('checked') == true){
        msg +="Android User";
        sendqry += "&androidapp";
    }
    if(jQuery("#alliosapps").prop('checked') == true){
        msg +="Ios User ";
        sendqry += "&iosapp";
    }
    if(jQuery("#allwepapps").prop('checked') == true){
         msg +="Web User ";
        sendqry += "&webapp";
    }
    
    if(sendqry != "action=get_user_list_search"){
        
        // getalluserforapiapp(sendqry, msg);
    }
    
});

jQuery(document).on("click","#alluserforspecifichk",function(){
    
    if(jQuery(this).prop('checked') == true){
        
        jQuery('.selectdeviceforuser').prop('checked',true);
        jQuery('.selectdeviceforuser').closest('label').css({'border':'1px solid #882a2a', 'background':'#fd0e35','color':'#f9b2b2'});	
        jQuery(this).closest('label').css({'border':'1px solid #882a2a', 'background':'#fd0e35','color':'#f9b2b2'});	

    }
    if(jQuery(this).prop('checked') != true){
        
        jQuery('.selectdeviceforuser').prop('checked' ,false);
        jQuery(this).closest('label').css({'border':'1px solid #ccc','background':'#eee','color':'#787878'});
        jQuery('.selectdeviceforuser').closest('label').css({'border':'1px solid #ccc','background':'#eee','color':'#787878'});

    }
    
});
	
function getalluserforapiapp(sendqry, msg){
    

    jQuery.ajax({
         type:"POST",
         url :  adminAjax.ajaxurl,
         data: sendqry,
         success:function(response){
            jQuery('#selbxuserlst').val(null).trigger('change');
             jQuery("#get-all-user-list").html(response); 

                        //	 alert(arr.length);
             
         }
    });
    
}
jQuery(document).ready(function() { 
    jQuery("#selbxuserlst").select2({
        placeholder: 'Select an user',
        multiple: true,
        allowClear: true,
    }).focus(); 
    
    jQuery(document).on('click','#specific-user', function(e){
        if(jQuery(this).is(":checked")){
            jQuery('.user_list_row').show();
            jQuery('.user_list_row').removeAttr('disabled');
        }else{
            jQuery('.user_list_row').hide();
            jQuery('.user_list_row').attr('disabled','true');
        }
        
    });
});