<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
?>
<div id="payment" class="woocommerce-checkout-payment">
	<?php if ( WC()->cart->needs_payment() ) : ?>
		<ul class="wc_payment_methods payment_methods methods">
			<?php
				if ( ! empty( $available_gateways ) ) {
					foreach ( $available_gateways as $gateway ) {
						wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
					}
				} else {
					echo '<li>' . apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_country() ? __( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : __( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>';
				}
			?>
		</ul>
		<?php if( !WC()->cart ){ ?>
			<script>jQuery('#place-order-div').css('display','block');</script>
		<?php } else{ ?>
			<script>jQuery('#place-order-div').css('display','none');</script>
	<?php	}?>
	<?php endif; ?>
	<!--
	This code has been commented to remove promcode link
	<label for="new" class="payment_actions_labels">
		<a class="promocode_anchor newpromocode_anchor" id="add-promo" href="javascript:void(0);" data-toggle="modal" data-target="#myPromoCode">Add promo code</a>
	</label>-->
	<script>
	/* jQuery(document).ready(function(){		
		if( jQuery('.promocode_anchor').length > 1 )         // use this if you are using id to check
		{
			 jQuery('.newpromocode_anchor').hide();
		}
	}); */
	
	</script>
	<!--<div id="place-order-div" class="form-row place-order" style="display: none;">
		<div class="col-md-12 col-sm-12 col-xs-12" style="width:100%;padding:10px;margin-top:-42px;">
		  <?php $p_code = get_user_meta(get_current_user_id(), "billing_postcode"); 
			if(empty($p_code)){$p_code=(!empty($_COOKIE['post_code']))?$_COOKIE['post_code']:'';}?>
		  <div class="col-md-12 col-sm-12 col-xs-12">
			<input class="input-text " name="billing_postcode" id="billing_postcode" placeholder=" Enter Billing Postcode e.g. SW1Y 4LG" value="<?php echo $p_code[0]; ?>" type="text" style="height:40px;border:1px solid #969999;border-radius:0;font-size:initial;">
		  </div>
			
		</div>
		
	</div>-->
	<noscript>
			<?php _e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
			<br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>" />
		</noscript>

		<?php wc_get_template( 'checkout/terms.php' ); ?>

		<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

		<?php echo apply_filters( 'woocommerce_order_button_html', '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>

		<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

		<?php wp_nonce_field( 'woocommerce-process_checkout' ); ?>
</div>
<style>
.woocommerce-checkout #payment div.payment_box.payment_method_stripe{
	margin: 0 0;
}
	
</style>
<?php
if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_after_payment' );
}
