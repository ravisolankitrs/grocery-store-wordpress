<?php /* Template Name: Q&A - DEVO */ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
.affix { top: 0px; }

.quest1{
	text-align:left;
	padding-left:20px;
}
</style>
<?php get_header(); ?>
</head>

<body>

<header class="entry-header">
	<div class="dev-page-header-inner dev-outer">
		<div class="dev-inner dev-text-centered">
			<h2 class="text-head">Frequently Answered Questions </h2> 
		</div>
	</div>
</header>

 
	<div class="col-md-2"></div>
    <div class="col-md-8">
	
      <div id="aboutDEVO"><p class="quest-title">About Devo</p> Devo is a combination between an on-demand delivery service and a digital marketplace that connects you with your local stores. Through the Devo marketplace, you can purchase items from your local stores and have them delivered in as little as 30 minutes thanks to our revolutionary technology and hand-picked Devo drivers.<br><br>
	  
		Our vision at Devo is to grow to become the world's leading commerce platform for local stores. We aim to help local stores worldwide connect with millions of customers online and on mobile channels.<br><br></div>
	  <div class="clearfix"></div>
<div id="usingDEVO"><p class="quest-title">Using Devo</p><p class="quest" style="font-weight: bold">How does Devo work?</p> With Devo, life couldn't be simpler - just enter your postcode on the homepage and you'll see a selection of local stores that we deliver straight to your door. Choose your preferred store, select your products, enter your address and payment details - we'll get your order to you in as little as 30 minutes!
 <br><br></div>
	  <div class="clearfix"></div>
      <div id="q1"><p class="quest" style="font-weight: bold">What kind of retailers do DEVO work with?</p>We hand pick only the top local stores available in your area. This can range from your local supermarket to your nearest corner store. The one thing you will not find on Devo are unlicensed, poor quality retailers.<br><br></div>
      <div id="q2"><p class="quest" style="font-weight: bold">What times can I order for?</p> We deliver to you 24 hours a day, 7 days a week. Ultimately, this also depends on the stores opening hours.<br><br></div>
      <div id="q3"><p class="quest" style="font-weight: bold">Where is Devo available?</p>Devo is currently available in specific areas of Zone 1-2 London. We are working on bringing Devo to more suburbs soon. <br><br> </div>
      <div id="q4"><p class="quest" style="font-weight: bold">Does DEVO accept cash?</p> We still use cash? Cash is so 2001. Joking aside - at Devo, we only take card payments as it allows us to create a safer environment for our drivers. It also helps us provide you with the best possible user experience.<br><br></div>
      <div id="q5"><p class="quest" style="font-weight: bold">What is the minimum order value?</p> No minimum order value, so you can order as little as you wish. However, there is a surcharge of <?php echo (function_exists('WC'))?get_woocommerce_currency_symbol():'�';?>2 on each order below <?php echo (function_exists('WC'))?get_woocommerce_currency_symbol():'�';?>10.<br><br></div>
      <div id="q6"><p class="quest" style="font-weight: bold">Do you charge the same prices as the retailer does in store?</p> In some cases yes, but depending on the retail partner, we may place a slight increase on some of their products on Devo.<br><br></div>
      <p class="quest-title quest1" style="font-weight: bold"><strong>Question about my order</p></strong>
      <div id="q8"><p class="quest" style="font-weight: bold">What if something is wrong with my order?</p>
	  Our team work around the clock (literally 24 hours a day) to make the Devo experience as blissful as possible for you. The team looks after your experience from the second you place your order, till it's placed in your hands. We wish we could promise nothing could go wrong - however in the rare case that a problem arises, do not hesitate to contact us at support@devo.co.uk and we will look into this for you.<br><br></div>
	  
	  <div id="q9"><p class="quest" style="font-weight: bold">What if something I ordered is not in stock?</p> If you ever order a product that we're unable to deliver, we'll offer you a substitute - a suitable is an alternative product in its place. In this case, your driver will always call once they arrive at the shop to discuss possible substitutes. If you're not happy with the substitute, simply decline the substitute your bill will be recalculated instantly. If you're happy with the substitute you'll be charged the price of the new product, not the one you originally ordered.<br><br></div>
      
      <div id="q10"><p class="quest" style="font-weight: bold">What if I am not around when my order arrives?</p> Bummer! However if you think you won't be at the delivery address to receive your order, please let us know by emailing support@devo.co.uk. Our drivers will try to call you once they reach your delivery address, if they are unable to get through, our customer service team will call and email. Once you place an order please keep an eye on your emails for any updates. If we are unable to deliver the order and cannot contact you, our delivery drivers will wait for up to 5 minutes before leaving to complete the next order. In this situation we are afraid that you will still be charged a delivery fee and a service fee for the order. To prevent this from happening, it's always a good idea to double check your contact and address details.<br><br></div>
      
      <div id="q12"><p class="quest" style="font-weight: bold">How long will my order take?</p> Deliveries typically arrive in about 30-45 minutes. During peak demand periods, it will be likely that delivery times will take longer.<br><br></div>
      <div id="q13"><p class="quest" style="font-weight: bold">Is service ever refused?</p> Persons placing an order for alcohol or tobacco products from our retail partners must be aged 18 or over. Alcoholic beverages and tobacco products can only be sold and delivered to persons aged 18 or over. Devo operates the Challenge 25 age verification policy whereby customers who are lucky enough not to look 25 or over, will be asked to provide proof of age to show that they are aged 18 or over. By placing an order that includes alcohol or tobacco products you confirm that you are at least 18 years old. Devo reserves the right to refuse to deliver any alcohol or tobacco products to any person who does not appear, or cannot prove they are, aged 18 or over. Devo also reserves the right to refuse to deliver any alcohol to any person who is, or appears to be, intoxicated.<br><br></div>
      <div id="q14"><p class="quest" style="font-weight: bold">What if I have a problem with my order?</p> We're here to help. Once you've placed an order, just contact our support team at support@devo.co.uk and we'll take care of the issue.<br><br></div>


	  <p class="quest-title quest1" style="font-weight: bold">All the other stuff</p> 	 	 <div id="q15"><p class="quest" style="font-weight: bold"><b>What if I have allergies or an allergic reaction?</b></p> We do not provide product information, other than the product name, on any of the Devo platforms. You should ensure to check the packaging and contact the manufacturer before you consume if you're at all ensure.<br><br> </div>
      <div id="q15"><p class="quest" style="font-weight: bold">Why aren't you in my area yet?</p> Bear with us! We value excellence at Devo and so providing an excellent service is our number one priority, and we plan to grow quickly through the country and hope to be working with local stores near you very soon.<br><br> </div>
    </div>

  </div>

</body>

<?php get_footer(); ?>
